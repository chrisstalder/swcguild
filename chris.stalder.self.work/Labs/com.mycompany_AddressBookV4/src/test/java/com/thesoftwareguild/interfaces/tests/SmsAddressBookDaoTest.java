package com.thesoftwareguild.interfaces.tests;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//import com.mycompanysts.dao.AddressInterface;
//import com.mycompanysts.dto.Address;
import com.thesoftwareguild.interfaces.dto.Address;
import com.thesoftwareguild.interfaces.dao.AddressBookDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class SmsAddressBookDaoTest {

    //private AddressInterface dao;
    private AddressBookDao addressDao;
    private Address address1;
    private Address address2;

    //File f=new File();
    public SmsAddressBookDaoTest() {

    }

//    public void resetAddressTextFile() {
//        f.delete();
//        try {
//            f.createNewFile();
//        } catch (IOException ex) {
//            Logger.getLogger(AddressBookCreateMethodTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    @Before
    public void setUp() {
        //resetAddressTextFile();
        //dao=new AddressDaoImpl(fileName);  

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        addressDao = ctx.getBean("addressDao", AddressBookDao.class);

        //create new address1 object        
        address1 = new Address();
        address1.setFirstName("test1");
        address1.setLastName("test11");
        address1.setStreetNumber("111");
        address1.setStreetName("1TEST Street");
        address1.setCity("TESTER");
        address1.setState("TS");
        address1.setZip("1234");

        address2 = new Address();
        address2.setFirstName("test2");
        address2.setLastName("test22");
        address2.setStreetNumber("222");
        address2.setStreetName("1TEST Street");
        address2.setCity("TESTER");
        address2.setState("TS");
        address2.setZip("12345");
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void TEST_create_One() {
        // pass address1 object to DAO crate method

        Address addressReturned = addressDao.create(address1);
        assertTrue(addressReturned.getId() > 0);
    }

//    @Test
//    public void TEST_create_TwoDifferent()
//    {   
//        // pass address1 object to DAO crate method
//  
//        Address addressReturned = dao.create(address1);
//        addressReturned = dao.create(address2);
//        assertTrue( addressReturned.getId() > 0 );
//    }
//    @Test
//    public void TEST_getAddressList_WhenEmpty() {
//        // Pre-Requisites
// 
//        // What to test?
//        List<Address> list = dao.getAddressList();
//        
//        // Expected Results
//        assertEquals( 0, list.size() );      
//    }
//    @Test
//    public void TEST_getAddressList_AfterOneAdded() {
//        // Pre-Requisites
//
//        Address addressReturned = dao.create(address1);
//        
//        // What to test?
//        List<Address> list = dao.getAddressList();
//        
//        assertEquals( 1, list.size() );
//        
//        Address foundAddress = list.get(0);
//        assertEquals( addressReturned.getCity(), foundAddress.getCity() );
//        // comparte the rest of the fields, including ID
//    }
//    @Test
//    public void TEST_getAddressList_AfterTwoAdded() {
//        // Pre-Requisites
//
//        Address addressReturned = dao.create(address1);
//        addressReturned = dao.create(address2);
//        
//        // What to test?
//        List<Address> list = dao.getAddressList();
//        
//        assertEquals( 2, list.size() );
//        
//        Address foundAddress1 = list.get(0);
//        assertEquals( addressReturned.getCity(), foundAddress1.getCity() );
//        
//        Address foundAddress2 = list.get(1);
//        assertEquals( addressReturned.getCity(), foundAddress2.getCity() );
//        // comparte the rest of the fields, including ID
//    }
    
}