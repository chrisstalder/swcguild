/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.interfaces.dao;

import com.thesoftwareguild.interfaces.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoImpl implements AddressBookDao{
    private List<Address> addresses = new ArrayList();
    private int nextId = 1;
   
    
    public AddressBookDaoImpl(){
        addresses = decode();
    }

    @Override
    public Address create(Address address) {
        address.setId(nextId);
        nextId++;
        addresses.add(address);
        
        encode();
        
        return address;
    }

    @Override
    public void update(Address address) {
        Address found = null;
        for (Address myAddress : addresses) {
            if(Objects.equals(myAddress.getId(), address.getId())){
                found = myAddress;
            }
        }
        addresses.remove(found);
        addresses.add(address);
        encode();
    }

    @Override
    public Address get(Integer id) {
        Address found = null;
        for (Address address : addresses) {
            if(Objects.equals(address.getId(), id))
                found = address;
        }
        return found;
    }

    @Override
    public void delete(Integer id) {
        Address found = null;
        
        for (Address myAddress : addresses) {
            if(Objects.equals(myAddress.getId(), id)){
                found = myAddress;
            }
        }
        addresses.remove(found);
        encode();
    }

    @Override
    public List<Address> list() {
        List<Address> newList = addresses;
        return newList;
    }

    @Override
    public List<Address> searchByLastName(String lastName) {
        List<Address> foundByLastNameList = new ArrayList();
        for (Address address : addresses) {
            if(lastName.equals(address.getLastName()))
                foundByLastNameList.add(address);
        }
        return foundByLastNameList;
    }

    @Override
    public List<Address> searchByCity(String city) {
        List<Address> foundByCityList = new ArrayList();
        for (Address address : addresses) {
            if(city.equals(address.getCity()))
                foundByCityList.add(address);
        }
        return foundByCityList;
    }

    @Override
    public List<Address> searchByState(String state) {
//        Map<String, List<Person>> addressByCityMap = new HashMap();
//        List<Address> foundByStateList = new ArrayList();
//        List<String> cityList = new ArrayList();
//  
//        for (Address address : addresses) {
//            if(state.equals(address.getState())){   //iterating through original address list to find all with a certain state
//                foundByStateList.add(address);      
//                cityList.add(address.getCity());    //adding each city to a separate list
//            }
//        }
//        
//        for (String city : cityList) {
//            List<String> peopleInCity = new ArrayList(); //declaring new list for each city
//            for (Address address : foundByStateList) {
//                if(address.getCity().equals(city)){        //doing some for loop wizadry
//                    peopleInCity.add(address.getFirstName());
//                }
//            }
//            addressByCityMap.put(city, peopleInCity); //adding to map city, and people
//            
//        }
//        
//        return addressByCityMap;
        List<Address> addressesByState = new ArrayList();
        for (Address address : addresses) {
            if(state.equals(address.getState()))
                addressesByState.add(address);
        }
        
        return addressesByState;
    }

    @Override
    public List<Address> searchByZip(String zip) {
        List<Address> foundByZipList = new ArrayList();
        for (Address address : addresses) {
            if(zip.equals(address.getZip()))
                foundByZipList.add(address);
        }
        return foundByZipList;
    }

    private void encode(){
        final String TOKEN = "::";
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("addresses.txt"));
            
            for (Address address : addresses) {
                out.print(address.getId());
                out.print(TOKEN);
                out.print(address.getFirstName());
                out.print(TOKEN);
                out.print(address.getLastName());
                out.print(TOKEN);
                out.print(address.getStreetNumber());
                out.print(TOKEN);
                out.print(address.getStreetName());
                out.print(TOKEN);
                out.print(address.getCity());
                out.print(TOKEN);
                out.print(address.getState());
                out.print(TOKEN);
                out.print(address.getZip());
                out.println();
            }
            
            out.flush();
            out.close();
            
            
        } catch (IOException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    private List<Address> decode(){
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("addresses.txt")));
            
            while (sc.hasNextLine()){
                
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");
                
                int id = Integer.parseInt(stringParts[0]);  
                
                Address address = new Address();
                
                address.setId(id);
                address.setFirstName(stringParts[1]);
                address.setLastName(stringParts[2]);
                address.setStreetNumber(stringParts[3]);
                address.setStreetName(stringParts[4]);
                address.setCity(stringParts[5]);
                address.setState(stringParts[6]);
                address.setZip(stringParts[7]);

                addresses.add(address);
                nextId = 1 + addresses.size();
                
            }
            
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return addresses;
    }
   
}
