/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.interfaces.controllers;

import com.mycompany.consoleio.ConsoleIO;
import com.thesoftwareguild.interfaces.dao.AddressBookDao;
import com.thesoftwareguild.interfaces.dto.Address;
import com.thesoftwareguild.interfaces.dto.Person;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class AddressBookController {

    ConsoleIO console = new ConsoleIO();
    AddressBookDao addressDao;

    public AddressBookController(AddressBookDao dao) {
        this.addressDao = dao;
    }

    public void run(SearchController search) {

        boolean repeat = true;

        while (repeat) {
            int selection;
            console.print("==============================");
            console.print("Welcome to Your Address Book!");
            console.print("==============================");
            console.print("What would you like to do?");
            console.print("1) add");
            console.print("2) remove");
            console.print("3) find");
            console.print("4) list");
            console.print("5) total");
            console.print("6) search");
            console.print("7) edit");
            console.print("8) exit");
            console.print("==============================");

            selection = console.getIntValue();

            switch (selection) {
                case 1:
                    addAddress();
                    break;
                case 2:
                    removeAddress();
                    break;
                case 3:
                    findAddress();
                    break;
                case 4:
                    listAddresses();
                    break;
                case 5:
                    totalAddresses();
                    break;
                case 6:
                    repeat = search.run();
                    break;
                case 7:
                    editAddress(search);
                    break;
                case 8:
                    repeat = false;
                    break;
                default:
                    console.print("That wasn't an option!");
            }
        }
    }

    public void addAddress() {
        String firstName =enterFirstName();
        String lastName = enterLastName();
        String houseNum = enterStreetNum();
        String street = enterStreetName();
        String city = enterCityName();
        String state = enterState();
        String zip = enterZip();
        Address address = new Address();
        address.setFirstName(firstName);
        address.setLastName(lastName);
        address.setStreetNumber(houseNum);
        address.setStreetName(street);
        address.setCity(city);
        address.setState(state);
        address.setZip(zip);
        addressDao.create(address);
    }

    public void removeAddress() {
        List<Address> list = addressDao.list();
        boolean isValid = false;
        int input = 0;

        console.print("Please enter the ID of the contact you wish to remove.");
        while (!isValid) {
            input = console.getIntValue();
            for (Address address : list) {
                if (address.getId() == input) {
                    isValid = true;
                }
            }
        }

        addressDao.delete(input);
    }

    public void findAddress() {
        List<Address> list = addressDao.list();

        console.print("Please enter the last name of the contact.");
        String lastName = console.getString();

        for (Address address : list) {

            if (address.getLastName().equals(lastName)) {
                displayAddress(address);
            }
        }

    }

    public void listAddresses() {
        List<Address> list = addressDao.list();

        for (Address address : list) {
            displayAddress(address);
        }
    }

    public void totalAddresses() {
        List<Address> list = addressDao.list();
        console.print("=======================================================================");
        console.print("You have a total of " + list.size() + " addresses in your address book.");
        console.print("=======================================================================");

    }

    public void editAddress(SearchController search) {
        List<Address> addresses = addressDao.list();
        Address address = new Address();
        if (addresses.size() > 0) {
            for (Address a : addresses) {
                displayAddress(a);
            }
            console.print("Input ID of Address you wish to edit: ");
            int id = console.getIntValue();
            for (Address a : addresses) {
                if (a.getId() == id) {
                    address = a;
                }
            }

            console.print("Person's first name (" + address.getFirstName() + "): ");
            String firstName = console.getString();
            if (!"".equals(firstName)) {
                address.setFirstName(firstName);
            } else {
                address.setFirstName(address.getFirstName());
            }

            console.print("Person's last name (" + address.getLastName() + "): ");
            String lastName = console.getString();
            if (!"".equals(lastName)) {
                address.setLastName(lastName);
            } else {
                address.setLastName(address.getLastName());
            }

            address.setFirstName(firstName);
            address.setLastName(lastName);

            console.print("Street number (" + address.getStreetNumber() + "): ");
            String house = console.getString();
            if ("".equals(house)) {
                house = address.getStreetNumber();
            }
            console.print("Street name (" + address.getStreetName() + "): ");
            String street = console.getString();
            if ("".equals(street)) {
                street = address.getStreetName();
            }

            console.print("City (" + address.getCity() + "): ");
            String city = console.getString();
            if ("".equals(city)) {
                city = address.getCity();
            }

            console.print("State (" + address.getState() + "): ");
            String state = console.getString();
            if ("".equals(state)) {
                state = address.getState();
            }

            console.print("Zipcode (" + address.getZip() + "): ");
            String zip = console.getString();
            if ("".equals(zip)) {
                zip = address.getZip();
            }
            address.setStreetNumber(house);
            address.setStreetName(street);
            address.setCity(city);
            address.setState(state);
            address.setZip(zip);

            addressDao.update(address);
        }
    }

    public void displayAddress(Address address) {
        console.print("=============================================================================");
        console.print(address.getId() + " | " + address.getFirstName() + " " + address.getLastName());
        console.print("  |" + address.getStreetNumber() + " " + address.getStreetName());
        console.print("  |" + address.getCity() + ", " + address.getState() + ", " + address.getZip());
        console.print("=============================================================================");
    }
    
    public String enterFirstName(){
        boolean isValid = false;
        String firstName = null;
        console.print("Please enter your new contact's first name:");
        while (!isValid) {
            firstName = console.getString();
            if (firstName instanceof String) {
                isValid = true;
            } else {
                console.print("That is not a valid name");
            }
        }
        return firstName;
    }
    
    public String enterLastName(){
        boolean isValid = false;
        String lastName = null;
        console.print("Please enter your new contact's last name:");
        while (!isValid) {
            lastName = console.getString();
            if (lastName instanceof String) {
                isValid = true;
            } else {
                console.print("That is not a valid name");
            }
        }
        return lastName;
    }
    
    public String enterStreetNum(){
        boolean isValid = false;
        String streetNum = null;
        console.print("Please enter your new contact's street number:");
        while (!isValid) {
            streetNum = console.getString();
            if (streetNum instanceof String) {
                isValid = true;
            } else {
                console.print("Invalid entry.");
            }
        }
        return streetNum;
    }
    
    public String enterStreetName(){
        boolean isValid = false;
        String streetName = null;
        console.print("Please enter your new contact's street number:");
        while (!isValid) {
            streetName = console.getString();
            if (streetName instanceof String) {
                isValid = true;
            } else {
                console.print("Invalid entry.");
            }
        }
        return streetName;
    }
    
    public String enterCityName(){
        boolean isValid = false;
        String city = null;
        console.print("Please enter your new contact's city:");
        while (!isValid) {
            city = console.getString();
            if (city instanceof String) {
                isValid = true;
            } else {
                console.print("That is not a valid city name");
            }
        }
        return city;
    }
    
    public String enterState(){
        boolean isValid = false;
        String state = null;
        console.print("Please enter your new contact's state:");
        while (!isValid) {
            state = console.getState();
            if (state instanceof String) {
                isValid = true;
            } else {
                console.print("That is not a valid state name");
            }
        }
        return state;
    }
    
    public String enterZip(){
        boolean isValid = false;
        String zip = null;
        console.print("Please enter your new contact's zip:");
        while (!isValid) {
            zip = console.getState();
            if (zip instanceof String) {
                isValid = true;
            } else {
                console.print("Invalid entry");
            }
        }
        return zip;
    }

}
