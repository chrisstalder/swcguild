/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.interfaces.dataencoding;

import com.thesoftwareguild.interfaces.controllers.AddressBookController;
import com.thesoftwareguild.interfaces.controllers.SearchController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        AddressBookController controller = ctx.getBean("addressBookController", AddressBookController.class);
        SearchController search = ctx.getBean("searchController", SearchController.class);
        controller.run(search);
    }
}
