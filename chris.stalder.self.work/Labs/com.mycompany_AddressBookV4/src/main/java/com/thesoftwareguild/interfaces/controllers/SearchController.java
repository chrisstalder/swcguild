/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.interfaces.controllers;

import com.mycompany.consoleio.ConsoleIO;
import com.thesoftwareguild.interfaces.dao.AddressBookDao;
import com.thesoftwareguild.interfaces.dto.Address;
import com.thesoftwareguild.interfaces.dto.Person;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class SearchController {
    AddressBookDao addressDao;   
    ConsoleIO console = new ConsoleIO();
    
    public SearchController(AddressBookDao dao){
        this.addressDao = dao;
    }
    
    public boolean run(){
        
        boolean repeat = true;
        while(repeat){
            console.print("1. Search By Last Name");
            console.print("2. Search By State");
            console.print("3. Search By City");
            console.print("4. Search By Zipcode");
            console.print("5. Exit");


            int input = console.getIntValue();

            switch(input){
                case 1:
                    searchByLastName();
                    break;
                case 2:
                    searchByState();
                    break;
                case 3:
                    searchByCity();
                    break;
                case 4:
                    searchByZip();
                    break;
                case 5:
                    repeat = false;
                    break;
                default:
                    console.print("Invalid entry.");
            }
        }
        
        return true;
    }
    
    public void searchByState(){
        List<Address> addresses = addressDao.list();
        List<String> stateList = new ArrayList();
        
        for (Address address : addresses) {
            
            if(!stateList.contains(address.getState())){        //prevents repeats from being listed
                stateList.add(address.getState());
            }
            
        }
        
        for (String string : stateList) {
            console.print(string);
        }
        console.print("Enter the state: ");
        String state = console.getState();
        List<Address> foundByState = addressDao.searchByState(state);
        listAddresses(foundByState);
//        List<String> stateList = new ArrayList();
//        
//        for (Address address : addresses) {
//            
//            if(!stateList.contains(address.getState())){        //prevents repeats from being listed
//                stateList.add(address.getState());
//            }
//            
//        }
//        
//        for (String string : stateList) {
//            console.print(string);
//        }
//        console.print("Enter the state: ");
//        String state = console.getState();
//        Map<String, List<Person>> foundByStateAndCityMap = addressDao.searchByState(state);
//        Set<String> cityKeySet = foundByStateAndCityMap.keySet();
//        Iterator i = cityKeySet.iterator();
//        
//        
//        while(i.hasNext()){
//            String currentCity = i.next().toString();
//            console.print(currentCity);
//            List<Person> peopleInCity = foundByStateAndCityMap.get(currentCity);
//            for (Person person : peopleInCity) {
//                console.print("     " + person.getFirstName() + " " + person.getLastName());
//            }
//        }
        
    }
    
    public void searchByCity(){
        List<Address> addresses = addressDao.list();
        List<String> cityList = new ArrayList();
        
        for (Address address : addresses) {
            
            if(!cityList.contains(address.getState())){        //prevents repeats from being listed
                cityList.add(address.getCity());
            }
            
        }
        
        for (String string : cityList) {
            console.print(string);
        }
        
        console.print("Input city: ");
        String city = console.getString();
        List<Address> foundByCityList = addressDao.searchByCity(city);
        
        listAddresses(foundByCityList);
    }
    
    public void searchByLastName(){
        console.print("Input last name: ");
        String lastName = console.getString();
        List<Address> foundByLastNameList = addressDao.searchByLastName(lastName);
        listAddresses(foundByLastNameList);
    }
    
    public void searchByZip(){
        
        List<Address> addresses = addressDao.list();
        List<String> zipList = new ArrayList();
        for (Address address : addresses) {
            
            if(!zipList.contains(address.getZip())){        //prevents repeats from being listed
                zipList.add(address.getZip());
            }
            
        }
        for (String zip : zipList) {
            console.print(zip);
        }
        
        console.print("Input zipcode: ");
        String zipcode = console.getString();
        List<Address> foundByZipCode = addressDao.searchByZip(zipcode);
        listAddresses(foundByZipCode);
    }
    
    public void listAddresses(List<Address> addressList){
        for (Address address : addressList) {
            console.print(" " + address.getId() + " | "  + address.getLastName() + ", " + address.getFirstName() + " " + 
                    address.getStreetNumber() + " " + address.getStreetName() + " " + address.getCity() + " " + address.getState());
                   
        }
    }
}
