/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.interfaces.dao;

import com.thesoftwareguild.interfaces.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBookLambdaImpl implements AddressBookDao {

    private List<Address> addresses = new ArrayList();
    private int nextId = addresses.size() + 1;

    public AddressBookLambdaImpl() {
        addresses = decode();
    }

    @Override
    public Address create(Address address) {
        address.setId(nextId);
        nextId++;
        addresses.add(address);

        encode();

        return address;
    }

    @Override
    public void update(Address address) {
        addresses
                .stream()
                .filter((Address a) -> a.getId() == address.getId())
                .collect(Collectors.toList()) //creating array list of one address
                .listIterator() //get 
                .remove();
        addresses.add(address);
        encode();
    }

    @Override
    public Address get(Integer id) {
        addresses
                .stream()
                .filter(a -> {
                    return a.getId() == id;
                }
                );
        return null;
    }

    @Override
    public void delete(Integer id) {
        addresses
                .stream()
                .filter((Address a) -> a.getId() == id)
                .collect(Collectors.toList())
                .listIterator()
                .remove();
        encode();
    }

    @Override
    public List<Address> list() {
        List<Address> newList = addresses;
        return newList;
    }

    @Override
    public List<Address> searchByLastName(String lastName) {
        return addresses
                .stream()
                .filter((Address a) -> a.getLastName().equals(lastName))
                .collect(Collectors.toList());
    }

    @Override
    public List<Address> searchByCity(String city) {
        return addresses
                .stream()
                .filter((Address a) -> a.getCity().equals(city))
                .collect(Collectors.toList());
    }

    @Override
    public List<Address> searchByState(String state) {
//        Map<String, List<Person>> foundByStateAndCityMap
//                = addresses
//                .stream()
//                .filter(a -> a.getState().equals(state))
//                .collect(Collectors.toMap(Address::getCity,
//                        (a) -> {
//                            List<Person> onePersonList = new ArrayList();
//                            onePersonList.add(a.getPerson());
//                            return onePersonList;
//                        },
//                        (old, latest) -> {
//                            old.addAll(latest);
//                            return old;
//                        }));
//        return foundByStateAndCityMap;
        return addresses
                .stream()
                .filter((Address a) -> a.getState().equals(state))
                .collect(Collectors.toList());
    }

    @Override
    public List<Address> searchByZip(String zip) {
        return addresses
                .stream()
                .filter((Address a) -> a.getZip() == zip)
                .collect(Collectors.toList());
    }

    private void encode() {
        final String TOKEN = "::";

        try {
            PrintWriter out = new PrintWriter(new FileWriter("addresses.txt"));

            for (Address address : addresses) {
                out.print(address.getId());
                out.print(TOKEN);
                out.print(address.getFirstName());
                out.print(TOKEN);
                out.print(address.getLastName());
                out.print(TOKEN);
                out.print(address.getStreetNumber());
                out.print(TOKEN);
                out.print(address.getStreetName());
                out.print(TOKEN);
                out.print(address.getCity());
                out.print(TOKEN);
                out.print(address.getState());
                out.print(TOKEN);
                out.print(address.getZip());
                out.println();
            }

            out.flush();
            out.close();

        } catch (IOException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private List<Address> decode() {
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("addresses.txt")));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split("::");

                int id = Integer.parseInt(stringParts[0]);

                Address address = new Address();

                address.setId(id);
                address.setFirstName(stringParts[1]);
                address.setLastName(stringParts[2]);
                address.setStreetNumber(stringParts[3]);
                address.setStreetName(stringParts[4]);
                address.setCity(stringParts[5]);
                address.setState(stringParts[6]);
                address.setZip(stringParts[7]);

                addresses.add(address);
                nextId = 1 + addresses.size();

            }

            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return addresses;
    }

}
