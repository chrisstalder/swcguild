<%-- 
    Document   : response
    Created on : May 27, 2016, 11:48:33 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="tipCalculator.css" rel="stylesheet" type="text/css">
        <title>Tip Calculator</title>
    </head>
    <body>
        <h1>Tip Calculator</h1>
        <table>
            <tr>
                <td>
                    Amount: 
                </td>
                <td>
                    ${amount}<br><br>
                </td>
            </tr>
            <tr>
                <td>
                    Tip% :
                </td>
                <td>
                    ${tipPercent}<br><br>
                </td>
            </tr>
            <tr>
                <td>
                    Tip:
                </td>
                <td>
                    ${tip}<br><br>
                </td>
            </tr>
            <tr>
                <td>
                    Total:
                </td>
                <td>
                    ${total}<br><br>
                </td>
            </tr>
        </table>
        <table>
            <form action="TipCalculatorServlet" method="POST">
                <tr>
                    <td>
                        Amount: 
                    </td>
                    <td>
                        <input type="text" name="amount"/><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tip %: 
                    </td>
                    <td>
                        <input type="text" name="tipPercent"/><br><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Enter" id="submitButton"/><br><br>
                    </td>
                </tr> 
            </form>
        </table>
    </body>

</html>
