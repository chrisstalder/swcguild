<%-- 
    Document   : response
    Created on : May 26, 2016, 3:55:47 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Sevens</title>
        <link href="siteLab.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body>

        <div class="header"><h1>Lucky Sevens</h1>
            <form action="SiteLabServlet" method="POST">
                <ul id="menu">
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="luckySevens">Lucky 7s</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="factorizer">Factorizer</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="interestCalc">Interest Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="floorCalc">Floor Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="tipCalc">Tip Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="unitConverter">Unit Converter</button></li>
                </ul>  
            </form>

        </div>
        <div id="content">
            <form class="content1" action="LuckySevensServlet" method="POST">
                Starting Bet: <input type="text" name="bet"/><br><br>
                <input type="submit" value="Play"/>
            </form>

            <table id="resultsTable">

                <tr>
                    <th colspan="2">Results</th>
                </tr>

                <tr>
                    <td>Starting Bet</td>
                    <td><div id="display1">${startBet}</div></td>
                </tr>

                <tr>
                    <td>Total Rolls Before Going Broke</td>
                    <td id="display2">${totalRolls}</td>
                </tr>

                <tr>
                    <td>Highest Amount Won</td>
                    <td id="display3">${maxMoney}</td>
                </tr>

                <tr>
                    <td>Roll Count at Highest Amount Won</td>
                    <td id="display4">${maxRolls}</td>
                </tr>

            </table>
        </div>


    </body>
</html>

