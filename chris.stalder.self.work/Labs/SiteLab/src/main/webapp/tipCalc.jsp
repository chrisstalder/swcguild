<%-- 
    Document   : index
    Created on : May 27, 2016, 11:48:23 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="siteLab.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <title>Tip Calculator</title>
    </head>
    <body>
        <div class="header"><h1>Tip Calculator</h1>
            <form action="SiteLabServlet" method="POST">
                <ul id="menu">
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="luckySevens">Lucky 7s</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="factorizer">Factorizer</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="interestCalc">Interest Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="floorCalc">Floor Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="tipCalc">Tip Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="unitConverter">Unit Converter</button></li>
                </ul>  
            </form>

        </div>
        <div id="content">
            <div class="content1">
                <table>
                    <form action="TipCalculatorServlet" method="POST">
                        <tr>
                            <td>
                                Amount: 
                            </td>
                            <td>
                                <input type="text" name="amount"/><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tip %: 
                            </td>
                            <td>
                                <input type="text" name="tipPercent"/><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Enter" id="submitButton"/><br><br>
                            </td>
                        </tr> 
                    </form>
                </table>
            </div>

        </div>

    </body>
</html>
