<%-- 
    Document   : response
    Created on : May 27, 2016, 1:14:56 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="siteLab.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <title>Unit Converter</title>
    </head>
    <body>
        <div class="header"><h1>Unit Converter</h1>
            <form action="SiteLabServlet" method="POST">
                <ul id="menu">
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="luckySevens">Lucky 7s</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="factorizer">Factorizer</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="interestCalc">Interest Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="floorCalc">Floor Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="tipCalc">Tip Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="unitConverter">Unit Converter</button></li>
                </ul>  
            </form>

        </div>
        <div id="content">
            <form action="UnitConverterServlet" method="POST">
                <ul id="units">
                    <li>     
                        <input type="submit" value="Temperature" name="unitType" class="unitButton"/>
                    </li>
                    <li>  
                        <input type="submit" value="Currency" name="unitType" class="unitButton"/>
                    </li>
                    <li>                
                        <input type="submit" value="Volume" name="unitType" class="unitButton"/>
                    </li>
                    <li>                
                        <input type="submit" value="Mass" name="unitType" class="unitButton"/>
                    </li>
                </ul>
            </form>
            <div id="div1">
                <c:if test= "${'temperature'.equals(typeOfUnit)}">

                    <form id="temperature" action="UnitConverterServlet" method="POST">
                        From: 
                        <select name="selectFrom">
                            <option value="celsius">Celsius</option>
                            <option value="fahrenheit">Fahrenheit</option>
                        </select>

                        <input type="text" name="convertFrom"/>
                        To:
                        <select name="selectTo">
                            <option value="fahrenheit">Fahrenheit</option>
                            <option value="celsius">Celsius</option>
                        </select> 
                        <br><br>
                        ${convertedResult} ${unitToConvertTo}
                        <br><br>
                        <input type="submit" value="Submit"/>
                    </form>

                </c:if>

                <c:if test= "${'currency'.equals(typeOfUnit)}">

                    <form id="currency" action="UnitConverterServlet" method="POST">
                        From:
                        <select name="selectFrom">
                            <option value="USD">USD</option>
                            <option value="EU">EU</option>
                            <option value="CD">CD</option>
                        </select>

                        <input type="text" name="convertFrom"/>
                        To:  
                        <select name="selectTo">
                            <option value="EU">EU</option>
                            <option value="USD">USD</option>
                            <option value="CD">CD</option>
                        </select>
                        <br><br>
                        ${convertedResult} ${unitToConvertTo}
                        <br><br>
                        <input type="submit" value="Submit"/>
                    </form>

                </c:if>

                <c:if test= "${'volume'.equals(typeOfUnit)}">

                    <form id="volume" action="UnitConverterServlet" method="POST">
                        From:
                        <select name="selectFrom">
                            <option value="Liter">Liter</option>
                            <option value="Cm3">Cubic CM</option>
                            <option value="Oz">Ounces</option>
                        </select>

                        <input type="text" name="convertFrom"/>
                        To:
                        <select name="selectTo">
                            <option value="Cm3">Cubic CM</option>                    
                            <option value="Liter">Liter</option>
                            <option value="Oz">Ounces</option>
                        </select> 
                        <br><br>
                        ${convertedResult} ${unitToConvertTo}
                        <br><br>
                        <input type="submit" value="Submit"/>
                    </form>

                </c:if>

                <c:if test= "${'mass'.equals(typeOfUnit)}">

                    <form id="mass" action="UnitConverterServlet" method="POST">
                        From:
                        <select name="selectFrom">
                            <option value="Pounds">Pounds</option>
                            <option value="Kg">Kilograms</option>
                        </select>

                        <input type="text" name="convertFrom"/>
                        To:
                        <select name="selectTo">
                            <option value="Kg">Kilograms</option>                    
                            <option value="Pounds">Pounds</option>
                        </select>
                        <br><br>
                        <div id="resultBox">
                            ${convertedResult} ${unitToConvertTo}
                        </div>
                        <br><br>
                        <input type="submit" value="Submit"/>
                    </form> 

                </c:if>
            </div>
        </div>

    </body>
</html>
