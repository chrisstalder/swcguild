<%-- 
    Document   : response
    Created on : May 27, 2016, 10:35:36 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="siteLab.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <title>Flooring Calculator</title>
    </head>
    <body>
        <div class="header"><h1>Flooring Calculator</h1>
            <form action="SiteLabServlet" method="POST">
                <ul id="menu">
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="luckySevens">Lucky 7s</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="factorizer">Factorizer</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="interestCalc">Interest Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="floorCalc">Floor Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="tipCalc">Tip Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="unitConverter">Unit Converter</button></li>
                </ul>  
            </form>

        </div>
        <div id="content">
            <div class="content1">
                <table>
                    <tr>
                        <td>
                            Width: 
                        </td>
                        <td>
                            ${width}<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Length: 
                        </td>
                        <td>
                            ${length}<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cost Per Sq Foot: 
                        </td>
                        <td>
                            ${cost}<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Area: 
                        </td>
                        <td>
                            ${area}<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Total Material Cost: 
                        </td>
                        <td>
                            ${totalMaterialCost}<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Estimated Time: 
                        </td>
                        <td>
                            ${estimatedTime}<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Labor: 
                        </td>
                        <td>
                            ${laborCost}<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Total: 
                        </td>
                        <td>
                            ${total}<br>
                        </td>
                    </tr> 
                </table>

                <table>
                    <form action="FlooringCalculatorServlet" method="POST">
                        <tr>
                            <td>
                                Width: 
                            </td>
                            <td>
                                <input type="text" name="width"/><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Length: 
                            </td>
                            <td>
                                <input type="text" name="length"/><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cost per 1 Sq Foot: 
                            </td>
                            <td>
                                <input type="text" name="cost"/><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Submit" id="submitButton"/>
                            </td>
                        </tr>  
                    </form>
                </table>
            </div>

        </div>
    </body>
</html>
