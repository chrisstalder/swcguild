<%-- 
    Document   : response
    Created on : May 26, 2016, 9:18:14 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="siteLab.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <title>Interest Calculator</title>
    </head>
    <body>
        <div class="header"><h1>Interest Calculator</h1>
            <form action="SiteLabServlet" method="POST">
                <ul id="menu">
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="luckySevens">Lucky 7s</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="factorizer">Factorizer</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="interestCalc">Interest Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="floorCalc">Floor Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="tipCalc">Tip Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="unitConverter">Unit Converter</button></li>
                </ul>  
            </form>

        </div>
        <div id="content">
            <div  class="interestCalcContent">
                <table id="resultTable">
                    <tr>
                        <td>
                            Principal: 
                        </td>
                        <td>
                            ${principal}<br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Annual rate: 
                        </td>
                        <td>
                            ${annualInt}<br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Compounded: 
                        </td>
                        <td>
                            ${compoundedType}<br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Period Interest: 
                        </td>
                        <td>
                            ${periodInt}<br><br>
                        </td>
                    </tr>
                    <c:forEach items="${years}" var="year">
                        <tr>
                            <td colspan="2" id="year">
                                Year ${year.id}<br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Beginning Balance: 
                            </td>
                            <td>
                                ${year.currentBal} <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Final Balance: 
                            </td>
                            <td>
                                ${year.finalBal}<br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Interest earned: 
                            </td>
                            <td>
                                ${year.totalInt}<br><br>
                            </td>
                        </tr>       
                    </c:forEach>
                </table>
            </div>

        </div>
    </body>
</html>
