<%-- 
    Document   : index
    Created on : May 26, 2016, 9:18:06 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="siteLab.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <title>Interest Calculator</title>
    </head>
    <body>
        <div class="header"><h1>Interest Calculator</h1>
            <form action="SiteLabServlet" method="POST">
                <ul id="menu">
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="luckySevens">Lucky 7s</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="factorizer">Factorizer</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="interestCalc">Interest Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="floorCalc">Floor Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="tipCalc">Tip Calc</button></li>
                    <li><button type="submit" class="btn btn-primary" name="menuButton" value="unitConverter">Unit Converter</button></li>
                </ul>  
            </form>

        </div>
        <div id="content">
            <div  class="content1">
                <table>
                    <form action="InterestCalculatorServlet" method="POST">
                        <tr>
                            <td>
                                How much will you be investing?
                            </td>
                            <td>
                                <input type="text" name="principal"/><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                What is your annual interest rate?
                            </td>
                            <td>
                                <input type="text" name="annualInt"/><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                What will be your compound periods? (Quarterly, monthly, daily)
                            </td>
                            <td>
                                <input type="text" name="period"/><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                How many years will you be investing?
                            </td>
                            <td>
                                <input type="text" name="years"/><br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Submit" id="submitButton"/>
                            </td>
                        </tr>  
                    </form>
                </table>
            </div>

        </div>

    </body>
</html>
