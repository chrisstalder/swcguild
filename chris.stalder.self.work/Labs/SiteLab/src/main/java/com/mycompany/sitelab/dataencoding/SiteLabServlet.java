/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sitelab.dataencoding;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "SiteLabServlet", urlPatterns = {"/SiteLabServlet"})
public class SiteLabServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request.getRequestDispatcher("home.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        String menuButton = request.getParameter("menuButton");
        
        if("luckySevens".equals(menuButton)){
            RequestDispatcher rd = request.getRequestDispatcher("luckySevens.jsp");
            rd.forward(request, response);
        }
        else if("factorizer".equals(menuButton)){
            RequestDispatcher rd = request.getRequestDispatcher("factorizer.jsp");
            rd.forward(request, response);
        }
        else if("interestCalc".equals(menuButton)){
            RequestDispatcher rd = request.getRequestDispatcher("interestCalc.jsp");
            rd.forward(request, response);
        }
        else if("floorCalc".equals(menuButton)){
            RequestDispatcher rd = request.getRequestDispatcher("floorCalc.jsp");
            rd.forward(request, response);
        }
        else if("tipCalc".equals(menuButton)){
            RequestDispatcher rd = request.getRequestDispatcher("tipCalc.jsp");
            rd.forward(request, response);
        }
        else if("unitConverter".equals(menuButton)){
            RequestDispatcher rd = request.getRequestDispatcher("unitConverter.jsp");
            rd.forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
