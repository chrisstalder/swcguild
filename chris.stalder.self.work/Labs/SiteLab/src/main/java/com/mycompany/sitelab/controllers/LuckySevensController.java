/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sitelab.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "LuckySevensServlet", urlPatterns = {"/LuckySevensServlet"})
public class LuckySevensController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        RequestDispatcher rd = request.getRequestDispatcher("luckySevens.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String input = request.getParameter("bet");
        float bet = Float.parseFloat(input);
        float startBet = bet;
        int totalRolls = 0;
        int maxRolls = 0;
        float maxMoney = bet;

        while (bet > 0) {

            int total = rollDice();
            totalRolls++;

            if (total == 7) {
                bet += 4;
                maxMoney = max(bet, maxMoney);
                maxRolls = max(totalRolls, maxRolls);
            } else {
                bet--;
            }
        }
        
        request.setAttribute("startBet", startBet);
        request.setAttribute("totalRolls", totalRolls);
        request.setAttribute("maxRolls", maxRolls);
        request.setAttribute("maxMoney", maxMoney);
        RequestDispatcher rd = request.getRequestDispatcher("luckySevensResponse.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public int rollDice() {
        Random r = new Random();

        int d1 = 1 + r.nextInt(6);
        int d2 = 1 + r.nextInt(6);
        int total = d1 + d2;
        return total;
    }

    public float max(float input1, float input2) {
        if (input1 > input2) {
            input2 = input1;
        }

        return input2;
    }

    public int max(int input1, int input2) {
        if (input1 > input2) {
            input2 = input1;
        }

        return input2;
    }
}
