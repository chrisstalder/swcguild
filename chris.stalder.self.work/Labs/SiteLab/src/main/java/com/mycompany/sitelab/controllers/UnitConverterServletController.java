/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sitelab.controllers;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "UnitConverterServlet", urlPatterns = {"/UnitConverterServlet"})
public class UnitConverterServletController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request.getRequestDispatcher("unitConverter.jsp");
        rd.forward(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String type = request.getParameter("unitType");
        if(type != null){
            String typeOfUnit = null;
            if ("Temperature".equals(type)) {
                typeOfUnit = "temperature";
            } else if ("Currency".equals(type)) {
                typeOfUnit = "currency";
            } else if ("Volume".equals(type)) {
                typeOfUnit = "volume";
            } else if ("Mass".equals(type)) {
                typeOfUnit = "mass";
            }
            request.setAttribute("typeOfUnit", typeOfUnit);
            RequestDispatcher rd = request.getRequestDispatcher("unitConverter.jsp");
            rd.forward(request, response);
        }else{

            String unitToConvertFrom = request.getParameter("selectFrom");
            String unitToConvertTo = request.getParameter("selectTo");
            double convertFrom = Double.parseDouble(request.getParameter("convertFrom"));
            double convertedResult = 0;
            switch(unitToConvertFrom){
                case ("celsius"):
                    switch(unitToConvertTo){
                        case ("fahrenheit"):
                            convertedResult = convertToFahrenheit(convertFrom);
                            break;
                        case ("celsius"):
                            convertedResult = convertFrom;
                            break;
                    }
                    break;
                case ("fahrenheit"):
                    switch(unitToConvertTo){
                        case ("fahrenheit"):
                            convertedResult = convertFrom;
                            break;
                        case ("celsius"):
                            convertedResult = convertToCelsius(convertFrom);
                            break;
                    }
                    break;
                case ("USD"):
                    switch(unitToConvertTo){
                        case ("EU"):
                            convertedResult = convertUSDToEU(convertFrom);
                            break;
                        case ("CD"):
                            convertedResult = convertUSDToCD(convertFrom);
                            break;
                        case ("USD"):
                            convertedResult = convertedResult = convertFrom;
                            break;
                    }
                    break;
                case ("EU"):
                    switch(unitToConvertTo){
                        case ("EU"):
                            convertedResult = convertFrom;
                            break;
                        case ("CD"):
                            convertedResult = convertEUToCD(convertFrom);
                            break;
                        case ("USD"):
                            convertedResult = convertEUToUSD(convertFrom);
                            break;
                    }
                    break;
                case ("CD"):
                    switch(unitToConvertTo){
                        case ("EU"):
                            convertedResult = convertCDToEU(convertFrom);
                            break;
                        case ("CD"):
                            convertedResult = convertFrom;
                            break;
                        case ("USD"):
                            convertedResult = convertCDToUSD(convertFrom);
                            break;
                    }
                    break;
                case ("Liter"):
                    switch(unitToConvertTo){
                        case ("Cm3"):
                            convertedResult = convertLiterToCM3(convertFrom);
                            break;
                        case ("Oz"):
                            convertedResult = convertLiterToOz(convertFrom);
                            break;
                        case ("Liter"):
                            convertedResult = convertFrom;
                            break;
                    }
                    break;
                case ("Cm3"):
                    switch(unitToConvertTo){
                        case ("Cm3"):
                            convertedResult = convertFrom;
                            break;
                        case ("Oz"):
                            convertedResult = convertCM3ToOz(convertFrom);
                            break;
                        case ("Liter"):
                            convertedResult = convertCM3ToLiter(convertFrom);
                            break;
                    }
                    break;
                case ("Oz"):
                    switch(unitToConvertTo){
                        case ("Cm3"):
                            convertedResult = convertOzToCM3(convertFrom);
                            break;
                        case ("Oz"):
                            convertedResult = convertFrom;
                            break;
                        case ("Liter"):
                            convertedResult = convertOzToLiter(convertFrom);
                            break;
                    }
                    break;
                case ("Pounds"):
                    switch(unitToConvertTo){
                        case ("Kg"):
                            convertedResult = convertPoundToKg(convertFrom);
                            break;
                        case ("Pounds"):
                            convertedResult = convertFrom;
                            break;
                    }
                    break;
                case ("Kg"):
                    switch(unitToConvertTo){
                        case ("Kg"):
                            convertedResult = convertFrom;
                            break;
                        case ("Pounds"):
                            convertedResult = convertKgToPound(convertFrom);
                            break;
                    }
                    break;
            }
            String unitType = null;
            if("fahrenheit".equals(unitToConvertFrom)||"celsius".equals(unitToConvertFrom))
                unitType = "temperature";
            if("USD".equals(unitToConvertFrom)||"EU".equals(unitToConvertFrom)||"CD".equals(unitToConvertFrom))
                unitType = "currency";
            if("Oz".equals(unitToConvertFrom)||"Cm3".equals(unitToConvertFrom)||"Liter".equals(unitToConvertFrom))
                unitType = "volume";
            if("Pounds".equals(unitToConvertFrom)||"Kg".equals(unitToConvertFrom))
                unitType = "mass";
            request.setAttribute("typeOfUnit", unitType);
            request.setAttribute("unitToConvertFrom", unitToConvertFrom);
            request.setAttribute("unitToConvertTo", unitToConvertTo);
            request.setAttribute("convertFrom", convertFrom);
            request.setAttribute("convertedResult", convertedResult);
            RequestDispatcher rd = request.getRequestDispatcher("unitConverterResponse.jsp");
            rd.forward(request, response);
        }
    }

    
    public double convertToFahrenheit(double convertFrom){
        double result = convertFrom * (9/5) + 32;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertToCelsius(double convertFrom){
        double result = ((convertFrom - 32)*5)/9;
        return result;
    }
    
    public double convertUSDToEU(double convertFrom){
        double result = convertFrom * .9;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertUSDToCD(double convertFrom){
        double result = convertFrom * 1.30;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertEUToUSD(double convertFrom){
        double result = convertFrom * 1.11;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertEUToCD(double convertFrom){
        double result = convertFrom * 1.45;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    
    public double convertCDToUSD(double convertFrom){
        double result = convertFrom * .77;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertCDToEU(double convertFrom){
        double result = convertFrom * .69;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertLiterToCM3(double convertFrom){
        double result = convertFrom /0.0010000;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertLiterToOz(double convertFrom){
        double result = convertFrom * 33.814;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertCM3ToLiter(double convertFrom){
        double result = convertFrom / 1000.0;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertCM3ToOz(double convertFrom){
        double result = convertFrom * 0.033814;
        return result;
    }
    
    public double convertOzToLiter(double convertFrom){
        double result = convertFrom / 33.814;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertOzToCM3(double convertFrom){
        double result = convertFrom / 0.033814;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertPoundToKg(double convertFrom){
        double result = convertFrom / 2.2046;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }
    
    public double convertKgToPound(double convertFrom){
        double result = convertFrom * 2.2046;
        double roundOff = (double) Math.round(result * 100) / 100;
        return roundOff;
    }

    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
