/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class Factorizer {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("What number would you like to factor?");
        int input = keyboard.nextInt();
        boolean isPerfect = false;
        boolean isPrime = false;
        int evenDivides = 0;
        int factorTotal = 0;
        
        System.out.println("The factors of " + input + " are:");
        for (int i = 1; i < input; i++){    //for loop will loop through all numbers before input
            if (input % i == 0){
                System.out.println(i); //prints out i
                factorTotal += i; //keeps track of number of factors
                evenDivides++; //keeps track of how many numbers can evenly divide into input
            }
            if (factorTotal == input){
                isPerfect = true;       //if all factors added together equal input it is a perfect #
            }
            if (evenDivides == 1){
                isPrime = true;        //if a number only has one # that can divide into it evenly (excluding itself) then it is prime
            }else{
                isPrime = false;
            }
        }
        
        if (isPerfect == true){
            System.out.println(input + " is a perfect number.");        //prints out if it is perfect or not
        }else{
            System.out.println(input + " is not a perfect number.");
        }
        
        if (isPrime == true){
            System.out.println(input + " is a prime number.");
        }else{                                                      //prints out if it is a prime number
            System.out.println(input + " is not a prime number.");
        }
    }
}
