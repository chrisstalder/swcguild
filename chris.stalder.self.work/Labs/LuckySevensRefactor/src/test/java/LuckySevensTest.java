/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LuckySevensTest {
    
    public LuckySevensTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testMaxInt(){
        LuckySevensMethods ls = new LuckySevensMethods();
        
        int result = ls.max(7, 5);
        
        Assert.assertEquals(7, result);
    }
    
    @Test
    public void testMaxFloat(){
        LuckySevensMethods ls = new LuckySevensMethods();
        
        float result = ls.max(101.1f, 50.0f);
        
        Assert.assertEquals(101.1f, result);
    }
    
    @Test
    public void testRollDice(){
        LuckySevensMethods ls = new LuckySevensMethods();
        
        int result = ls.rollDice();
        
        Assert.assertTrue(result >= 2 && result <= 12);
    }
    
}
