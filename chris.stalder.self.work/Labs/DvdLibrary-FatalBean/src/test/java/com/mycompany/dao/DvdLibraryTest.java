/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Dvd;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class DvdLibraryTest {
    
    public DvdLibraryTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
@Test
    public void testCreate(){
        DvdLibraryDaoImpl instance = new DvdLibraryDaoImpl();
        Dvd dvd = new Dvd();
        Date date = new Date();
        dvd.setTitle("Pulp Fiction");
        dvd.setReleaseDate(date);
        dvd.setDirector("Quentin Tarantino");
        dvd.setRating("5 stars");
        dvd.setStudio("Some studio");
        dvd.setNote("Great film");

        Dvd result = instance.create(dvd);
        
        Assert.assertEquals(dvd, result);
    }
    
//    @Test
//    public void testGet(){
//        DvdLibraryDaoImpl instance = new DvdLibraryDaoImpl();
//        String name = "";
//        List<Dvd> result = instance.get(name);
//        
//        Assert.assertTrue(!result.isEmpty());
//    }
    
    @Test
    public void testUpdate(){
        DvdLibraryDaoImpl instance = new DvdLibraryDaoImpl();
        List<Dvd> list = instance.list();
        int nextId = list.size();
        Dvd dvd = new Dvd();
        Date date = new Date();
        dvd.setTitle("Pulp Fiction");
        dvd.setReleaseDate(date);
        dvd.setDirector("Quentin Tarantino");
        dvd.setRating("5 stars");
        dvd.setStudio("Some studio");
        dvd.setNote("Great film");
        
        
        for (Dvd dvd1 : list) {
            if(dvd1.getId() == dvd.getId())
                dvd = dvd1;
        }
        
        dvd.setNote("Fantastic film!");
        instance.update(dvd);
        
        String result = dvd.getNote();
        
        Assert.assertEquals("Fantastic film!", result);
    }
    
    @Test
    public void testDelete(){
        DvdLibraryDaoImpl instance = new DvdLibraryDaoImpl();
        List<Dvd> list = instance.list();
        
        int currentArraySize = list.size();
        
        Dvd dvd = new Dvd();
        for (Dvd dvd1 : list) {
            if(dvd1.getId() == dvd.getId())
                dvd = dvd1;
        }
        
        instance.delete(dvd);
        boolean result = false;
        for (Dvd dvd1 : list) {
            if(dvd1.getId() == dvd.getId())
                result = true;
        }
        
        Assert.assertFalse(result);
    }
}
