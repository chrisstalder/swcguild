/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.consoleio.ConsoleIO;
import com.mycompany.dao.DvdDao;
import com.mycompany.dao.DvdLibraryDaoLambdaImpl;
import com.mycompany.dto.Dvd;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class DvdLibraryController {

    public ConsoleIO console;
    public DvdDao dao;

    public DvdLibraryController(DvdDao dao) {
        this.dao = dao;
    }

    public void run() {
//        SearchController search = new SearchController(dao);
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SearchController search = ctx.getBean("searchController", SearchController.class);

        boolean repeat = true;
        while (repeat) {
            int selection;
            console.print("=============================");
            console.print("Welcome to your DVD Library");
            console.print("=============================");
            console.print("|(1) Add a Dvd              |");
            console.print("|(2) Remove a DVD           |");
            console.print("|(3) List DVDs              |");
            console.print("|(4) Display a DVD          |");
            console.print("|(5) Search for a DVD       |");
            console.print("|(6) Edit a DVD             |");
            console.print("|(7) Exit                   |");
            console.print("=============================");
            console.print("");
            console.print("What would you like to do?");

            selection = console.getIntValue();

            switch (selection) {
                case 1:
                    addDvd();
                    break;
                case 2:
                    removeDvd();
                    break;
                case 3:
                    listDvds();
                    break;
                case 4:
                    displayDvd();
                    break;
                case 5:
//                    search.run();
                    repeat = false;
                    break;
                case 6:
                    editDvd();
                    break;
                case 7:
                    repeat = false;
                    break;
                default:
                    console.print("That was not one of your options!");
            }
        }

    }

    public void addDvd() {
        boolean isValid = false;
        Date date = new Date();
        String rating = null;
        console.print("Please enter the title of the DVD you wish to add: ");
        String title = console.getString();
        while (!isValid) {
            console.print("Please enter the release date (January 1, 1970): ");
            date = console.tryParse();
            if (date != null) {
                isValid = true;
            } else {
                console.print("Invalid date.");
            }
        }
        isValid = false;
        while (!isValid) {
            console.print("Please enter the MPAA rating (G, PG, PG-13, R): ");
            rating = console.getString();
            if (("r".equals(rating)) || ("pg-13".equals(rating)) || ("pg".equals(rating)) || ("g".equals(rating)) || ("nc-17".equals(rating))) {
                isValid = true;
            } else {
                console.print("That is not a MPAA rating.");
            }
        }
        console.print("Please enter the director's name: ");
        String director = console.getString();
        console.print("Please enter the name of the studio: ");
        String studio = console.getString();
        console.print("Please enter any notes you have about the DVD: ");
        String note = null;
        note = console.getString();

        Dvd newDvd = new Dvd();
        newDvd.setTitle(title);
        newDvd.setReleaseDate(date);
        newDvd.setRating(rating);
        newDvd.setDirector(director);
        newDvd.setStudio(studio);
        newDvd.setNote(note);

        dao.create(newDvd);

    }

    public void removeDvd() {
        List<Dvd> list = dao.list();
        listDvds();
        console.print("Please enter the ID of the DVD you wish to remove.");
        int id = console.getIntValue();
        Dvd found = new Dvd();
        for (Dvd dvd : list) {
            if (dvd.getId() == id) {
                found = dvd;
            }
        }

        dao.delete(found);

    }

    public void listDvds() {
        List<Dvd> list = dao.list();
        DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
        for (Dvd dvd : list) {
            console.print("=================================================================");
            console.print(dvd.getId() + " |" + dvd.getTitle() + " " + format.format(dvd.getReleaseDate()));
            console.print("  |" + dvd.getDirector() + ", " + dvd.getStudio());
            console.print("  |" + dvd.getRating() + ", " + dvd.getNote());
            console.print("=================================================================");
        }
    }

    public void displayDvd() {
        DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
        console.print("Enter the title of the movie you want displayed.");
        String title = console.getString();
        List<Dvd> getDvd = dao.get(title);
        Dvd dvd = getDvd.get(0);
        if (dvd != null) {
            console.print("=================================================================");
            console.print(dvd.getId() + " |" + dvd.getTitle() + " " + format.format(dvd.getReleaseDate()));
            console.print("  |" + dvd.getDirector() + ", " + dvd.getStudio());
            console.print("  |" + dvd.getRating() + ", " + dvd.getNote());
            console.print("=================================================================");
        } else {
            console.print("That title is not currently in the library!");
        }

    }

    public void editDvd() {
        DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
        Date date = new Date();
        console.print("Enter the title of the DVD you wish to edit.");
        boolean isValid = false;
        List<Dvd> getDvd = new ArrayList();
        String title = null;
        while (!isValid) {
            title = console.getString();
            getDvd = dao.get(title);
            if (!getDvd.isEmpty()) {
                isValid = true;
            } else {
                console.print("Invalid title. Enter a valid title.");
            }
        }
        Dvd dvd = getDvd.get(0);
        console.print("=================================================================");
        console.print(dvd.getId() + " |" + dvd.getTitle() + " " + format.format(dvd.getReleaseDate()));
        console.print("  |" + dvd.getDirector() + ", " + dvd.getStudio());
        console.print("  |" + dvd.getRating() + ", " + dvd.getNote());
        console.print("=================================================================");

        console.print("What would you like to edit?");
        console.print("1) Title");
        console.print("2) Date");
        console.print("3) Director");
        console.print("4) Studio");
        console.print("5) Rating");
        console.print("6) Note");
        int selection = console.getIntValue();

        switch (selection) {
            case 1:
                console.print("Edit title: ");
                title = console.getString();
                dvd.setTitle(title);
                break;
            case 2:
                console.print("Edit date: ");
                date = console.tryParse();
                dvd.setReleaseDate(date);
                break;
            case 3:
                console.print("Edit director: ");
                String director = console.getString();
                dvd.setDirector(director);
                break;
            case 4:
                console.print("Edit studio: ");
                String studio = console.getString();
                dvd.setStudio(studio);
                break;
            case 5:
                console.print("Edit rating: ");
                String rating = console.getString();
                dvd.setRating(rating);
                break;
            case 6:
                console.print("Edit note: ");
                String note = console.getString();
                dvd.setNote(note);
                break;
            default:
                console.print("That wasn't an option.");
                break;
        }

        dao.update(dvd);
    }

}
