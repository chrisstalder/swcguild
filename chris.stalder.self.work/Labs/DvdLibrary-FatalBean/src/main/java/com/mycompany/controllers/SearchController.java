/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.consoleio.ConsoleIO;
import com.mycompany.dao.DvdDao;
import com.mycompany.dto.Dvd;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 *
 * @author apprentice
 */
public class SearchController {
    DvdDao dao;
    
//    DvdLibraryController libraryController = new DvdLibraryController();
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    DvdLibraryController libraryController = ctx.getBean("dvdLibraryController", DvdLibraryController.class);
        
    ConsoleIO console = new ConsoleIO();
    
    public SearchController(DvdDao dao){
        this.dao = dao;
    }
    
    public void run(){
        
        boolean repeat = true;
        
        while(repeat){
            int selection;
            console.print("===========================");
            console.print("Welcome to your DVD Library");
            console.print("===========================");
            console.print("1) Search Dvds");
            console.print("2) List by Studio");
            console.print("3) List by MPAA Rating");
            console.print("4) List by Director");
            console.print("5) Average Age of Movies");
            console.print("6) Find Newest Movie");
            console.print("7) Find Oldest Movie");
            console.print("8) Find Average Number of Notes" );
            console.print("9) Find All Movies Released Before N Years" );
            console.print("0) Exit" );
            console.print("===========================");
            console.print("");
            console.print("What would you like to do?");

            selection = console.getIntValue();

            switch(selection){
                case 1:
                    searchDvds();
                    break;
                case 2:
                    listDvdByStudio();
                    break;
                case 3:
                    listDvdByRating();
                    break;
                case 4:
                    listDvdByDirector();
                    break;
                case 5:
                    findAvgAgeOfMoviesInLibrary();
                    break;
                case 6:
                    findNewestMovie();
                    break;
                case 7:
                    findOldestMovie();
                    break;
                case 8:
                    findAvgNumOfNotes();
                    break;
                case 9:
                    findAllMoviesReleasedInNYears();
                    break;
                case 0:
                    libraryController.run();
                    repeat = false;
                    break;
                default:
                    console.print("That was not one of your options!");
            }
        }
        
    }
    
    public void searchDvds(){
        List<Dvd> list = dao.list();
        DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
        console.print("Enter the title of the movie(s) you are searching for.");
        String title = console.getString();
        for (Dvd dvd : list) {
            if(dvd.getTitle().equals(title)){
                console.print("=================================================================");
                console.print(dvd.getId() + " |" + dvd.getTitle() + " " + format.format(dvd.getReleaseDate()));
                console.print("  |" + dvd.getDirector() + ", " + dvd.getStudio());
                console.print("  |" + dvd.getRating() + ", " + dvd.getNote());  
                console.print("=================================================================");
            }
        }
        if(dao.get(title) == null){
            console.print("There are no titles with that name currently in the library!");
        }
    }
    
    public void listDvdByStudio(){
       List<Dvd> dvdList = dao.list();
       List<String> studios = new ArrayList();
        for (Dvd dvd : dvdList) {
            if(!studios.contains(dvd.getStudio()))         //create a list of studios and in library
                studios.add(dvd.getStudio());
        }
        
        for (String studio : studios) {
            console.print(studio);                  //display each studio in library
        }
        
       console.print("Input studio: ");
       String studio = console.getString();
       List<Dvd> foundByStudio =  dao.listDvdByStudio(studio);
       listDvd(foundByStudio);
    }
    
    public void listDvdByRating(){
        console.print("Input rating: ");
        String rating = console.getString();
        List<Dvd> foundByRating = dao.listDvdByRating(rating);
        for (Dvd dvd : foundByRating) {
            console.print(dvd.getTitle());
        }
        
        listDvd(foundByRating);
    }
    
    public void listDvdByDirector(){
        List<Dvd> dvdList = dao.list();
        List<String> directorList = new ArrayList();
        
        for (Dvd dvd : dvdList) {
            if(!directorList.contains(dvd.getDirector()))
                directorList.add(dvd.getDirector());
        }
        
        for (String string : directorList) {
            console.print(string);
        }
        
        console.print("Input director's name: ");
        String director = console.getString();
        
        Map<String, List<Dvd>> foundByDirectorAndRatingMap = dao.listDvdByDirector(director);
        Set<String> ratingKeySet = foundByDirectorAndRatingMap.keySet();
        Iterator i = ratingKeySet.iterator();
        
        while(i.hasNext()){
            String currentRating = i.next().toString();
            console.print(currentRating);
            List<Dvd> moviesByRating = foundByDirectorAndRatingMap.get(currentRating);
            for (Dvd dvd : moviesByRating) {
                console.print("     " + dvd.getTitle() + " " + dvd.getStudio());
            }
        }
    }
    
    public void findAvgAgeOfMoviesInLibrary(){
        List<Dvd> dvdList = dao.list();
        int totalAge = 0;
        for (Dvd dvd : dvdList) {
            Date date = dvd.getReleaseDate();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            LocalDate releaseDate = LocalDate.of(year, month, day);
            LocalDate now = LocalDate.now();
            Period age = Period.between(releaseDate, now);
            totalAge += age.getYears();
        }
        
        int avgAge = totalAge/dvdList.size();
        
        console.print("The average age of the movies in your library is " + avgAge + " years.");
        
    }
    public void findNewestMovie(){
        
        Dvd newestDvd = dao.findNewestMovie();
        displayDvd(newestDvd);
    }
    
    public void findOldestMovie(){
        
        Dvd oldestDvd = dao.findOldestMovie();
        displayDvd(oldestDvd);
    }
    
    public void findAvgNumOfNotes(){
        
        int avgNumOfNotes = dao.findAvgNumOfNotes();
        console.print("You have an average of " + avgNumOfNotes + " notes in your library.");
        
    }
    
    public void findAllMoviesReleasedInNYears(){
        console.print("Input how many years: ");
        int n = console.getIntValue();
        
        Calendar now = Calendar.getInstance();
        now.add(Calendar.YEAR, -n);                 //subtracting n years from current time
        int year = now.get(Calendar.YEAR);      
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);
        now.set(year, month, day);                  //setting now to current year month day. Not sure if this step is needed
        Date endOfRange = now.getTime();            //creating Date object from Calendar object

        List<Dvd> foundByAgeDvdList = dao.findAllMoviesReleasedInNYears(endOfRange);

        listDvd(foundByAgeDvdList);
    }
    
    public void listDvd(List<Dvd> dvdList){
        DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
        for (Dvd dvd : dvdList) {
            console.print("=================================================================");
            console.print(dvd.getId() + " |" + dvd.getTitle() + " " + format.format(dvd.getReleaseDate()));
            console.print("  |" + dvd.getDirector() + ", " + dvd.getStudio());
            console.print("  |" + dvd.getRating() + ", " + dvd.getNote());  
            console.print("=================================================================");
        }
        
    }
    
    public void displayDvd(Dvd dvd){
        DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
        console.print("=================================================================");
            console.print(dvd.getId() + " |" + dvd.getTitle() + " " + format.format(dvd.getReleaseDate()));
            console.print("  |" + dvd.getDirector() + ", " + dvd.getStudio());
            console.print("  |" + dvd.getRating() + ", " + dvd.getNote());  
            console.print("=================================================================");
    }
}
