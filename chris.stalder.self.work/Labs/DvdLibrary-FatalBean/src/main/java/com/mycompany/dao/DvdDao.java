/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Dvd;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DvdDao {
    
    public Dvd create(Dvd dvd);
    public List<Dvd> get(String title);
    public void update(Dvd dvd);
    public void delete(Dvd dvd);
    public List<Dvd> list();
    public void encode();
    public List<Dvd> decode();
    public List<Dvd> listDvdByStudio(String studio);
    public List<Dvd> listDvdByRating(String rating);
    public Map<String, List<Dvd>> listDvdByDirector(String director);
    public Dvd findNewestMovie();
    public Dvd findOldestMovie();
    public int findAvgNumOfNotes();
    public List<Dvd> findAllMoviesReleasedInNYears(Date n);
}
