/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.statecapitals;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class StateCapitals {
    
    public Map<String, String> stateCapitalsMap(){
        
        Map<String, String> states = new HashMap();
        
        states.put("Alabama", "Montgomery");
        states.put("Alaska", "Juneau");
        states.put("Arizona", "Phoenix");
        states.put("Arkansas", "Little Rock");
        states.put("California", "Sacramento");
        states.put("Colorado", "Denver");
        
        return states;
    }
    
    public void states(Map<String, String> states){
        Set<String> keySet = states.keySet();
        System.out.println("States:");
        System.out.println("=================");
        for (String string : keySet) {
            System.out.println(string);
        }
        
    }
    
    public Collection<String> stateCapitals(Map<String, String> states){
        Collection<String> capitals = states.values();
        System.out.println("Capitals:");
        System.out.println("=================");
        for (String capital : capitals) {
            System.out.println(capital);
        }
        return capitals;
    }
    
    public void stateAndCapitals(Map<String, String> states){
        Set<String> keySet = states.keySet();
        Collection<String> capitals = states.values();
        Iterator i = keySet.iterator();
        
        System.out.println("States and Capitals:");
        System.out.println("=================");
        while(i.hasNext()){
   
            String key = (String) i.next();
            String value = (String) states.get(key);
            System.out.println(key + " - " + value);
        }
        
    }
    
}
