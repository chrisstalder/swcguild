
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author apprentice
 */
public class SimpleCalculator {
    Scanner keyboard = new Scanner(System.in);
    public void run(){
        
        boolean useAgain = true;
        boolean isValid = false;
        while (useAgain) {
        
        
            System.out.println("Simple Calculator");
            System.out.println("==============");
            String input = "";
            System.out.println("Add");
            System.out.println("Subtract");
            System.out.println("Multiply");
            System.out.println("Divide");
            System.out.println("Exit");

            
            while (!isValid){
                System.out.println("What would you like to do?");
                
                try{
                    input = keyboard.nextLine().toLowerCase();
                    isValid = true;
                }catch(Exception ex){
                    System.out.println("That is invalid!");
                }
            }
            isValid = false;
            switch (input) {
                case ("add"):
                    int []array = getInput();
                    add(array);
                    break;
                case ("subtract"):
                    array = getInput();
                    subtract(array);
                    break;
                case ("multiply"):
                    array = getInput();
                    multiply(array);
                    break;
                case ("divide"):
                    float[] floatArray = getFloatInput();
                    divide(floatArray);
                    break;
                case ("exit"):
                    System.out.println("Thanks for using the Simple Calculator!");
                    useAgain = false;
                    break;

            }
            
        }
    }

    public int[] getInput(){
        boolean isValid = false;
        int num1 = 0;
        int num2 = 0;
        while (!isValid){
            System.out.print("Number 1: ");
            String a = keyboard.nextLine(); 
                try{
                    num1 = Integer.parseInt(a);
                    isValid = true;
                }catch(Exception ex){
                    System.out.println("That is invalid!");
                }
        }
        isValid = false;
        while (!isValid){
            System.out.print("Number 2: ");
            String b = keyboard.nextLine(); 
                try{
                    num2 = Integer.parseInt(b);
                    isValid = true;
                }catch(Exception ex){
                    System.out.println("That is invalid!");
                }
         }
        int[] numArray = {num1, num2};
        return numArray;
        
    }
    
    public float[] getFloatInput(){
        boolean isValid = false;
        float num1 = 0;
        float num2 = 0;
        while (!isValid){
            System.out.print("Number 1: ");
            String a = keyboard.nextLine(); 
                try{
                    num1 = Float.parseFloat(a);
                    isValid = true;
                }catch(Exception ex){
                    System.out.println("That is invalid!");
                }
        }
        isValid = false;
        while (!isValid){
            System.out.print("Number 2: ");
            String b = keyboard.nextLine(); 
                try{
                    num2 = Float.parseFloat(b);
                    isValid = true;
                }catch(Exception ex){
                    System.out.println("That is invalid!");
                }
         }
        float[] numArray = {num1, num2};
        return numArray;
        
    }
    
    public void add(int[]numArray){
        int sum = numArray[0] + numArray[1];
        System.out.println(numArray[0] + " + " + numArray[1] + " = " + sum + "\n");
    }
    public void subtract(int[]numArray){
        int sum = numArray[0] - numArray[1];
        System.out.println(numArray[0] + " - " + numArray[1] + " = " + sum + "\n");
    }
    public void multiply(int[] numArray){
        int sum = numArray[0] * numArray[1];
        System.out.println(numArray[0] + " X " + numArray[1] + " = " + sum + "\n");
    }
    public void divide(float[] numArray){
        float sum = numArray[0] / numArray[1];
        System.out.println(numArray[0] + " / " + numArray[1] + " = " + sum + "\n");
    }
}
