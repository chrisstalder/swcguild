/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProductInventoryTests;

import com.mycompany.productinventory.dao.InventoryDao;
import com.mycompany.productinventory.dto.Produce;
import com.mycompany.productinventory.dto.Product;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InventoryDaoTest {
    InventoryDao instance = new InventoryDao();
    Product product = new Produce();
    
    public InventoryDaoTest() {
    }
    
    @Before
    public void setUp() {  
        product.setName("lettuce");
        product.setDept("produce");
        product.setRetailPrice(.99);
        product.setWholesalePrice(.49);
        product.setStock(5);
        product.setStockLevel(2);
        product.setDeliveredOn(new Date());
    }
    
    @After
    public void tearDown() {
//        instance.delete(product);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCreate(){
        instance.create(product);
        List<Product> productList = instance.list();
        
        Assert.assertTrue(productList.contains(product));
    }
    
    @Test
    public void testGet(){
        Product p = instance.get(11);
        Assert.assertEquals(11, p.getId());
    }
     
    @Test
    public void testUpdate(){
        
        product.setName("iceburg lettuce");
        instance.update(product);
        Assert.assertEquals("iceburg lettuce", product.getName());
    }
    
    @Test
    public void testDelete(){
        instance.delete(product);
        List<Product> productList = instance.list();
        Assert.assertTrue(!productList.contains(product));
    }
    
}
