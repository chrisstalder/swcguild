/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class ConsoleIO {
    
    Scanner keyboard = new Scanner(System.in);
    
    public int getIntValue(){
        String input = keyboard.nextLine();
        int value = 0;
        boolean isValid = false;
        while(!isValid){
            try{
                value = Integer.parseInt(input);
                isValid = true;
            }catch(Exception e){
                System.out.println("Invalid entry.");
                input = keyboard.nextLine();
            }
        }
        return value;
    }
    
    
    public float getFloatValue(){
        String input = keyboard.nextLine();
        float value = Float.parseFloat(input);
        return value;
    }
    
    
    public double getDoubleValue(){
        boolean isValid = false;
        double value = 0;
        while (!isValid){
            
            String input = keyboard.nextLine();
            
            try{
                
                value = Double.parseDouble(input);
                isValid = true;
            } catch(Exception ex) {
                System.out.println("Invalid entry.");
            }
            
        }
        
        return value;
    }
    public double getArea(){
        
        boolean isValid = false;
        double value = 0;
        String input = keyboard.nextLine();
        if(!"".equals(input)){
            while (!isValid){
                try{
                    value = Double.parseDouble(input);
                    if(value > 0)
                        isValid = true;
                } catch(Exception ex) {
                    System.out.println("Invalid entry.");
                    input = keyboard.nextLine();
                }
            }
        }
                
        return value;
    }
    
    
    
    public String getString(){
        String input = keyboard.nextLine().toLowerCase();
        return input;
    }
    
    
    public Date getDate(String dateInString){
        
        
        DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
        Date date = new Date();
        try {
            date = format.parse(dateInString);
        } catch (ParseException ex) {
            Logger.getLogger(ConsoleIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

    public Date tryParse(){
        String[] formatStrings = {"M/d/y", "M-d-y", "MMMM dd, yyyy", "MMMM dd yyyy", "MMMM-dd-yyyy", "MMMM/dd/yyyy", "MMMMddyyyy", "MMddyyyy", "MM/dd/yyyy", "MM-dd-yyyy", "MM dd yyyy", "MM ddyyyy", "MMdd yyyy", "MMddyy"};
        String dateString = keyboard.nextLine();
        for (String formatString : formatStrings)
        {
            try
            {
                return new SimpleDateFormat(formatString).parse(dateString);
            }
            catch (ParseException e) {}
        }

        return null;
    }
    
    public String dateToString(Date date){
        DateFormat format = new SimpleDateFormat("MMddYYYY");
        String string = format.format(date);
        return string;
    }
    public Date stringToDate(String dateInString){
        try {
            DateFormat format = new SimpleDateFormat("MMddYYYY");
            Date date = format.parse(dateInString);
            return date;
        } catch (ParseException ex) {
            Logger.getLogger(ConsoleIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void print(String message){
        System.out.println(message);
    }
    
    public void menu(List<String> menuItems, String title){
        //I have no idea what I was doing here, but I'm too afraid to delete it
        String longest = "";
        for (String menuItem : menuItems) {
            if (menuItem.length() > longest.length()){
                longest = menuItem;
            }
        }
        
        int width = longest.length() + 4;
        int halfWidth = (width - title.length())/2;
        
        String header = "=";
        for(int i = 0; i <= width; i++){
            header+="=";
        }
        header += "=";
        
        String half = "";
        for (int i = 0; i <= halfWidth; i++) {
            half += " ";
        }
        
        String string = "";
        for(int i = 0; i <= width; i++){
            string+=" ";
        }
        int i = 1;
        System.out.println(header);
        System.out.println("|" + half + title + half + "|");
        for (String menuItem : menuItems) {
            System.out.println("|" + string + "|");
            
            int space = longest.length() - menuItem.length();
            String holder = " ";
            for(int z = 0; z < space; z++){
                holder+=" ";
            }
            if(i < 10)
                holder+= " ";
            
            System.out.println("|" + i + ") " + menuItem + holder + "|");
            
            i++;
        }
        System.out.println(header);
    }
    
    public boolean getYesNo(){
        boolean yes = false;
        String input = keyboard.nextLine();
        
        if("y".equals(input) || "yes".equals(input) || "Y".equals(input) || "YES".equals(input))
            yes = true;
        
        return yes;
    }
    
    public String getState(){
        String state = keyboard.nextLine().toUpperCase();
        boolean isValid = false;
        while(!isValid){
            switch (state){
                case ("ALABAMA"):
                    state = "AL";
                    break;
                case ("ALASKA"):
                    state = "Ak";
                    break;
                case ("ARIZONA"):
                    state = "AZ";
                    break;
                case ("ARKANSAS"):
                    state = "AR";
                    break;
                case ("CALIFORNIA"):
                    state = "CA";
                    break;
                case ("COLORADO"):
                    state = "CO";
                    break;
                case ("CONNECTICUT"):
                    state = "CT";
                    break;
                case ("DELAWARE"):
                    state = "DE";
                    break;
                case ("FLORIDA"):
                    state = "FL";
                    break;
                case ("GEORGIA"):
                    state = "GA";
                    break;
                case ("HAWAII"):
                    state = "HI";
                    break;
                case ("IDAHO"):
                    state = "ID";
                    break;
                case ("ILLINOIS"):
                    state = "IL";
                    break;
                case ("INDIANA"):
                    state = "IN";
                    break;
                case ("IOWA"):
                    state = "IA";
                    break;
                case ("KANSAS"):
                    state = "KS";
                    break;
                case ("KENTUCKY"):
                    state = "KY";
                    break;
                case ("LOUISIANA"):
                    state = "LA";
                    break;
                case ("MAINE"):
                    state = "ME";
                    break;
                case ("MARYLAND"):
                    state = "MD";
                    break;
                case ("MASSACHUSETTS"):
                    state = "MA";
                    break;
                case ("MICHIGAN"):
                    state = "MI";
                    break;
                case ("MINNESOTA"):
                    state = "MN";
                    break;
                case ("MISSISSIPPI"):
                    state = "MS";
                    break;
                case ("MISSOURI"):
                    state = "MO";
                    break;
                case ("MONTANA"):
                    state = "MT";
                    break;
                case ("NEBRASKA"):
                    state = "NE";
                    break;
                case ("NEVADA"):
                    state = "NV";
                    break;
                case ("NEW HAMPSHIRE"):
                    state = "NH";
                    break;
                case ("NEW JERSEY"):
                    state = "NJ";
                    break;
                case ("NEW MEXICO"):
                    state = "NM";
                    break;
                case ("NEW YORK"):
                    state = "NY";
                    break;
                case ("NORTH CAROLINA"):
                    state = "NC";
                    break;
                case ("NORTH DAKOTA"):
                    state = "ND";
                    break;
                case ("OHIO"):
                    state = "OH";
                    break;
                case ("OKLAHOMA"):
                    state = "OK";
                    break;
                case ("OREGON"):
                    state = "OR";
                    break;
                case ("PENNSYLVANIA"):
                    state = "PA";
                    break;
                case ("RHODE ISLAND"):
                    state = "RI";
                    break;
                case ("SOUTH CAROLINA"):
                    state = "SC";
                    break;
                case ("SOUTH DAKOTA"):
                    state = "SD";
                    break;
                case ("TENNESSEE"):
                    state = "TN";
                    break;
                case ("TEXAS"):
                    state = "TX";
                    break;
                case ("UTAH"):
                    state = "UT";
                    break;
                case ("VERMONT"):
                    state = "VT";
                    break;
                case ("VIRGINIA"):
                    state = "VA";
                    break;
                case ("WASHINGTON"):
                    state = "WA";
                    break;
                case ("WEST VIRGINIA"):
                    state = "WV";
                    break;
                case ("WISCONSIN"):
                    state = "WI";
                    break;
                case ("WYOMING"):
                    state = "WY";
                    break;
                case ("AL"):
                    state = "AL";
                    break;
                case ("AK"):
                    state = "AK";
                    break;
                case ("AZ"):
                    state = "AZ";
                    break;
                case ("AR"):
                    state = "AR";
                    break;
                case ("CA"):
                    state = "CA";
                    break;
                case ("CO"):
                    state = "CO";
                    break;
                case ("CT"):
                    state = "CT";
                    break;
                case ("DE"):
                    state = "DE";
                    break;
                case ("FL"):
                    state = "FL";
                    break;
                case ("GA"):
                    state = "GA";
                    break;
                case ("HI"):
                    state = "HI";
                    break;
                case ("ID"):
                    state = "ID";
                    break;
                case ("IL"):
                    state = "IL";
                    break;
                case ("IN"):
                    state = "IN";
                    break;
                case ("IA"):
                    state = "IA";
                    break;
                case ("KS"):
                    state = "KS";
                    break;
                case ("KY"):
                    state = "KY";
                    break;
                case ("LA"):
                    state = "LA";
                    break;
                case ("ME"):
                    state = "ME";
                    break;
                case ("MD"):
                    state = "MD";
                    break;
                case ("MA"):
                    state = "MA";
                    break;
                case ("MI"):
                    state = "MI";
                    break;
                case ("MN"):
                    state = "MN";
                    break;
                case ("MS"):
                    state = "MS";
                    break;
                case ("MO"):
                    state = "MO";
                    break;
                case ("MT"):
                    state = "MT";
                    break;
                case ("NE"):
                    state = "NE";
                    break;
                case ("NV"):
                    state = "NV";
                    break;
                case ("NH"):
                    state = "NH";
                    break;
                case ("NJ"):
                    state = "NJ";
                    break;
                case ("NM"):
                    state = "NM";
                    break;
                case ("NY"):
                    state = "NY";
                    break;
                case ("NC"):
                    state = "NC";
                    break;
                case ("ND"):
                    state = "ND";
                    break;
                case ("OH"):
                    state = "OH";
                    break;
                case ("OK"):
                    state = "OK";
                    break;
                case ("OR"):
                    state = "OR";
                    break;
                case ("PA"):
                    state = "PA";
                    break;
                case ("RI"):
                    state = "RI";
                    break;
                case ("SC"):
                    state = "SC";
                    break;
                case ("SD"):
                    state = "SD";
                    break;
                case ("TN"):
                    state = "TN";
                    break;
                case ("TX"):
                    state = "TX";
                    break;
                case ("UT"):
                    state = "UT";
                    break;
                case ("VT"):
                    state = "VT";
                    break;
                case ("VA"):
                    state = "VA";
                    break;
                case ("WA"):
                    state = "WA";
                    break;
                case ("WV"):
                    state = "WV";
                    break;
                case ("WI"):
                    state = "WI";
                    break;
                case ("WY"):
                    state = "WY";
                    break;
                default:
                    System.out.println("Input valid state.");
                    state = null;
                    
            }
            if(state != null){
               isValid = true;
            }else{
                state = keyboard.nextLine().toUpperCase();
            }
            
        }

        return state;
    }
    
    public boolean getPassword(String string){
        String password = keyboard.nextLine();
        
        if(password.equals(string)){
            return true;
        }else
            return false;
    }
    
    public double validateDouble(){
        boolean isValid = false;
        double n = getDoubleValue();
        while(!isValid){
            if (n > 0)
                isValid = true;
            else{
                System.out.println("Invalid Entry.");
                n = getDoubleValue();
            }
        }
        
        return n;
    }
}
