/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.controllers;

import com.mycompany.productinventory.dao.StoreDao;
import com.mycompany.productinventory.dto.Store;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class StoreController {
    StoreDao storeDao = new StoreDao();
    
    ConsoleIO console = new ConsoleIO();
    
    public void run(){
        
        boolean repeat = true;
        while(repeat){
            
            console.print("|======================================|");
            console.print("|            Store Menu:               |");
            console.print("|======================================|");
            console.print("|1) Create Store                       |");
            console.print("|2) List Stores                        |");
            console.print("|3) Use Store                          |");
            console.print("|4) Edit Store                         |");
            console.print("|5) Delete Store                       |");
            console.print("|6) Exit                               |");
            console.print("========================================");

            int selection = console.getIntValue();
            
            switch(selection){
                case 1:
                    createStore();
                    break;
                case 2:
                    listStores();
                    break;
                case 3:
                    useStore();
                    repeat = false;
                    break;
                case 4:
                    break;
                case 5:
                    break;
            }
        }
        
    }
    
    public void createStore(){
        console.print("Input name: ");
        String name = console.getString();
        console.print("Input type of store: ");
        String type = console.getString();
        
        Store store = new Store();
        store.setName(name);
        store.setType(type);
        storeDao.create(store);
    }
    public void listStores(){
        List<Store> storeList = storeDao.list();
        for (Store store : storeList) {
            console.print(store.getName());
        }
    }
    
    public void useStore(){
        InventoryController inventoryController = new InventoryController();
        List<Store> storeList = storeDao.list();
        for (Store store : storeList) {
            console.print(store.getId() + " " + store.getName());
        }
        
        console.print("Input id: ");
        int id = console.getIntValue();
        Store store = storeDao.get(id);
        inventoryController.run(store);
    }
}
