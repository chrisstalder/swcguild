/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.dto;

import java.util.Date;

/**
 *
 * @author apprentice
 */
public class Product {
    
    protected String name;
    protected String dept;
    protected double retailPrice;
    protected int stock;
    protected int initialStock;
    protected int stockLevel;
    protected double wholesalePrice;
    protected int id;
    protected Date deliveredOn;
    protected Store store;

    public double calculateSpeedOfSale(){
        Date currentDate = new Date();
        long current = currentDate.getTime();
        long receivedOn = deliveredOn.getTime();
        long timeInStock = current - receivedOn;
        double speedOfSale = (initialStock - stock)/(timeInStock);
        return speedOfSale;
    }
    public int getInitialStock() {
        return initialStock;
    }

    public void setInitialStock(int initialStock) {
        this.initialStock = initialStock;
    }
    
    public Date getDeliveredOn() {
        return deliveredOn;
    }

    public void setDeliveredOn(Date deliveredOn) {
        this.deliveredOn = deliveredOn;
    }
    
    public boolean warnRestock(){
        return true;
    }
    
    public boolean warnRestock(int stockLevel){
       if (stock < stockLevel) {
           return true;
       }
       
       return false;
    }
    
    public boolean sellProduct(int amountToSell){
        stock -= amountToSell;
        return warnRestock(stockLevel);
    }
    
    public void buyProduct(int amountToBuy){
        stock += amountToBuy;
        initialStock = amountToBuy;
    }
    
    public double calcWholeValue(){
        return stock * wholesalePrice;
    }
    
    public double calcRetailValue(){
        return stock * retailPrice;
    }
    
    public double calcPotentialProfit(){
        return stock * (retailPrice - wholesalePrice);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(int stockLevel) {
        this.stockLevel = stockLevel;
    }

    public double getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(double wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
    
    
}
