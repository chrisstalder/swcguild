/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Store {
    protected List<Product> inventory = new ArrayList();
    protected int id;
    protected String name;
    protected Product bestSeller;
    protected Product slowSeller;
    protected String type;

    public Product calculateBestSeller(){
        for (Product product : inventory) {
            if(bestSeller.calculateSpeedOfSale() < product.calculateSpeedOfSale())
                this.bestSeller = product;
        }
        
        return bestSeller;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public List<Product> getInventory() {
        return inventory;
    }

    public void setInventory(List<Product> inventory) {
        this.inventory = inventory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product getBestSeller() {
        return bestSeller;
    }

//    public void setBestSeller(Product bestSeller) {
//        this.bestSeller = bestSeller;
//    }

    public Product getSlowSeller() {
        return slowSeller;
    }

    public void setSlowSeller(Product slowSeller) {
        this.slowSeller = slowSeller;
    }
    
    
}
