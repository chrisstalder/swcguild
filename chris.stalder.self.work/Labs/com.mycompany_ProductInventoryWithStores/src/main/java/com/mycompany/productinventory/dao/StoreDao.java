/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.dao;

import com.mycompany.productinventory.dto.Product;
import com.mycompany.productinventory.dto.Store;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class StoreDao {

    private List<Store> storeList = new ArrayList();
    private int nextId = storeList.size() + 1;

    public StoreDao(){
//        storeList = decode();
    }
    public Store create(Store store) {
        store.setId(nextId);
        nextId++;
        storeList.add(store);
        encode();
        return store;
    }

    public Store get(int id) {
        for (Store store : storeList) {
            if (store.getId() == id) {
                return store;
            }
        }
        return null;
    }

    public void update(Store store) {
        Store found = new Store();
        for (Store s : storeList) {
            if (s.getId() == store.getId()) {
                found = s;
            }
        }

        storeList.remove(found);
        storeList.add(store);
        encode();
    }

    public void delete(Store store) {
        Store found = new Store();
        for (Store s : storeList) {
            if (s.getId() == store.getId()) {
                found = s;
            }
        }

        storeList.remove(found);
        encode();
    }
    
    public List<Store> list(){
        return storeList;
    }

    public void encode() {
        final String TOKEN = "::";
        try {
            for (Store s : storeList) {
                PrintWriter out = new PrintWriter(new FileWriter(s.getName() + ".txt"));
                
                for (Product product : s.getInventory()) {
                    out.print(product.getId());
                    out.print(TOKEN);
                    out.print(product.getName());
                    out.print(TOKEN);
                    out.print(product.getDept());
                    out.print(TOKEN);
                    out.print(product.getRetailPrice());
                    out.print(TOKEN);
                    out.print(product.getWholesalePrice());
                    out.print(TOKEN);
                    out.print(product.getInitialStock());
                    out.print(TOKEN);
                    out.print(product.getStock());
                    out.print(TOKEN);
                    out.print(product.getStockLevel());
                    out.print(TOKEN);
                    out.print(product.getDeliveredOn());
                    out.print(TOKEN);
                    out.print(product.getStore().getName());
                    out.println();
                }

                out.flush();
                out.close();
            }

        } catch (IOException ex) {
            Logger.getLogger(StoreDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Store> decode() {
        File dir = new File(".");
        File[] directoryListing = dir.listFiles();
        
        List<Product> inventory = new ArrayList();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                Store store = new Store();
                try {
                    Scanner sc = new Scanner(new BufferedReader(new FileReader(child.toString())));

                    while (sc.hasNextLine()) {
                        String currentLine = sc.nextLine();
                        String[] stringParts = currentLine.split("::");
                        
                        int id = Integer.parseInt(stringParts[0]);
                        double retailPrice = Double.parseDouble(stringParts[3]);
                        double wholeSale = Double.parseDouble(stringParts[4]);
                        int initialStock = Integer.parseInt(stringParts[5]);
                        int stock = Integer.parseInt(stringParts[6]);
                        int stockLevel = Integer.parseInt(stringParts[7]);
                        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                        Date date = new Date();
                        try {
                            date = format.parse(stringParts[8]);
                        } catch (ParseException ex) {
                            Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                          Product product = null;
                          product.setId(id);
                          product.setName(stringParts[1]);
                          product.setDept(stringParts[2]);
                          product.setRetailPrice(retailPrice);
                          product.setWholesalePrice(wholeSale);
                          product.setInitialStock(initialStock);
                          product.setStock(stock);
                          product.setStockLevel(stockLevel);
                          product.setDeliveredOn(date);
                          
                          inventory.add(product);
                    }
                    
                    store.setId(nextId);
                    nextId++;
                    store.setName(child.getName().replace(".txt", ""));
                    store.setInventory(inventory);
                    store.setType(inventory.get(0).getStore().getType());
                    storeList.add(store);
                    
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(StoreDao.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
            }
        }

        return storeList;

    }
}
