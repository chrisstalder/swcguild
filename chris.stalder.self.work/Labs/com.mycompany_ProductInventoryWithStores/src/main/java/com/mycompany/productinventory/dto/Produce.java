/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.dto;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author apprentice
 */
public class Produce extends Product {

//    protected Date deliveredOn;
    protected int shelfLife;
    protected Date daysToExpire;

//    public Date getDeliveredOn() {
//        return deliveredOn;
//    }
//
//    public void setDeliveredOn(Date deliveredOn) {
//        this.deliveredOn = deliveredOn;
//    }

    public int getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(int shelfLife) {
        this.shelfLife = shelfLife;
    }

    @Override
    public boolean warnRestock() {
        return super.warnRestock(stockLevel);
    }

    public void setdaysToExpire() {
        
        Calendar now = Calendar.getInstance();
        now.setTime(deliveredOn);
        now.add(Calendar.DAY_OF_YEAR, +shelfLife);
        daysToExpire = now.getTime();
    }

    public boolean isExpired() {
        setdaysToExpire();
        if (daysToExpire.before(new Date())) {
            return true;
        }

        return false;
    }

    public boolean sellProduct(int amountToSell) {
        stock -= amountToSell;
        return warnRestock(stockLevel);
    }
    
    public void buyProduct(int amountToBuy){
        stock += amountToBuy;
        deliveredOn = new Date();
    }
}
