/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author apprentice
 */

import java.util.Scanner;
import java.util.Random;

public class Game {
    
    Scanner keyboard = new Scanner(System.in);
    Random r = new Random();
    public void run(){
        boolean playAgain = true;
        boolean isValid = false;
        int numOfRounds = 0;
        System.out.println("Rules: Rock beats scissor, paper beat rock, scissors beat paper");
        System.out.println("Please enter your choice starting with a capital letter.");
        
        while(playAgain){

            int wins = 0;
            int losses = 0; //counters for w/t/l
            int ties = 0;
            isValid = false;
            
        

        //if player inputs a number that is not between 1 and 10, game will not play
           
            while (!isValid){
                
                while (!isValid){            
                    System.out.println("How many rounds would you like to play?");
                    String input = keyboard.nextLine();
                    try{
                        numOfRounds = Integer.parseInt(input);
                        isValid = true;
                    } catch(Exception ex) {}
                }   
                
                if (numOfRounds >= 1 && numOfRounds <= 10){
                    for (int i = 1; i <= numOfRounds; i++){ //while i is <= number of rounds, the player keeps playing

                        String playerChoice = userInput("On shoot. Rock. Paper. Scissors. Shoot!");
                        int computerChoice = 1 +r.nextInt(3);
                        String result = playRockPaperScissors(playerChoice, computerChoice);

                        if (null != result)switch (result) {
                            case "win":
                                wins++;
                                break;
                            case "tie":
                                ties++;
                                break;
                            case "loss":
                                losses++;
                                break;
                            default:
                                break;
                        }
                        
                    }
                    results(wins, losses, ties);
                    isValid = true;
                }else{
                    System.out.println("Please pick a number between 1 and 10"); 
                    isValid = false;
                }
            }
            
            
            String play = userInput("Would you like to play again?");
            playAgain = checkPlayAgain(play);
            
        }
    }

    
    public  boolean checkPlayAgain(String input) {
        boolean playAgain = true;
        boolean isValid = false;
        while (!isValid){
         
            if (("No".equals(input)) || ("NO".equals(input)) || ("n".equals(input)) || ("no".equals(input))){
                playAgain = false;
                isValid = true;
            }else if (("Yes".equals(input)) || ("YES".equals(input)) || ("y".equals(input)) || ("yes".equals(input))){
                playAgain = true;
                isValid = true;
            }else {
                System.out.println("Sorry, I didn't get that. Do you want to play again?");
                input = keyboard.nextLine();
                isValid = false;
            }
        }
        return playAgain;
        
    }

    public void results(int wins, int losses, int ties) {
        System.out.println("You won " + wins + " times! You lost " + losses + " times! And you tied " + ties + " times!");
        
        if (wins > losses){
            System.out.println("You beat the computer! Good job!");
        }else if (wins < losses){
            System.out.println("You lost to the computer! Loser!");
        }else if (wins == losses){
            System.out.println("You tied the computer!");
        }
    }
    
    public int userIntInput(String prompt){
        System.out.println(prompt);
        int input = keyboard.nextInt();
        
        return input;
    }
    
    public String userInput(String prompt){
        System.out.println(prompt);
        String input = keyboard.next();
        
        return input;
    }
    
    public String playRockPaperScissors(String player, int computer){
        boolean isValid = false;
        String result = "";
        while (!isValid){
            switch (player){
                case "Rock":
                    switch (computer){
                        case 1:
                            result = "win";
                            System.out.println("Computer plays scissors! You win!");
                            break;
                        case 2:
                            result = "tie";
                            System.out.println("Computer plays rock! You tie!");
                            break;
                        case 3:
                            result = "loss";
                            System.out.println("Computer plays paper! You lose!");
                            break;
                    }
                    isValid = true;
                    break;
                case "Paper":
                    switch (computer){
                        case 1:
                            result = "win";
                            System.out.println("Computer plays rock! You win!");
                            break;
                        case 2:
                            result = "tie";
                            System.out.println("Computer plays paper! You tie!");
                            break;
                        case 3:
                            result = "loss";
                            System.out.println("Computer plays scissors! You lose!");
                            break;
                    }
                    isValid = true;
                    break;
                case "Scissors":
                    switch (computer){
                        case 1:
                            result = "win";
                            System.out.println("Computer plays paper! You win!");
                        break;
                        case 2:
                            result = "tie";
                            System.out.println("Computer plays scissors! You tie!");
                        break;
                        case 3:
                            result = "loss";
                            System.out.println("Computer plays rock! You lose!");
                        break;
                    }
                    isValid = true;
                    break;
                default:
                    isValid = false;
                    result = "";
                    System.out.println(player + " is not an option!");
                    player = keyboard.nextLine();
                    
            }
        }
        return result;
    }
          
}
