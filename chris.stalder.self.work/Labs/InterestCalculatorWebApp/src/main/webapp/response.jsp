<%-- 
    Document   : response
    Created on : May 26, 2016, 9:18:14 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="interestCalculator.css" rel="stylesheet" type="text/css">
        <title>Interest Calculator</title>
    </head>
    <body>
        <h1>Interest Calculator</h1>
        <table id="resultTable">
            <tr>
                <td>
                    Principal: 
                </td>
                <td>
                    ${principal}<br><br>
                </td>
            </tr>
            <tr>
                <td>
                    Annual rate: 
                </td>
                <td>
                    ${annualInt}<br><br>
                </td>
            </tr>
            <tr>
                <td>
                    Compounded: 
                </td>
                <td>
                    ${compoundedType}<br><br>
                </td>
            </tr>
            <tr>
                <td>
                    Period Interest: 
                </td>
                <td>
                    ${periodInt}<br><br>
                </td>
            </tr>
            <c:forEach items="${years}" var="year">
                <tr>
                    <td colspan="2" id="year">
                        Year ${year.id}<br>
                    </td>
                </tr>
                <tr>
                    <td>
                        Beginning Balance: 
                    </td>
                    <td>
                        ${year.currentBal} <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        Final Balance: 
                    </td>
                    <td>
                        ${year.finalBal}<br>
                    </td>
                </tr>
                <tr>
                    <td>
                        Interest earned: 
                    </td>
                    <td>
                        ${year.totalInt}<br><br>
                    </td>
                </tr>       
            </c:forEach>
        </table>


    </body>
</html>
