/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interestcalculatorwebapp.dto;

/**
 *
 * @author apprentice
 */
public class Year {
    int id;
    float totalInt;
    float finalBal;
    float currentBal;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTotalInt() {
        return totalInt;
    }

    public void setTotalInt(float totalInt) {
        this.totalInt = totalInt;
    }

    public float getFinalBal() {
        return finalBal;
    }

    public void setFinalBal(float finalBal) {
        this.finalBal = finalBal;
    }

    public float getCurrentBal() {
        return currentBal;
    }

    public void setCurrentBal(float currentBal) {
        this.currentBal = currentBal;
    }
    
    
}
