/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interestcalculatorwebapp.controller;

import com.mycompany.interestcalculatorwebapp.dto.Year;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "InterestCalculatorServlet", urlPatterns = {"/InterestCalculatorServlet"})
public class InterestCalculatorServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        float principal = 0f, annualInt = 0f;
        int compoundedPeriod = 0;

        String input = request.getParameter("principal");
        principal = Float.parseFloat(input);

        float currentBal = principal;

        input = request.getParameter("annualInt");

        annualInt = Float.parseFloat(input);

        String compoundedType = request.getParameter("period");
        if ("quarterly".equals(compoundedType)) {
            compoundedPeriod = 4;
        } else if ("monthly".equals(compoundedType)) {
            compoundedPeriod = 12;
        } else if ("daily".equals(compoundedType)) {
            compoundedPeriod = 365;
        }

        float periodInt = periodInt(annualInt, compoundedPeriod);

        int totalYears = Integer.parseInt(request.getParameter("years"));

        List<Year> years = new ArrayList();
        for (int i = 1; i <= totalYears; i++) {
            Year year = new Year();
            float totalInt = 0;
            float finalBal = (currentBal * (1 + (periodInt / 100)));
            float interest = finalBal - currentBal;
            totalInt += interest;
            currentBal = finalBal;
            year.setId(i);
            year.setCurrentBal(currentBal);
            year.setFinalBal(finalBal);
            year.setTotalInt(totalInt);
            years.add(year);
        }
   
        request.setAttribute("principal", principal);
        request.setAttribute("annualInt", annualInt);
        request.setAttribute("compoundedType", compoundedType);
        request.setAttribute("periodInt", periodInt);
        request.setAttribute("years", years);

        RequestDispatcher rd = request.getRequestDispatcher("response.jsp");
        rd.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public float periodInt(float intRate, float numOfPeriods) {
        float periodInt = (intRate / numOfPeriods);
        return periodInt;
    }
}
