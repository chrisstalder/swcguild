/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.luckysevens.controller;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevenController {
    
    public void run(){
         
        Scanner keyboard = new Scanner(System.in);
        boolean isValid = false;
        float bet = 0f;
        while (!isValid){
            
            System.out.println("How much would you like to bet?");
            String input = keyboard.nextLine();
            try{
                bet = Float.parseFloat(input);
                isValid = true;
            } catch(Exception ex) {    
            }
            
        }
        
        int totalRolls = 0;
        int maxRolls = 0;
        float maxMoney = bet;
        
        
        while (bet > 0){

            int total = rollDice();
            totalRolls++;
            
            if (total == 7){
                bet += 4;
                maxMoney = max(bet, maxMoney);
                maxRolls = max(totalRolls, maxRolls);
            }else{
                bet--;
            }
        }
        
        System.out.println("You are broke after " + totalRolls + " rolls.");
        System.out.println("You should have quit after " + maxRolls + " rolls, when you had " + maxMoney);
    }
    
    public int rollDice(){
        Random r = new Random();
        
        int d1 = 1 + r.nextInt(6);
        int d2 = 1 + r.nextInt(6);
        int total = d1 + d2;
        return total;
    }
    
    public float max(float input1, float input2){
        if (input1 > input2){
            input2 = input1;
        }
        
        return input2;
    }
    
    public int max(int input1, int input2){
        if (input1 > input2){
            input2 = input1;
        }
        
        return input2;
    }
}

