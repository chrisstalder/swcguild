/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//for some reason, onclick() would not work in html file so I used addEventListener to to do the job

window.onload = function () {
  document.getElementById("buttonRoll").addEventListener("click", rollDice);
  hideResults();
}

//functions to hide and show results table

function hideResults(){
  var results = document.getElementById("results");
  results.style.display = "none";
}

function showResults(){
  results.style.display = "block"
}

function rollDice()
{

  var startBet = document.getElementById("startBet").value;
  var bet = startBet;
  var maxMoney = bet;
  var totalRollsNum = 0;
  var maxRollsNum = 0;
  var successRolls = 0;

  var display1 = document.getElementById("display1");
  var display2 = document.getElementById("display2");
  var display3 = document.getElementById("display3");
  var display4 = document.getElementById("display4");
  var playButton = document.getElementById("buttonRoll");

  while (bet > 0){
    //randomly generates a number 1 - 6
    d1 = Math.floor(Math.random() * 6) + 1;
    d2 = Math.floor(Math.random() * 6) + 1;
    diceTotal = d1 + d2;
      console.log("d1=" + d1 + " d2=" + d2 + " t=" + diceTotal); //used for debugging
      totalRollsNum++;
    if (diceTotal == 7){
      bet += 4;
      successRolls++;
      if (bet > maxMoney){
        maxMoney = bet;
        maxRollsNum = totalRollsNum;
      }
    }else {
      bet--;
      successRolls = 0;
    }
  }
  display1.innerHTML = "$" + startBet;
  display2.innerHTML = totalRollsNum;
  display3.innerHTML = "$" + maxMoney;
  display4.innerHTML = maxRollsNum;
  playButton.innerHTML = "Play Again";
  showResults();

}