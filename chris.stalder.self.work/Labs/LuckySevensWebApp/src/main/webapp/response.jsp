<%-- 
    Document   : response
    Created on : May 26, 2016, 3:55:47 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Sevens</title>
        <link href="luckySevens.css" rel="stylesheet" type="text/css">
        <!--<script type="text/javascript" src="luckySevens.js"></script>-->
    </head>
    <body>
        <div id="div1">

            <h1>Lucky Sevens</h1>

            <form action="LuckySevensServlet" method="POST">
                Starting Bet: <input type="text" name="bet"/><br>
                <input type="submit" value="Play"/>
            </form>
<!--            Starting Bet:
            <INPUT id="startBet" TYPE="text" NAME="startBet" VALUE="0"><P>
                <button id="buttonRoll">Play</button>-->

        </div>
        <div id="results">
            <table>

                <tr>
                    <th colspan="2">Results</th>
                </tr>

                <tr>
                    <td>Starting Bet</td>
                    <td><div id="display1">${startBet}</div></td>
                </tr>

                <tr>
                    <td>Total Rolls Before Going Broke</td>
                    <td id="display2">${totalRolls}</td>
                </tr>

                <tr>
                    <td>Highest Amount Won</td>
                    <td id="display3">${maxMoney}</td>
                </tr>

                <tr>
                    <td>Roll Count at Highest Amount Won</td>
                    <td id="display4">${maxRolls}</td>
                </tr>

            </table>
        </div>

    </body>
</html>

