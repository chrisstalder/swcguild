/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbookweb;

import com.mycompany.addressbookweb.dao.AddressBookDao;
import com.mycompany.addressbookweb.dto.Address;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/address")
public class AddressController {
    
    private AddressBookDao addressBookDao;
    
    @Inject
    public AddressController(AddressBookDao dao){
        this.addressBookDao = dao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.POST)
    @ResponseBody
    public Address create(@Valid @RequestBody Address address){
        return addressBookDao.create(address);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseBody
    public Address show(@PathVariable("id") Integer addressId){
        
        Address address = addressBookDao.get(addressId);
        
        return address;
    }
    
    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public String edit(@PathVariable("id") Integer addressId, Map model){
        Address address = addressBookDao.get(addressId);
        model.put("address", address);
        return "edit";
    }
    
    @RequestMapping(value="/", method=RequestMethod.PUT)
    @ResponseBody
    public Address editSubmit(@Valid @RequestBody Address address){
        addressBookDao.update(address);
        return address;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseBody
    public Address delete(@PathVariable("id") Integer addressId){
        Address address = addressBookDao.get(addressId);
        addressBookDao.delete(address);
        return address;
    }
}
