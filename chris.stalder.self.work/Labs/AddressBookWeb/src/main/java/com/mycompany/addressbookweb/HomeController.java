/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbookweb;

import com.mycompany.addressbookweb.dao.AddressBookDao;
import com.mycompany.addressbookweb.dto.Address;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {
    
    private AddressBookDao addressBookDao;
    
    @Inject
    public HomeController(AddressBookDao dao){
        this.addressBookDao = dao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(Map model){
        List<Address> addresses = addressBookDao.list();
        
        model.put("addresses", addresses);
        
        return "home";
    }
}
