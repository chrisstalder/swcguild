/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbookweb;

import com.mycompany.addressbookweb.dao.AddressBookDao;
import com.mycompany.addressbookweb.dto.Address;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/search")
public class SearchController {
    
    private AddressBookDao addressBookDao;
    
    @Inject
    public SearchController(AddressBookDao dao){
        this.addressBookDao = dao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(Map model){
        List<Address> addresses = addressBookDao.list();
        
        model.put("addresses", addresses);
        
        return "search";
    }
    
    @RequestMapping(value="/searchByState", method=RequestMethod.GET)
    public String searchByState(@RequestParam("state") String state, Map model){
 

        List<Address> foundByState = addressBookDao.searchByState(state);
        model.put("found", foundByState);
        return "search";
        
    }
    
    @RequestMapping(value="/searchByCity", method=RequestMethod.GET)
    public String searchByCity(@RequestParam("city") String city, Map model){

        List<Address> foundByCity = addressBookDao.searchByCity(city);
        model.put("found", foundByCity);
        return "search";
    }
    
    @RequestMapping(value="/searchByLastName", method=RequestMethod.GET)
    public String searchByLastName(@RequestParam("lastName") String lastName, Map model){

        List<Address> foundByLastName = addressBookDao.searchByLastName(lastName);
        model.put("found", foundByLastName);
        return "search";
    }
    
    @RequestMapping(value="/searchByZip", method=RequestMethod.GET)
    public String searchByZip(@RequestParam("zip") String zipcode, Map model){

        List<Address> foundByZipCode = addressBookDao.searchByZip(zipcode);
        model.put("found", foundByZipCode);
        return "search";
    }

}
