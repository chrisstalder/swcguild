<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book | Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search/">Search</a></li>
                </ul>    
            </div>

            <div class="row">

                <div class="col-md-8">
                    <table id="address-table" class="table table-hover">
                        <tr>
                            <td>First Name</td>
                            <td>Last Name</td>
                            <td>Street #</td>
                            <td>Street Name</td>
                            <td>City</td>
                            <td>State</td>
                            <td>Zip</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                        <c:forEach items="${addresses}" var="address">
                            <tr id="address-row-${address.id}">
                                <td><a data-address-id="${address.id}" data-toggle="modal" data-target="#showAddressModal">${address.firstName}</a></td>
                                <td>${address.lastName}</td>
                                <td>${address.streetNumber}</td>
                                <td>${address.streetName}</td>
                                <td>${address.city}</td>
                                <td>${address.state}</td>
                                <td>${address.zip}</td>
                                <td><a data-address-id="${address.id}" data-toggle="modal" data-target="#editAddressModal">Edit</a></td>
                                <td><a data-address-id="${address.id}" class="delete-link">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>

                <div class="col-md-4">
                    <form method="POST" class="form-horizontal">

                        <div class="form-group">
                            <label for="first-name" class="col-md-4 control-label">First: </label>
                            <div class="col-md-8">
                                <input type="text" id="first-name" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last-name" class="col-md-4 control-label">Last: </label>
                            <div class="col-md-8">
                                <input type="text" id="last-name" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="street-number" class="col-md-4 control-label">Street #: </label>
                            <div class="col-md-8">
                                <input type="text" id="street-number" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="street-name" class="col-md-4 control-label">Street Name: </label>
                            <div class="col-md-8">
                                <input type="text" id="street-name" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="city" class="col-md-4 control-label">City: </label>
                            <div class="col-md-8">
                                <input type="text" id="city" class="form-control"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="state" class="col-md-4 control-label">State: </label>
                            <div class="col-md-8">
                                <input type="text" id="state" class="form-control"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="zip" class="col-md-4 control-label">Zip: </label>
                            <div class="col-md-8">
                                <input type="text" id="zip" class="form-control"/>
                            </div>
                        </div>

                        <input id="create-submit" type="submit" class="btn btn-group-lg center-block"/><br>
                    </form><br>
                    <div id="add-address-validation-errors" class="pull-right"></div>
                </div>

            </div>

        </div>
        <div id="showAddressModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Address Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">

                            <tr>
                                <th>First Name:</th>
                                <td id="address-first-name"></td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td id="address-last-name"></td>
                            </tr>
                            <tr>
                                <th>Street Number:</th>
                                <td id="address-street-number"></td>
                            </tr>
                            <tr>
                                <th>Street Name:</th>
                                <td id="address-street-name"></td>
                            </tr>
                            <tr>
                                <th>City:</th>
                                <td id="address-city"></td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td id="address-state"></td>
                            </tr>
                            <tr>
                                <th>Zip:</th>
                                <td id="address-zip"></td>
                            </tr>

                        </table>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="editAddressModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Address Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <input type="hidden" id="edit-id"/>
                            <tr>
                                <th>First Name:</th>
                                <td>
                                    <input type="text" id="edit-address-first-name"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td>
                                    <input type="text" id="edit-address-last-name"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Street Number:</th>
                                <td>
                                    <input type="text" id="edit-address-street-number"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Street Name:</th>
                                <td>
                                    <input type="text" id="edit-address-street-name"/>
                                </td>
                            </tr>
                            <tr>
                                <th>City:</th>
                                <td>
                                    <input type="text" id="edit-address-city"/>
                                </td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td>
                                    <input type="text" id="edit-address-state"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Zip:</th>
                                <td>
                                    <input type="text" id="edit-address-zip"/>
                                </td>
                            </tr>

                        </table><br>

                        <div id="edit-address-validation-errors" class="pull-right"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="edit-address-button">Save changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/app.js"></script>
    </body>
</html>

