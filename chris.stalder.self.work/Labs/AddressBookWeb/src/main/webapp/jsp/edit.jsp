<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book | Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                 <ul class="nav nav-tabs">
                 <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                </ul>    
            </div>
                
                <div class="row">
                    
                    <div class="col-md-6">
                        <form method="POST" action="./" class="form-horizontal">
                            
                            <input type="hidden" name="id" value="${address.id}"/>
                            
                            <div class="form-group">
                                <label for="first-name" class="col-md-4 control-label">First: </label>
                                <div class="col-md-8">
                                    <input id="first-name" name="firstName" value="${address.firstName}"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="last-name" class="col-md-4 control-label">Last: </label>
                                <div class="col-md-8">
                                    <input id="last-name" name="lastName" value="${address.lastName}"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="street-number" class="col-md-4 control-label">Street Number: </label>
                                <div class="col-md-8">
                                    <input id="street-number" name="streetNumber" value="${address.streetNumber}"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="street-name" class="col-md-4 control-label">Street Name: </label>
                                <div class="col-md-8">
                                    <input id="street-name" name="streetName" value="${address.streetName}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="city" class="col-md-4 control-label">City: </label>
                                <div class="col-md-8">
                                    <input id="city" name="city" value="${address.city}"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="state" class="col-md-4 control-label">State: </label>
                                <div class="col-md-8">
                                    <input id="state" name="state" value="${address.state}"/>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label for="zip" class="col-md-4 control-label">Zip: </label>
                                <div class="col-md-8">
                                    <input id="zip" name="zip" value="${address.zip}"/>
                                </div>
                            </div>
                            
                            <input type="submit" class="btn btn-group-lg center-block"/>
                        </form>
                        
                    </div>
                    
                </div>
            
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

