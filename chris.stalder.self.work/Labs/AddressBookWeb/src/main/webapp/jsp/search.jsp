<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book | Search</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                </ul>    
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="navbar">
                        <ul id="searchTabs" class="nav nav-tabs">
                            <li class="active"><a href="#searchByState">By State</a></li>
                            <li><a href="#searchByCity">By City</a></li>
                            <li><a href="#searchByLastName">By Last Name</a></li>
                            <li><a href="#searchByZip">By Zip</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--tab-content-->     
        <section id="searchByState" class="tab-content hide">
            <form method="GET" action="./searchByState">
                State: <input type="text" name="state"/>
                <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
            </form>
        </section>

        <section id="searchByCity" class="tab-content hide">
            <form method="GET" action="./searchByCity">
                City: <input type="text" name="city"/>
                <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
            </form>
        </section>

        <section id="searchByLastName" class="tab-content hide">
            <form method="GET" action="./searchByLastName">
                Last Name: <input type="text" name="lastName"/>
                <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
            </form>
        </section>
        <section id="searchByZip" class="tab-content hide">
            <form method="GET" action="./searchByZip">
                Zip: <input type="text" name="zip"/>
                <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
            </form>
        </section>
        <div class="container">
            <div class="col-md-12">
                <table class="table table-hover">
                    <tr>
                        <td>First Name</td>
                        <td>Last Name</td>
                        <td>Street #</td>
                        <td>Street Name</td>
                        <td>City</td>
                        <td>State</td>
                        <td>Zip</td> 
                    </tr>
                    <c:forEach items="${found}" var="found">
                        <tr>
                            <td>${found.firstName}</td>
                            <td>${found.lastName}</td>
                            <td>${found.streetNumber}</td>
                            <td>${found.streetName}</td>
                            <td>${found.city}</td>
                            <td>${found.state}</td>
                            <td>${found.zip}</td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>


        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/custom.js"></script>
    </body>
</html>

