/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryweb.dto;

import java.util.Date;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author apprentice
 */
public class Dvd {
    private int id;
    
    @NotEmpty(message="You must enter a title")
    private String title;
    
    @DateTimeFormat(pattern="MM/dd/yyyy")
    private Date releaseDate;
    
    @NotEmpty(message="You must enter a rating")
    private String rating;
    
    @NotEmpty(message="You must enter a director")
    private String director;
    
    @NotEmpty(message="You must enter a studio")
    private String studio;
    
    @NotEmpty(message="You must enter a note")
    private String note;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public String getRating() {
        return rating;
    }

    public String getDirector() {
        return director;
    }

    public String getStudio() {
        return studio;
    }

    public String getNote() {
        return note;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
}
