/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryweb.dto;

/**
 *
 * @author apprentice
 */
public class Note {
    
    private int id;
    private int dvd_title;
    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDvd_title() {
        return dvd_title;
    }

    public void setDvd_title(int dvd_title) {
        this.dvd_title = dvd_title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    
}
