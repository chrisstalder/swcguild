/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryweb.controller;

import com.mycompany.dvdlibraryweb.dao.DvdDao;
import com.mycompany.dvdlibraryweb.dao.NoteDao;
import com.mycompany.dvdlibraryweb.dto.Dvd;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/search")
public class SearchController {

    private DvdDao dvdDao;
    private NoteDao noteDao;

    @Inject
    public SearchController(DvdDao dao, NoteDao noteDao) {
        this.dvdDao = dao;
        this.noteDao = noteDao;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String search(Map model) {

        List<Dvd> dvds = dvdDao.list();
        model.put("dvds", dvds);
        return "search";
    }

    @RequestMapping(value = "/listByStudio", method = RequestMethod.GET)
    public String listByStudio(@RequestParam("studio") String studio, Map model) {
        List<Dvd> foundByStudio = dvdDao.listDvdByStudio(studio);
        model.put("found", foundByStudio);
        return "search";
    }

    @RequestMapping(value = "/listByDirector", method = RequestMethod.GET)
    public String listByDirector(@RequestParam("director") String director, Map model) {
        List<Dvd> foundByDirector = dvdDao.listDvdByDirector(director);
        model.put("found", foundByDirector);
        return "search";
    }

    @RequestMapping(value = "/listByRating", method = RequestMethod.GET)
    public String listByRating(@RequestParam("rating") String rating, Map model) {
        List<Dvd> foundByRating = dvdDao.listDvdByRating(rating);
        model.put("found", foundByRating);
        return "search";
    }

    @RequestMapping(value = "/findNewest", method = RequestMethod.GET)
    public String findNewest(Map model) {
        List<Dvd> newest = dvdDao.findNewestMovie();
        model.put("found", newest);
        return "search";
    }

    @RequestMapping(value = "/findOldest", method = RequestMethod.GET)
    public String listByStudio(Map model) {
        List<Dvd> oldest = dvdDao.findOldestMovie();
        model.put("found", oldest);
        return "search";
    }

    @RequestMapping(value = "/listByNYears", method = RequestMethod.GET)
    public String listByNYears(@RequestParam("nYears") String nYears, Map model) {
        int n = Integer.parseInt(nYears);
        Calendar now = Calendar.getInstance();
        now.add(Calendar.YEAR, -n);                 //subtracting n years from current time
        int year = now.get(Calendar.YEAR);      
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);
        now.set(year, month, day);                  //setting now to current year month day. Not sure if this step is needed
        Date endOfRange = now.getTime();            //creating Date object from Calendar object

        List<Dvd> foundByAge = dvdDao.findAllMoviesReleasedInNYears(endOfRange);

        model.put("found", foundByAge);
        return "search";
    }

    @RequestMapping(value = "/avgNotes", method = RequestMethod.GET)
    public String avgNotes(Map model) {
        int avgNotes = noteDao.findAvgNumOfNotes();
        model.put("found", avgNotes);
        return "search";
    }

}
