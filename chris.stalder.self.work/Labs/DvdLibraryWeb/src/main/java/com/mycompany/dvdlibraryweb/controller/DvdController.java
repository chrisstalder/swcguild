/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryweb.controller;

import com.mycompany.dvdlibraryweb.dao.DvdDao;
import com.mycompany.dvdlibraryweb.dto.Dvd;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/dvd")
public class DvdController {
    
    private DvdDao dvdDao;
    
    @Inject
    public DvdController(DvdDao dao){
        this.dvdDao = dao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.POST)
    @ResponseBody
    public Dvd create(@Valid @RequestBody Dvd dvd){
        return dvdDao.create(dvd);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseBody
    public Dvd delete(@PathVariable("id") Integer dvdId){
        Dvd dvd = dvdDao.get(dvdId);
        dvdDao.delete(dvd);
        
        return dvd;
    }
    
    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public String edit(@PathVariable("id") Integer dvdId, Map model){
        
        Dvd dvd = dvdDao.get(dvdId);
        model.put("dvd", dvd);
        return "edit";
    }
    
    @RequestMapping(value="/", method=RequestMethod.PUT)
    @ResponseBody
    public Dvd editSubmit(@Valid @RequestBody Dvd dvd){
        
        dvdDao.update(dvd);
        return dvd;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseBody
    public Dvd show(@PathVariable("id") Integer dvdId){
        Dvd dvd = dvdDao.get(dvdId);

        return dvd;
    }
    
}
