/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryweb.dao;

import com.mycompany.dvdlibraryweb.dto.Dvd;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DvdDao {
    
    public Dvd create(Dvd dvd);
    public Dvd get(int id);
    public void update(Dvd dvd);
    public void delete(Dvd dvd);
    public List<Dvd> list();
    public List<Dvd> listDvdByStudio(String studio);
    public List<Dvd> listDvdByRating(String rating);
    public List<Dvd> listDvdByDirector(String director);
    public List<Dvd> findNewestMovie();
    public List<Dvd> findOldestMovie();
    public List<Dvd> findAllMoviesReleasedInNYears(Date n);
}
