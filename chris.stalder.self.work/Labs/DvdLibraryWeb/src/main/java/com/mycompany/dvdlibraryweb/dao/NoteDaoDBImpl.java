/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryweb.dao;

import com.mycompany.dvdlibraryweb.dto.Note;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class NoteDaoDBImpl implements NoteDao {

    private static final String SQL_INSERT_NOTE = "INSERT INTO notes (title_id, note) VALUES (?, ?)";
    private static final String SQL_UPDATE_NOTE = "UPDATE notes SET title_id = ?, note = ? WHERE id = ?";
    private static final String SQL_DELETE_NOTE = "DELETE notes WHERE id = ?";
    private static final String SQL_SELECT_NOTE = "SELECT * FROM notes WHERE id = ?";
    private static final String SQL_GET_NOTE_LIST = "SELECT * FROM notes";
    private JdbcTemplate jdbcTemplate;

    public NoteDaoDBImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Note create(Note note) {
        
        jdbcTemplate.update(SQL_INSERT_NOTE,
                note.getDvd_title(),
                note.getText());
        
        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        
        note.setId(id);
        
        return note;
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Note get(int id) {
        
        return jdbcTemplate.queryForObject(SQL_SELECT_NOTE, new NoteMapper(), id);
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(Note note) {
        
        jdbcTemplate.update(SQL_UPDATE_NOTE,
                note.getDvd_title(),
                note.getText(),
                note.getId());
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Note note) {
        
        jdbcTemplate.update(SQL_DELETE_NOTE, note.getId());
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Note> list() {
        return jdbcTemplate.query(SQL_GET_NOTE_LIST, new NoteMapper());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int findAvgNumOfNotes() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final class NoteMapper implements RowMapper<Note> {

        @Override
        public Note mapRow(ResultSet rs, int i) throws SQLException {
            Note note = new Note();

            note.setId(rs.getInt("id"));
            note.setDvd_title(rs.getInt("title_id"));
            note.setText(rs.getString("note"));

            return note;
        }

    }
}
