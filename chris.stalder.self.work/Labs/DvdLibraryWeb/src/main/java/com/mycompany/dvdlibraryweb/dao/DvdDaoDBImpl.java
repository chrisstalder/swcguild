/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryweb.dao;

import com.mycompany.dvdlibraryweb.dto.Dvd;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class DvdDaoDBImpl implements DvdDao {

    private static final String SQL_INSERT_DVD = "INSERT INTO dvd_library (title, director, rating, studio, release_date) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_DVD = "UPDATE dvd_library SET title = ?, director = ?, rating = ?, studio = ?, release_date = ?, WHERE id = ?";
    private static final String SQL_DELETE_DVD = "DELETE FROM dvd_library WHERE id = ?";
    private static final String SQL_SELECT_DVD = "SELECT * FROM dvd_library WHERE id = ?";
    private static final String SQL_SELECT_DVD_BY_STUDIO = "SELECT * FROM dvd_library WHERE studio = ?";
    private static final String SQL_SELECT_DVD_BY_RATING = "SELECT * FROM dvd_library WHERE rating = ?";
    private static final String SQL_SELECT_DVD_BY_DIRECTOR = "SELECT * FROM dvd_library WHERE director = ?";
    private static final String SQL_SELECT_DVD_BY_YEAR = "SELECT * FROM dvd_library WHERE release_date > ?";
    private static final String SQL_SELECT_NEWEST = "SELECT * FROM dvd_library WHERE (release_date) in (SELECT MAX(release_date) as release_date from dvd_library)";
    private static final String SQL_SELECT_OLDEST = "SELECT * FROM dvd_library WHERE (release_date) in (SELECT MIN(release_date) as release_date from dvd_library)";
    private static final String SQL_GET_DVD_LIST = "SELECT * FROM dvd_library";

    private JdbcTemplate jdbcTemplate;

    public DvdDaoDBImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Dvd create(Dvd dvd) {
        
        jdbcTemplate.update(SQL_INSERT_DVD,
                dvd.getTitle(),
                dvd.getDirector(),
                dvd.getRating(),
                dvd.getStudio(),
                dvd.getReleaseDate());
        
        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        
        dvd.setId(id);
        
        return dvd;
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Dvd get(int id) {
        
        return jdbcTemplate.queryForObject(SQL_SELECT_DVD, new DvdMapper(), id);
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(Dvd dvd) {

        jdbcTemplate.update(SQL_UPDATE_DVD,
                dvd.getTitle(),
                dvd.getDirector(),
                dvd.getRating(),
                dvd.getStudio(),
                dvd.getReleaseDate(),
                dvd.getId());
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Dvd dvd) {
        
        jdbcTemplate.update(SQL_DELETE_DVD, dvd.getId());
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Dvd> list() {
        
        return jdbcTemplate.query(SQL_GET_DVD_LIST, new DvdMapper());
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Dvd> listDvdByStudio(String studio) {
        return jdbcTemplate.query(SQL_SELECT_DVD_BY_STUDIO, new DvdMapper(), studio);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Dvd> listDvdByRating(String rating) {
        return jdbcTemplate.query(SQL_SELECT_DVD_BY_RATING, new DvdMapper(), rating);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Dvd> listDvdByDirector(String director) {
        return jdbcTemplate.query(SQL_SELECT_DVD_BY_DIRECTOR, new DvdMapper(), director);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Dvd> findNewestMovie() {
        return jdbcTemplate.query(SQL_SELECT_NEWEST, new DvdMapper());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Dvd> findOldestMovie() {
        return jdbcTemplate.query(SQL_SELECT_OLDEST, new DvdMapper());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Dvd> findAllMoviesReleasedInNYears(Date n) {
        return jdbcTemplate.query(SQL_SELECT_DVD_BY_YEAR, new DvdMapper(), n);
    }

    private static final class DvdMapper implements RowMapper<Dvd> {

        @Override
        public Dvd mapRow(ResultSet rs, int i) throws SQLException {

            Dvd dvd = new Dvd();
            
            dvd.setId(rs.getInt("id"));
            dvd.setTitle(rs.getString("title"));
            dvd.setDirector(rs.getString("director"));
            dvd.setRating(rs.getString("rating"));
            dvd.setStudio(rs.getString("studio"));
            dvd.setReleaseDate(rs.getDate("release_date"));
            
            return dvd;
        }

    }
}
