/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryweb.dao;

import com.mycompany.dvdlibraryweb.dto.Dvd;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoImpl implements DvdDao {

    private List<Dvd> dvds = new ArrayList();

    private int nextId = dvds.size() + 1;

    public DvdLibraryDaoImpl() {
        dvds = decode();
    }

    @Override
    public Dvd create(Dvd dvd) {
        dvd.setId(nextId++);
        dvds.add(dvd);
        encode();
        return dvd;
    }

    @Override
    public Dvd get(int id) {

//        for (Dvd dvd : dvds) {
//            if(dvd.getId()==id)
//                return dvd;
//        }
//        return null;
        if (dvds.isEmpty()) {
            return null;
        } else {
            try {

                Dvd dvd = dvds.stream()
                        .filter(a -> a.getId() == id)
                        .findFirst()
                        .get();

                //.filter(Contact::getId == id)
                //.collect(Collectors.toList())
                //.get(0);
                //.
                return dvd;
            } catch (NoSuchElementException noSuchElementException) {

                return null;
            }
        }
    }

    @Override
    public void update(Dvd dvd) {
//        Dvd found = null;
//        for (Dvd dvd1 : dvds) {
//            if (dvd1.getId() == dvd.getId()){
//                found = dvd1;
//            }
//        }
//        dvds.remove(found);
//        dvds.add(dvd);
        this.delete(dvd);
        dvds.add(dvd);
        encode();
    }

    @Override
    public void delete(Dvd dvd) {

//        Dvd found = null;
//        
//        for (Dvd myDvd : dvds) {
//            if(myDvd.getId() == dvd.getId())
//                found = myDvd;
//        }
        dvds.remove(get(dvd.getId()));
        encode();

    }

    @Override
    public List<Dvd> list() {
        return new java.util.ArrayList(dvds);
    }

    
    public void encode() {

        final String TOKEN = "::";

        try {
            PrintWriter out = new PrintWriter(new FileWriter("dvdlibrary.txt"));
            DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
            for (Dvd dvd : dvds) {
                out.print(dvd.getId());
                out.print(TOKEN);
                out.print(dvd.getTitle());
                out.print(TOKEN);
                if (dvd.getReleaseDate() != null) {
                    out.print(format.format(dvd.getReleaseDate()));
                }

                out.print(TOKEN);
                out.print(dvd.getRating());
                out.print(TOKEN);
                out.print(dvd.getDirector());
                out.print(TOKEN);
                out.print(dvd.getStudio());
                out.print(TOKEN);
                out.print(dvd.getNote());
                out.println();
            }

            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(DvdLibraryDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    
    public List<Dvd> decode() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("dvdlibrary.txt")));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split("::");

                Dvd dvd = new Dvd();

                int id = Integer.parseInt(stringParts[0]);

                String dateInString = stringParts[2];
                DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");

                Date date = new Date();

                try {
                    date = format.parse(dateInString);
                } catch (ParseException ex) {
                    Logger.getLogger(DvdLibraryDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
                }

                dvd.setId(id);
                dvd.setTitle(stringParts[1]);
                dvd.setReleaseDate(date);
                dvd.setRating(stringParts[3]);
                dvd.setDirector(stringParts[4]);
                dvd.setStudio(stringParts[5]);
                dvd.setNote(stringParts[6]);

                dvds.add(dvd);
                nextId = 1 + dvds.size();

            }

            sc.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DvdLibraryDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dvds;
    }

    @Override
    public List<Dvd> listDvdByStudio(String studio) {
        List<Dvd> foundByStudio = new ArrayList();
        for (Dvd dvd : dvds) {
            if (dvd.getStudio().equals(studio)) {
                foundByStudio.add(dvd);
            }
        }

        return foundByStudio;
    }

    @Override
    public List<Dvd> listDvdByDirector(String director) {
//        Map<String, List<Dvd>> foundByDirectorAndRatingMap = new HashMap();
        List<Dvd> foundByDirector = new ArrayList();
        List<String> listOfRatings = new ArrayList();

        for (Dvd dvd : dvds) {
            if (director.equals(dvd.getDirector())) {
                foundByDirector.add(dvd);
                listOfRatings.add(dvd.getRating());
            }
        }

//        for (String rating : listOfRatings) {
//            List<Dvd> moviesByRating = new ArrayList();         //declaring new list for each rating
//            for (Dvd dvd : foundByDirector) {
//                if(rating.equals(dvd.getRating()))
//                    moviesByRating.add(dvd);
//            }
//            foundByDirectorAndRatingMap.put(rating, moviesByRating);
//        }
        return foundByDirector;
    }

    @Override
    public List<Dvd> findNewestMovie() {
        List<Dvd> newestDvdList = new ArrayList();
        Dvd newestDvd = new Dvd();
        Date date = new Date(0);
        newestDvd.setReleaseDate(date);
        for (Dvd dvd : dvds) {
            if (dvd.getReleaseDate().after(newestDvd.getReleaseDate())) {
                newestDvd = dvd;
            }
        }
        newestDvdList.add(newestDvd);
        return newestDvdList;
    }

    @Override
    public List<Dvd> findOldestMovie() {
        List<Dvd> oldestDvdList = new ArrayList();
        Dvd oldestDvd = new Dvd();
        Date date = new Date();
        oldestDvd.setReleaseDate(date);
        for (Dvd dvd : dvds) {
            if (dvd.getReleaseDate().before(oldestDvd.getReleaseDate())) {
                oldestDvd = dvd;
            }
        }
        oldestDvdList.add(oldestDvd);
        return oldestDvdList;
    }

    
    public int findAvgNumOfNotes() {
        int numOfNotes = 0;
        for (Dvd dvd : dvds) {
            if (!"".equals(dvd.getNote())) {
                numOfNotes++;
            }
        }
        int avgNumOfNotes = (numOfNotes / dvds.size());
        return avgNumOfNotes;
    }

    @Override
    public List<Dvd> findAllMoviesReleasedInNYears(Date n) {
        List<Dvd> foundByAge = new ArrayList();

        for (Dvd dvd : dvds) {
            if (n.before(dvd.getReleaseDate())) {
                foundByAge.add(dvd);
            }
        }
        return foundByAge;
    }

    @Override
    public List<Dvd> listDvdByRating(String rating) {
        List<Dvd> foundByRating = new ArrayList();
        for (Dvd dvd : dvds) {
            if (rating.equals(dvd.getRating())) {
                foundByRating.add(dvd);
            }
        }

        return foundByRating;
    }

}
