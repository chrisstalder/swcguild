/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $('#create-submit').on('click', function (e) {
//        alert('hey listen');
        e.preventDefault();
        $('#add-dvd-validation-errors').empty();
        var dvdData = JSON.stringify({
            title: $('#title-input').val(),
            releaseDate: $('#release-date-input').val(),
            rating: $('#rating-input').val(),
            director: $('#director-input').val(),
            studio: $('#studio-input').val(),
            note: $('#note-input').val()
        });

        $.ajax({
            url: contextRoot + "/dvd/",
            type: "POST",
            data: dvdData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                console.log(data);
                var tableRow = buildDvdRow(data);
                $('#dvd-table').append($(tableRow));

                title: $('#title-input').val('');
                releaseDate: $('#release-date-input').val('');
                rating: $('#rating-input').val('');
                director: $('#director-input').val('');
                studio: $('#studio-input').val('');
                note: $('#note-input').val('');

            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;
                
                $.each(errors, function(index, error) {
                    
                    $('#add-dvd-validation-errors').append(error.message + "<br />");
                
                });
            }
        });

    });
    
    $('#showDvdModal').on('show.bs.modal', function (e){
        
        var link = $(e.relatedTarget);
        
        var dvdId = link.data('dvd-id');
        
        $.ajax({
            
            url: contextRoot + "/dvd/" + dvdId,
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status){
                $('#dvd-title').text(data.title);
                $('#dvd-release-date').text(data.releaseDate);
                $('#dvd-rating').text(data.rating);
                $('#dvd-director').text(data.director);
                $('#dvd-studio').text(data.studio);
                $('#dvd-notes').text(data.notes);
            },error: function(data, status){
                alert('error')
            }
        });
        
    });
    
    $('#editDvdModal').on('show.bs.modal', function (e){
        var link = $(e.relatedTarget);
        
        var dvdId = link.data('dvd-id');
        
        $.ajax({
            
            url: contextRoot + "/dvd/" + dvdId,
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status){
                console.log(data);
                $('#edit-dvd-title').val(data.title);
                $('#edit-dvd-release-date').val(data.releaseDate);
                $('#edit-dvd-rating').val(data.rating);
                $('#edit-dvd-director').val(data.director);
                $('#edit-dvd-studio').val(data.studio);
                $('#edit-dvd-notes').val(data.note);
                $('#edit-id').val(data.id);
            },error: function(data, status){
                alert('error');
            }
        });
        
    });
    
    $('#edit-dvd-button').on('click', function(e){
        $('#edit-dvd-validation-errors').empty();
        var dvdData = JSON.stringify({
            title: $('#edit-dvd-title').val(),
            releaseDate: $('#edit-dvd-release-date').val(),
            rating: $('#edit-dvd-rating').val(),
            director: $('#edit-dvd-director').val(),
            studio: $('#edit-dvd-studio').val(),
            note: $('#edit-dvd-notes').val(),
            id: $('#edit-id').val()
        });
        
        $.ajax({
            url: contextRoot + "/dvd/",
            type: "PUT",
            data: dvdData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                
                $('#editDvdModal').modal('hide');
                
                var tableRow = buildDvdRow(data);
                
                $('#dvd-row-'+ data.id).replaceWith($(tableRow));
            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;
                
                $.each(errors, function(index, error) {
                    
                    $('#edit-dvd-validation-errors').append(error.message + "<br />");
                
                });
            }

        });
        
    });

    function buildDvdRow(data) {

        return "<tr id='dvd-row-" + data.id + "'>  \n\
                <td><a data-dvd-id='" + data.id + " data-toggle='modal' data-target='#showDvdModal'>" + data.title + "</a></td>  \n\
                <td> " + data.releaseDate + "</td><td> " + data.rating + "</td><td> " + data.director + "</td><td> " + data.studio + "</td>    \n\
                <td> " + data.note + "</td>  \n\
                <td> <a data-dvd-id='" + data.id + "' data-toggle='modal' data-target='#editDvdModal'>Edit</a>  </td>   \n\
                <td> <a data-dvd-id='" + data.id + "' class='delete-link'>Delete</a>  </td>   \n\
                </tr>  ";

    }

});

$(document).on('click', '.delete-link', function(e){
    
    e.preventDefault();
    
    var dvdId = $(e.target).data('dvd-id');
    
    $.ajax({
        
        type: 'DELETE',
        url: contextRoot + "/dvd/" + dvdId,
        success: function(data,status){
            $('#dvd-row-'+ dvdId).remove();
        },
        error: function(data,status){
            
        }
    });
    
});