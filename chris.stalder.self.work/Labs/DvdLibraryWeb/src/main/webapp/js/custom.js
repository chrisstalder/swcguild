/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
                $('#searchTabs > li > a').click(function (event) {
                    event.preventDefault();//stop browser to take action for clicked anchor

                    //get displaying tab content jQuery selector
                    var active_tab_selector = $('#searchTabs > li.active > a').attr('href');

                    //find actived navigation and remove 'active' css
                    var actived_nav = $('#searchTabs > li.active');
                    actived_nav.removeClass('active');

                    //add 'active' css into clicked navigation
                    $(this).parents('li').addClass('active');

                    //hide displaying tab content
                    $(active_tab_selector).removeClass('active');
                    $(active_tab_selector).addClass('hide');

                    //show target tab content
                    var target_tab_selector = $(this).attr('href');
                    $(target_tab_selector).removeClass('hide');
                    $(target_tab_selector).addClass('active');
                });
            });