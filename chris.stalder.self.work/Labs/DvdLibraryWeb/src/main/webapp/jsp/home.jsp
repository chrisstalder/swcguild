<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library | Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search/">Search</a></li>
                </ul>    
            </div>

            <div class="row">
                <div class="col-md-8">
                    <table class="table table-hover table-bordered" id="dvd-table">
                        <tr>
                            <td>Title</td>
                            <td>Release Date</td>
                            <td>Rating</td>
                            <td>Director</td>
                            <td>Studio</td>
                            <td>Note</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                        <c:forEach items="${dvds}" var="dvd">
                            <tr id="dvd-row-${dvd.id}">
                                <td><a data-dvd-id="${dvd.id}" data-toggle="modal" data-target="#showDvdModal">${dvd.title}</a></td>
                                <td>${dvd.releaseDate}</td>
                                <td>${dvd.rating}</td>
                                <td>${dvd.director}</td>
                                <td>${dvd.studio}</td>
                                <td>${dvd.note}</td>
                                <td><a data-dvd-id="${dvd.id}" data-toggle="modal" data-target="#editDvdModal">Edit</a></td>
                                <td><a data-dvd-id="${dvd.id}" class="delete-link">Delete</a></td>
                            </tr>

                        </c:forEach>
                    </table>
                </div>
                <div class="col-md-4">
                    <form method="POST" class="form-horizontal">

                        <div class="form-group">
                            <label for="title" class="col-md-4 control-label">Title: </label>
                            <div class="col-md-8">
                                <input type="text" id="title-input" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="release-date" class="col-md-4 control-label">Release Date: </label>
                            <div class="col-md-8">
                                <input type="text" id="release-date-input" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="rating" class="col-md-4 control-label">Rating: </label>
                            <div class="col-md-8">
                                <input type="text" id="rating-input" class="form-control"/>
<!--                                <label class="radio-inline"><input type="radio" name="rating" value="G" id="rating">G</label>
                                <label class="radio-inline"><input type="radio" name="rating" value="PG" id="rating">PG</label>
                                <label class="radio-inline"><input type="radio" name="rating" value="PG-13" id="rating">PG-13</label>
                                <label class="radio-inline"><input type="radio" name="rating" value="R" id="rating">R</label>
                                <label class="radio-inline"><input type="radio" name="rating" value="NC-17" id="rating">NC-17</label>-->
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="director" class="col-md-4 control-label">Director: </label>
                            <div class="col-md-8">
                                <input type="text" id="director-input" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="studio" class="col-md-4 control-label">Studio </label>
                            <div class="col-md-8">
                                <input type="text" id="studio-input" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="note" class="col-md-4 control-label">Note: </label>
                            <div class="col-md-8">
                                <input type="text" id="note-input" class="form-control"/>
                                <!--<textarea class="form-control" rows="5" id="note" name="note"></textarea>-->
                            </div>
                        </div>
                        <input id="create-submit" type="submit" class="btn btn-primary center-block" value="Submit"/>
                    </form><br>
                    <div id="add-dvd-validation-errors" class="pull-right"></div>
                </div>
            </div>

        </div>

        <div id="showDvdModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Dvd Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered" id="show-dvd-table">

                            <tr>
                                <th>Title:</th>
                                <td id="dvd-title"></td>
                            </tr>
                            <tr>
                                <th>Release Date:</th>
                                <td id="dvd-release-date"></td>
                            </tr>
                            <tr>
                                <th>Rating:</th>
                                <td id="dvd-rating"></td>
                            </tr>
                            <tr>
                                <th>Director:</th>
                                <td id="dvd-director"></td>
                            </tr>
                            <tr>
                                <th>Studio:</th>
                                <td id="dvd-studio"></td>
                            </tr>
                            <tr>
                                <th>Notes:</th>
                                <td id="dvd-notes"></td>
                            </tr>

                        </table>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        <div id="editDvdModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Dvd Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <input type="hidden" id="edit-id"/>
                            <tr>
                                <th>Title:</th>
                                <td>
                                    <input type="text" id="edit-dvd-title"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Release Date:</th>
                                <td>
                                    <input type="text" id="edit-dvd-release-date"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Rating:</th>
                                <td>
                                    <input type="text" id="edit-dvd-rating"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Director:</th>
                                <td>
                                    <input type="text" id="edit-dvd-director"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Studio:</th>
                                <td>
                                    <input type="text" id="edit-dvd-studio"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Notes:</th>
                                <td>
                                    <input type="text" id="edit-dvd-notes"/>
                                </td>
                            </tr>

                        </table>

                        <div id="edit-dvd-validation-errors" class="pull-right"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success" data-primary="modal" id="edit-dvd-button">Save Changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/dvdApp.js"></script>

    </body>
</html>

