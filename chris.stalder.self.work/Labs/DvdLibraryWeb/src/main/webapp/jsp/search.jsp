<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library | Search</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                </ul>    
            </div>

            <div class="row">

                <div class="col-md-10">
                    <div class="navbar">
                        <ul id="searchTabs" class="nav nav-tabs">
                            <li><a href="#listByStudio">By Studio</a></li>
                            <li><a href="#listByDirector">By Director</a></li>
                            <li><a href="#listByRating">By Rating</a></li>
                            <li><a href="#findNewest">Newest</a></li>
                            <li><a href="#findOldest">Oldest</a></li>
                            <li><a href="#listByNYears">By Age</a></li>
<!--                            <li><a href="#avgNotes">Average # of Notes</a></li>-->
                        </ul>
                    </div>
                </div>               
            </div>

            <section id="listByStudio" class="tab-content hide">
                <form method="GET" action="./listByStudio">
                    Studio: <input type="text" name="studio"/>
                    <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
                </form>
            </section>

            <section id="listByDirector" class="tab-content hide">
                <form method="GET" action="./listByDirector">
                    Director: <input type="text" name="director"/>
                    <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
                </form>
            </section>

            <section id="listByRating" class="tab-content hide">
                <form method="GET" action="./listByRating">
                    Rating: <input type="text" name="rating"/>
                    <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
                </form>
            </section>

            <section id="findNewest" class="tab-content hide">
                <form method="GET" action="./findNewest">
                    <!--                        Studio: <input type="text" name="newest"/>-->
                    <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
                </form>
            </section>

            <section id="findOldest" class="tab-content hide">
                <form method="GET" action="./findOldest">
                    <!--                        Studio: <input type="text" name="studio"/>-->
                    <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
                </form>
            </section>

            <section id="listByNYears" class="tab-content hide">
                <form method="GET" action="./listByNYears">
                    Find all movies within how many years: <input type="text" name="nYears"/>
                    <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
                </form>
            </section>

<!--            <section id="avgNotes" class="tab-content hide">
                <form method="GET" action="./avgNotes">
                    Studio: <input type="text" name="studio"/>
                    <input type="submit" class="btn btn-primary" value="Submit"/><br><br>
                </form>
            </section>-->

            <div class="row">
                <div class="col-md-10">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <td>Title</td>
                            <td>Release Date</td>
                            <td>Rating</td>
                            <td>Director</td>
                            <td>Studio</td>
                            <td>Note</td>
                        </tr>
                        <c:forEach items="${found}" var="dvd">
                            <tr>
                                <td>${dvd.title}</td>
                                <td>${dvd.releaseDate}</td>
                                <td>${dvd.rating}</td>
                                <td>${dvd.director}</td>
                                <td>${dvd.studio}</td>
                                <td>${dvd.note}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/custom.js"></script>
    </body>


