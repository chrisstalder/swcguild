<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library | Show</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                </ul>    
            </div>

            <div class="row">

                <div class="col-md-6">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <td>Title:</td>
                            <td>${dvd.title}</td>
                        </tr>
                        <tr>
                            <td>Release Date: </td>
                            <td>${dvd.releaseDate}</td>
                        </tr>
                        <tr>
                            <td>Rating: </td>
                            <td>${dvd.rating}</td>
                        </tr>
                        <tr>
                            <td>Director: </td>
                            <td>${dvd.director}</td>
                        </tr>
                        <tr>
                            <td>Studio </td>
                            <td>${dvd.studio}</td>
                        </tr>
                        <tr>
                            <td>Note </td>
                            <td>${dvd.note}</td>
                        </tr>
                    </table>
                </div>               
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
    <!--    <body>
            <div class="container">
                <h1>DVD Library</h1>
                <hr/>
                <div class="navbar">
                     <ul class="nav nav-tabs">
                     <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    </ul>    
                </div>
                    
                    <div class="row">
                        <div class="col-md-8">
                            <table class="table table-hover table-bordered">
                                <tr>
                                    <td>Title</td>
                                    <td>Release Date</td>
                                    <td>Rating</td>
                                    <td>Director</td>
                                    <td>Studio</td>
                                    <td>Note</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>
  

</html>

