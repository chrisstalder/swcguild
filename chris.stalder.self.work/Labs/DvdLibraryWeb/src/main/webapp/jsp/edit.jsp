<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library | Edit</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>

            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                </ul>    
            </div>

            <div class="row">

                <div class="col-md-6">
                    <form method="POST" action="./" class="form-horizontal">

                        <input type="hidden" name="id" value="${dvd.id}"/>

                        <div class="form-group">
                            <label for="title" class="col-md-4 control-label">Title: </label>
                            <div class="col-md-8">
                                <input id="title" name="title" value="${dvd.title}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="release-date" class="col-md-4 control-label">Release Date: </label>
                            <div class="col-md-8">
                                <input id="release-date" name="releaseDate" value="${dvd.releaseDate}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="rating" class="col-md-4 control-label">Rating: </label>
                            <div class="col-md-8">
                                <label class="radio-inline"><input type="radio" name="rating" value="G">G</label>
                                <label class="radio-inline"><input type="radio" name="rating" value="PG">PG</label>
                                <label class="radio-inline"><input type="radio" name="rating" value="PG-13">PG-13</label>
                                <label class="radio-inline"><input type="radio" name="rating" value="R">R</label>
                                <label class="radio-inline"><input type="radio" name="rating" value="NC-17">NC-17</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="director" class="col-md-4 control-label">Director: </label>
                            <div class="col-md-8">
                                <input id="director" name="director" value="${dvd.director}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="studio" class="col-md-4 control-label">Studio </label>
                            <div class="col-md-8">
                                <input id="studio" name="studio" value="${dvd.studio}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="note" class="col-md-4 control-label">Note: </label>
                            <div class="col-md-8">
                                <!--<input id="note" name="note"/>-->
                                <textarea class="form-control" rows="5" id="note" name="note"></textarea>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary center-block" value="Submit"/>
                    </form>

                </div>

            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

