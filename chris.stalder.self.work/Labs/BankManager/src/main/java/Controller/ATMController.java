/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import dao.AccountDao;
import dto.Account;
import dto.Checking;
import dto.Saving;

/**
 *
 * @author apprentice
 */
public class ATMController {

    AccountDao accountDao = new AccountDao();
    ConsoleIO io = new ConsoleIO();
//    Account checking = new Checking();
//    Account savings = new Saving();

    public void run() {

        initializeAccounts();
        int password = 1234;
        int pin = io.PromptGetInt("Enter pin.");
        if (pin == password) {
            boolean repeat = true;

            while (repeat) {
                int menuChoice = io.PromptGetInt(
                        "Welcome to the ATM!"
                        + " 1) View Checking Account"
                        + " 2) View Savings Account"
                        + " 3) Exit");

                switch (menuChoice) {

                    case 1:
                        runChecking();
                        break;

                    case 2:
                        runSaving();
                        break;

                    case 3:
                        repeat = false;
                        break;
                    default:
                        io.PrintString("Invalid entry, please select a menu option");
                        break;

                }
            }

        }else
            io.PrintString("Invalid pin.");
    }

    public void runChecking() {

        Account checking = accountDao.get(1);

        int menuOption = io.PromptGetInt(
                "\nPlease choose an option:"
                + "1) Make a deposit"
                + "2) Make a withdrawal\n"
                + "3) Check Balance\n"
        );

        switch (menuOption) {

            case 1:
                makeDeposit(checking);
                break;
            case 2:
                makeWithdrawl(checking);
                break;
            case 3:
                checkBalance(checking);
                break;
            default:
                io.PrintString("Invalid entry!!");
                break;

        }

    }

    public void runSaving() {

        Account saving = accountDao.get(2);

        int menuOption = io.PromptGetInt(
                "\nPlease choose an option:"
                + "1) Make a deposit"
                + "2) Make a withdrawal"
                + "3) Check Balance\n"
        );

        switch (menuOption) {

            case 1:
                makeDeposit(saving);
                break;
            case 2:
                makeWithdrawl(saving);
                break;
            case 3:
                checkBalance(saving);
                break;
            default:
                io.PrintString("Invalid entry!!");
                break;

        }

    }

    public void addAccount() {

    }

    public void initializeAccounts() {

        Account saving = new Saving();
        Account checking = new Checking();

        saving.setBalance(2000d);
        checking.setBalance(1000d);

        accountDao.create(checking);
        accountDao.create(saving);

    }

    public void makeDeposit(Account account) {

        double deposit = io.PromptGetDouble("Please enter amount to deposit");
        account.deposit(deposit);

    }

    public void makeWithdrawl(Account account) {

        double withdrawl = io.PromptGetDouble("Please enter amount to withdraw");
        account.withdraw(withdrawl);

    }

    public void checkBalance(Account account) {

        io.PrintString(Double.toString(account.getBalance()));

    }

}
