/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author apprentice
 */
public class Account {

    protected double balance;
    protected double depositWaitingToClear;
    private int id;

    public void deposit(double amount) {
        if (amount > 10000d) {
            depositWaitingToClear += amount;
        } else {
            this.balance += amount;
        }
    }

    public void withdraw(double amount) {

        if (amount <= balance) {
            this.balance -= amount;
        } else {
            System.out.println("Insufficient funds!!");
        }
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getDepositWaitingToClear() {
        return depositWaitingToClear;
    }

    public void setDepositWaitingToClear(double depositWaitingToClear) {
        this.depositWaitingToClear = depositWaitingToClear;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
