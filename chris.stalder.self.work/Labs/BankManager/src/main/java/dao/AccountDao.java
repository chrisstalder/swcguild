/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dto.Account;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class AccountDao {

    private List<Account> accounts = new ArrayList();
    private int nextId = accounts.size() + 1;

    public void create(Account newAccount) {

        newAccount.setId(nextId);
        nextId++;
        accounts.add(newAccount);

    }

    public Account get(int id) {

        for (Account account : accounts) {
            if (id == account.getId()) {
                return account;
            }

        }
        return null;
    }
    
    public void update(Account account) {
        
        Account found = null;
        
        for (Account myAccount : accounts) {
            if (account.getId() == myAccount.getId()) {
                found = myAccount;
            }
        }
        
        accounts.remove(found);
        accounts.add(account);
    
    }
    
        public void delete(Account account) {
 
        accounts.remove(account);

    
    }
    

}
