/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Item;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface ItemDaoInterface {
    public Item create(Item item);
    public Item get(Item item);
    public void update(Item item);
    public void delete(Item item);
    public List<Item> list();
    public void encode();
    public List<Item> decode();
}
