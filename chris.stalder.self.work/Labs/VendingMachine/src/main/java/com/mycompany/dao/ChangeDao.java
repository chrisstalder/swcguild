/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Change;
import com.mycompany.interfaces.ChangeDaoInterface;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class ChangeDao implements ChangeDaoInterface{
    private List<Change> cList = new ArrayList();
    
    @Override
    public double makeChange(Double dollar){
        double dollarsToChange = dollar * 100;
        return dollarsToChange;
    }
    
    @Override
    public Change getCoins(Double dollarsToChange){
        
        Change myChange = new Change();
        int quarters = 0;
        int dimes = 0;
        int nickels = 0;
        int pennies = 0;
        
        while (dollarsToChange > 0){
            for(quarters = 0; dollarsToChange >= 25; quarters++){
                dollarsToChange -= 25;
               
            }
            for(dimes = 0; dollarsToChange >= 10; dimes++){
                dollarsToChange -= 10;
                
            }
            for(nickels = 0; dollarsToChange >= 5; nickels++){
                dollarsToChange -= 5;
                
            }
            for(pennies = 0; dollarsToChange > 0; pennies++){
                dollarsToChange -= 1;
                
            }
        }
        
        myChange.setQuarter(quarters);
        myChange.setDime(dimes);
        myChange.setNickel(nickels);
        myChange.setPenny(pennies);
        cList.add(myChange);
        
        
        return myChange;
    }

}
