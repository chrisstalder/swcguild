package com.mycompany.controllers;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class ConsoleIO {
    
    Scanner keyboard = new Scanner(System.in);
    
    public int getIntValue(){
        String input = keyboard.nextLine();
        int value = Integer.parseInt(input);
        return value;
    }
    
    
    public float getFloatValue(){
        String input = keyboard.nextLine();
        float value = Float.parseFloat(input);
        return value;
    }
    
    
    public double getDoubleValue(){
        String input = keyboard.nextLine();
        double value = Double.parseDouble(input);
        return value;
    }
    
    
    public String getString(){
        String input = keyboard.nextLine().toLowerCase();
        return input;
    }
    
    public Date getDate(){
        String dateInString = keyboard.nextLine();
        DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
        Date date = new Date();
        try {
            date = format.parse(dateInString);
        } catch (ParseException ex) {
            Logger.getLogger(ConsoleIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }
}