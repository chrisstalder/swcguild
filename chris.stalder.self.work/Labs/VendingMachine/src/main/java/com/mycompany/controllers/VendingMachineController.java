/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.dao.ChangeDao;
import com.mycompany.dao.ItemDao;
import com.mycompany.dto.Change;
import com.mycompany.dto.Item;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class VendingMachineController {
    ConsoleIO console = new ConsoleIO();
    ItemDao iDao = new ItemDao();
    ChangeDao cDao = new ChangeDao();
    public void run(){
        
        boolean repeat = true;
        int input;
        while(repeat){
           
            System.out.println("Welcome to the Vending Machine!");
            displayAll();
            System.out.println("What would you like to do?");
            System.out.println("1)Buy Item");
            System.out.println("2)Display Inventory");
//            System.out.println("3)Check Funds");
//            System.out.println("4)Return Change");
            System.out.println("3)Exit");
            
            input = console.getIntValue();
            
            switch(input){
                case 1:
                    buyItem();
                    break;
                case 2:
                    displayAll();
                    break;
//                case 3:
//                    //check funds
//                    break;
//                case 4:
//                    //return change
//                    break;
                case 3:
                    repeat = false;
                    break;
                case 9:
                    createInventory();
                    break;
                default:
                    System.out.println("That wasn't an option!");
            }
            
        }
       
    }
    
    public void buyItem(){
        List<Item> inventory = iDao.list();
        Change myChange = new Change();
        
        System.out.println("Input a dollar amount");
        double dollar = console.getDoubleValue();
        double change = cDao.makeChange(dollar);
        System.out.println("What would you like to buy? Please enter the item's ID");

        int id = console.getIntValue();
        for (Item item : inventory) {
            
            double itemCost = cDao.makeChange(item.getPrice());
            
            if(item.getId() == id) {
                if (change >= itemCost){
                    change -= itemCost;
                    int stock = item.getInventory();
                    int newStock = stock - 1;
                    item.setInventory(newStock);
                    myChange = cDao.getCoins(change);
                    System.out.println("Item purchased!");
                    System.out.println("Vending....");
                    System.out.println("There ya go, here is your change.");
                    System.out.println(myChange.getQuarter() + " quarters, " + myChange.getDime() + " dimes, " + myChange.getNickel() + " nickels, " + myChange.getPenny() + " pennies");
                }else
                    System.out.println("Insufficient funds!");
            }
        }
    }

    public void displayAll(){
        List<Item> inventory = iDao.list();
        System.out.println("===============================");
        for (Item item : inventory) {
            if (item.getInventory() <= 0)
                System.out.println(item.getName() + " is out of stock!");
            else
                System.out.println(item.getId() + " | " + item.getName() + " " + item.getPrice() + " " + item.getInventory());
        }
        System.out.println("===============================");
    }
    
    public void checkFunds(){
        
    }
    
    public void returnChange(){
        
    }
    
    public void checkInventory(){
        
    }
    
    public void createInventory(){
        System.out.println("Oh, I hadn't realized it was time to restock.");
        System.out.println("What is the name of the item?");
        String name = console.getString();
        System.out.println("What is the price?");
        Double price = console.getDoubleValue();
        System.out.println("And how many are you putting in inventory?");
        int amount = console.getIntValue();
        
        Item newItem = new Item();
        newItem.setName(name);
        newItem.setPrice(price);
        newItem.setInventory(amount);
        
        iDao.create(newItem);
    }
}
