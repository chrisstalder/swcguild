/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;


import com.mycompany.dto.Item;
import com.mycompany.interfaces.ItemDaoInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class ItemDao implements ItemDaoInterface{
    private List<Item> inventory = new ArrayList();
    private int nextId = 1 + inventory.size();
    
    public ItemDao(){
        inventory = decode();
    }
    public Item create(Item item){
        item.setId(nextId);
        nextId++;
        inventory.add(item);
        encode();
        return item;
    }
    
    public Item get(Item myItem){
        for (Item item : inventory) {
            if(item.getId() == myItem.getId())
                return item;
        }
        return null;
    }
    
    public void update(Item myItem){
        Item found = null;
        for (Item item : inventory) {
            if(item.getId() == myItem.getId())
                found = item; 
        }
        
        inventory.remove(found);
        inventory.add(myItem);
        encode();
        
    }
    
    public void delete(Item myItem){
        Item found = null;
        for (Item item : inventory) {
            if(item.getId()==myItem.getId())
                found = item;
        }
        
        inventory.remove(found);
        encode();
    }
    
    public List<Item> list(){
        return inventory;
    }
    
    public void encode(){
        
        final String TOKEN = "::";
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("inventory.txt"));
            
            for (Item item : inventory) {
                out.print(item.getId());
                out.print(TOKEN);
                out.print(item.getName());
                out.print(TOKEN);
                out.print(item.getPrice());
                out.print(TOKEN);
                out.print(item.getInventory());
                out.println();
            }
            
            out.flush();
            out.close();
            
            
        } catch (IOException ex) {
            Logger.getLogger(ItemDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Item> decode(){
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("inventory.txt")));
            
            while(sc.hasNextLine()){
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");
                
                int id = Integer.parseInt(stringParts[0]);
                double price = Double.parseDouble(stringParts[2]);
                int amount = Integer.parseInt(stringParts[3]);
                
                Item item = new Item();
                
                item.setId(id);
                item.setName(stringParts[1]);
                item.setPrice(price);
                item.setInventory(amount);
                
                inventory.add(item);
                nextId = 1 + inventory.size();
            }
            
            sc.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ItemDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inventory;
    }
}
