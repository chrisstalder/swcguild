/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

/**
 *
 * @author apprentice
 */
public class Change {
    private int quarter;
    private int dime;
    private int nickel;
    private int penny;
    private int change;
    private double dollars;

    public int getQuarter() {
        return quarter;
    }

    public int getDime() {
        return dime;
    }

    public int getNickel() {
        return nickel;
    }

    public int getPenny() {
        return penny;
    }

    public int getChange() {
        return change;
    }

    public double getDollars() {
        return dollars;
    }

    public void setQuarter(int quarter) {
        this.quarter = quarter;
    }

    public void setDime(int dime) {
        this.dime = dime;
    }

    public void setNickel(int nickel) {
        this.nickel = nickel;
    }

    public void setPenny(int penny) {
        this.penny = penny;
    }

    public void setChange(int change) {
        this.change = change;
    }

    public void setDollars(double dollars) {
        this.dollars = dollars;
    }
    
    
}