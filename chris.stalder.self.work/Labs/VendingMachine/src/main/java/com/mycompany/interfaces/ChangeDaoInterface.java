/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Change;

/**
 *
 * @author apprentice
 */
public interface ChangeDaoInterface {
    
    public double makeChange(Double dollar);
    
    public Change getCoins(Double dollarsToChange);
    
}
