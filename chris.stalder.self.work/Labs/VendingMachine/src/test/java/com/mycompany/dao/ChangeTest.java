/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Change;
import com.mycompany.interfaces.ChangeDaoInterface;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ChangeTest {
    
    public ChangeTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testMakeChange() {
        ChangeDao instance = new ChangeDao();
        double dollar = 1.0;
        double result = instance.makeChange(dollar);
        
        Assert.assertEquals(100.0, result, 0.0);
    }

    /**
     * Test of getCoins method, of class ChangeDao.
     */
    @Test
    public void testGetCoins() {
        
        ChangeDao instance = new ChangeDao();
        double dollarToChange = 25.0;
        Change result = instance.getCoins(dollarToChange);
        
        Assert.assertEquals(1, result.getQuarter());
    }
}
