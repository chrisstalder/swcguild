/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Item;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ItemDaoTest {
    
    public ItemDaoTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class ItemDao.
     */
    @Test
    public void testCreate(){
        ItemDao instance = new ItemDao();
        Item item = new Item();
        item.setName("M&Ms");
        item.setPrice(.75);
        item.setInventory(3);

        Item result = instance.create(item);
        
        Assert.assertEquals(item, result);
    }
    
    @Test
    public void testGet(){
        ItemDao instance = new ItemDao();
        Item item = new Item();
        Item result = instance.get(item);
        
        Assert.assertNull(result);
    }
    
    @Test
    public void testUpdate(){
        ItemDao instance = new ItemDao();
        List<Item> list = instance.list();
        int nextId = list.size();
        Item item = new Item();
        item.setName("M&Ms");
        item.setPrice(.75);
        item.setInventory(3);
        
        
        for (Item item1 : list) {
            if(item1.getId() == item.getId())
                item = item1;
        }
        
        item.setInventory(5);
        instance.update(item);
        
        int result = item.getInventory();
        
        Assert.assertEquals(5, result);
    }
    
    @Test
    public void testDelete(){
        ItemDao instance = new ItemDao();
        List<Item> list = instance.list();
        
        int currentArraySize = list.size();
        
        Item item = new Item();
        for (Item item1 : list) {
            if(item1.getId() == item.getId())
                item = item1;
        }
        
        instance.delete(item);
        boolean result = false;
        for (Item item1 : list) {
            if(item1.getId() == item.getId())
                result = true;
        }
        
        Assert.assertFalse(result);
    }
    
}
