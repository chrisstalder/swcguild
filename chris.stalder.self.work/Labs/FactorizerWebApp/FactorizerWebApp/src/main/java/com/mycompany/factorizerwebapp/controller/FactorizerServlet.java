package com.mycompany.factorizerwebapp.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(urlPatterns = {"/FactorizerServlet"})
public class FactorizerServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean isPerfect = false;
        boolean isPrime = false;
        int evenDivides = 0;
        int factorTotal = 0;
        int input = 0;

        String userInput = request.getParameter("userInput");

        input = Integer.parseInt(userInput);

        List<Integer> factors = new ArrayList();
        for (int i = 1; i < input; i++) {    //for loop will loop through all numbers before input
            if (input % i == 0) {
                factors.add(i);
                factorTotal += i; //keeps track of number of factors
                evenDivides++; //keeps track of how many numbers can evenly divide into input
            }
            isPerfect = isPerfect(factorTotal, input);
            isPrime = isPrime(evenDivides);
        }

        String perfectMessage = printIsPerfect(isPerfect, input);

        String primeMessage = printIsPrime(isPrime, input);

        request.setAttribute("input", input);
        request.setAttribute("factors", factors);
        request.setAttribute("primeMessage", primeMessage);
        request.setAttribute("perfectMessage", perfectMessage);
        RequestDispatcher rd = request.getRequestDispatcher("response.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String printIsPrime(boolean isPrime, int input) {
        String message = "";
        if (isPrime == true) {
            message = (input + " is a prime number.");
        } else {                                                      //prints out if it is a prime number
            message = (input + " is not a prime number.");
        }

        return message;
    }

    public String printIsPerfect(boolean isPerfect, int input) {
        String message = "";
        if (isPerfect == true) {
            message = (input + " is a perfect number.");        //prints out if it is perfect or not
        } else {
            message = (input + " is not a perfect number.");
        }

        return message;
    }

    public boolean isPrime(int evenDivides) {
        boolean isPrime;
        isPrime = evenDivides == 1; //if a number only has one # that can divide into it evenly (excluding itself) then it is prime
        return isPrime;
    }

    public boolean isPerfect(int factorTotal, int input) {
        boolean isPerfect = false;
        if (factorTotal == input) {
            isPerfect = true;       //if all factors added together equal input it is a perfect #
        }
        return isPerfect;
    }
}
