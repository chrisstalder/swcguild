<%-- 
    Document   : response
    Created on : May 26, 2016, 8:27:45 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Factorizer</title>
    </head>
    <body>
        <h1>Factorizer</h1>
        The factors of ${input} are: ${factors}.
        ${primeMessage}
        ${perfectMessage}
    </body>
</html>
