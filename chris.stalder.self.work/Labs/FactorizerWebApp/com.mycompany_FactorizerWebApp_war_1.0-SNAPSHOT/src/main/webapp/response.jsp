<%-- 
    Document   : response
    Created on : May 26, 2016, 8:27:45 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="factorizer.css" rel="stylesheet" type="text/css">
        <title>Factorizer</title>
    </head>
    <body>
        <h1>Factorizer</h1>
        The factors of ${input} are:<br> 
        <c:forEach items="${factors}" var="factor">
            ${factor}<br>
        </c:forEach>
        ${primeMessage}<br>
        ${perfectMessage}<br>
    </body>
</html>
