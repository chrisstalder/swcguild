/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class LuckySevens {
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);

        
        System.out.println("How much would you like to bet?");
        
        float bet = keyboard.nextFloat();
        int totalRolls = 0;
        int maxRolls = 0;
        float maxMoney = bet;
        
        
        while (bet > 0){
            int d1 = (int)(Math.random() * 6 + 1);
            int d2 = (int)(Math.random() * 6 + 1);
            int total = d1 + d2;
            totalRolls++;
            
            if (total == 7){
                bet += 4;
                if (bet > maxMoney){
                    maxMoney = bet;
                    maxRolls = totalRolls;
                }
            }else{
                bet--;
            }
        }
        
        System.out.println("You are broke after " + totalRolls + " rolls.");
        System.out.println("You should have quit after " + maxRolls + ", when you had " + maxMoney);
    }
}
