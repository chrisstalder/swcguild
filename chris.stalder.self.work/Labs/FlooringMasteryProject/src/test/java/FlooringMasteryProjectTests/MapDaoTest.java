/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FlooringMasteryProjectTests;

import com.mycompany.dao.MapDaoImpl;
import com.mycompany.dto.Order;
import com.mycompany.interfaces.MapDao;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MapDaoTest {
    MapDao instance = new MapDaoImpl();

    Set<String> keySet = instance.list();
    List<Order> orderList = new ArrayList();
    int nextId = orderList.size() + 1;
    
    
    public MapDaoTest() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
    @Test
    public void testGet(){
        
        String type = "05132016";
        List<Order> result = instance.get(type);
        Assert.assertNotNull(result);
    }
    
    @Test
    public void testUpdate(){
        Order order = new Order();
        order.setOrderId(nextId);
        order.setArea(100);
        order.setCostPerSqFoot(5.15);
        order.setLaborCostPerSqFoot(4.75);
        order.setCustomerName("tester");
        order.setState("OH");
        order.setTaxRate(6.25);
        order.setMaterialCost(515);
        order.setLaborCost(475);
        order.setTax(61.875);
        order.setTotal(1051.875);
        order.setDate("05112016");
        
        
        Order result = order;
        
        result.setArea(105.0);
        orderList.add(result);
        instance.update(result.getDate(), orderList);
        Assert.assertEquals(105.0, result.getArea(), 0.0);
    }
    
    @Test
    public void testDelete(){
        Order order = new Order();
        order.setOrderId(nextId);
        order.setArea(100);
        order.setCostPerSqFoot(5.15);
        order.setLaborCostPerSqFoot(4.75);
        order.setCustomerName("tester");
        order.setState("OH");
        order.setTaxRate(6.25);
        order.setMaterialCost(515);
        order.setLaborCost(475);
        order.setTax(61.875);
        order.setTotal(1051.875);
        order.setDate("05112016");
        
        instance.delete(order.getDate());
        List<Order> result = orderList;
        Assert.assertFalse(result.contains(order));
    }
}
