/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FlooringMasteryProjectTests;

import com.mycompany.dao.FileDaoImpl;
import com.mycompany.interfaces.FileDao;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FileDaoTest {
    
    FileDao instance = new FileDaoImpl();
    List<String> list = new ArrayList();
    String file = null;
    public FileDaoTest() {
    }
    
    @Before
    public void setUp() {
        file = "05122016";
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCreate(){
        String file1 = "05122016";
        
        String result = instance.Create(file1);
        
        Assert.assertEquals(file, result);
        
    }
    
    @Test
    public void testGet(){
        String string = "05132016";
        String result = instance.get(string);
        Assert.assertNotNull(result);
    }
    
//    @Test
//    public void testUpdate(){
//        
//        
//        String result = null;
//        for (String name : list) {
//            if (name.equals)
//                result = tax1;
//        }
//        result.setTaxRate(6.50);
//        tDao.update(result);
//        Assert.assertEquals(6.50, result.getTaxRate(), 0.0);
//    }
//    
//    @Test
//    public void testDelete(){
//        tDao.delete(tax);
//        List<Tax> result = list;
//        Assert.assertFalse(result.contains(tax));
//    }
}
