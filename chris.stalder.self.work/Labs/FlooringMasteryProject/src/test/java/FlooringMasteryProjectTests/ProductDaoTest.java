/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FlooringMasteryProjectTests;

import com.mycompany.dao.ProductDaoImpl;
import com.mycompany.dto.Product;
import com.mycompany.interfaces.ProductDao;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class ProductDaoTest {
    ProductDao instance = new ProductDaoImpl();
    List<Product> list = instance.list();
    int nextId = list.size() + 1;
    Product product = new Product();
    
    public ProductDaoTest() {
    }
    
    @Before
    public void setUp() {
        product.setId(nextId);
        product.setProductType("wood");
        product.setCostPerSqFoot(5.15);
        product.setLaborPerSqFoot(4.75);
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCreate(){
        
        
        Product result = instance.Create(product);
        
        Assert.assertEquals(product, result);
        
    }
    
    @Test
    public void testGet(){
        String type = "wood";
        Product result = instance.get(type);
        Assert.assertEquals(product.getLaborPerSqFoot(), result.getLaborPerSqFoot(), 0.0);
    }
    
    @Test
    public void testUpdate(){
        
        
        Product result = new Product();
        for (Product product1 : list) {
            if (product.getId() == product1.getId())
                result = product1;
        }
        result.setProductType("carpet");
        instance.update(result);
        Assert.assertEquals("carpet", result.getProductType());
    }
    
    @Test
    public void testDelete(){
        instance.delete(product);
        List<Product> result = list;
        Assert.assertFalse(result.contains(product));
    }
}
