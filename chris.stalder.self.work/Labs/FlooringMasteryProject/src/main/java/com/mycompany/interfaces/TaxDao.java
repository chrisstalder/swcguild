/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Tax;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface TaxDao {
    public Tax Create(Tax tax);
    public Tax get(String state);
    public void update(Tax tax);
    public void delete(Tax tax);
    public List<Tax> list();
}
