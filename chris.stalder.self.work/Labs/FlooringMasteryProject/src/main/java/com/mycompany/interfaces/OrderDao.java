/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Order;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface OrderDao {
    
    public Order Create(Order order);
    public Order get(int orderId);
    public void update(Order order);
    public void delete(Order order);
    public List<Order> list();
}
