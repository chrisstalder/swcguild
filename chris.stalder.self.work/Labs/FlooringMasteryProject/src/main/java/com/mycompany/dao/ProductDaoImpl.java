/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Product;
import com.mycompany.interfaces.ProductDao;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class ProductDaoImpl implements ProductDao{
    
    List<Product> productList = new ArrayList();
    private int nextId = productList.size()+1;

    
    public ProductDaoImpl(){
        productList = decode();
    }
    @Override
    public Product Create(Product product) {
        product.setId(nextId);
        nextId++;
        productList.add(product);
        encode();
        return product;
    }

    @Override
    public Product get(String type) {
        for (Product product : productList) {
            if(type.equals(product.getProductType()))
                return product;
        }
        
        return null;
    }

    @Override
    public void update(Product product) {
        Product found = null;
        for (Product product1 : productList) {
            if(product1.getId() == product.getId())
                found = product1;
        }
        
        productList.remove(found);
        productList.add(product);
        
        encode();
    }

    @Override
    public void delete(Product product) {
        Product found = null;
        for (Product product1 : productList) {
            if(product1.getId() == product.getId())
                found = product1;
        }
        
        productList.remove(found);
        
        
        encode();
    }

    @Override
    public List<Product> list() {
        return productList;
    }
    
    private void encode(){
        
        final String TOKEN = ",";
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("products.txt"));
            
            for (Product product : productList) {
                
                out.print(product.getProductType());
                out.print(TOKEN);
                out.print(product.getCostPerSqFoot());
                out.print(TOKEN);
                out.print(product.getLaborPerSqFoot());
                out.println();
            }
            
            
            out.flush();
            out.close();
            
        } catch (IOException ex) {
            Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    private List<Product> decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("products.txt")));
            
            while(sc.hasNextLine()){
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split(",");
                
                double costPerSqFoot = Double.parseDouble(stringParts[1]);
                double laborCostPerSqFt = Double.parseDouble(stringParts[2]);
                
                Product product = new Product();
                
                product.setProductType(stringParts[0]);
                product.setCostPerSqFoot(costPerSqFoot);
                product.setLaborPerSqFoot(laborCostPerSqFt);
                
                productList.add(product);
                nextId = productList.size()+ 1;
            }
            
            sc.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return productList;
    }

}
