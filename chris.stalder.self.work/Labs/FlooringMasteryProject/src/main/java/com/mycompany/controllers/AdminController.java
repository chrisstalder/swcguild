/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.dao.AdminDao;
import com.mycompany.dao.FileDaoImpl;
import com.mycompany.dao.PasswordDao;
import com.mycompany.dao.ProductDaoImpl;
import com.mycompany.dao.TaxDaoImpl;
import com.mycompany.dao.XMLProductDaoImpl;
import com.mycompany.dao.XMLTaxDaoImpl;
import com.mycompany.dto.Product;
import com.mycompany.dto.Tax;
import com.mycompany.interfaces.FileDao;
import com.mycompany.interfaces.ProductDao;
import com.mycompany.interfaces.TaxDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class AdminController {
    ConsoleIO console = new ConsoleIO();
    AdminDao adminDao = new AdminDao();
    TaxDao tDao = new TaxDaoImpl();
    FileDao fDao = new FileDaoImpl();
    ProductDao pDao = new ProductDaoImpl();
    PasswordDao passwordDao = new PasswordDao();
    XMLProductDaoImpl XMLpDao = new XMLProductDaoImpl();
    XMLTaxDaoImpl XMLtDao = new XMLTaxDaoImpl();
    boolean test;
    boolean txt;
    
    public void run(){
        
        OrderController controller = new OrderController();
        String password = passwordDao.decode();
        console.print("Enter admin password.");
        boolean adminPassword = console.getPassword(password);
        if(adminPassword){
            boolean repeat = true;
            while(repeat){
                test = adminDao.decode();
                txt = adminDao.currentFileType();
                
                List<String> menuItems = new ArrayList();
                menuItems.add("List Products");
                menuItems.add("List Taxes");
                menuItems.add("Create Product");
                menuItems.add("Create Tax");
                menuItems.add("Edit Product");
                menuItems.add("Edit Tax");
                menuItems.add("Delete Product");
                menuItems.add("Delete Tax");
                menuItems.add("Set Mode");
                menuItems.add("Set File Type");
                menuItems.add("Change Password");
                menuItems.add("Quit");
                console.menu(menuItems, "ADMIN MENU");
                
                if(test)
                    console.print("TEST MODE");
                String selection = console.getString();
                boolean isValid = false;
                int input = 0;
                while(!isValid){
                    try{
                        input = Integer.parseInt(selection);
                        isValid = true;
                    }catch(Exception e){
                        console.print("Invalid entry.");
                        selection = console.getString();
                    }
                }

                switch(input){
                    case 1:
                        listProducts();
                        break;
                    case 2:
                        listTaxes();
                        break;
                    case 3:
                        createProduct();
                        break;
                    case 4:
                        createTax();
                        break;
                    case 5:
                        editProduct();
                        break;
                    case 6:
                        editTax();
                        break;
                    case 7:
                        deleteProduct();
                        break;
                    case 8:
                        deleteTax();
                        break;
                    case 9:
                        setMode();
                        break;
                    case 10:
                        setFileType();
                        break;
                    case 11:
                        changePassword();
                        break;
                    case 12:
                        repeat = false;
                        controller.run();
                        break;
                    default:
                        console.print("Invalid Entry.");
                }
            }
        }else
            controller.run();
    }
    
    public void listProducts(){
        OrderController controller = new OrderController();
        if(txt)
            controller.listProducts();
        else if(!txt){
            List<Product> productList = XMLpDao.list();
            for (Product product : productList) {
                System.out.println(product.getProductType() + " " + product.getCostPerSqFoot() + " " + product.getLaborPerSqFoot());
            }
        }
    }
    
    public void listTaxes(){
        if(txt){
            List<Tax> taxes = tDao.list();

            for (Tax tax : taxes) {
                console.print(tax.getState() + " - " + tax.getTaxRate());
            }
        }else if(!txt){
            List<Tax> taxes = XMLtDao.list();
            for (Tax tax : taxes) {
                console.print(tax.getState() + " - " + tax.getTaxRate());
            }
        }
    }
    public void createTax(){
        TaxDaoImpl states = new TaxDaoImpl();
        List<String> validStates = states.listValidStates();
        Tax tax = new Tax();
        boolean isValid = false;
        String myState = null;
        
        for (String validState : validStates) {
            console.print(validState);
        }
        console.print("These are the states we currently have taxes for.");
        while(!isValid){
            int i = 0;
            console.print("Input State");
            myState = console.getState();
            for (String state : validStates) {
                if(myState.equals(state)){
                    isValid = false;
                    i++;
                }
            }
            if(i==0)
                isValid = true;
        }
        console.print("Input tax rate");
        double taxRate = console.getDoubleValue();
        boolean validTax = false;
        while(!validTax){
            if ((taxRate > 0)&& (taxRate < 20)){
                validTax = true;
            }else{
                console.print("Invalid Entry.");
                taxRate = console.getDoubleValue();
            }
        }
        tax.setState(myState);
        tax.setTaxRate(taxRate);
        if(!test){
            if(txt)
                tDao.Create(tax);
            else if(!txt)
                XMLtDao.Create(tax);
        }
    }
    
    public void createProduct(){
        Product myProduct = new Product();
        boolean isValid = false;
        List<Product> products = pDao.list();
        
        String type = null;
        double costPerSqFoot = 0;
        double laborCostPerSqFoot = 0;
        
        while(!isValid){
            console.print("Product Type: ");
            int i = 0;
            type = console.getString();
            for (Product product : products) {
                if(type.equals(product.getProductType())){
                    console.print("That product already exists.");
                    type = console.getString(); 
                    i++;
                }
            }
            
            if(i == 0){
                isValid = true;
            }
        }
        isValid = false;
        
        while(!isValid){
            console.print("Cost Per Sq Foot: ");
            costPerSqFoot = console.getDoubleValue();
            if((costPerSqFoot > 0)&&(costPerSqFoot < 100))
                isValid = true;
        }
        isValid = false;
        
        while(!isValid){
            console.print("Cost of Labor Per Sq Foot: ");
            laborCostPerSqFoot = console.getDoubleValue();
            if((laborCostPerSqFoot > 0)&&(laborCostPerSqFoot < 100))
                isValid = true;
        }
        
        myProduct.setProductType(type);
        myProduct.setCostPerSqFoot(costPerSqFoot);
        myProduct.setLaborPerSqFoot(laborCostPerSqFoot);
        if(!test){
            if(txt)
                pDao.Create(myProduct);
            else if(!txt)
                XMLpDao.Create(myProduct);
        }
    }
    
    public void editProduct(){
        OrderController controller = new OrderController();
        List<Product> products = pDao.list();
        controller.listProducts();
        console.print("Enter product type: ");
        String type = console.getString();
        Product myProduct = controller.getProduct(type);
        
        console.print("Material Cost Per Sq Foot (" + myProduct.getCostPerSqFoot() + ") : " );
        double costPerSqFoot = console.validateDouble();
        console.print("Labor Cost Per Sq Foot (" + myProduct.getLaborPerSqFoot() + ") : " );
        double laborCostPerSqFoot = console.validateDouble();
        
        myProduct.setCostPerSqFoot(costPerSqFoot);
        myProduct.setLaborPerSqFoot(laborCostPerSqFoot);
        if(!test){
            if(txt)
                pDao.update(myProduct);
            else if(!txt)
                XMLpDao.update(myProduct);
        }
    }
    
    public void editTax(){
        OrderController controller = new OrderController();
        List<Tax> taxes = tDao.list();
        for (Tax tax : taxes) {
            console.print(tax.getState() + " | " + tax.getTaxRate());
        }
        double taxRate = 0;
        console.print("Enter the State: ");
        String state = console.getState();
        boolean validState = controller.validateState(state);
        while(!validState){
            console.print("Invalid Entry.");
            state = console.getState();
            validState = controller.validateState(state);
        }
        
        Tax tax = controller.getTax(state);
        
        console.print("Tax rate (" + tax.getTaxRate() + ") : ");
        console.print("Do you wish to edit this?");
        boolean answer = console.getYesNo();
        if(answer){
            console.print("Enter new tax rate: ");
            taxRate = console.validateDouble();
            tax.setTaxRate(taxRate);
            if(!test){
                if(txt)
                    tDao.update(tax);
                else if(!txt)
                    XMLtDao.update(tax);
            }
        }
        
    }
    
    public void deleteProduct(){
        OrderController controller = new OrderController();
        controller.listProducts();
        console.print("Enter product type: ");
        String type = console.getString(); 
        Product myProduct = controller.getProduct(type);
        
        console.print("Are you sure you want to delete " + myProduct.getProductType() + "?(y/n)");
        boolean answer = console.getYesNo();
        if((answer)&&(!test)){
            if(txt)
                pDao.delete(myProduct);
            else if(!txt)
                XMLpDao.delete(myProduct);
            console.print("Product deleted.");
        }
    }
    
    public void deleteTax(){
        OrderController controller = new OrderController();
        TaxDaoImpl instance = new TaxDaoImpl();
        
        List<String> validStates = instance.listValidStates();
        for (String validState : validStates) {
            console.print(validState);
        }
        
        console.print("Enter state: ");
        String state = console.getState();
        boolean validState = controller.validateState(state);
        while(!validState){
            console.print("Invalid Entry.");
            state = console.getState();
            validState = controller.validateState(state);
        }
        Tax tax = controller.getTax(state);
        
        console.print("Are you sure you want to delete the tax rate for " + tax.getState() + "?(y/n)");
        boolean answer = console.getYesNo();
        if((answer)&&(!test)){
            if(txt)
                tDao.delete(tax);
            else if(!txt)
                XMLtDao.delete(tax);
            console.print("Tax deleted.");
        }
    }
    
    public void setFileType(){

        if (txt == true){
            console.print("Currently, you are saving to .txt files.");
            console.print("Would you like to save to .xml files?");
            boolean answer = console.getYesNo();
            if(answer)
                adminDao.setCurrentFileType("xml");
            
        }else if (test == false){
            console.print("Currently, you are saving to .xml files.");
            console.print("Would you like to save to .txt files?");
            boolean answer = console.getYesNo();
            if(answer)
                adminDao.setCurrentFileType("txt");
        }
        
    }
    
    public void setMode(){
        if (test == true){
            console.print("Currently, you are in test mode.");
            console.print("Would you like to enter production mode?");
            boolean answer = console.getYesNo();
            if(answer)
                adminDao.setMode("production");
            
        }else if (test == false){
            console.print("Currently, you are in production mode.");
            console.print("Would you like to enter test mode?");
            boolean answer = console.getYesNo();
            if(answer)
                adminDao.setMode("test");
        }
    }
    public void changePassword(){
        console.print("Enter new password");
        String password = console.getString();
        passwordDao.setPassword(password);
    }
}
