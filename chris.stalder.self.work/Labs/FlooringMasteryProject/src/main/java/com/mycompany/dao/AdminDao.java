/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class AdminDao {
    boolean test;
    boolean txt;

    public boolean currentMode(){
        return test;
    }
    
    public boolean currentFileType(){
        return txt;
    }
    
    public void setMode(String mode){
        if ("production".equals(mode)){
                test = false;
        }else if("test".equals(mode)){
            test = true;
        }
        
        encode();
    }
    
    public void setCurrentFileType(String fileType){
        if("txt".equals(fileType))
            txt = true;
        else if("xml".equals(fileType))
            txt = false;
        encode();
    }
    
    public void encode(){
        final String TOKEN = ",";
        try {
            PrintWriter out = new PrintWriter(new FileWriter("config.txt"));
            
            if(test == false){
                out.print("production");
            }else if (test == true){
                out.print("test");
            }
            out.print(TOKEN);
            if(txt == false){
                out.print("xml");
            }else if (txt == true){
                out.print("txt");
            }
            
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public boolean decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("config.txt")));
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                String[] stringPart = line.split(",");
                
                if ("production".equals(stringPart[0])){
                    test = false;
                }else if("test".equals(stringPart[0])){
                    test = true;
                }
                
                if("txt".equals(stringPart[1]))
                    txt = true;
                else if("xml".equals(stringPart[1]))
                    txt = false;
                
            }
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return test;
    }
}
