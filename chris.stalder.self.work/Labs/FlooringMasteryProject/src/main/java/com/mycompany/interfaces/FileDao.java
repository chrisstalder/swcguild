/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import java.io.File;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface FileDao {
    public String Create(String file);
    public String get(String file);
    public void update(String file);
    public void delete(String file);
    public List<String> list();
}
