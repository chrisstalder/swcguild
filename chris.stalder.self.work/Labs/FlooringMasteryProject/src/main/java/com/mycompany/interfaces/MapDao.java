/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Order;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public interface MapDao {
    
    public List<Order> get(String string);
    
    public void update(String date, List<Order> orderList);
    
    public void delete(String date);
    
    public Set<String> list();
    
    
}
