/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Product;
import com.mycompany.interfaces.ProductDao;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author apprentice
 */
public class XMLProductDaoImpl implements ProductDao{
 List<Product> productList = new ArrayList();
    private int nextId = productList.size()+1;
    private int numChildEle = 4;
    
    public XMLProductDaoImpl(){
        productList = decodeXML();
    }
    @Override
    public Product Create(Product product) {
        product.setId(nextId);
        nextId++;
        productList.add(product);
        encodeXML();
        return product;
    }

    @Override
    public Product get(String type) {
        for (Product product : productList) {
            if(type.equals(product.getProductType()))
                return product;
        }
        
        return null;
    }

    @Override
    public void update(Product product) {
        Product found = null;
        for (Product product1 : productList) {
            if(product1.getId() == product.getId())
                found = product1;
        }
        
        productList.remove(found);
        productList.add(product);
        
        encodeXML();
    }

    @Override
    public void delete(Product product) {
        Product found = null;
        for (Product product1 : productList) {
            if(product1.getId() == product.getId())
                found = product1;
        }
        
        productList.remove(found);
        
        encodeXML();
    }

    @Override
    public List<Product> list() {
        return productList;
    }
    
    public void encodeXML(){
        Document dom;
        Element e = null;
        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        
        DocumentBuilder db;
        try {
            db = dbf.newDocumentBuilder();
            dom = db.newDocument();
            
            Element rootEle = dom.createElement("products");
            
            int i = 1;
            for (Product product : productList) {
                e = dom.createElement("product" + i);
                               
                String cost1 = Double.toString(product.getCostPerSqFoot());
                String cost2 = Double.toString(product.getLaborPerSqFoot());
                Element type = dom.createElement("type");

                Element costPerSqFoot = dom.createElement("costPerSqFoot");
                Element laborCost = dom.createElement("laborCostPerSqFoot");
                

                type.appendChild(dom.createTextNode(product.getProductType()));

                costPerSqFoot.appendChild(dom.createTextNode(cost1));

                laborCost.appendChild(dom.createTextNode(cost2));
                

                e.appendChild(type);
                e.appendChild(costPerSqFoot);
                e.appendChild(laborCost);
                rootEle.appendChild(e);
                i++;
            }

            dom.appendChild(rootEle);

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                tr.transform(new DOMSource(dom), 
                                     new StreamResult(new FileOutputStream("products.xml")));

            } catch (TransformerException te) {
                System.out.println(te.getMessage());
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }
        } catch (ParserConfigurationException pce) {
            System.out.println("UsersXML: Error trying to instantiate DocumentBuilder " + pce);
        }
        
    }
    
    private List<Product> decodeXML(){
        
        Document dom;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            
            try {
                dom = db.parse("products.xml");
                
                Element doc = dom.getDocumentElement();

              NodeList nodeList = doc.getElementsByTagName("*"); 

                for (int i = 0; i < nodeList.getLength(); i+=numChildEle) {
                    Product product = new Product();
                    Node node = nodeList.item(i);
                    for (int j=0; j<node.getChildNodes().getLength(); j++)
                    {
                        Node node2 = node.getChildNodes().item(j);
                        
                        if("type".equals(node2.getNodeName()))
                            product.setProductType(node2.getTextContent());
                        else if("costPerSqFoot".equals(node2.getNodeName())){
                            double matCost = Double.parseDouble(node2.getTextContent());
                            product.setCostPerSqFoot(matCost);
                        }else if("laborCostPerSqFoot".equals(node2.getNodeName())){
                            double labCost = Double.parseDouble(node2.getTextContent());
                            product.setLaborPerSqFoot(labCost);
                        }
                       
                    }
                    productList.add(product); 
                }

                
            } catch (SAXException ex) {
                Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return productList;
    }
    
}
