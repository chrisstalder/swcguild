/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.interfaces.FileDao;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class FileDaoImpl implements FileDao{

    private List<String> fileNames = new ArrayList();
    
    
    public FileDaoImpl(){
        fileNames = decode();
    }
    @Override
    public String Create(String file) {
        int i = 0;
        for (String fileName : fileNames) {
            if(file.equals(fileName))
                i++;
        }
        
        if(i == 0){
            fileNames.add(file);
            encode();
        }
        return file;
    }

    @Override
    public String get(String file) {
        for (String fileName : fileNames) {
            if(file.equals(fileName))
                return fileName;
        }
        return null;
    }

    @Override
    public void update(String file) {
        String found = null;
        for (String fileName : fileNames) {
            if(file.equals(fileName))
                found = fileName;
        }
        
        fileNames.remove(found);
        fileNames.add(file);
        encode();
    }

    @Override
    public void delete(String file) {
        String found = null;
        for (String fileName : fileNames) {
            if(file.equals(fileName))
                found = fileName;
        }
        
        fileNames.remove(found);
        
        encode();
    }

    @Override
    public List<String> list() {
        return fileNames;
    }
    
    private void encode(){
        
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("fileList.txt"));
            
            for (String fileName : fileNames) {
                out.print(fileName);
                out.println();
            }
            
            out.flush();
            out.close();
            
        } catch (IOException ex) {
            Logger.getLogger(FileDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<String> decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("fileList.txt")));
            
            while(sc.hasNextLine()){
                String currentLine = sc.nextLine();
                
                fileNames.add(currentLine);
                
            }
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return fileNames;
    }
    
}
