/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Tax;
import com.mycompany.interfaces.TaxDao;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author apprentice
 */
public class XMLTaxDaoImpl implements TaxDao {
    
    private List<Tax> taxes = new ArrayList();
    private List<String> validStates = new ArrayList();
    private int nextId = taxes.size() + 1;
    private int numChildEle = 3;
    
    public XMLTaxDaoImpl(){
        taxes = decodeXML();
    }
    @Override
    public Tax Create(Tax tax) {
        tax.setId(nextId);
        nextId++;
        taxes.add(tax);
        validStates.add(tax.getState());
        encodeXML();
        return tax;
    }

    @Override
    public Tax get(String state) {
        for (Tax tax : taxes) {
            if(state.equals(tax.getState()))
                return tax;
        }
        
        return null;
    }

    @Override
    public void update(Tax tax) {
        Tax found = null;
        
        for (Tax t : taxes) {
            if(tax.getId() == t.getId())
                found = t;
        }
        taxes.remove(found);
        taxes.add(tax);
        encodeXML();
    }

    @Override
    public void delete(Tax tax) {
        Tax found = null;
        
        for (Tax t : taxes) {
            if(tax.getId() == t.getId())
                found = t;
        }
        taxes.remove(found);
        validStates.remove(tax.getState());
        encodeXML();
    }

    @Override
    public List<Tax> list() {
        return taxes;
    }
    
    public void encodeXML(){
        Document dom;
        Element e = null;
        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        
        DocumentBuilder db;
        try {
            db = dbf.newDocumentBuilder();
            dom = db.newDocument();
            
            Element rootEle = dom.createElement("taxes");
            int i = 1;
            for (Tax tax : taxes) {
                e = dom.createElement("tax" + i);
                
                String tax1 = Double.toString(tax.getTaxRate());
                Element state = dom.createElement("state");
                Element taxRate = dom.createElement("taxRate");
                
                state.appendChild(dom.createTextNode(tax.getState()));
                taxRate.appendChild(dom.createTextNode(tax1));
                e.appendChild(state);
                e.appendChild(taxRate);
                rootEle.appendChild(e);
                i++;
            }

            dom.appendChild(rootEle);

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                tr.transform(new DOMSource(dom), 
                                     new StreamResult(new FileOutputStream("tax.xml")));

            } catch (TransformerException te) {
                System.out.println(te.getMessage());
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }
        } catch (ParserConfigurationException pce) {
            System.out.println("UsersXML: Error trying to instantiate DocumentBuilder " + pce);
        }
        
    }
    
    private List<Tax> decodeXML(){
        
        Document dom;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            
            try {
                dom = db.parse("tax.xml");
                
                Element doc = dom.getDocumentElement();

                NodeList nodeList = doc.getElementsByTagName("*"); 

                for (int i = 0; i < nodeList.getLength(); i+=numChildEle) {
                    Tax tax = new Tax();
                    Node node = nodeList.item(i);
                    for (int j=0; j<node.getChildNodes().getLength(); j++)
                    {
                        Node node2 = node.getChildNodes().item(j);
                        
                        if("state".equals(node2.getNodeName()))
                            tax.setState(node2.getTextContent());
                        else if("taxRate".equals(node2.getNodeName())){
                            double taxRate = Double.parseDouble(node2.getTextContent());
                            tax.setTaxRate(taxRate);
                        }
                       
                    }
                    taxes.add(tax); 
                }

                
            } catch (SAXException ex) {
                Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return taxes;
    }
    
    public List<String> listValidStates(){
        return validStates;
    }
    
}
