/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class AddressBookDao {
    private List<Address> addresses = new ArrayList();
    private int nextId = 1;
   
    
    public AddressBookDao(){
        addresses = decode();
    }
    
    public Address create(Address address){
        address.setId(nextId);
        nextId++;
        addresses.add(address);
        
        encode();
        
        return address;
    }
    
    public Address get(String lastName){
        
        for (Address address : addresses) {
            if(address.getLastName() == lastName)
                return address;
        }
        return null;
    }
    
    public void delete(Address address){
        Address found = null;
        
        for (Address myAddress : addresses) {
            if(myAddress.getId() == address.getId()){
                found = myAddress;
            }
        }
        addresses.remove(found);
        encode();
    }
    
    public void update(Address address){
        
        for (Address myAddress : addresses) {
            if(myAddress.getId() == address.getId()){
                addresses.remove(myAddress);
                addresses.add(address);
            }
        }
        encode();
    }
    public List<Address> list(){
        return addresses;
    }
    
    public void totalAddresses(List<Address> addresses){
        System.out.println(addresses.size());
    }
    
    public void encode(){
        final String TOKEN = "::";
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("addresses.txt"));
            
            for (Address address : addresses) {
                out.print(address.getId());
                out.print(TOKEN);
                out.print(address.getFirstName());
                out.print(TOKEN);
                out.print(address.getLastName());
                out.print(TOKEN);
                out.print(address.getHouseNumber());
                out.print(TOKEN);
                out.print(address.getStreetName());
                out.print(TOKEN);
                out.print(address.getCity());
                out.print(TOKEN);
                out.print(address.getState());
                out.print(TOKEN);
                out.print(address.getZipcode());
                out.print(TOKEN);
                out.print(address.getApartment());
                out.print(TOKEN);
                out.print(address.getCountry());
                out.println();
            }
            
            out.flush();
            out.close();
            
            
        } catch (IOException ex) {
            Logger.getLogger(AddressBookDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Address> decode(){
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("addresses.txt")));
            
            while (sc.hasNextLine()){
                
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");
                
                int id = Integer.parseInt(stringParts[0]);
                int houseNumber = Integer.parseInt(stringParts[3]);
                
                Address address = new Address();
                
                address.setId(id);
                address.setFirstName(stringParts[1]);
                address.setLastName(stringParts[2]);
                address.setHouseNumber(houseNumber);
                address.setStreetName(stringParts[4]);
                address.setCity(stringParts[5]);
                address.setState(stringParts[6]);
                address.setZipcode(stringParts[7]);
                address.setApartment(stringParts[8]);
                address.setCountry(stringParts[9]);
                
                addresses.add(address);
                nextId = 1 + addresses.size();
                
            }
            
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AddressBookDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return addresses;
    }
}
