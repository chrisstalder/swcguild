/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIO {
    
    Scanner keyboard = new Scanner(System.in);
    
    public int getIntValue(){
        String input = keyboard.nextLine();
        int value = Integer.parseInt(input);
        return value;
    }
    
    
    public float getFloatValue(){
        String input = keyboard.nextLine();
        float value = Float.parseFloat(input);
        return value;
    }
    
    
    public double getDoubleValue(){
        String input = keyboard.nextLine();
        double value = Double.parseDouble(input);
        return value;
    }
    
    
    public String getString(){
        String input = keyboard.nextLine().toLowerCase();
        return input;
    }
    
}
