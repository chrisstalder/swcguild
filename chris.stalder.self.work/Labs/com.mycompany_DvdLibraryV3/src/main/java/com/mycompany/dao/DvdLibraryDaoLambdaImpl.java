/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;
import com.mycompany.dto.Dvd;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoLambdaImpl implements DvdDao{
    
    private List<Dvd> dvds = new ArrayList();
    
    private int nextId = dvds.size() + 1;
    
    
    public DvdLibraryDaoLambdaImpl(){
        dvds = decode();
    }

    public Dvd create(Dvd dvd){
        dvd.setId(nextId);
        nextId++;
        dvds.add(dvd);
        
        encode();
        
        return dvd;
    }
    
    public List<Dvd> get(String title){
        return dvds
            .stream()
            .filter(d -> d.getTitle().equals(title))
            .collect(Collectors.toList());
        
    }
    
    public void update(Dvd dvd){
        List<Dvd> found = dvds
            .stream()
            .filter(d -> dvd.getId() == d.getId())
            .collect(Collectors.toList());
        dvds.remove(found.get(0));
        dvds.add(dvd);
        encode();
    }
    
    public void delete(Dvd dvd){
        
        List<Dvd> found = dvds
            .stream()
            .filter(d -> dvd.getId()==d.getId())
            .collect(Collectors.toList());
        dvds.remove(found.get(0));
        
        encode();
        
    } 
    
    public List<Dvd> list(){
        return dvds;
    }
    
    public void encode(){
        
        final String TOKEN = "::";
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("dvdlibrary.txt"));
            DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");

            for (Dvd dvd : dvds) {
                out.print(dvd.getId());
                out.print(TOKEN);
                out.print(dvd.getTitle());
                out.print(TOKEN);
                out.print(format.format(dvd.getReleaseDate()));
                out.print(TOKEN);
                out.print(dvd.getRating());
                out.print(TOKEN);
                out.print(dvd.getDirector());
                out.print(TOKEN);
                out.print(dvd.getStudio());
                out.print(TOKEN);
                out.print(dvd.getNote());
                out.println();
            }
            
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(DvdLibraryDaoLambdaImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public List<Dvd> decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("dvdlibrary.txt")));
            
            while (sc.hasNextLine()){
                
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");
                
                Dvd dvd = new Dvd();
                
                int id = Integer.parseInt(stringParts[0]);
                String rating = stringParts[3];
                String dateInString = stringParts[2];
                DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
                
                Date date = new Date();
                
                try {
                    date = format.parse(dateInString);
                } catch (ParseException ex) {
                    Logger.getLogger(DvdLibraryDaoLambdaImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                
           
                dvd.setId(id);
                dvd.setTitle(stringParts[1]);
                dvd.setReleaseDate(date);
                dvd.setRating(stringParts[3]);
                dvd.setDirector(stringParts[4]);
                dvd.setStudio(stringParts[5]);
                dvd.setNote(stringParts[6]);
                
                dvds.add(dvd);
                nextId = 1 + dvds.size();
                
            }
            
            sc.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DvdLibraryDaoLambdaImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dvds;
    }

    @Override
    public List<Dvd> listDvdByStudio(String studio) {
        List<Dvd> foundByStudio = dvds
            .stream()
            .filter(d -> d.getStudio().equals(studio))
            .collect(Collectors.toList());
        
        return foundByStudio;
    }

    @Override
    public Map<String, List<Dvd>> listDvdByDirector(String director) {
        Map<String, List<Dvd>> foundByDirectorAndRating = dvds
            .stream()
            .filter(d -> d.getDirector().equals(director))
            .collect(Collectors.groupingBy(Dvd::getRating));
        
        return foundByDirectorAndRating;
    }

    @Override
    public Dvd findNewestMovie() {
        Comparator<Dvd> compareReleaseDate = Comparator.comparing(Dvd::getReleaseDate);
        
        Dvd newestMovie = dvds
            .stream()
            .max(compareReleaseDate)
            .get();

        return newestMovie;
    }

    @Override
    public Dvd findOldestMovie() {
        Comparator<Dvd> compareReleaseDate = Comparator.comparing(Dvd::getReleaseDate);
        Dvd oldestMovie = dvds
            .stream()
            .min(compareReleaseDate)
            .get();
            
        
        return oldestMovie;
    }

    @Override
    public int findAvgNumOfNotes() {
        
        long numOfNotes = dvds
            .stream()
            .filter(d -> !d.getNote().equals(""))
            .count();
        
        int avgNumOfNotes = (int) numOfNotes / dvds.size();
        
        return avgNumOfNotes;
            
    }

    @Override
    public List<Dvd> findAllMoviesReleasedInNYears(Date n) {
        List<Dvd> releasedBeforeNYears = dvds
            .stream()
            .filter(d -> d.getReleaseDate().after(n))
            .collect(Collectors.toList());
        
        return releasedBeforeNYears;
    }

    @Override
    public List<Dvd> listDvdByRating(String rating) {
        List<Dvd> foundByRatingList = dvds
            .stream()
            .filter(d -> d.getRating().equals(rating))
            .collect(Collectors.toList());
        
        return foundByRatingList;
    }
    
}
