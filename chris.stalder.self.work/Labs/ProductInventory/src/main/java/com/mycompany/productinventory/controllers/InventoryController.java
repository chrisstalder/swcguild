/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.controllers;

import com.mycompany.productinventory.dao.InventoryDao;
import com.mycompany.productinventory.dto.Alcohol;
import com.mycompany.productinventory.dto.Avacado;
import com.mycompany.productinventory.dto.Banana;
import com.mycompany.productinventory.dto.Beer;
import com.mycompany.productinventory.dto.Dairy;
import com.mycompany.productinventory.dto.Orange;
import com.mycompany.productinventory.dto.Produce;
import com.mycompany.productinventory.dto.Product;
import com.mycompany.productinventory.dto.RedWine;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class InventoryController {

    ConsoleIO console = new ConsoleIO();
    InventoryDao inventoryDao = new InventoryDao();

    public void run() {
//        initializeInventory();
        boolean repeat = true;

        while (repeat) {
            console.print("|======================================|");
            console.print("|            Inventory Menu:           |");
            console.print("|======================================|");
            console.print("|1) Check Stock                        |");
            console.print("|2) Sell Product                       |");
            console.print("|3) Order Stock                        |");
            console.print("|4) Display Inventory                  |");
            console.print("|5) Total Retail Value of Inventory    |");
            console.print("|6) Total Wholesale Value of Inventory |");
            console.print("|7) Potential Profit                   |");
            console.print("|8) Create Product                     |");
            console.print("|9) Exit                               |");
            console.print("========================================");

            int selection = console.getIntValue();

            switch (selection) {
                case 1:
                    checkStock();
                    break;
                case 2:
                    sellProduct();
                    break;
                case 3:
                    orderStock();
                    break;
                case 4:
                    displayInventory();
                    break;
                case 5:
                    calculateRetailValueOfInventory();
                    break;
                case 6:
                    calculateWholesaleValueOfInventory();
                    break;
                case 7:
                    calculatePotentialProfit();
                    break;
                case 8:
                    createProduct();
                    break;
                case 9:
                    repeat = false;
                    break;

            }
        }
    }

    public void checkStock() {
        displayByDept();

    }

    public void sellProduct() {
        displayByDept();

        console.print("Choose ID: ");
        int id = console.getIntValue();
        
        Product product = inventoryDao.get(id);
        if(product.getStock()  > 0){
            console.print("How many are you selling? ");
            int amount = console.getIntValue();

            boolean warn = product.sellProduct(amount);
            if (warn) {
                console.print("Warning. Stock Low. Order more.");
            }
        }else
            console.print("You have none in stock. Order more.");

    }

    public void orderStock() {
        String dept = displayByDept();

        console.print("Choose ID: ");
        int id = console.getIntValue();

        Product product = inventoryDao.get(id);

        console.print("How many are you buying? ");
        int amount = console.getIntValue();

        product.buyProduct(amount);
        inventoryDao.encode(dept);

    }

    public void displayInventory() {
        List<Product> productList = inventoryDao.list();
        for (Product product : productList) {
            console.print(product.getId() + " " + product.getName() + " " + product.getStock() + " " + product.getRetailPrice());
        }
    }

    public void displayDepts() {
        List<String> departmentNames = inventoryDao.departmentList();
        for (String departmentName : departmentNames) {
            console.print("===========================");
            console.print(departmentName);
            console.print("===========================");
        }
    }

    public String displayByDept() {
        displayDepts();
        console.print("Enter department: ");
        String dept = console.getString();
        if (dept != null) {
            if ("produce".equals(dept)) {
                listProduce();
            } else if ("alcohol".equals(dept)) {
                listAlcohol();
            } else if ("dairy".equals(dept)) {
                listDairy();
            } else {
                console.print("That is not a valid department.");
            }
        }

        return dept;
    }

    public boolean isExpired(Produce produce) {
        return produce.isExpired();
    }
    public boolean isExpired(Dairy dairy) {
        return dairy.isExpired();
    }

    public void listProduce() {
        List<Product> produce = inventoryDao.sortByDept("produce");
        console.print(" =============================================================");
        console.print("|    ID  |    Product Name    |   Stock   |   Retail Price    |");
        console.print(" =============================================================");
        for (Product product : produce) {
            Produce p = (Produce) product;
            console.print(" =============================================================");
            console.print("|    " + product.getId() + "   |     " + product.getName() + "       |       " + product.getStock() + "      |    " + product.getRetailPrice());
            console.print(" =============================================================");
            if (isExpired(p)) {
                console.print(product.getName() + " is expired. Order more.");
                product.setStock(0);
            }
        }
    }

    public void listDairy() {
        List<Product> produce = inventoryDao.sortByDept("dairy");
        console.print(" =============================================================");
        console.print("|    ID  |    Product Name    |   Stock   |   Retail Price    |");
        console.print(" =============================================================");
        for (Product product : produce) {
            Dairy p = (Dairy) product;
            console.print(" =============================================================");
            console.print("|    " + product.getId() + "   |     " + product.getName() + "       |       " + product.getStock() + "      |    " + product.getRetailPrice());
            console.print(" =============================================================");
            if (isExpired(p)) {
                console.print(product.getName() + " is expired. Order more.");
                product.setStock(0);
            }
        }
    }

    public void listAlcohol() {
        List<Product> alcohol = inventoryDao.sortByDept("alcohol");
        console.print(" =============================================================");
        console.print("|    ID  |    Product Name    |   Stock   |   Retail Price    |");
        console.print(" =============================================================");
        for (Product product : alcohol) {
            Alcohol p = (Alcohol) product;
            console.print(" =============================================================");
            console.print("|    " + p.getId() + "   |     " + p.getName() + "       |       " + p.getStock() + "      |    " + p.getRetailPrice());
            console.print(" =============================================================");
        }
    }

    public void calculateRetailValueOfInventory() {
        DecimalFormat df = new DecimalFormat("#.00");
        double totalRetailValue = inventoryDao.calculateRetailValueOfInventory();
        console.print("The total retail value of this store is $" + df.format(totalRetailValue));
    }

    public void calculateWholesaleValueOfInventory() {
        DecimalFormat df = new DecimalFormat("#.00");
        double totalWholesaleValue = inventoryDao.calculateWholesaleValueOfInventory();
        console.print("The total wholesale value of this store is $" + df.format(totalWholesaleValue));
    }

    public void calculatePotentialProfit() {
        DecimalFormat df = new DecimalFormat("#.00");
        double potentialProfit = inventoryDao.calculatePotentialProfit();
        console.print("Your potential profit is $" + df.format(potentialProfit));
    }

    public void initializeInventory() {
        RedWine redWine = new RedWine();
        Beer beer = new Beer();

        redWine.setDept("alcohol");
        redWine.setName("2 Buck Chuck");
        redWine.setRetailPrice(1.99);
        redWine.setStock(24);
        redWine.setStockLevel(12);
        redWine.setWholesalePrice(1.37);
        redWine.setABV(11.5);
        redWine.setAlcoholType("merlot");

        beer.setDept("alcohol");
        beer.setName("keystone light");
        beer.setRetailPrice(14.99);
        beer.setStock(24);
        beer.setStockLevel(12);
        beer.setWholesalePrice(13.85);
        beer.setABV(5.5);
        beer.setAlcoholType("beer");

        inventoryDao.create(redWine);
        inventoryDao.create(beer);

        Avacado a = new Avacado();
        Banana b = new Banana();
        Orange o = new Orange();

        a.setDept("produce");
        a.setName("avacado");
        a.setRetailPrice(1.39);
        a.setShelfLife(3);
        a.setStock(100);
        a.setStockLevel(25);
        a.setWholesalePrice(.99);
        a.setDeliveredOn(new Date());

        b.setDept("produce");
        b.setName("banana");
        b.setRetailPrice(.19);
        b.setShelfLife(2);
        b.setStock(100);
        b.setStockLevel(25);
        b.setWholesalePrice(.10);
        b.setDeliveredOn(new Date());

        o.setDept("produce");
        o.setName("orange");
        o.setRetailPrice(1.09);
        o.setShelfLife(4);
        o.setStock(100);
        o.setStockLevel(25);
        o.setWholesalePrice(.80);
        o.setDeliveredOn(new Date());

        inventoryDao.create(a);
        inventoryDao.create(b);
        inventoryDao.create(o);
    }

    public void createProduct() {
        displayDepts();
        console.print("Enter department: ");
        String dept = console.getString();
        if (dept != null) {
            if ("produce".equals(dept)) {
                createProduce();
            } else if ("alcohol".equals(dept)) {
                createAlcohol();
            } else if ("dairy".equals(dept)) {
                createDairy();
            } else {
                console.print("That is not a valid department.");
            }
        }
    }

    public void createProduce() {

        Produce produce = new Produce();
        console.print("Enter produce name: ");
        String name = console.getString();
        console.print("Enter wholesale cost: ");
        double wholesale = console.getDoubleValue();
        console.print("Enter retail price: ");
        double retail = console.getDoubleValue();
        console.print("Enter amount at which you wish to be warned if stock goes below: ");
        int stocklevel = console.getIntValue();
        console.print("Enter how many days this product can be on the shelf: ");
        int shelfLife = console.getIntValue();

        produce.setName(name);
        produce.setWholesalePrice(wholesale);
        produce.setRetailPrice(retail);
        produce.setStockLevel(stocklevel);
        produce.setShelfLife(shelfLife);
        produce.setDept("produce");
        produce.setDeliveredOn(new Date());
        inventoryDao.create(produce);

    }

    public void createAlcohol() {

        Alcohol alcohol = new Alcohol();
        console.print("Enter name: ");
        String name = console.getString();
        console.print("Enter wholesale cost: ");
        double wholesale = console.getDoubleValue();
        console.print("Enter retail price: ");
        double retail = console.getDoubleValue();
        console.print("Enter ABV: ");
        double abv = console.getDoubleValue();
        console.print("Enter alcohol type(beer, wine, liquor): ");
        String type = console.getString();
        console.print("Enter amount at which you wish to be warned if stock goes below: ");
        int stockLevel = console.getIntValue();

        alcohol.setName(name);
        alcohol.setWholesalePrice(wholesale);
        alcohol.setRetailPrice(retail);
        alcohol.setABV(abv);
        alcohol.setAlcoholType(type);
        alcohol.setStockLevel(stockLevel);
        alcohol.setDept("alcohol");
        alcohol.setDeliveredOn(new Date());
        inventoryDao.create(alcohol);

    }
    
    public void createDairy() {

        Dairy dairyProduct = new Dairy();
        console.print("Enter the dairy product name: ");
        String name = console.getString();
        console.print("Enter wholesale cost: ");
        double wholesale = console.getDoubleValue();
        console.print("Enter retail price: ");
        double retail = console.getDoubleValue();
        console.print("Enter amount at which you wish to be warned if stock goes below: ");
        int stocklevel = console.getIntValue();
        console.print("Enter how many days this product can be on the shelf: ");
        int shelfLife = console.getIntValue();

        dairyProduct.setName(name);
        dairyProduct.setWholesalePrice(wholesale);
        dairyProduct.setRetailPrice(retail);
        dairyProduct.setStockLevel(stocklevel);
        dairyProduct.setShelfLife(shelfLife);
        dairyProduct.setDept("dairy");
        dairyProduct.setDeliveredOn(new Date());
        inventoryDao.create(dairyProduct);

    }
}
