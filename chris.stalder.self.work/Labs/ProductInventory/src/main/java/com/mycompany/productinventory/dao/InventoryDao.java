/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.dao;

import com.mycompany.productinventory.dto.Alcohol;
import com.mycompany.productinventory.dto.Dairy;
import com.mycompany.productinventory.dto.Produce;
import com.mycompany.productinventory.dto.Product;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class InventoryDao {

    private List<Product> products = new ArrayList();
    private int nextId = products.size() + 1;

    public InventoryDao() {
        products = decode();
    }

    public Product create(Product product) {
        product.setId(nextId);
        nextId++;
        products.add(product);
        encode(product.getDept());
        return product;
    }

    public Product get(int id) {
        Product product = null;
        for (Product p : products) {
            if (p.getId() == id) {
                product = p;
            }
        }

        return product;
    }

    public void update(Product product) {
        Product found = null;
        for (Product p : products) {
            if (p.getId() == product.getId()) {
                found = p;
            }
        }

        products.remove(found);
        products.add(product);
        encode(product.getDept());
    }

    public void delete(Product product) {
        Product found = null;
        for (Product p : products) {
            if (p.getId() == product.getId()) {
                found = p;
            }
        }

        products.remove(found);
        encode(product.getDept());
    }

    public List<Product> list() {
        return new ArrayList(products);
    }

    public List<String> departmentList() {
        List<String> departments = new ArrayList();
        for (Product p : products) {
            if (!departments.contains(p.getDept())) {
                departments.add(p.getDept());
            }
        }

        return departments;
    }

    public List<Product> sortByDept(String dept) {
        List<Product> productsByDept = new ArrayList();
        for (Product product : products) {
            if (product.getDept().equals(dept)) {
                productsByDept.add(product);
            }
        }

        return productsByDept;

    }

    public double calculateRetailValueOfInventory() {
        double totalRetailValue = 0;
        for (Product product : products) {
            totalRetailValue += product.calcRetailValue();
        }
        return totalRetailValue;
    }

    public double calculateWholesaleValueOfInventory() {
        double totalWholesaleValue = 0;
        for (Product product : products) {
            totalWholesaleValue += product.calcWholeValue();
        }
        return totalWholesaleValue;
    }

    public double calculatePotentialProfit() {
        double potentialProfit = 0;
        for (Product product : products) {
            potentialProfit += product.calcPotentialProfit();
        }
        return potentialProfit;
    }

    public void encode(String dept) {
        List<Product> productListByDept = new ArrayList();
        for (Product product : products) {
            if (product.getDept().equals(dept)) {
                productListByDept.add(product);
            }
        }

        if ("alcohol".equals(dept)) {
            encodeAlcohol(productListByDept, dept);
        } else if ("produce".equals(dept)) {
            encodeProduce(productListByDept, dept);

        } else if ("dairy".equals(dept)) {
            encodeDairy(productListByDept, dept);

        }

    }

    public void encodeProduce(List<Product> productListByDept, String type) {
        final String TOKEN = "::";
        try {
            DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            PrintWriter out = new PrintWriter(new FileWriter(type + ".txt"));

            for (Product product : productListByDept) {

                Produce produce = (Produce) product;
                out.print(product.getId());
                out.print(TOKEN);
                out.print(product.getName());
                out.print(TOKEN);
                out.print(product.getRetailPrice());
                out.print(TOKEN);
                out.print(product.getWholesalePrice());
                out.print(TOKEN);
                out.print(product.getStock());
                out.print(TOKEN);
                out.print(format.format(produce.getDeliveredOn()));
                out.print(TOKEN);
                out.print(produce.getShelfLife());
                out.println();

            }
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void encodeAlcohol(List<Product> productListByDept, String type) {
        final String TOKEN = "::";
        try {
            DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            PrintWriter out = new PrintWriter(new FileWriter(type + ".txt"));

            for (Product product : productListByDept) {

                Alcohol alcohol = (Alcohol) product;

                out.print(product.getId());
                out.print(TOKEN);
                out.print(product.getName());
                out.print(TOKEN);
                out.print(product.getRetailPrice());
                out.print(TOKEN);
                out.print(product.getWholesalePrice());
                out.print(TOKEN);
                out.print(product.getStock());
                out.print(TOKEN);
                out.print(alcohol.getABV());
                out.print(TOKEN);
                out.print(alcohol.getAlcoholType());
                out.print(TOKEN);
                out.print(alcohol.getStockLevel());
                out.print(TOKEN);
                out.print(format.format(product.getDeliveredOn()));
                out.println();
            }
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void encodeDairy(List<Product> productListByDept, String type) {
        final String TOKEN = "::";
        try {
            DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            PrintWriter out = new PrintWriter(new FileWriter(type + ".txt"));

            for (Product product : productListByDept) {

                Dairy dairyProduct = (Dairy) product;
                out.print(product.getId());
                out.print(TOKEN);
                out.print(product.getName());
                out.print(TOKEN);
                out.print(product.getRetailPrice());
                out.print(TOKEN);
                out.print(product.getWholesalePrice());
                out.print(TOKEN);
                out.print(product.getStock());
                out.print(TOKEN);
                out.print(format.format(dairyProduct.getDeliveredOn()));
                out.print(TOKEN);
                out.print(dairyProduct.getShelfLife());
                out.println();

            }
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public List<Product> decode() {
        File dir = new File(".");
        File[] directoryListing = dir.listFiles();
        List<Product> productList = new ArrayList();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.getName().equals("alcohol.txt")) {
                    productList = decodeAlcohol(child);
                } else if (child.getName().equals("produce.txt")) {
                    productList = decodeProduce(child);
                
                } else if (child.getName().equals("dairy.txt")) {
                    productList = decodeDairy(child);
                }
            }
        }

        return productList;
    }

    public List<Product> decodeAlcohol(File file) {
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(file.toString())));

            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split("::");

                Alcohol alcohol = new Alcohol();

                int id = Integer.parseInt(stringParts[0]);
                double retailPrice = Double.parseDouble(stringParts[2]);
                double wholesalePrice = Double.parseDouble(stringParts[3]);
                int stock = Integer.parseInt(stringParts[4]);
                double ABV = Double.parseDouble(stringParts[5]);
                int stockLevel = Integer.parseInt(stringParts[7]);
                
                DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                Date date = new Date();
                try {
                    date = format.parse(stringParts[8]);
                } catch (ParseException ex) {
                    Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
                }
                alcohol.setDept(file.getName().replace(".txt", ""));
                alcohol.setId(id);
                alcohol.setName(stringParts[1]);
                alcohol.setRetailPrice(retailPrice);
                alcohol.setWholesalePrice(wholesalePrice);
                alcohol.setStock(stock);
                alcohol.setABV(ABV);
                alcohol.setAlcoholType(stringParts[6]);
                alcohol.setStockLevel(stockLevel);
                alcohol.setDeliveredOn(date);

                products.add(alcohol);
                
                nextId = products.size() + 1;
            }
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return products;
    }

    public List<Product> decodeProduce(File file) {
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(file.toString())));

            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split("::");

                Produce produce = new Produce();

                int id = Integer.parseInt(stringParts[0]);
                double retailPrice = Double.parseDouble(stringParts[2]);
                double wholesalePrice = Double.parseDouble(stringParts[3]);
                int stock = Integer.parseInt(stringParts[4]);
                DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                Date date = new Date();
                try {
                    date = format.parse(stringParts[5]);
                } catch (ParseException ex) {
                    Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
                }
                int shelfLife = Integer.parseInt(stringParts[6]);

                produce.setDept(file.getName().replace(".txt", ""));
                produce.setId(id);
                produce.setName(stringParts[1]);
                produce.setRetailPrice(retailPrice);
                produce.setWholesalePrice(wholesalePrice);
                produce.setStock(stock);
                produce.setDeliveredOn(date);
                produce.setShelfLife(shelfLife);

                products.add(produce);
                nextId = products.size() + 1;

            }
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return products;
    }

    public List<Product> decodeDairy(File file) {
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(file.toString())));

            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split("::");

                Dairy dairyProduct = new Dairy();

                int id = Integer.parseInt(stringParts[0]);
                double retailPrice = Double.parseDouble(stringParts[2]);
                double wholesalePrice = Double.parseDouble(stringParts[3]);
                int stock = Integer.parseInt(stringParts[4]);
                DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                Date date = new Date();
                try {
                    date = format.parse(stringParts[5]);
                } catch (ParseException ex) {
                    Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
                }
                int shelfLife = Integer.parseInt(stringParts[6]);

                dairyProduct.setDept(file.getName().replace(".txt", ""));
                dairyProduct.setId(id);
                dairyProduct.setName(stringParts[1]);
                dairyProduct.setRetailPrice(retailPrice);
                dairyProduct.setWholesalePrice(wholesalePrice);
                dairyProduct.setStock(stock);
                dairyProduct.setDeliveredOn(date);
                dairyProduct.setShelfLife(shelfLife);

                products.add(dairyProduct);
                nextId = products.size() + 1;

            }
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return products;
    }
}
