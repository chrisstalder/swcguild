/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productinventory.dto;

/**
 *
 * @author apprentice
 */
public class Alcohol extends Product{
    protected double ABV;
    protected String alcoholType;
    
    public double getABV() {
        return ABV;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public String getAlcoholType() {
        return alcoholType;
    }

    public void setAlcoholType(String alcoholType) {
        this.alcoholType = alcoholType;
    }

   @Override
    public boolean warnRestock(){
        return super.warnRestock(stockLevel);
    }

    
    
}
