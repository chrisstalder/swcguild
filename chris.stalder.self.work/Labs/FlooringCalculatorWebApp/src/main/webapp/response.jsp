<%-- 
    Document   : response
    Created on : May 27, 2016, 10:35:36 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="flooringCalculator.css" rel="stylesheet" type="text/css">
        <title>Flooring Calculator</title>
    </head>
    <body>
        <h1>Flooring Calculator</h1>
        <table>
            <tr>
                <td>
                    Width: 
                </td>
                <td>
                    ${width}<br>
                </td>
            </tr>
            <tr>
                <td>
                    Length: 
                </td>
                <td>
                    ${length}<br>
                </td>
            </tr>
            <tr>
                <td>
                    Cost Per Sq Foot: 
                </td>
                <td>
                    ${cost}<br>
                </td>
            </tr>
            <tr>
                <td>
                    Area: 
                </td>
                <td>
                    ${area}<br>
                </td>
            </tr>
            <tr>
                <td>
                    Total Material Cost: 
                </td>
                <td>
                    ${totalMaterialCost}<br>
                </td>
            </tr>
            <tr>
                <td>
                    Estimated Time: 
                </td>
                <td>
                    ${estimatedTime}<br>
                </td>
            </tr>
            <tr>
                <td>
                    Labor: 
                </td>
                <td>
                    ${laborCost}<br>
                </td>
            </tr>
            <tr>
                <td>
                    Total: 
                </td>
                <td>
                    ${total}<br>
                </td>
            </tr> 
        </table>
        
        <table>
            <form action="FlooringCalculatorServlet" method="POST">
                <tr>
                    <td>
                        Width: 
                    </td>
                    <td>
                        <input type="text" name="width"/><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        Length: 
                    </td>
                    <td>
                        <input type="text" name="length"/><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cost per 1 Sq Foot: 
                    </td>
                    <td>
                        <input type="text" name="cost"/><br><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Submit" id="submitButton"/>
                    </td>
                </tr>  
            </form>
        </table>

            

    </body>
</html>
