/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringcalculatorwebapp.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "FlooringCalculatorServlet", urlPatterns = {"/FlooringCalculatorServlet"})
public class FlooringCalculatorServlet extends HttpServlet {


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        int width = Integer.parseInt(request.getParameter("width"));
        int length = Integer.parseInt(request.getParameter("length"));
        double costPerSqFt = Double.parseDouble(request.getParameter("cost"));
        double area = width * length;
        double totalMaterialCost = costPerSqFt * area;
        int hours= (int)area/20;
        int minutes = (int)area%20;
        String totalTime = hours + "hr:" + minutes +"min";
        int totalTimeInMinutes = (hours*60)+minutes;
        int count = 0;
        while(totalTimeInMinutes>0){
            count++;
            totalTimeInMinutes -= 15;
        }
        
        double billing = 21.5;
        double labor = count * billing;
        double total = totalMaterialCost + labor;
        
        request.setAttribute("width", width);
        request.setAttribute("length", length);
        request.setAttribute("cost", costPerSqFt);
        request.setAttribute("area", area);
        request.setAttribute("totalMaterialCost", totalMaterialCost);
        request.setAttribute("estimatedTime", totalTime);
        request.setAttribute("laborCost", labor);
        request.setAttribute("total", total);
        
        RequestDispatcher rd = request.getRequestDispatcher("response.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
