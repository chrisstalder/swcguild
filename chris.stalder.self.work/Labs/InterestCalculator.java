/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class InterestCalculator {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("How much will you be investing?");
        float principal = keyboard.nextFloat();
        float currentBal = principal;
        
        System.out.println("And what is your annual interest rate?");
        float annualInt = keyboard.nextFloat();
        float quarterInt = (float) (annualInt / 4.0);
        float totalInt = 0f;
        
        System.out.println("And for how long will you be investing?");
        float years = keyboard.nextFloat();
        
        for (int i = 0; i < years; i++){
            float finalBal = (currentBal * (1 + (quarterInt/100)));
            totalInt = finalBal - currentBal;
            System.out.println("Year " + i + "- Beginning Balance: " + currentBal + " Final Balance: " + finalBal + " Interest earned: " + totalInt);
            currentBal = finalBal;
        }
        
    }
}
