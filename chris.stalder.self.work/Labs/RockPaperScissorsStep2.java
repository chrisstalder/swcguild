/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class RockPaperScissorsStep2 {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        
        System.out.println("Rules: Rock beats scissor, paper beat rock, scissors beat paper");
        System.out.println("Please enter your choice starting with a capital letter.");
        
        System.out.println("How many rounds would you like to play?");
        int numOfRounds = keyboard.nextInt();
        
        if (numOfRounds < 1){
            System.out.println("Fine I didn't want to play with you anyways!");
        }else if (numOfRounds > 10){
            System.out.println("That's too many rounds! I don't got time for that, yo!");
        }else{
            for (int i = 1; i <= numOfRounds; i++){
                
                int computer = (int)(Math.random()* 3 + 1);
                
                System.out.println("On shoot. Rock. Paper. Scissors. Shoot!");
                String answer = keyboard.next();
            
                switch (answer){
                    case "Rock":
                        switch (computer){
                            case 1:
                                System.out.println("Computer plays rock! You tied!");
                                break;
                            case 2:
                                System.out.println("Computer plays paper! You lose!");
                                break;
                            case 3:
                                System.out.println("Computer plays scissors! You win!");
                                break;
                        }
                        break;
                    case "Paper":
                        switch (computer){
                            case 1:
                                System.out.println("Computer plays rock! You win!");
                                break;
                            case 2:
                                System.out.println("Computer plays paper! You tie!");
                                break;
                            case 3:
                                System.out.println("Computer plays scissors! You lose!");
                                break;
                        }
                        break;
                    case "Scissors":
                        switch (computer){
                            case 1:
                                System.out.println("Computer plays rock! You lose!");
                                break;
                            case 2:
                                System.out.println("Computer plays paper! You win!");
                                break;
                            case 3:
                                System.out.println("Computer plays scissors! You tie!");
                                break;
                        }
                        break;
                    default:
                        System.out.println(answer + " is not an option!");
                }
            }
        }
    }
}
