/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class RockPaperScissorsStep3 {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        int wins = 0;
        int losses = 0; //counters for w/t/l
        int ties = 0;
        
        
        System.out.println("Rules: Rock beats scissor, paper beat rock, scissors beat paper");
        System.out.println("Please enter your choice starting with a capital letter.");
        
        System.out.println("How many rounds would you like to play?");
        int numOfRounds = keyboard.nextInt();   //input for number of rounds
        
        
        //if player inputs a number that is not between 1 and 10, game will not play
        if (numOfRounds < 1){
            System.out.println("Fine I didn't want to play with you anyways!");
        }else if (numOfRounds > 10){
            System.out.println("That's too many rounds! I don't got time for that, yo!");
        }else{
            for (int i = 1; i <= numOfRounds; i++){ //while i is <= number of rounds, the player keeps playing
                
                int computer = (int)(Math.random()* 3 + 1); //randomly generated number, computers choice
                
                System.out.println("On shoot. Rock. Paper. Scissors. Shoot!");
                String answer = keyboard.next();
            
                //the whole game in nested switch statements
                switch (answer){
                    case "Rock":
                        switch (computer){
                            case 1:
                                System.out.println("Computer plays rock! You tied!");
                                ties++;
                                break;
                            case 2:
                                System.out.println("Computer plays paper! You lose!");
                                losses++;
                                break;
                            case 3:
                                System.out.println("Computer plays scissors! You win!");
                                wins++;
                                break;
                        }
                        break;
                    case "Paper":
                        switch (computer){
                            case 1:
                                System.out.println("Computer plays rock! You win!");
                                wins++;
                                break;
                            case 2:
                                System.out.println("Computer plays paper! You tie!");
                                ties++;
                                break;
                            case 3:
                                System.out.println("Computer plays scissors! You lose!");
                                losses++;
                                break;
                        }
                        break;
                    case "Scissors":
                        switch (computer){
                            case 1:
                                System.out.println("Computer plays rock! You lose!");
                                losses++;
                                break;
                            case 2:
                                System.out.println("Computer plays paper! You win!");
                                wins++;
                                break;
                            case 3:
                                System.out.println("Computer plays scissors! You tie!");
                                ties++;
                                break;
                        }
                        break;
                    default:
                        System.out.println(answer + " is not an option!");
                }
            }
            
            System.out.println("You won " + wins + " times! You lost " + losses + " times! And you tied " + ties + " times!");
            
            if (wins > losses){
                System.out.println("You beat the computer! Good job!");
            }else if (wins < losses){
                System.out.println("You lost to the computer! Loser!");
            }else{
                System.out.println("You tied the computer!");
            }
        }
    }
}
