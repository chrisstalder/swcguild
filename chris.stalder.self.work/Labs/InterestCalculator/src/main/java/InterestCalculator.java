/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class InterestCalculator {
    static Scanner keyboard = new Scanner(System.in);

    public void run() {
        boolean isValid = false;
        float principal = 0f, annualInt = 0f;
        int compoundedPeriod = 0, years = 0;
        
        while (!isValid){
            
            String input = userInput("How much will you be investing?");
            
            try{
                principal = Float.parseFloat(input);
                isValid = true;
            } catch(Exception ex) {    
            }
            
        }
        
        float currentBal = principal;
        isValid = false;
        while (!isValid){
            
            String input = userInput("And what is your annual interest rate?");
            
            try{
                annualInt = Float.parseFloat(input);
                isValid = true;
            } catch(Exception ex) {    
            }
            
        }
        
        isValid = false;
        while (!isValid){
            
            String input = userInput("And what will be your compound periods? (Quarterly, monthly, daily)");
            
            try{
                compoundedPeriod = Integer.parseInt(input);
                isValid = true;
            } catch(Exception ex) {    
            }
            
        }

        float periodInt = periodInt(annualInt, compoundedPeriod);

        isValid = false;
        while (!isValid){
            
            String input = userInput("And for how long will you be investing?");
            
            try{
                years = Integer.parseInt(input);
                isValid = true;
            } catch(Exception ex) {    
            }
            
        }
        
        calculateBalance(years, currentBal, periodInt);
        
    }

    /**
     *
     * @param years
     * @param currentBal
     * @param periodInt
     */
    public void calculateBalance(float years, float currentBal, float periodInt) {
        float totalInt;
        for (int i = 0; i < years; i++){
            float finalBal = (currentBal * (1 + (periodInt/100)));
            totalInt = finalBal - currentBal;
            System.out.println("Year " + i + "- Beginning Balance: " + currentBal + " Final Balance: " + finalBal + " Interest earned: " + totalInt);
            currentBal = finalBal;
        }
    }
    
    public String userInput(String prompt){
        System.out.println(prompt);
        String input = keyboard.nextLine();
        
        return input;
    }
    
    public float periodInt(float intRate, float numOfPeriods){
        float periodInt = (intRate / numOfPeriods);
        return periodInt;
    }
}
