/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Order;
import java.util.List;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public interface OrderDao {
    
    public Order create(Order order);
    
    public List<Order> get(String string);
    
    public void update(String date, List<Order> orderList);
    
    public void delete(Order order);
    
    public Set<String> list();    
    
    public Order get(int globalId);
}
