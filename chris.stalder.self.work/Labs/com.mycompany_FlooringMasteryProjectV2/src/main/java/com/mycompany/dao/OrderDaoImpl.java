/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Order;
import com.mycompany.dto.Product;
import com.mycompany.dto.Tax;
import com.mycompany.interfaces.ProductDao;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mycompany.interfaces.OrderDao;

/**
 *
 * @author apprentice
 */
public class OrderDaoImpl implements OrderDao {

    Map<String, List<Order>> orderMap = new HashMap();
    Set<String> orderMapKeySet = orderMap.keySet();
    int globalId = 1;

    public OrderDaoImpl() {
        orderMap = decode();
    }

    public void setGlobalId() {
        for (String string : orderMapKeySet) {
            List<Order> orderLists = orderMap.get(string);
            for (Order orderList : orderLists) {
                globalId++;
            }
        }
    }

    @Override
    public Order create(Order order) {
        order.setGlobalId(globalId);
        globalId++;
        List<Order> list = new ArrayList();
        int nextId = 1;
        for (String string : orderMapKeySet) {
            if (string.equals(order.getDate())) {
                list = orderMap.get(string);
                nextId = list.size() + 1;
            }
        }
        order.setOrderId(nextId);
        list.add(order);
        update(order.getDate(), list);
        return order;
    }

    @Override
    public List<Order> get(String stringDate) {

        Set<String> keySet = orderMap.keySet();
        List<Order> orderList = new ArrayList();
        for (String string : keySet) {
            if (string.equals(stringDate)) {
                orderList = orderMap.get(string);
                return orderList;
            }
        }
        return null;
    }

    @Override
    public Order get(int globalId) {
        for (String string : orderMapKeySet) {
            List<Order> orderListPerDay = orderMap.get(string);
            for (Order order : orderListPerDay) {
                if (order.getGlobalId() == globalId) {
                    return order;
                }
            }
        }
        return null;
    }

    @Override
    public void update(String date, List<Order> orderList) {

        orderMap.remove(date);
        orderMap.put(date, orderList);
        encode();
    }

    @Override
    public void delete(Order order) {
        Order found = new Order();
        List<Order> list = new ArrayList();
        int nextId = 1;
        for (String string : orderMapKeySet) {
            if (string.equals(order.getDate())) {
                list = orderMap.get(string);
                for (Order orderToRemove : list) {
                    if (orderToRemove.getGlobalId() == order.getGlobalId()) {
                        found = orderToRemove;
                    }
                }
            }
        }

        list.remove(found);
        update(order.getDate(), list);

    }

    public void testSave(List<Order> testList) {
        final String TOKEN = ",";

        try {
            PrintWriter out = new PrintWriter(new FileWriter("test.txt"));

            out.println("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");

            for (Order order : testList) {
                out.print(order.getDate());
                out.print(TOKEN);
                out.print(order.getOrderId());
                out.print(TOKEN);
                out.print(order.getCustomerName());
                out.print(TOKEN);
                out.print(order.getState());
                out.print(TOKEN);
                out.print(order.getTaxRate());
                out.print(TOKEN);
                out.print(order.getProduct().getProductType());
                out.print(TOKEN);
                out.print(order.getArea());
                out.print(TOKEN);
                out.print(order.getCostPerSqFoot());
                out.print(TOKEN);
                out.print(order.getLaborCostPerSqFoot());
                out.print(TOKEN);
                out.print(order.getMaterialCost());
                out.print(TOKEN);
                out.print(order.getLaborCost());
                out.print(TOKEN);
                out.print(order.getTax());
                out.print(TOKEN);
                out.print(order.getTotal());
                out.print(TOKEN);
                out.print(order.getGlobalId());
                out.println();
            }

            out.flush();
            out.close();

        } catch (IOException ex) {
            Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void encode() {
        final String TOKEN = ",";

        for (String string : orderMapKeySet) {
            File directory = new File("./Orders");
            try {
                File file = new File(directory, "ORDERS_" + string + ".txt");
                PrintWriter out = new PrintWriter(new FileWriter(file));
                List<Order> orders = orderMap.get(string);

                out.println("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");

                for (Order order : orders) {
                    out.print(order.getOrderId());
                    out.print(TOKEN);
                    String name = order.getCustomerName().replaceAll("(,)", "~,");
                    out.print(name);
                    out.print(TOKEN);
                    out.print(order.getState());
                    out.print(TOKEN);
                    out.print(order.getTaxRate());
                    out.print(TOKEN);
                    out.print(order.getProduct().getProductType());
                    out.print(TOKEN);
                    out.print(order.getArea());
                    out.print(TOKEN);
                    out.print(order.getCostPerSqFoot());
                    out.print(TOKEN);
                    out.print(order.getLaborCostPerSqFoot());
                    out.print(TOKEN);
                    out.print(order.getMaterialCost());
                    out.print(TOKEN);
                    out.print(order.getLaborCost());
                    out.print(TOKEN);
                    out.print(order.getTax());
                    out.print(TOKEN);
                    out.print(order.getTotal());
                    out.print(TOKEN);
                    out.print(order.getGlobalId());
                    out.println();
                }

                out.flush();
                out.close();

            } catch (IOException ex) {
                Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Set<String> list() {
        Set<String> keySet = orderMap.keySet();
        return keySet;
    }

    public Map<String, List<Order>> getMap() {
        Map<String, List<Order>> newMap = orderMap;
        return newMap;
    }

    private File deleteEmptyFile(String fileName) {

        File f = new File(fileName);

        if (!f.exists()) {
            throw new IllegalArgumentException("Delete: no such file or directory: " + fileName);
        }

        if (!f.canWrite()) {
            throw new IllegalArgumentException("Delete: write protected");
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String fileString = fileName.substring(7, 15);
            try {
                br.readLine();
                if (br.readLine() == null) {
                    boolean success = f.delete();
                    if (!success) {
                        throw new IllegalArgumentException("Delete: deletion failed");
                    }

                }
            } catch (IOException ex) {
                Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return f;
    }

    private Map<String, List<Order>> decode() {

        ProductDao pDao = new ProductDaoImpl();
        File dir = new File("./Orders");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {

                List<Order> orderList = new ArrayList();
                try {

                    Scanner sc = new Scanner(new BufferedReader(new FileReader(child.toString())));
                    File f = deleteEmptyFile(child.toString());
                    if (f.exists()) {
                        sc.nextLine();
                        while (sc.hasNextLine()) {

                            String currentLine = sc.nextLine();

                            String checkedLine = currentLine.replaceAll("(~,)", "");

                            String[] stringParts = checkedLine.split(",");

                            int id = Integer.parseInt(stringParts[0]);
                            int global = Integer.parseInt(stringParts[12]);
                            Tax stateTax = new Tax();
                            stateTax.setState(stringParts[2]);

                            double taxRate = Double.parseDouble(stringParts[3]);

                            Product product = new Product();

                            List<Product> productList = pDao.list();

                            for (Product product1 : productList) {
                                if (stringParts[4].equals(product1.getProductType())) {
                                    product = product1;
                                }
                            }

                            double area = Double.parseDouble(stringParts[5]);
                            double costPerSqFoot = Double.parseDouble(stringParts[6]);
                            double laborCostPerSqFoot = Double.parseDouble(stringParts[7]);
                            double matCost = Double.parseDouble(stringParts[8]);
                            double labCost = Double.parseDouble(stringParts[9]);
                            double tax = Double.parseDouble(stringParts[10]);
                            double total = Double.parseDouble(stringParts[11]);

                            setGlobalId();

                            Order order = new Order();
                            order.setGlobalId(globalId);
                            order.setOrderId(id);
                            order.setCustomerName(stringParts[1]);
                            order.setState(stateTax.getState());
                            order.setTaxRate(taxRate);
                            order.setProduct(product);
                            order.setArea(area);
                            order.setCostPerSqFoot(costPerSqFoot);
                            order.setLaborCostPerSqFoot(laborCostPerSqFoot);
                            order.setMaterialCost(matCost);
                            order.setLaborCost(labCost);
                            order.setTax(tax);
                            order.setTotal(total);
                            order.setDate(child.toString().replace("./Orders/ORDERS_", "").replace(".txt", ""));
                            order.setGlobalId(global);

                            orderList.add(order);
                            globalId = orderList.size() + 1;
                        }
                        orderMap.put((child.toString().replace("./Orders/ORDERS_", "").replace(".txt", "")), orderList);

                    }

                    sc.close();

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return orderMap;
    }

}
