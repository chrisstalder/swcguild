/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

import java.util.Date;

/**
 *
 * @author apprentice
 */
public class Order {
    private int orderId;
    private int globalId;
    private String customerName;
    private String state;
    private double taxRate;
    private Product product;
    private double area;
    private double laborCostPerSqFoot;
    private double costPerSqFoot;
    private double laborCost;
    private double materialCost;
    private double tax;
    private double total;
    private String date;

    public int getOrderId() {
        return orderId;
    }

    public int getGlobalId() {
        return globalId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getState() {
        return state;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public Product getProduct() {
        return product;
    }

    public double getArea() {
        return area;
    }

    public double getLaborCostPerSqFoot() {
        return laborCostPerSqFoot;
    }

    public double getCostPerSqFoot() {
        return costPerSqFoot;
    }

    public double getLaborCost() {
        return laborCost;
    }

    public double getMaterialCost() {
        return materialCost;
    }

    public double getTax() {
        return tax;
    }

    public double getTotal() {
        return total;
    }

    public String getDate() {
        return date;
    }

    
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setGlobalId(int globalId) {
        this.globalId = globalId;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setLaborCostPerSqFoot(double laborCostPerSqFoot) {
        this.laborCostPerSqFoot = laborCostPerSqFoot;
    }

    public void setCostPerSqFoot(double costPerSqFoot) {
        this.costPerSqFoot = costPerSqFoot;
    }

    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }

    public void setMaterialCost(double materialCost) {
        this.materialCost = materialCost;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    
}
