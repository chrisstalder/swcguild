/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aspects;

import org.aspectj.lang.annotation.After;

/**
 *
 * @author apprentice
 */
public class Timing {
    private long startTime;
    private long endTime;
    
    public void startTime(){
        startTime = System.currentTimeMillis();
    }
    
    public void totalTime(){
        endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("It took " + totalTime + " milliseconds to execute.");
    }
}
