/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;
import com.mycompany.consoleio.ConsoleIO;
import com.mycompany.dao.AdminDao;
import com.mycompany.dao.OrderDaoImpl;
import com.mycompany.dao.TaxDaoImpl;
import com.mycompany.dto.Order;
import com.mycompany.dto.Product;
import com.mycompany.dto.Tax;
import com.mycompany.interfaces.ProductDao;
import com.mycompany.interfaces.TaxDao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import com.mycompany.interfaces.OrderDao;

/**
 *
 * @author apprentice
 */
public class OrderController {

    ConsoleIO console = new ConsoleIO();
    AdminDao adminDao;
    OrderDao orderDao;
    ProductDao productDao;
    TaxDao taxDao;
    boolean test;
    private List<Order> testList = new ArrayList();

    public OrderController(AdminDao adminDao, OrderDao orderDao, ProductDao productDao, TaxDao taxDao) {
        this.adminDao = adminDao;
        this.orderDao = orderDao;
        this.productDao = productDao;
        this.taxDao = taxDao;
    }

    public void run(AdminController admin) {

        boolean repeat = true;
        while (repeat) {

            List<String> menuItems = new ArrayList();
            menuItems.add("Display Orders");
            menuItems.add("Add an Order");
            menuItems.add("Edit an Order");
            menuItems.add("Remove an Order");
            menuItems.add("Save Current Work");
            menuItems.add("Admin");
            menuItems.add("Quit");
            console.menu(menuItems, "Flooring Program");
            test = adminDao.decode();
            if (test) {
                console.print("TEST MODE");
            }

            String input = console.getString();
            boolean isValid = false;
            int selection = 0;
            while (!isValid) {
                try {
                    selection = Integer.parseInt(input);
                    isValid = true;
                } catch (Exception e) {
                    console.print("Invalid entry.");
                    input = console.getString();
                }
            }
            switch (selection) {
                case 1:
                    displayOrders();
                    break;
                case 2:
                    newOrder();
                    break;
                case 3:
                    editOrder();
                    break;
                case 4:
                    removeOrder();
                    break;
                case 5:
                    saveWork();
                    break;
                case 6:
                    repeat = admin.run();
                    break;
                case 7:
                    console.print("Good Bye.");
                    repeat = false;
                    break;
                default:
                    console.print("Invalid Entry.");
            }
        }

    }

    public List<Order> displayOrders() {
        List<Order> orderList = new ArrayList();
        Set<String> keySet = orderDao.list();
        if (keySet.size() > 0) {
            console.print("===========================");
            for (String string : keySet) {
                System.out.println(string);
            }
            console.print("===========================");
            console.print("Enter the date: ");
            Date date = console.tryParse();
            boolean validEntry = false;
            while (!validEntry) {
                if (date != null) {
                    validEntry = true;
                } else {
                    console.print("Invalid Entry.");
                    console.print("Enter the date: ");
                    date = console.tryParse();
                }
            }
            String stringDate = console.dateToString(date);
            orderList = orderDao.get(stringDate);

            if (orderList != null) {
                console.print("==================================================================================");
                for (Order order : orderList) {
                    console.print("----------------------------------------------------------------------------------");
                    System.out.println(order.getOrderId() + " " + order.getCustomerName() + " " + order.getState() + " " + order.getTaxRate() + " " + order.getProduct().getProductType() + " "
                            + order.getArea() + " " + order.getCostPerSqFoot() + " " + order.getLaborCostPerSqFoot() + " " + order.getMaterialCost() + " "
                            + order.getLaborCost() + " " + order.getTax() + " " + order.getTotal());
                    console.print("----------------------------------------------------------------------------------");
                }
                console.print("==================================================================================");
            } else {
                console.print("There are no orders for that day.");
            }
        } else {
            console.print("There are no orders.");
        }
        return orderList;
    }

    public Order getOrderId(List<Order> orderList) {
        boolean isValid = false;
        Order order = null;
        while (!isValid) {
            if (orderList != null) {

                console.print("Enter the ID: ");
                int id = console.getIntValue();
                for (Order myOrder : orderList) {
                    if (myOrder.getOrderId() == id) {
                        order = myOrder;
                        isValid = true;
                    }
                }

                if (order == null) {
                    console.print("Invalid Entry.");
                }
            }
        }
        return order;
    }

    public void newOrder() {

        Order order = new Order();
        console.print("Input customer name: ");
        boolean isValid = false;
        String name = null;
        while (!isValid) {
            name = console.getString();
            if (name != null) {
                isValid = true;
            } else {
                console.print("Invalid Entry.");
                name = console.getString();
            }
        }
        displayValidStates();
        console.print("Input State: ");
        String state = console.getState();
        boolean validState = validateState(state);
        while (!validState) {
            console.print("Invalid Entry.");
            state = console.getState();
            validState = validateState(state);
        }
        Tax taxRate = getTax(state);
        listProducts();
        console.print("Input product type: ");
        String type = console.getString();
        Product myProduct = getProduct(type);

        console.print("Input area of floor: ");
        double area = console.getDoubleValue();
        boolean validArea = false;
        while (!validArea) {
            if (area > 0) {
                validArea = true;
            } else {
                console.print("Invalid Entry.");
                area = console.getDoubleValue();
            }
        }
        myProduct = getCosts(myProduct, area);

        double tax = ((myProduct.getMaterialCost() + myProduct.getLaborCost()) * taxRate.getTaxRate()) / 100;
        double total = myProduct.getMaterialCost() + myProduct.getLaborCost() + tax;

        DateFormat df = new SimpleDateFormat("MMM dd, yyyy");
        Date dateObject = new Date();

        console.print("Your current date is " + (df.format(dateObject)));
        console.print("Would you like to change the date?");
        boolean answer = console.getYesNo();

        String dateObjectString = console.dateToString(dateObject);
        if (answer) {
            console.print("Enter the new date (mm/dd/yyyy)");
            Date date = console.tryParse();
            String stringDate = console.dateToString(date);
            dateObjectString = stringDate;
        }

        order.setCustomerName(name);
        order.setState(state);
        order.setTaxRate(taxRate.getTaxRate());
        order.setProduct(myProduct);
        order.setArea(area);
        order.setCostPerSqFoot(myProduct.getCostPerSqFoot());
        order.setLaborCostPerSqFoot(myProduct.getLaborPerSqFoot());
        order.setMaterialCost(myProduct.getMaterialCost());
        order.setLaborCost(myProduct.getLaborCost());
        order.setTax(tax);
        order.setTotal(total);
        order.setDate(dateObjectString);

        orderSummary(order);
        console.print("Do you wish to continue?(y/n)");
        answer = console.getYesNo();

        if (answer) {

            if (!test) {
                orderDao.create(order);
                console.print("Order placed.");
            } else {
                testList.add(order);
            }
        }
    }

    public void editOrder() {
        List<Order> orderList = displayOrders();
        if (orderList.size() > 0) {
            Order order = getOrderId(orderList);
            orderList.remove(order);
            console.print("Customer name (" + order.getCustomerName() + "): ");
            String name = console.getString();
            if (name == null) {
                order.setCustomerName(order.getCustomerName());
            }

            console.print("State (" + order.getState() + "): ");
            String state = console.getString();
            if ("".equals(state)) {
                order.setState(order.getState());
            } else {
                boolean validState = validateState(state);
                while (!validState) {
                    console.print("Invalid Entry.");
                    state = console.getState();
                    validState = validateState(state);
                }
                order.setState(state);
                Tax tax = getTax(state);
                order.setTaxRate(tax.getTaxRate());
            }

            order = editTypeAndArea(order);

            console.print("Date (" + order.getDate() + "): ");
            String dateObjectString = null;
            Date date = console.tryParse();
            if (date == null) {
                dateObjectString = order.getDate();
            } else {
                dateObjectString = console.dateToString(date);
            }

            Set<String> keySet = orderDao.list();

            List<Order> list = new ArrayList();
            List<Order> newList = new ArrayList();
            int nextId = 1;
            for (String string : keySet) {
                if (string.equals(dateObjectString)) {
                    list = orderDao.get(string);
                    for (Order myOrder : list) {
                        if (myOrder.getGlobalId() == order.getGlobalId()) {
                            list.remove(myOrder);
                        } else {
                            newList.add(myOrder);
                        }
                    }
                    nextId = list.size() + 1;
                    order.setOrderId(nextId);
                }
            }
            order.setDate(dateObjectString);
            newList.add(order);
            if (!test) {
                orderDao.update(dateObjectString, newList);
            }
        } else {
            console.print("There are no orders for that day.");
        }
    }

    public void removeOrder() {
        List<Order> orderList = displayOrders();
        Order order = getOrderId(orderList);
        console.print(order.getOrderId() + " " + order.getCustomerName() + " " + order.getState() + " " + order.getTaxRate() + " " + order.getProduct().getProductType() + " "
                + order.getArea() + " " + order.getCostPerSqFoot() + " " + order.getLaborCostPerSqFoot() + " " + order.getMaterialCost() + " "
                + order.getLaborCost() + " " + order.getTax() + " " + order.getTotal());
        console.print("Are you sure that you want to delete this order?(y/n)");
        boolean answer = console.getYesNo();
        if (answer) {
            if (!test) {
                console.print("Removing Order.");
                orderDao.delete(order);
            }
        }

    }

    public void saveWork() {
        if (test) {
            OrderDaoImpl save = new OrderDaoImpl();
            save.testSave(testList);
            console.print("Saving test info to test.txt");
        }
    }

    public List<Product> listProducts() {
        List<Product> products = new ArrayList();
        products = productDao.list();
        for (Product product : products) {
            System.out.println(product.getProductType() + " " + product.getCostPerSqFoot() + " " + product.getLaborPerSqFoot());
        }
        return products;
    }

    public Product getProduct(String productType) {
        List<Product> products = productDao.list();
        Product myProduct = new Product();
        boolean isValid = false;
        int i = 0;
        while (!isValid) {
            for (Product product : products) {
                if (productType.equals(product.getProductType())) {
                    myProduct = product;
                    isValid = true;
                    i++;
                }
            }
            if (i == 0) {
                console.print("Invalid Entry.");
                productType = console.getString();
            }
        }
        return myProduct;
    }

    public Product getCosts(Product myProduct, double area) {
        double materialCost = area * myProduct.getCostPerSqFoot();
        double laborCost = area * myProduct.getLaborPerSqFoot();
        myProduct.setMaterialCost(materialCost);
        myProduct.setLaborCost(laborCost);
        return myProduct;
    }

    public Tax getTax(String state) {
        List<Tax> taxes = taxDao.list();
        Tax tax = new Tax();
        for (Tax myTax : taxes) {
            if (state.equals(myTax.getState())) {
                tax = myTax;
            }
        }

        return tax;
    }

    public Order editTypeAndArea(Order order) {

        Product myProduct = new Product();

        listProducts();
        console.print("Product Type (" + order.getProduct().getProductType() + "): ");

        String type = console.getString();

        if (!type.equals("")) {
            myProduct = getProduct(type);
        } else {
            myProduct = order.getProduct();
        }

        console.print("Area of floor: (" + order.getArea() + " sq.ft): ");
        Double area = console.getArea();

        if (area != 0) {
            order.setArea(area);
        } else {
            area = order.getArea();
        }

        myProduct = getCosts(myProduct, area);
        double tax = ((myProduct.getMaterialCost() + myProduct.getLaborCost()) * order.getTaxRate()) / 100;
        double total = myProduct.getMaterialCost() + myProduct.getLaborCost() + tax;

        order.setProduct(myProduct);
        order.setCostPerSqFoot(myProduct.getCostPerSqFoot());
        order.setLaborCostPerSqFoot(myProduct.getLaborPerSqFoot());
        order.setMaterialCost(myProduct.getMaterialCost());
        order.setLaborCost(myProduct.getLaborCost());
        order.setTax(tax);
        order.setTotal(total);

        return order;
    }

    public void orderSummary(Order order) {
        console.print("Customer name: " + order.getCustomerName());
        console.print("State: " + order.getState());
        System.out.println("Tax Rate: " + order.getTaxRate());
        console.print("Product Type: " + order.getProduct().getProductType());
        System.out.println("Area: " + order.getArea());
        System.out.println("Cost Per Sq Foot: " + order.getCostPerSqFoot());
        System.out.println("Cost of Labor Per Sq Foot: " + order.getLaborCostPerSqFoot());
        System.out.println("Total Material Cost: " + order.getMaterialCost());
        System.out.println("Total Labor Cost: " + order.getLaborCost());
        System.out.println("Tax: " + order.getTax());
        System.out.println("Total: " + order.getTotal());

    }

    public boolean validateState(String state) {
        boolean isValid = false;
        TaxDaoImpl instance = new TaxDaoImpl();
        List<String> validStates = new ArrayList();
        validStates = instance.listValidStates();
        for (String validState : validStates) {
            if (validState.equals(state)) {
                isValid = true;
            }
        }
        return isValid;
    }

    public void displayValidStates() {
        List<String> stateList = new ArrayList();
        TaxDaoImpl instance = new TaxDaoImpl();
        stateList = instance.listValidStates();
        for (String string : stateList) {
            console.print(string);
        }
    }

}
