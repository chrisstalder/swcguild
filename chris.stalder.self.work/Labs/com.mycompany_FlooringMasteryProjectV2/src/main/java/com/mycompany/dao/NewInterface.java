/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

/**
 *
 * @author apprentice
 */
public interface NewInterface {

    boolean currentFileType();

    boolean currentMode();

    void setCurrentFileType(String fileType);

    void setMode(String mode);
    
}
