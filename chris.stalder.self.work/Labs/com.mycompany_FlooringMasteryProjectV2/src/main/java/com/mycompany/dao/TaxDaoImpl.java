/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Tax;
import com.mycompany.interfaces.TaxDao;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class TaxDaoImpl implements TaxDao {
    
    private List<Tax> taxes = new ArrayList();
    private List<String> validStates = new ArrayList();
    private int nextId = taxes.size() + 1;
    
    public TaxDaoImpl(){
        taxes = decode();
    }
    @Override
    public Tax Create(Tax tax) {
        tax.setId(nextId);
        nextId++;
        taxes.add(tax);
        validStates.add(tax.getState());
        encode();
        
        return tax;
    }

    @Override
    public Tax get(String state) {
        for (Tax tax : taxes) {
            if(state.equals(tax.getState()))
                return tax;
        }
        
        return null;
    }

    @Override
    public void update(Tax tax) {
        Tax found = null;
        
        for (Tax t : taxes) {
            if(tax.getId() == t.getId())
                found = t;
        }
        taxes.remove(found);
        taxes.add(tax);
        encode();
        
    }

    @Override
    public void delete(Tax tax) {
        Tax found = null;
        
        for (Tax t : taxes) {
            if(tax.getId() == t.getId())
                found = t;
        }
        taxes.remove(found);
        validStates.remove(tax.getState());
        encode();
    }

    @Override
    public List<Tax> list() {
        return taxes;
    }
    
    private void encode(){
        
        final String TOKEN = ",";
        try {
            PrintWriter out = new PrintWriter(new FileWriter("taxes.txt"));
            
            for (Tax tax : taxes) {
                out.print(tax.getState());
                out.print(TOKEN);
                out.print(tax.getTaxRate());
                out.println();
            }
            
            out.flush();
            out.close();
            
            
        } catch (IOException ex) {
            Logger.getLogger(TaxDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private List<Tax> decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("taxes.txt")));
            
            while (sc.hasNextLine()){
                
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split(",");
                
                double taxRate = Double.parseDouble(stringParts[1]);
                
                Tax tax = new Tax();
                
                tax.setState(stringParts[0]);
                tax.setTaxRate(taxRate);
                
                validStates.add(tax.getState());
                taxes.add(tax);
                
                nextId = taxes.size() + 1;
            }
            
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TaxDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return taxes;
        
    }
    
    public List<String> listValidStates(){
        return validStates;
    }
    
}
