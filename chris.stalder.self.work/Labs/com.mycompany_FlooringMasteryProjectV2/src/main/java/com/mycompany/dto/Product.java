/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

/**
 *
 * @author apprentice
 */
public class Product {
    private int id;
    private String productType;
    private double costPerSqFoot;
    private double laborPerSqFoot;
    private double materialCost;
    private double laborCost;

    public int getId() {
        return id;
    }

    public String getProductType() {
        return productType;
    }

    public double getCostPerSqFoot() {
        return costPerSqFoot;
    }

    public double getLaborPerSqFoot() {
        return laborPerSqFoot;
    }

    public double getMaterialCost() {
        return materialCost;
    }

    public double getLaborCost() {
        return laborCost;
    }

    
    public void setId(int id) {
        this.id = id;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setCostPerSqFoot(double costPerSqFoot) {
        this.costPerSqFoot = costPerSqFoot;
    }

    public void setLaborPerSqFoot(double laborPerSqFoot) {
        this.laborPerSqFoot = laborPerSqFoot;
    }

    public void setMaterialCost(double materialCost) {
        this.materialCost = materialCost;
    }

    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }
    
    
}
