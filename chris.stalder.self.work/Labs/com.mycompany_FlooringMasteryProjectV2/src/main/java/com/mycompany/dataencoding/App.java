/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dataencoding;

import com.mycompany.controllers.AdminController;
import com.mycompany.controllers.OrderController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        OrderController controller = ctx.getBean("orderController", OrderController.class);
        AdminController admin = ctx.getBean("adminController", AdminController.class);
        controller.run(admin);

    }
}
