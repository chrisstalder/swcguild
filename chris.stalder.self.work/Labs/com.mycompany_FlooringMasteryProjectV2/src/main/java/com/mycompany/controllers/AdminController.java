/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.consoleio.ConsoleIO;
import com.mycompany.dao.AdminDao;
import com.mycompany.dao.PasswordDao;
import com.mycompany.dao.TaxDaoImpl;
import com.mycompany.dto.Product;
import com.mycompany.dto.Tax;
import com.mycompany.interfaces.ProductDao;
import com.mycompany.interfaces.TaxDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class AdminController {

    ConsoleIO console = new ConsoleIO();
    AdminDao adminDao;
    TaxDao taxDao;
    ProductDao productDao;
    PasswordDao passwordDao;
    boolean test;

    public AdminController(AdminDao adminDao, TaxDao taxDao, ProductDao productDao, PasswordDao passwordDao) {
        this.adminDao = adminDao;
        this.taxDao = taxDao;
        this.productDao = productDao;
        this.passwordDao = passwordDao;
    }

    public boolean run() {

        String password = passwordDao.decode();
        console.print("Enter admin password.");
        boolean adminPassword = console.getPassword(password);
        if (adminPassword) {
            boolean repeat = true;
            while (repeat) {
                test = adminDao.decode();

                List<String> menuItems = new ArrayList();
                menuItems.add("List Products");
                menuItems.add("List Taxes");
                menuItems.add("Create Product");
                menuItems.add("Create Tax");
                menuItems.add("Edit Product");
                menuItems.add("Edit Tax");
                menuItems.add("Delete Product");
                menuItems.add("Delete Tax");
                menuItems.add("Set Mode");
                menuItems.add("Change Password");
                menuItems.add("Quit");
                console.menu(menuItems, "ADMIN MENU");

                if (test) {
                    console.print("TEST MODE");
                }
                String selection = console.getString();
                boolean isValid = false;
                int input = 0;
                while (!isValid) {
                    try {
                        input = Integer.parseInt(selection);
                        isValid = true;
                    } catch (Exception e) {
                        console.print("Invalid entry.");
                        selection = console.getString();
                    }
                }

                switch (input) {
                    case 1:
                        listProducts();
                        break;
                    case 2:
                        listTaxes();
                        break;
                    case 3:
                        createProduct();
                        break;
                    case 4:
                        createTax();
                        break;
                    case 5:
                        editProduct();
                        break;
                    case 6:
                        editTax();
                        break;
                    case 7:
                        deleteProduct();
                        break;
                    case 8:
                        deleteTax();
                        break;
                    case 9:
                        setMode();
                        break;
                    case 10:
                        changePassword();
                        break;
                    case 11:
                        repeat = false;
                        break;
                    default:
                        console.print("Invalid Entry.");
                }
            }
            return true;
        } else {
            return true;
        }
    }

    public List<Product> listProducts() {
        List<Product> products = productDao.list();
        for (Product product : products) {
            System.out.println(product.getProductType() + " " + product.getCostPerSqFoot() + " " + product.getLaborPerSqFoot());
        }
        return products;
    }

    public void listTaxes() {
        List<Tax> taxes = taxDao.list();
        for (Tax tax : taxes) {
            console.print(tax.getState() + " - " + tax.getTaxRate());
        }
    }

    public void createTax() {
        TaxDaoImpl states = new TaxDaoImpl();
        List<String> validStates = states.listValidStates();
        Tax tax = new Tax();
        boolean isValid = false;
        String myState = null;

        for (String validState : validStates) {
            console.print(validState);
        }
        console.print("These are the states we currently have taxes for.");
        while (!isValid) {
            int i = 0;
            console.print("Input State");
            myState = console.getState();
            for (String state : validStates) {
                if (myState.equals(state)) {
                    isValid = false;
                    i++;
                }
            }
            if (i == 0) {
                isValid = true;
            }
        }
        console.print("Input tax rate");
        double taxRate = console.getDoubleValue();
        boolean validTax = false;
        while (!validTax) {
            if ((taxRate > 0) && (taxRate < 20)) {
                validTax = true;
            } else {
                console.print("Invalid Entry.");
                taxRate = console.getDoubleValue();
            }
        }
        tax.setState(myState);
        tax.setTaxRate(taxRate);
        if (!test) {
            taxDao.Create(tax);
        }
    }

    public void createProduct() {
        Product myProduct = new Product();
        boolean isValid = false;
        List<Product> products = productDao.list();
        String type = null;
        double costPerSqFoot = 0;
        double laborCostPerSqFoot = 0;
        while (!isValid) {
            console.print("Product Type: ");
            int i = 0;
            type = console.getString();
            for (Product product : products) {
                if (type.equals(product.getProductType())) {
                    console.print("That product already exists.");
                    type = console.getString();
                    i++;
                }
            }
            if (i == 0) {
                isValid = true;
            }
        }
        isValid = false;
        while (!isValid) {
            console.print("Cost Per Sq Foot: ");
            costPerSqFoot = console.getDoubleValue();
            if ((costPerSqFoot > 0) && (costPerSqFoot < 100)) {
                isValid = true;
            }
        }
        isValid = false;
        while (!isValid) {
            console.print("Cost of Labor Per Sq Foot: ");
            laborCostPerSqFoot = console.getDoubleValue();
            if ((laborCostPerSqFoot > 0) && (laborCostPerSqFoot < 100)) {
                isValid = true;
            }
        }
        myProduct.setProductType(type);
        myProduct.setCostPerSqFoot(costPerSqFoot);
        myProduct.setLaborPerSqFoot(laborCostPerSqFoot);
        if (!test) {
            productDao.Create(myProduct);
        }
    }

    public void editProduct() {
        List<Product> products = productDao.list();
        listProducts();
        console.print("Enter product type: ");
        String type = console.getString();
        Product myProduct = getProduct(type);
        console.print("Material Cost Per Sq Foot (" + myProduct.getCostPerSqFoot() + ") : ");
        double costPerSqFoot = console.validateDouble();
        console.print("Labor Cost Per Sq Foot (" + myProduct.getLaborPerSqFoot() + ") : ");
        double laborCostPerSqFoot = console.validateDouble();
        myProduct.setCostPerSqFoot(costPerSqFoot);
        myProduct.setLaborPerSqFoot(laborCostPerSqFoot);
        if (!test) {
            productDao.update(myProduct);
        }
    }

    public void editTax() {
        List<Tax> taxes = taxDao.list();
        for (Tax tax : taxes) {
            console.print(tax.getState() + " | " + tax.getTaxRate());
        }
        double taxRate = 0;
        console.print("Enter the State: ");
        String state = console.getState();
        boolean validState = validateState(state);
        while (!validState) {
            console.print("Invalid Entry.");
            state = console.getState();
            validState = validateState(state);
        }
        Tax tax = getTax(state);
        console.print("Tax rate (" + tax.getTaxRate() + ") : ");
        console.print("Do you wish to edit this?");
        boolean answer = console.getYesNo();
        if (answer) {
            console.print("Enter new tax rate: ");
            taxRate = console.validateDouble();
            tax.setTaxRate(taxRate);
            if (!test) {
                taxDao.update(tax);
            }
        }

    }

    public void deleteProduct() {
        listProducts();
        console.print("Enter product type: ");
        String type = console.getString();
        Product myProduct = getProduct(type);
        console.print("Are you sure you want to delete " + myProduct.getProductType() + "?(y/n)");
        boolean answer = console.getYesNo();
        if ((answer) && (!test)) {
            productDao.delete(myProduct);
            console.print("Product deleted.");
        }
    }

    public void deleteTax() {
        TaxDaoImpl instance = new TaxDaoImpl();
        List<String> validStates = instance.listValidStates();
        for (String validState : validStates) {
            console.print(validState);
        }
        console.print("Enter state: ");
        String state = console.getState();
        boolean validState = validateState(state);
        while (!validState) {
            console.print("Invalid Entry.");
            state = console.getState();
            validState = validateState(state);
        }
        Tax tax = getTax(state);
        console.print("Are you sure you want to delete the tax rate for " + tax.getState() + "?(y/n)");
        boolean answer = console.getYesNo();
        if ((answer) && (!test)) {
            taxDao.delete(tax);
            console.print("Tax deleted.");
        }
    }

    public void setMode() {
        if (test == true) {
            console.print("Currently, you are in test mode.");
            console.print("Would you like to enter production mode?");
            boolean answer = console.getYesNo();
            if (answer) {
                adminDao.setMode("production");
            }

        } else if (test == false) {
            console.print("Currently, you are in production mode.");
            console.print("Would you like to enter test mode?");
            boolean answer = console.getYesNo();
            if (answer) {
                adminDao.setMode("test");
            }
        }
    }

    public void changePassword() {
        console.print("Enter new password");
        String password = console.getString();
        passwordDao.setPassword(password);
    }

    public Product getProduct(String productType) {
        List<Product> products = new ArrayList();
        Product myProduct = new Product();
        products = productDao.list();
        boolean isValid = false;
        int i = 0;
        while (!isValid) {
            for (Product product : products) {
                if (productType.equals(product.getProductType())) {
                    myProduct = product;
                    isValid = true;
                    i++;
                }
            }
            if (i == 0) {
                console.print("Invalid Entry.");
                productType = console.getString();
            }
        }
        return myProduct;
    }

    public boolean validateState(String state) {
        boolean isValid = false;
        TaxDaoImpl instance = new TaxDaoImpl();
        List<String> validStates = new ArrayList();
        validStates = instance.listValidStates();

        for (String validState : validStates) {
            if (validState.equals(state)) {
                isValid = true;
            }
        }
        return isValid;
    }

    public Tax getTax(String state) {
        List<Tax> taxes = taxDao.list();
        Tax tax = new Tax();
        for (Tax myTax : taxes) {
            if (state.equals(myTax.getState())) {
                tax = myTax;
            }
        }
        return tax;
    }
}
