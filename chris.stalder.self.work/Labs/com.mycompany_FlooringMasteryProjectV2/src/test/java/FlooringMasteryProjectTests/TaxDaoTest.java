/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FlooringMasteryProjectTests;

import com.mycompany.dao.TaxDaoImpl;
import com.mycompany.dto.Tax;
import com.mycompany.interfaces.TaxDao;
import java.util.List;
//import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author apprentice
 */
public class TaxDaoTest {
    TaxDao tDao = new TaxDaoImpl();
    List<Tax> list = tDao.list();
    int nextId = list.size() + 1;
    Tax tax = new Tax();
    
    public TaxDaoTest() {
    }
    
    @Before
    public void setUp() {
        tax.setId(1);
        tax.setState("OH");
        tax.setTaxRate(6.25);
    }
    
    @After
    public void tearDown() {
        
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCreate(){
        
        
        Tax result = tDao.Create(tax);
        
        Assert.assertEquals(tax, result);
        
    }
    
    @Test
    public void testGet(){
        String state = "OH";
        Tax result = tDao.get(state);
        Assert.assertEquals(tax.getTaxRate(), result.getTaxRate(), 0.0);
    }
    
    @Test
    public void testUpdate(){
        
        
        Tax result = new Tax();
        for (Tax tax1 : list) {
            if (tax.getId() == tax1.getId())
                result = tax1;
        }
        result.setTaxRate(6.50);
        tDao.update(result);
        Assert.assertEquals(6.50, result.getTaxRate(), 0.0);
    }
    
    @Test
    public void testDelete(){
        tDao.delete(tax);
        List<Tax> result = list;
        Assert.assertFalse(result.contains(tax));
    }
}
