/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.statecapitals2;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class StateCapitals2 {
    
    
    public static Capital setCapital(String name, int population, double sqrMiles){
        Capital capital = new Capital();
        capital.getName();
        capital.setName(name);
        capital.getPopulation();
        capital.setPopulation(population);
        capital.getSqrMiles();
        capital.setSqrMiles(sqrMiles);
        
        return capital;
    }
    
    public void stateAndCapitals(Map<String, Capital> states){
        Set<String> keySet = states.keySet();
        Iterator i = keySet.iterator();

        while(i.hasNext()){
   
            String key = (String) i.next();
            Capital value = states.get(key);
            System.out.println("===========");
            System.out.println(key + ":");
            System.out.println("===========");
            System.out.println("Capital Name: " + value.getName());
            System.out.println("Population: " + value.getPopulation());
            System.out.println("Square Miles: " +value.getSqrMiles());
            System.out.println("===========");
        }
    }  
    public void capitalsOfMinPop(Map<String, Capital> states, String prompt){
        Scanner keyboard = new Scanner(System.in);
        Set<String> keySet = states.keySet();
        Iterator i = keySet.iterator();
        int population = 0;
        boolean isValid = false;
        
        while(!isValid){
            System.out.println(prompt);
            String answer = keyboard.nextLine();
            try{
                    population = Integer.parseInt(answer);
                    isValid = true;
                } catch(Exception ex) {    
                }
        }

        while(i.hasNext()){
            
            String key = (String) i.next();
            Capital value = states.get(key);
            if (value.getPopulation() >= population){
                System.out.println("===========");
                System.out.println(key + ":");
                System.out.println("===========");
                System.out.println("Capital Name: " + value.getName());
                System.out.println("Population: " + value.getPopulation());
                System.out.println("Square Miles: " +value.getSqrMiles());
                System.out.println("===========");
            }
        }
    }  
}
