/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.statecapitals2;

/**
 *
 * @author apprentice
 */
public class Capital {
    
    private String name;
    private Integer population;
    private Double sqrMiles;
    
    public void setName(String name) {
        this.name = name;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public void setSqrMiles(Double sqrMiles) {
        this.sqrMiles = sqrMiles;
    }
   
    
    
    public String getName() {
        return name;
    }

    public Integer getPopulation() {
        return population;
    }

    public Double getSqrMiles() {
        return sqrMiles;
    }

    
    
}
