/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.statecapitals2;

import static com.mycompany.statecapitals2.StateCapitals2.setCapital;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class App {
    
    public static void main(String[] args) {
        Map<String, Capital> states = new HashMap();
        StateCapitals2 sc = new StateCapitals2();
        
        Capital montgomery = setCapital("Montgomery", 201332, 156.2);
        Capital juneau = setCapital("Juneau", 32660, 3255);
        Capital phoenix = setCapital("Phoenix", 3252000, 517);
        Capital littleRock = setCapital("Little Rock", 197357, 116.8);
        Capital sacramento = setCapital("Sacramento", 479686, 101.1);
        Capital denver = setCapital("Denver", 649495, 151);
        
        states.put("Alabama", montgomery);
        states.put("Alaska", juneau);
        states.put("Arizona", phoenix);
        states.put("Arkansas", littleRock);
        states.put("California", sacramento);
        states.put("Colorado", denver);

        sc.stateAndCapitals(states);
        
        sc.capitalsOfMinPop(states, "What is the minimum population?");
    }
}
