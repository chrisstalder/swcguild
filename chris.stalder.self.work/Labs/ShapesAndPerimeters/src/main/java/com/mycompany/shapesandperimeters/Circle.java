package com.mycompany.shapesandperimeters;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Circle extends Shape{
    public double area(double radius){
        double area = (radius * radius)*Math.PI;
        return area;
    }
    
    public double perimeter(double radius){
        double perimeter = 2 * Math.PI * radius;
        return perimeter;
    }
}
