package com.mycompany.shapesandperimeters;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Rectangle extends Shape{
    public double area(double width, double height){
        double area = width * height;
        return area;
    }
    
    public double perimeter(double width, double height){
        double perimeter = (width + height) * 2;
        return perimeter;
    }
}
