package com.mycompany.shapesandperimeters;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Square extends Shape{
    
    public double area(double sideLength){
        double area = sideLength * sideLength;
        return area;
    }
    
    public double perimeter(double sideLength){
        double perimeter = sideLength * 4;
        return perimeter;
    }
}
