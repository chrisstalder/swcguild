package com.mycompany.shapesandperimeters;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Triangle extends Shape{
    public double area(double base, double height){
        double area = (base * height)/2;
        return area;
    }
    
    public double perimeter(double a, double b, double c){
        double perimeter = a + b + c;
        return perimeter;
    }
}
