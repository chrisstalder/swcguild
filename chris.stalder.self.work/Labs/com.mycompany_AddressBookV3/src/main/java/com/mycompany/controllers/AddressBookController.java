/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.consoleio.ConsoleIO;
import com.mycompany.dao.AddressBookDao;
import com.mycompany.dto.Address;
import com.mycompany.dto.Person;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class AddressBookController {
    ConsoleIO console = new ConsoleIO();
    AddressBookDao addressDao;
    
    
    public AddressBookController(AddressBookDao dao){
        this.addressDao = dao;
    }
    
    public void run(SearchController search){
        
        boolean repeat = true;
              
        while(repeat){
            int selection;
            console.print("==============================");
            console.print("Welcome to Your Address Book!");
            console.print("==============================");
            console.print("What would you like to do?");
            console.print("1) add");
            console.print("2) remove");
            console.print("3) find");
            console.print("4) list");
            console.print("5) total");
            console.print("6) search");
            console.print("7) edit");
            console.print("8) exit");
            console.print("==============================");
            
            selection = console.getIntValue();
            
            switch(selection){
                case 1:
                    addAddress();
                    break;
                case 2:
                    removeAddress();
                    break;
                case 3:
                    findAddress();
                    break;
                case 4:
                    listAddresses();
                    break;
                case 5:
                    totalAddresses();
                    break;
                case 6:
                    repeat = search.run();
                    break;
                case 7:
                    editAddress(search);
                    break;
                case 8:
                    repeat = false;
                    break;
                default:
                    console.print("That wasn't an option!");
            }
        }
    }
    
    public void addAddress(){
        boolean isValid = false;
        String firstName = null;
        String lastName = null;
        int houseNum = 0;
        String street = null;
        String city = null;
        String state = null;
        String zip = null;
        String aptNum = null;
        String country = null;
        
        console.print("Please enter your new contact's first name:");
        while(!isValid){
                firstName = console.getString();
                if(firstName instanceof String)
                    isValid = true;
                else
                    console.print("That is not a valid name"); 
        }
        isValid = false;
        
        console.print("Please enter your new contact's last name:");
        while(!isValid){
                lastName = console.getString();
                if(lastName instanceof String)
                    isValid = true;
                else
                    console.print("That is not a valid name"); 
        }
        isValid = false;
        
        console.print("Please enter your new contact's house number:");
        houseNum = console.getIntValue();
        
        console.print("Please enter your new contact's street:");
        while(!isValid){
                street = console.getString();
                if(street instanceof String)
                    isValid = true;
                else
                    console.print("That is not a valid street name"); 
        }
        isValid = false;
        
        console.print("Please enter your new contact's city:");
        while(!isValid){
                city = console.getString();
                if(city instanceof String)
                    isValid = true;
                else
                    console.print("That is not a valid city name"); 
        }
        isValid = false;
        
        console.print("Please enter your new contact's state:");
        while(!isValid){
                state = console.getState();
                if(state instanceof String)
                    isValid = true;
                else
                    console.print("That is not a valid state name"); 
        }
        isValid = false;
        
        console.print("Please enter your new contact's zipcode:");
        zip = console.getString();
        
        console.print("Please enter your new contact's apartment number (if applicable):");
        aptNum = console.getString();
        
        console.print("Please enter your new contact's country:");
        while(!isValid){
                country = console.getString();
                if(country instanceof String)
                    isValid = true;
                else
                    console.print("That is not a valid name"); 
        }
 
        Address address = new Address();
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        address.setPerson(person);
        address.setHouseNumber(houseNum);
        address.setStreetName(street);
        address.setCity(city);
        address.setState(state);
        address.setZipcode(zip);
        address.setApartment(aptNum);
        address.setCountry(country);
        
        addressDao.create(address);
        addressDao.create(address);
    }
    
    public void removeAddress(){
        List<Address> list = addressDao.list();
        boolean isValid = false;
        int input = 0;
        
        console.print("Please enter the ID of the contact you wish to remove.");
        while(!isValid){
            input = console.getIntValue();
            for (Address address : list) {
                if(address.getId() == input)
                    isValid = true;
            }
        }
        Address found = new Address();
        
        for (Address address : list) {
            if (address.getId() == input)
                found = address;
        }
        addressDao.delete(found);
        addressDao.delete(found);
    }
    
    public void findAddress(){
        List<Address> list = addressDao.list();
//        List<Address> lambdaList = addressDao.list();
        
        console.print("Please enter the last name of the contact.");
        String lastName = console.getString();
        
        for (Address address : list) {

            if (address.getPerson().getLastName().equals(lastName)){
                console.print("=============================================================================");
                console.print(address.getId() + " | " + address.getPerson().getFirstName() + " " + address.getPerson().getLastName());
                console.print("  |" + address.getHouseNumber() + " " + address.getStreetName());
                console.print("  |" + address.getCity() + ", " + address.getState() + ", " + address.getZipcode() + ", " + address.getCountry());
                console.print("=============================================================================");
            }
        }
       
    }
    
    public void listAddresses(){
       List<Address> list = addressDao.list();
       
        for (Address address : list) {
            console.print("=============================================================================");
            console.print(address.getId() + " | " + address.getPerson().getFirstName() + " " + address.getPerson().getLastName());
            console.print("  |" + address.getHouseNumber() + " " + address.getStreetName());
            console.print("  |" + address.getCity() + ", " + address.getState() + ", " + address.getZipcode() + ", " + address.getCountry());  
            console.print("=============================================================================");
        }
    }
    
    public void totalAddresses(){
         List<Address> list = addressDao.list();
        console.print("=======================================================================");
        console.print("You have a total of " + list.size() + " addresses in your address book.");
        console.print("=======================================================================");
        
    }
    
    public void editAddress(SearchController search){
        List<Address> addresses = addressDao.list();
        Address address = new Address();
        Person person = new Person();
        if(addresses.size() > 0){
            search.listAddresses(addresses);
            console.print("Input ID of Address you wish to edit: ");
            int id = console.getIntValue();
            for (Address a : addresses) {
                if(a.getId() == id)
                    address = a;
            }
            
            console.print("Person's first name (" + address.getPerson().getFirstName() + "): ");
            String firstName = console.getString();
            if(!"".equals(firstName)){
                person.setFirstName(firstName);    
            }else
                person.setFirstName(address.getPerson().getFirstName());
            
            console.print("Person's last name (" + address.getPerson().getLastName() + "): ");
            String lastName = console.getString();
            if(!"".equals(lastName)){
                person.setLastName(lastName);    
            }else
                person.setLastName(address.getPerson().getLastName());
            
            address.setPerson(person);
            
            console.print("Street number (" + address.getHouseNumber()+ "): ");
            String house = console.getString();
            if("".equals(house)){
                address.setHouseNumber(address.getHouseNumber());
            }else{
                int houseNum = Integer.parseInt(house);
                
                address.setHouseNumber(houseNum);
            }

            
            console.print("Street name (" + address.getStreetName() + "): ");
            String street = console.getString();
            if("".equals(street)){
                street = address.getStreetName();
            }
            
            console.print("City (" + address.getCity() + "): ");
            String city = console.getString();   
            if("".equals(city)){
                city = address.getCity();
            }
            
            console.print("State (" + address.getState() + "): ");
            String state = console.getString();
            if("".equals(state)){
                state = address.getState();
            }
            
            console.print("Zipcode (" + address.getZipcode()+ "): ");
            String zip = console.getString();
            if("".equals(zip)){
                zip = address.getZipcode();
            }
            
            console.print("Apt Number (" + address.getApartment()+ "): ");
            String apt = console.getString();
            if("".equals(apt)){
                apt = address.getApartment();
            }
            
            console.print("Country (" + address.getCountry()+ "): ");
            String country = console.getString();
            if("".equals(country)){
                country = address.getCountry();
            }

            address.setStreetName(street);
            address.setCity(city);
            address.setState(state);
            address.setZipcode(zip);
            address.setApartment(apt);
            address.setCountry(country);
            
            addressDao.update(address);
        }
    }

}
