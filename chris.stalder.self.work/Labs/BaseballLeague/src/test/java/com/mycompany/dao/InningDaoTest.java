/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Inning;
import com.mycompany.interfaces.InningDaoInterface;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InningDaoTest {
    
    public InningDaoTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCreate(){
        InningDaoInterface instance = new InningDao();
        Inning inning = new Inning();
        int homeScore = 2;
        int awayScore = 1;
        inning.setHomeScore(homeScore);
        inning.setAwayScore(awayScore);
 
        Inning result = instance.create(inning);
        
        Assert.assertEquals(inning, result);
    }
    
    @Test
    public void testGet(){
        InningDao instance = new InningDao();
        int number = 1;
        
        Inning inning = new Inning();
        inning.setNum(number);
        
        Inning expected = instance.create(inning);
        Inning result = instance.get(number);
        
        Assert.assertSame(result, expected);
    }
    
    @Test
    public void testUpdate(){
        InningDao instance = new InningDao();
        List<Inning> list = instance.list();
        int nextId = list.size();
        Inning inning = new Inning();
        inning.setNum(nextId);
        int homeScore = 2;
        int awayScore = 1;
        inning.setHomeScore(homeScore);
        inning.setAwayScore(awayScore);
        
        
        for (Inning inning1 : list) {
            if(inning1.getNum() == inning.getNum())
                inning = inning1;
        }
        
        inning.setHomeScore(3);
        instance.update(inning);
        
        int result = inning.getHomeScore();
        
        Assert.assertEquals(3, result);
    }
    
    @Test
    public void testDelete(){
        InningDao instance = new InningDao();
        List<Inning> list = instance.list();
        
        int currentArraySize = list.size();
        
        Inning inning = new Inning();
        for (Inning inning1 : list) {
            if(inning1.getNum() == inning.getNum())
                inning = inning1;
        }
        
        instance.delete(inning);
        boolean result = false;
        for (Inning inning1 : list) {
            if(inning1.getNum() == inning.getNum())
                result = true;
        }
        
        Assert.assertFalse(result);
    }
}
