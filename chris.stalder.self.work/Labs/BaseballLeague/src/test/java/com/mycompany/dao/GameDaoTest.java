/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Game;
import com.mycompany.dto.Team;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class GameDaoTest {
    
    public GameDaoTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class GameDao.
     */
   @Test
    public void testCreate(){
        GameDao instance = new GameDao();
        Team away = new Team();
        Team home = new Team();
        Game game = new Game();
        game.setId(1);
        game.setAway(away);
        game.setHome(home);
        game.setAwayScore(3);
        game.setHomeScore(5);
        
//        Game expected = instance.create(game);
        
        Game result = instance.create(game);
        
        Assert.assertEquals(game, result);
    }
    
    @Test
    public void testGet(){
        GameDao instance = new GameDao();
        int id = 1;
        Team away = new Team();
        Team home = new Team();
        Game game = new Game();
        game.setId(id);
        game.setAway(away);
        game.setHome(home);
        game.setAwayScore(3);
        game.setHomeScore(5);
        
        Game expected = instance.create(game);
        Game result = instance.get(id);
        
        Assert.assertSame(result, expected);
    }
    
    @Test
    public void testUpdate(){
        GameDao instance = new GameDao();
        List<Game> list = instance.list();
        int nextId = list.size();
        Game game = new Game();
        game.setId(nextId);
        int homeScore = 2;
        int awayScore = 1;
        game.setHomeScore(homeScore);
        game.setAwayScore(awayScore);
        
        
        for (Game game1 : list) {
            if(game1.getId() == game.getId())
                game = game1;
        }
        
        game.setHomeScore(3);
        instance.update(game);
        
        int result = game.getHomeScore();
        
        Assert.assertEquals(3, result);
    }
    
    @Test
    public void testDelete(){
        GameDao instance = new GameDao();
        List<Game> list = instance.list();
        
        int currentArraySize = list.size();
        
        Game game = new Game();
        for (Game game1 : list) {
            if(game1.getId() == game.getId())
                game = game1;
        }
        
        instance.delete(game);
        boolean result = false;
        for (Game game1 : list) {
            if(game1.getId() == game.getId())
                result = true;
        }
        
        Assert.assertFalse(result);
    }
    
}
