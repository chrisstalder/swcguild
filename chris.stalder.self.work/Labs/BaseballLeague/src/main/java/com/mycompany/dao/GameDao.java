/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Game;
import com.mycompany.dto.Inning;
import com.mycompany.dto.Team;
import com.mycompany.interfaces.GameDaoInterface;
import com.mycompany.interfaces.TeamDaoInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class GameDao implements GameDaoInterface{
    
    TeamDaoInterface tDao = new TeamDao();
    private List<Game> games = new ArrayList();
    private int nextId = games.size() + 1;
    
    public GameDao(){
        games = decode();
    }
    @Override
    public Game create(Game game) {
        game.setId(nextId);
        nextId++;
        games.add(game);
        encode();
        return game;
    }

   
    @Override
    public Game get(int id) {
        for (Game game : games) {
            if(id == game.getId())
                return game;
        }
        return null;
    }

    @Override
    public void update(Game game) {
        Game found = null;
        for (Game game1 : games) {
            if(game1.getId() == game.getId())
                found = game1;
        }
        games.remove(found);
        games.add(game);
    }

    @Override
    public void delete(Game game) {
        Game found = null;
        for (Game game1 : games) {
            if(game1.getId() == game.getId())
                found = game1;
        }
        games.remove(found);
        encode();
    
    }

    @Override
    public List<Game> list() {
        return games;
    }
    
    private void encode(){
        
        final String TOKEN = "::";
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("games.txt"));
            
            for (Game game : games) {
                out.print(game.getId());
                out.print(TOKEN);
                out.print(game.getHome().getTeamName());
                out.print(TOKEN);
                out.print(game.getHomeScore());
                out.print(TOKEN);
                out.print(game.getAway().getTeamName());
                out.print(TOKEN);
                out.print(game.getAwayScore());
                out.println();
            }
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(GameDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private List<Game> decode(){
        
        Scanner sc;
        try {
            sc = new Scanner(new BufferedReader(new FileReader("games.txt")));
            
            while (sc.hasNextLine()){
                Game game = new Game();
                
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split("::");
                
                int id = Integer.parseInt(stringParts[0]);
              
                
                List<Team> teamList = tDao.list();
                Team team = new Team();
                String homeTeam = stringParts[1];
                for (Team myTeam : teamList) {
                    if(homeTeam.equals(myTeam.getTeamName())){
                        team = myTeam;
                        game.setHome(team);
                    }
                }
                
                int homeScore = Integer.parseInt(stringParts[2]);
                
                Team team2 = new Team();
                String awayTeam = stringParts[3];
                for (Team team1 : teamList) {
                    if(awayTeam.equals(team1.getTeamName())){
                        team2 = team1;
                        game.setAway(team2);
                    }
                }
                
                int awayScore = Integer.parseInt(stringParts[4]);
                
                game.setId(id);
                game.setHomeScore(homeScore);
                game.setAwayScore(awayScore);
            
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GameDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return games;
    }
    
}
