/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Team {
    private int teamId;
    private String teamName;
    private List<Player> playerList = new ArrayList();
    private int wins;
    private int losses;
    private double teamAvg;
    private League league;

    public void setLeague(League league) {
        this.league = league;
    }

    public League getLeague() {
        return league;
    }
    

    public int getTeamId() {
        return teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public double getTeamAvg() {
        return teamAvg;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public void setTeamAvg(double teamAvg) {
        this.teamAvg = teamAvg;
    }
    
}
