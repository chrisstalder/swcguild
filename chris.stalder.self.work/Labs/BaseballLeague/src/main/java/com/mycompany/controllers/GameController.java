/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.dao.GameDao;
import com.mycompany.dao.TeamDao;
import com.mycompany.dto.Game;
import com.mycompany.dto.Inning;
import com.mycompany.dto.Team;
import com.mycompany.interfaces.GameDaoInterface;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class GameController {
    
    public void run(){
        
        ConsoleIO console = new ConsoleIO();
        TeamDao tDao = new TeamDao();
        GameDao gDao = new GameDao();
        
        List<Team> teamList = tDao.list();
        Team home = new Team();
        Team away = new Team();
        boolean playAgain = true;
        boolean isValid = false;
        boolean anotherValidation = false;
        String homeTeam = null;
        String awayTeam = null;
        
        
        if (teamList.size() < 2){
                System.out.println("You need more teams to play!");
                playAgain = false;
        }
        
        while(playAgain){
            int counter = 0;
            
            for (Team team : teamList) {
                if (team.getPlayerList().size()>=9){
                    System.out.println("-------------------------------");
                    System.out.println(team.getTeamId() + " | " + team.getTeamName() + " | " + team.getTeamAvg());
                    System.out.println("-------------------------------");
                    counter++;
                }
            }
            
            if(counter >= 2){
                System.out.println("Which team would you like to play?");
                while(!anotherValidation){
                    System.out.println("Home: ");
                    Team validateTeam = new Team();
                    while(!isValid){
                        homeTeam = console.getString();
                        for (Team team : teamList) {
                            if (team.getTeamName().equals(homeTeam)){
                                isValid = true;
                                validateTeam = team;
                            }
                        }
                    }
                    isValid = false;
                    if (validateTeam.getPlayerList().size() < 9)
                        System.out.println("You need more players on this team to play!");
                    else
                        anotherValidation = true;
                }
                anotherValidation = false;

                for (Team team : teamList) {
                    if(homeTeam.equals(team.getTeamName()))
                        home = team;
                }


                while(!anotherValidation){
                    System.out.println("Away: ");
                    Team validateTeam = new Team();
                    while(!isValid){
                        awayTeam = console.getString();
                        for (Team team : teamList) {
                            if (team.getTeamName().equals(awayTeam)){
                                isValid = true;
                                validateTeam = team;
                            }
                        }
                    }
                    isValid = false;
                    if (validateTeam.getPlayerList().size() < 9)
                        System.out.println("You need more players on this team to play!");
                    else
                        anotherValidation = true;
                }
                anotherValidation = false;

                for (Team team : teamList) {
                    if(awayTeam.equals(team.getTeamName()))
                        away = team;
                }

                Game game = createGame(home, away);

                System.out.println("The winner is " + game.getWinner().getTeamName() + "!");
                System.out.println("Would you like to play again? (y/n)");
                String input = console.getString();
                if("y".equals(input))
                    playAgain = true;
                else if ("n".equals(input))
                    playAgain = false;
            }else{
                System.out.println("There must be 9 players on a team to play. Create more players!");
                playAgain = false;
            } 
        }
    }
    
    public Game createGame(Team home, Team away){
        InningController iController = new InningController();
        GameDao gDao = new GameDao();
        GameDaoInterface gDaoInterface = gDao;
        Game game = new Game();
        game.setHome(home);
        game.setAway(away);
        int homeScore = game.getHomeScore();
        int awayScore = game.getAwayScore();

        int numOfInnings = 9;
        for (int i = 0; i <=numOfInnings; i++) {
            int numOfScores = iController.numOfScores();
            Inning inning = iController.calculate(numOfScores, home, away);
            homeScore += inning.getHomeScore();
            awayScore += inning.getAwayScore();
        }

        while(homeScore == awayScore){
            numOfInnings = 1;
            for (int i = 0; i <=numOfInnings; i++) {
                int numOfScores = iController.numOfScores();
                Inning inning = iController.calculate(numOfScores, home, away);
                homeScore += inning.getHomeScore();
                awayScore += inning.getAwayScore();
                if(homeScore == awayScore)
                    numOfInnings++;
            }
        }

        game.setHomeScore(homeScore);
        game.setAwayScore(awayScore);

        if (homeScore > awayScore){
            game.setWinner(home);
            game.setLoser(away);
        }else if (homeScore < awayScore){
            game.setWinner(away);
            game.setLoser(home);
        }
        
        calculateWinner(game, homeScore, awayScore);
        
        gDaoInterface.create(game);
        return game;
    }
    
    public void calculateWinner(Game game, int homeScore, int awayScore) {
        
        Team home = game.getHome();
        Team away = game.getAway();
        
        if (homeScore > awayScore){
            game.setWinner(home);
            game.setLoser(home);
        }
        else if (homeScore < awayScore){
            game.setWinner(away);
            game.setLoser(home);
        }
            
    }

}
