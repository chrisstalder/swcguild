/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Inning;
import com.mycompany.interfaces.InningDaoInterface;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class InningDao implements InningDaoInterface{
    List<Inning> innings = new ArrayList();
    private int nextId = innings.size()+1;
    
    @Override
    public Inning create(Inning inning) {
        inning.setNum(nextId);
        nextId++;
        innings.add(inning);
        encode();
        return inning;
    }

    @Override
    public Inning get(int number) {
        for (Inning inning : innings) {
            if(number == inning.getNum())
                return inning;
        }
        return null;
    }

    @Override
    public void update(Inning inning) {
        Inning found = null;
        for (Inning inning1 : innings) {
            if (inning1.getNum() == inning.getNum())
                found = inning1;
        }
        
        innings.remove(found);
        innings.add(inning);
        encode();
    }

    @Override
    public void delete(Inning inning) {
        Inning found = null;
        for (Inning inning1 : innings) {
            if (inning1.getNum() == inning.getNum())
                found = inning1;
        }
        
        innings.remove(found);
        
        encode();
    }

    @Override
    public List<Inning> list() {
        return innings;
    }
    
    
    public void encode(){
        
    }
    
    public void decode(){
        
    }

    
    
}
