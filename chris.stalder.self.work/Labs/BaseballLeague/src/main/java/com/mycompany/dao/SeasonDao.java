/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Season;
import com.mycompany.dto.Team;
import com.mycompany.interfaces.SeasonDaoInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class SeasonDao implements SeasonDaoInterface{
    private List<Season> seasons = new ArrayList();
    private int nextId = seasons.size() + 1;
    
    public SeasonDao(){
        seasons = decode();
    }
    
    @Override
    public Season create(Season season) {
        season.setId(nextId);
        nextId++;
        seasons.add(season);
        encode();
        return season;
    }

    @Override
    public Season get(int id) {
        for (Season season : seasons) {
            if(id == season.getId())
                return season;
        }
        return null;
    }

    @Override
    public void update(Season season) {
        Season found = null;
        for (Season season1 : seasons) {
            if(season1.getId() == season.getId())
                found = season1;
        }
        seasons.remove(found);
        seasons.add(season);
        encode();
    }

    @Override
    public void delete(Season season) {
        Season found = null;
        for (Season season1 : seasons) {
            if(season1.getId() == season.getId())
                found = season1;
        }
        seasons.remove(found);
        encode();
    }

    @Override
    public List<Season> list() {
        return seasons;
    }
    
    private void encode(){
        
        final String TOKEN = "::";
        try {
            PrintWriter out = new PrintWriter(new FileWriter("seasons.txt"));
            
            for (Season season : seasons) {
                
                out.print(season.getId());
                out.print(TOKEN);
                out.print(season.getChampion().getTeamName());
                out.println();
            }
            
            out.flush();
            out.close();
            
        } catch (IOException ex) {
            Logger.getLogger(SeasonDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private List<Season> decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("seasons.txt")));
            
            while(sc.hasNextLine()){
                TeamDao tDao = new TeamDao();
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split("::");
                
                int id = Integer.parseInt(stringParts[0]);
                Team champions = new Team();
                List<Team> list = tDao.list();
                for (Team team : list) {
                    if (stringParts[1].equals(team.getTeamName()))
                        champions = team;
                }
                
                Season season = new Season();
                season.setId(id);
                season.setChampion(champions);
                
                seasons.add(season);
                nextId = seasons.size() + 1;
            }
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SeasonDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return seasons;
    }
    
}
