/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Season;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface SeasonDaoInterface {
    public Season create(Season season);
    public Season get(int id);
    public void update(Season season);
    public void delete(Season season);
    public List<Season> list();
}
