/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Player;
import com.mycompany.dto.Team;
import com.mycompany.interfaces.PlayerDaoInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class PlayerDao implements PlayerDaoInterface{
    private List<Player> playerList = new ArrayList();
    private int nextId = 1 + playerList.size();
    
    public PlayerDao(){
        playerList = decode();
    }
    
    public Player create(Player player){
        player.setPlayerId(nextId);
        nextId++;
//        player.setAvg(generateAvg());
        playerList.add(player);
        encode();
        return player;
    }
    
    public Player get(int id){
        
        for (Player player : playerList) {
            if (player.getPlayerId() == id)
                return player;
        }
        
        return null;
        
    }
    
    public void update(Player player){
        
        Player found = new Player();
        for (Player player1 : playerList) {
            if (player1.getPlayerId() == player.getPlayerId())
                found = player1;
        }
        
        playerList.remove(found);
        playerList.add(player);
        encode();
        
    }
    
    public void delete(Player player){
        
        Player found = new Player();
        for (Player player1 : playerList) {
            if (player1.getPlayerId() == player.getPlayerId())
                found = player1;
        }
        
        playerList.remove(found);
        encode();
    }
    
    public List<Player> list(){
        return playerList;
    }
    
    public double generateAvg(){
        Random r = new Random();
        
        double avg = r.nextDouble();
        
        return avg;
    }
    
    public void encode(){
        
        PrintWriter out = null;
        try {
            final String TOKEN = "::";
            out = new PrintWriter(new FileWriter("players.txt"));
            for (Player player : playerList) {
                
                out.print(player.getPlayerId());
                out.print(TOKEN);
                out.print(player.getName());
                out.print(TOKEN);
                out.print(player.getTeam().getTeamName());
                out.print(TOKEN);
                out.print(player.getJersey());
                out.print(TOKEN);
                out.print(player.getAvg());
                out.println();
            }
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(PlayerDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    
    
    public List<Player> decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("players.txt")));
            
            while(sc.hasNextLine()){
                
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split("::");
                
                int id = Integer.parseInt(stringParts[0]);
                int jersey = Integer.parseInt(stringParts[3]);
                double avg = Double.parseDouble(stringParts[4]);
                
                Team team = new Team();
                team.setTeamName(stringParts[2]);
                
                Player player = new Player();
                player.setPlayerId(id);
                player.setName(stringParts[1]);
                player.setTeam(team);
                player.setJersey(jersey);
                player.setAvg(avg);
                playerList.add(player);
                nextId = 1 + playerList.size();
                
            }
            
            sc.close();
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PlayerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return playerList;
    }
    
}
