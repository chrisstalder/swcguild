/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.League;
import com.mycompany.dto.Team;
import com.mycompany.interfaces.LeagueDaoInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class LeagueDao implements LeagueDaoInterface{

    private List<League> leagues = new ArrayList();
    private int nextId = leagues.size() + 1;
    
    public LeagueDao(){
        leagues = decode();
    }
    
    @Override
    public League create(League league) {
        league.setId(nextId);
        nextId++;
        leagues.add(league);
        encode();
        return league;
    }

    @Override
    public League get(int id) {
        for (League league : leagues) {
            if(id == league.getId())
                return league;
        }
        return null;
    }

    @Override
    public void update(League league) {
        League found = null;
        for (League league1 : leagues) {
            if(league1.getId() == league.getId())
                found = league1;
        }
        leagues.remove(found);
        leagues.add(league);
        encode();
    }

    @Override
    public void delete(League league) {
        League found = null;
        for (League league1 : leagues) {
            if(league1.getId() == league.getId())
                found = league1;
        }
        leagues.remove(found);
        encode();
    }

    @Override
    public List<League> list() {
       return leagues; 
    }
    
    private void encode(){
        
        final String TOKEN = "::";
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("leagues.txt"));
            
            for (League league : leagues) {
                List<Team> list = league.getTeams();
                List<String> teamName = new ArrayList();
                for (Team team : list) {
                   teamName.add(team.getTeamName());
                }
                out.print(league.getId());
                out.print(TOKEN);
                out.print(league.getName());
                out.print(TOKEN);
                out.print(teamName);
                out.println();
                
            }
            
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(LeagueDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private List<League> decode(){
        
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("leagues.txt")));
            
            while (sc.hasNextLine()){
                TeamDao tDao = new TeamDao();
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split("::");
                int id = Integer.parseInt(stringParts[0]);
                String teamString = stringParts[2].replace("[", "").replace("]", "");
                String[] teamArray = teamString.split(", ");
                
                List<Team> teamList = tDao.list();
                Team team = new Team();
                List<Team> newList = new ArrayList();
                for (String string : teamArray) {
                    String teamName = string;
                    for (Team team1 : teamList) {
                        if(teamName.equals(team1.getTeamName())){
                            team = team1;
                            newList.add(team);
                        }
                    }
                }

                League league = new League();
                league.setId(id);
                league.setName(stringParts[1]);
                league.setTeams(newList);
                
                
                leagues.add(league);
                nextId = leagues.size() + 1;
            }
            
            sc.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LeagueDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return leagues;
    }
    
}
