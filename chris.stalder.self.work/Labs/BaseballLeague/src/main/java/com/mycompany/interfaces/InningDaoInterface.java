/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Inning;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface InningDaoInterface {
    
    public Inning create(Inning inning);
    
    public Inning get(int number);
    
    public void update(Inning inning);
    
    public void delete(Inning inning);
    
    public List<Inning> list();
    
}
