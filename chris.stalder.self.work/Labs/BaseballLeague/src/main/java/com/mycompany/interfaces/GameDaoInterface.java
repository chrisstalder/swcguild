/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Game;
import com.mycompany.dto.Team;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface GameDaoInterface {
    
    public Game create(Game game);
    public Game get(int id);
    public void update(Game game);
    public void delete(Game game);
    public List<Game> list();
    
}
