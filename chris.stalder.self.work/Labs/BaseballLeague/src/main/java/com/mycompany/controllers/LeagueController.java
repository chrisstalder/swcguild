/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.dao.LeagueDao;
import com.mycompany.dao.TeamDao;
import com.mycompany.dto.League;
import com.mycompany.dto.Team;
import com.mycompany.interfaces.LeagueDaoInterface;
import com.mycompany.interfaces.TeamDaoInterface;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class LeagueController {
    LeagueDaoInterface lDao = new LeagueDao();
    TeamDao tDao = new TeamDao();
    
    public void createLeague(){
        boolean addMore = true;
        ConsoleIO console = new ConsoleIO();
        League league = new League();
        
        System.out.println("What would you like to call your league?");
        String name = console.getString();
        league.setName(name);
        
        List<Team> myTeams = league.getTeams();
        
        while(addMore){
            List<Team> teams= tDao.list();
            List<Team> teamList= teams;
            for (Team team : teamList) {
                System.out.println(team.getTeamId() + " | " + team.getTeamName());
            }
            System.out.println("Please enter the name of the team you want to include in this league.");
            String teamName = console.getString();

            for (Team team : teams) {
                if(teamName.equals(team.getTeamName())){
                    myTeams.add(team);
//                    teamList.remove(team);
                }
            }
            
            System.out.println("Do you want to add more?(y/n)");
            String input = console.getString();
            if("y".equals(input)){
                addMore = true;
            }else
                addMore = false;
        }
        league.setTeams(myTeams);
        lDao.create(league);
    }
    
    
    
}
