/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.League;
import com.mycompany.dto.Player;
import com.mycompany.dto.Team;
import com.mycompany.interfaces.TeamDaoInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TeamDao implements TeamDaoInterface{
    private PlayerDao pDao = new PlayerDao();
    private List<Team> teamList= new ArrayList();
    private int nextId = 1 + teamList.size();
    
    public TeamDao(){
        teamList = decode();
    }
    
    public Team create(Team team){
        team.setTeamId(nextId);
        nextId++;
        teamList.add(team);
        encode();
        return team;
    }
    
    public Team get(String name){
        
        for (Team team : teamList) {
            if(team.getTeamName().equals(name))
                return team;
        }
        return null;
    }
    
    public void update(Team team){
        Team found = new Team();
        
        for (Team team1 : teamList) {
            if (team1.getTeamId() == team.getTeamId())
                found = team1;
        }
        
        teamList.remove(found);
        teamList.add(team);
        encode();
    }
    
    public void delete(Team team){
        Team found = new Team();
        
        for (Team team1 : teamList) {
            if (team1.getTeamId() == team.getTeamId())
                found = team1;
        }
        
        teamList.remove(found);
        encode();
    }
    
    public List<Team> list(){
        return teamList;
    }
    
    public List<Player> listPlayers(Team team){
        List<Player> playerList = team.getPlayerList();
        
        return playerList;
    }
    
    public void tradePlayer(Team from, Team to, Player player){

        player.setTeam(to);
        pDao.update(player);
        addPlayer(to, player);
        
        List<Player> fromlist = from.getPlayerList();
        Player found = new Player();
        for (Player player1 : fromlist) {
            if(player1.getPlayerId() == player.getPlayerId())
                found = player1;
                
        }
        
        fromlist.remove(found);
        
        from.setTeamAvg(generateTeamAvg(fromlist));
        update(from);
    }
    
    public void addPlayer(Team team, Player player){
        List<Player> list = team.getPlayerList();
        list.add(player);
        encode();
        
    }
    
    public double generateTeamAvg(List<Player> playerList){
        double total = 0;
        for (Player player : playerList) {
            total += player.getAvg();
        }
        double teamAvg = total / playerList.size();
        
        return teamAvg;
    }
    
    
    private void encode(){
        
        final String TOKEN = ("::");
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("teams.txt"));
            
            for (Team team : teamList) {
                List<Player> list = team.getPlayerList();
                List<String> players = new ArrayList();
                for (Player player : list) {
                    String name = player.getName();
                    players.add(name);
                }
                out.print(team.getTeamId());
                out.print(TOKEN);
                out.print(team.getTeamName());
                out.print(TOKEN);
                out.print(players);
                out.print(TOKEN);
                out.print(team.getTeamAvg());
                out.print(TOKEN);
                out.print(team.getWins());
                out.print(TOKEN);
                out.print(team.getLosses());
                
                out.println();
            }
            
            out.flush();
            out.close();
            
        } catch (IOException ex) {
            Logger.getLogger(TeamDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private List<Team> decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("teams.txt")));
            
            while(sc.hasNextLine()){
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");
                
                Team team = new Team();
                
                int id = Integer.parseInt(stringParts[0]);
                team.setTeamId(id);
                
                double teamAvg = Double.parseDouble(stringParts[3]);
                
                String playerString = stringParts[2].replace("[", "").replace("]", "");
                String[] playerArray = playerString.split(", ");
                
                List<Player> playerList = pDao.list();
                Player player = new Player();
                List<Player> newlist = new ArrayList();
                
                for (String string : playerArray) {
                    String name = string;
                    for (Player player1 : playerList) {
                        if (player1.getName().equals(name)){
                            player = player1;
                            newlist.add(player);
                        }
                        
                    }
                    
                }
                
                int wins = Integer.parseInt(stringParts[4]);
                int losses = Integer.parseInt(stringParts[5]);
                
               
           
                team.setTeamName(stringParts[1]);
                team.setPlayerList(newlist);
                team.setTeamAvg(teamAvg);
                team.setWins(wins);
                team.setLosses(losses);
                
                teamList.add(team);
                nextId = 1 + teamList.size();
                
            }
            
            sc.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TeamDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return teamList;
    }
}