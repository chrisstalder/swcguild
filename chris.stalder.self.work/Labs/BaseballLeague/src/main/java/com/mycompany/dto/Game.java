/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Game {
    private List<Inning> innings = new ArrayList();
    private Team winner;
    private Team loser;
    private int id;
    private Team home;
    private Team away;
    private int homeScore;
    private int awayScore;

    public List<Inning> getInnings() {
        return innings;
    }

    public Team getWinner() {
        return winner;
    }

    public Team getLoser() {
        return loser;
    }

    public int getId() {
        return id;
    }

    public Team getHome() {
        return home;
    }

    public Team getAway() {
        return away;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setInnings(List<Inning> innings) {
        this.innings = innings;
    }

    public void setWinner(Team winner) {
        this.winner = winner;
        
    }

    public void setLoser(Team loser) {
        this.loser = loser;
        
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setHome(Team home) {
        this.home = home;
    }

    public void setAway(Team away) {
        this.away = away;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }
    
    
}
