/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.dto.Inning;
import com.mycompany.dto.Team;
import java.util.Random;

/**
 *
 * @author apprentice
 */
public class InningController {
    
    
    
    public int numOfScores(){
        Random r = new Random();
        int numOfScores = r.nextInt(2);
        return numOfScores;
    }
    
    public Inning calculate(int numOfScores, Team home, Team away){
        Random r = new Random();
        int homeScore = 0;
        int awayScore = 0;
        double homeAvg = home.getTeamAvg();
        double awayAvg = away.getTeamAvg();
        
        double homePerformance = r.nextDouble() + homeAvg+ .0015;
        double awayPerformance = r.nextDouble() + awayAvg;
        
        for (int i = 0; i <= numOfScores; i++) {
            if (homePerformance > awayPerformance){
                homeScore++;
            }else if (awayPerformance > homePerformance){
                awayScore++;
            }     
        }
        
        Inning inning = new Inning();
        inning.setHomeScore(homeScore);
        inning.setAwayScore(awayScore);
        return inning;
    }
}
