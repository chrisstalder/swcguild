/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.Player;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface PlayerDaoInterface {
    public Player create(Player player);
    public Player get(int id);
    public void update(Player player);
    public void delete(Player player);
    public List<Player> list();
    
}
