/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.dao.LeagueDao;
import com.mycompany.dao.SeasonDao;
import com.mycompany.dto.Game;
import com.mycompany.dto.League;
import com.mycompany.dto.Season;
import com.mycompany.dto.Team;
import com.mycompany.interfaces.LeagueDaoInterface;
import com.mycompany.interfaces.SeasonDaoInterface;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class SeasonController {
    
    SeasonDaoInterface sDao = new SeasonDao();
    LeagueDaoInterface lDao = new LeagueDao();
    
    public void run(){
        List<League> leagues = lDao.list();
        ConsoleIO console = new ConsoleIO();
        
        for (League league : leagues) {
            System.out.println(league.getId() + " | " + league.getName());
        }
        System.out.println("Enter the id of the league you would like to generate a season for: ");
        int input = console.getIntValue();
        League myLeague = lDao.get(input);
        Season season = generateSeason(myLeague);
//        calculateChampion(season);
        System.out.println("The " + season.getChampion().getTeamName() + " are the champions!");
        
    }
    
    public Season generateSeason(League league){
        List<Team> teamList = league.getTeams();
        List<Game> games = new ArrayList();
        GameController controller = new GameController();
        Game game = new Game();
        List<Team> roster = teamList;
        
        for (Team team : teamList){

            for (Team home : roster) {
                if(team.getTeamId() != home.getTeamId()){
                    System.out.println(team.getTeamName() + " @ " + home.getTeamName());
                    game = controller.createGame(home, team);
                    System.out.println("The " + game.getWinner().getTeamName() + " win!");
                    setWinsLosses(game);
                    games.add(game);
                }
            }
            

            for (Team away : roster) {
                if(team.getTeamId() != away.getTeamId()){
                    System.out.println(away.getTeamName() + " @ " + team.getTeamName());
                    game = controller.createGame(team, away);
                    System.out.println("The " + game.getWinner().getTeamName() + " win!");
                    setWinsLosses(game);
                    games.add(game);
                }
            }
     
        }

        Season season = new Season();
        season.setGames(games);
        season.setChampion(calculateChampion(season));
        sDao.create(season);
        return season;
    }
    
    public void setWinsLosses(Game game){
        Team winner = game.getWinner();
        int wins = winner.getWins();
        wins ++;
        winner.setWins(wins);
        Team loser = game.getLoser();
        int losses = loser.getLosses();
        losses ++;
        loser.setLosses(losses);
    }
    public Team calculateChampion(Season season){
        List<Game> games = season.getGames();
        List<Team> winners = new ArrayList();
        Team winner = new Team();
        int mostWins = 0;
        int wins;
        Team champion = new Team();
        
        for (Game game : games){
            winner = game.getWinner();
            winners.add(winner);
        }
        
        for (Team team : winners){
            wins = 0;
            for (Team winner1 : winners) {
                if(team.getTeamId() == winner1.getTeamId())
                    wins++;
            }
            team.setWins(wins);
            if(wins > mostWins){
                mostWins = wins;
                champion = team;
            }
        }
//        for (Team winner1 : winners) {
//            int wins = winner1.getWins();
//            if(wins > mostWins)
//                champion = winner1;
//        }
        
        return champion;
        
    }
}
