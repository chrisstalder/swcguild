/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.dto.League;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface LeagueDaoInterface {
    
    public League create(League league);
    public League get(int id);
    public void update(League league);
    public void delete(League league);
    public List<League> list();
    
}
