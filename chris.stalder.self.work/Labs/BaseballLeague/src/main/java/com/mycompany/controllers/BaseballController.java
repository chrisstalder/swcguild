/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.dao.LeagueDao;
import com.mycompany.dao.PlayerDao;
import com.mycompany.dao.TeamDao;
import com.mycompany.dto.League;
import com.mycompany.dto.Player;
import com.mycompany.dto.Team;
import com.mycompany.interfaces.LeagueDaoInterface;
import com.mycompany.interfaces.PlayerDaoInterface;
import java.util.ArrayList;
import java.util.List;
import com.mycompany.interfaces.TeamDaoInterface;

/**
 *
 * @author apprentice
 */
public class BaseballController {
    
    TeamDao tDao = new TeamDao();
    TeamDaoInterface tInterface = tDao;
    PlayerDao pDao = new PlayerDao();
    PlayerDaoInterface pInterface = pDao;
    LeagueDaoInterface lInterface = new LeagueDao();
    
    ConsoleIO console = new ConsoleIO();
    GameController game = new GameController();
    SeasonController season = new SeasonController();
    LeagueController league = new LeagueController();
    
    public void run(){
        boolean repeat = true;
        int input;
        
        while (repeat){
            System.out.println("================================");
            System.out.println("Welcome to Your Baseball League!");
            System.out.println("================================");
            System.out.println("1) Create a League");
            System.out.println("2) Create a Team");
            System.out.println("3) Create a Player");
            System.out.println("4) List Teams");
            System.out.println("5) List Players");
            System.out.println("6) Trade a Player");
            System.out.println("7) Delete a Player");
            System.out.println("8) Play a Game");
            System.out.println("9) Simulate a Season");
            System.out.println("0) Exit");
            
            input = console.getIntValue();
            
            switch(input){
                case 1:
                    league.createLeague();
                    break;
                case 2:
                    createTeam();
                    break;
                case 3:
                    createPlayer();
                    break;
                case 4:
                    listTeams();
                    break;
                case 5:
                    listPlayers();
                    break;
                case 6:
                    tradePlayer();
                    break;
                case 7:
                    deletePlayer();
                    break;
                case 8:
                    game.run();
                    break;
                case 9:
                    season.run();
                    break;
                case 0:
                    System.out.println("Thanks for coming by!");
                    repeat = false;
                    break;
                default:
                    System.out.println("That wasn't an option!");
            }
            
        }
    }
    
    public void createTeam(){
        LeagueDao lDao = new LeagueDao();
        List<Team> list = tDao.list();
        int i = 0;
        
        Team newTeam = new Team();
        System.out.println("Team name: ");
        String name = console.getString();
        for (Team team : list) {
            if(team.getTeamName().equals(name))
                i++;
        }
        
        if (i == 0){
            newTeam.setTeamName(name);
            
            
            
            tDao.create(newTeam);
            
        }
        else
            System.out.println("That team already exists!");

    }
    
    public void createPlayer(){
        List<Team> list = tDao.list();
        boolean isValid = false;
        boolean anotherValidation = false;
        String teamName = null;
        
        Player newPlayer = new Player();
        
        if(list.size() > 0){
            System.out.println("Player name: ");
            String name = console.getString();
            System.out.println("Player team: ");
            
            while(!anotherValidation){
                Team validateTeam = new Team();
                while(!isValid){
                    teamName = console.getString();
                    for (Team team : list) {
                        if (team.getTeamName().equals(teamName)){
                            isValid = true;
                            validateTeam = team;
                        }
                    }
                }
                isValid = false;
                if (validateTeam.getPlayerList().size() >= 9)
                    System.out.println("You can't add any more players to that team!");
                else
                    anotherValidation = true;
            }
            anotherValidation = false;
            
            double avg = pDao.generateAvg();
            newPlayer.setAvg(avg);
            for (Team team : list) {
                if (team.getTeamName().equals(teamName))
                    newPlayer.setTeam(team);
            }
            
            
            
            System.out.println("Jersey Number: ");
            int jersey = console.getIntValue();
            
            newPlayer.setName(name);
            
            newPlayer.setJersey(jersey);
            
            pDao.create(newPlayer);
            for (Team team1 : list) {
                if(team1.getTeamName().equals(teamName)){
                    tDao.addPlayer(team1, newPlayer);
                }
            }
            
            teamAverage(newPlayer.getTeam());
            
        }else
            System.out.println("You need to create a team first!");
    }
    
    public void listTeams(){
        List<Team> list = tDao.list();
        for (Team team : list) {
            System.out.println("-------------------------------");
            System.out.println(team.getTeamId() + " | " + team.getTeamName() + " | " + team.getTeamAvg());
            System.out.println("-------------------------------");
        }
    }
    
    public void listPlayers(){
        List<Team> list = tDao.list();
        for (Team team : list) {
            System.out.println("-------------------------------");
            System.out.println(team.getTeamId() + " | " + team.getTeamName());
            System.out.println("-------------------------------");
        }
        System.out.println("Which team would you like to look at?");
        String name = console.getString();
        
        for (Team team : list) {
            if(team.getTeamName().equals(name)){
                
                List<Player> playerList = tDao.listPlayers(team);
                 
                System.out.println("-------------------------------");
                System.out.println(team.getTeamName().toUpperCase());
                for (Player player : playerList) {
                    System.out.println("-------------------------------");
                    System.out.println("    " + player.getPlayerId() + " | " + player.getName() + " - " + player.getJersey() + " " + player.getAvg());
                }
            }
        }
    }
    
    public void tradePlayer(){
        boolean isValid = false;
        String team1 = null;
        String team2 = null;
        String playerName = null;
        List<Team> list = tDao.list();
        List<Player> playerList = pDao.list();
        Team from = new Team();
        Team to = new Team();
        Player myPlayer = new Player();
        Player myPlayer2 = new Player();
        System.out.println("What team will you be trading from?");
        while(!isValid){
            
            team1 = console.getString();
        
            for (Team team : list) {
                if (team.getTeamName().equals(team1))
                    isValid = true;
            }
        }
        isValid = false;
        
        for (Team team : list) {
            if(team.getTeamName().equals(team1))
                from = team;
        }
        
        List<Player> fromPlayers = from.getPlayerList();
        for (Player fromPlayer : fromPlayers) {
            System.out.println(fromPlayer.getName() + " | " + fromPlayer.getAvg());
        }
        System.out.println("What player will you be trading?");
        
        while (!isValid){
            playerName = console.getString();
            for (Player player : playerList) {
                if (player.getName().equals(playerName))
                    isValid = true;
            }
        }
        isValid = false;
        
        for (Player player : playerList) {
            if (player.getName().equals(playerName))
                myPlayer = player;
        }
        
        
        System.out.println("What team will you be trading to?");
        while (!isValid){
            team2 = console.getString();
            
            for (Team team : list) {
                if(team.getTeamName().equals(team2))
                    isValid = true;
            }
        }
        isValid = false;
        
        for (Team team : list) {
            if(team.getTeamName().equals(team2))
                to = team;
        }
        List<Player> toPlayers = to.getPlayerList();
        for (Player toPlayer : toPlayers) {
            System.out.println(toPlayer.getName() + " | " + toPlayer.getAvg());
        }
        System.out.println("What player will you be trading?");
        
        while (!isValid){
            playerName = console.getString();
            for (Player player : playerList) {
                if (player.getName().equals(playerName))
                    isValid = true;
            }
        }
        isValid = false;
        
        for (Player player : playerList) {
            if (player.getName().equals(playerName))
                myPlayer2 = player;
        }
//        System.out.println("What player will you be trading?");
//        
//        while (!isValid){
//            playerName = console.getString();
//            for (Player player : playerList) {
//                if (player.getName().equals(playerName))
//                    isValid = true;
//            }
//        }
//        
//        for (Player player : playerList) {
//            if (player.getName().equals(playerName))
//                myPlayer = player;
//        }
//        
        myPlayer.setTeam(to);
        myPlayer2.setTeam(from);
        pDao.update(myPlayer);
        pDao.update(myPlayer2);
        tDao.addPlayer(to, myPlayer);
        tDao.addPlayer(from, myPlayer2);
        
        
        List<Player> fromlist = from.getPlayerList();
        Player found = new Player();
        for (Player player1 : fromlist) {
            if(player1.getPlayerId() == myPlayer.getPlayerId())
                found = player1;
                
        }
        
        fromlist.remove(found);
        
        Player found2 = new Player();
        fromlist.remove(found);
        List<Player> toList = to.getPlayerList();
        found2 = new Player();
        for (Player player1 : toList) {
            if(player1.getPlayerId() == myPlayer2.getPlayerId())
                found2 = player1;
                
        }
        
        toList.remove(found2);
        
    }
    
    public void deletePlayer(){
        
        List<Player> playerList= pDao.list();
        List<Player> multiples = new ArrayList();
        
        String name = null;
        boolean isValid = false;
        int i = 0;
        System.out.println("Enter the name of the player you wish to delete: ");
        
        while(!isValid){
            name = console.getString();
            for (Player player : playerList) {
                if(player.getName().equals(name))
                    isValid = true;
            }
        }
        
        Player found = null;
        
        for (Player player : playerList) {
            if(player.getName().equals(name)){
                found = player;
                i++;
                if (i > 1)
                    multiples.add(player);
            }
            
            if(i > 1){
                System.out.println("There were multiple players with that name.");
                for (Player multiple : multiples) {
                    System.out.println(multiple.getPlayerId() + " | " + multiple.getName());
                }
                System.out.println("Please enter the ID of the person you want to delete");
                int id = console.getIntValue();
                for (Player multiple : multiples) {
                    if(multiple.getPlayerId() == id)
                        found = multiple;
                }
            }
        }
        
            Team team = found.getTeam();
            team.getPlayerList().remove(found);
            tDao.update(team);
            pDao.delete(found);
            
    }
    
    public void teamAverage(Team team){
  
        double teamAvg = tDao.generateTeamAvg(team.getPlayerList());
        team.setTeamAvg(teamAvg);
        tDao.update(team);
            
    }
}