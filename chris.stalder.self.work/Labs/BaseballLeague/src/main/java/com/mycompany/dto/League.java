/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class League {
    private int id;
    private String name;
    private List<Game> Games = new ArrayList();
    private List<Team> Teams = new ArrayList();
    private List<Season> Seasons = new ArrayList();

    public List<Game> getGames() {
        return Games;
    }

    public List<Team> getTeams() {
        return Teams;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Season> getSeasons() {
        return Seasons;
    }

    public void setGames(List<Game> Games) {
        this.Games = Games;
    }

    public void setTeams(List<Team> Teams) {
        this.Teams = Teams;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSeasons(List<Season> Seasons) {
        this.Seasons = Seasons;
    }
    
    
}
