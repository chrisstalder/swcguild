/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

/**
 *
 * @author apprentice
 */
public class Player {
    private int playerId;
    private String name;
    private Team team;
    private int jersey;
    private double avg;
    
    public int getPlayerId() {
        return playerId;
    }

    public Team getTeam() {
        return team;
    }

    public int getJersey() {
        return jersey;
    }

    public double getAvg() {
        return avg;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void setJersey(int jersey) {
        this.jersey = jersey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }
    
}
