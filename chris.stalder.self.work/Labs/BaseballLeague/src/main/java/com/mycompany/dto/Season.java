/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Season {
    private int id;
    private Team champion;
    private List<Game> games = new ArrayList();

    public int getId() {
        return id;
    }

    public Team getChampion() {
        return champion;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setChampion(Team champion) {
        this.champion = champion;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }
    
    
}
