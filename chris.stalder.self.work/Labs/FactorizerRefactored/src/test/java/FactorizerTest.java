/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FactorizerTest {
    
    public FactorizerTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testPrintIsPrime(){
        Factorizer f = new Factorizer();
        
        String result = f.printIsPrime(true, 3);
        
        Assert.assertEquals(3 + " is a prime number.", result);
        
    }
    
    @Test
    public void testPrintIsPerfect(){
        Factorizer f = new Factorizer();
        
        String result = f.printIsPerfect(true, 6);
        
        Assert.assertEquals(6 + " is a perfect number.", result);
        
    }
    
    @Test
    public void testIsPrime(){
        Factorizer f = new Factorizer();
        
        boolean result = f.isPrime(1);
        
        Assert.assertEquals(true, result);
    }
    
    @Test
    public void testIsPerfect(){
        Factorizer f = new Factorizer();
        
        boolean result = f.isPerfect(6, 6);
        
        Assert.assertEquals(true, result);
    }
}
