/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class Factorizer {
    
    public void run() {
        
        Scanner keyboard = new Scanner(System.in);
        boolean isPerfect = false;
        boolean isPrime = false;
        int evenDivides = 0;
        int factorTotal = 0;
        int input;
        
        System.out.println("What number would you like to factor?");
        input = keyboard.nextInt();
        
        
        System.out.println("The factors of " + input + " are:");
        for (int i = 1; i < input; i++){    //for loop will loop through all numbers before input
            if (input % i == 0){
                System.out.println(i); //prints out i
                factorTotal += i; //keeps track of number of factors
                evenDivides++; //keeps track of how many numbers can evenly divide into input
            }
            isPerfect = isPerfect(factorTotal, input);
            isPrime = isPrime(evenDivides);
        }
        
        String perfectMessage = printIsPerfect(isPerfect, input);
        
        String primeMessage = printIsPrime(isPrime, input);
        
        System.out.println("");
        System.out.println(perfectMessage);
        System.out.println("");
        System.out.println(primeMessage);
        
        
    }

    public String printIsPrime(boolean isPrime, int input) {
        String message = "";
        if (isPrime == true){
            message = (input + " is a prime number.");
        }else{                                                      //prints out if it is a prime number
            message = (input + " is not a prime number.");
        }
        
        return message;
    }

    public String printIsPerfect(boolean isPerfect, int input) {
        String message = "";
        if (isPerfect == true){
            message = (input + " is a perfect number.");        //prints out if it is perfect or not
        }else{
            message = (input + " is not a perfect number.");
        }
        
        return message;
    }

    public boolean isPrime(int evenDivides) {
        boolean isPrime;
        isPrime = evenDivides == 1; //if a number only has one # that can divide into it evenly (excluding itself) then it is prime
        return isPrime;
    }

    public boolean isPerfect(int factorTotal, int input) {
        boolean isPerfect = false;
        if (factorTotal == input){
            isPerfect = true;       //if all factors added together equal input it is a perfect #
        }
        return isPerfect;
    }
    
    
}
