/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.dao.AddressBookDao;
import com.mycompany.dao.AddressBookDaoImpl;
import com.mycompany.dao.AddressBookLambdaImpl;
import com.mycompany.dto.Address;
import com.mycompany.dto.Person;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class SearchController {
    AddressBookDao addressDao = new AddressBookDaoImpl();
    AddressBookDao addressLambdaDao = new AddressBookLambdaImpl();
    
    ConsoleIO console = new ConsoleIO();
    public void run(){
        AddressBookController addressBook = new AddressBookController();
        boolean repeat = true;
        while(repeat){
            console.print("1. Search By Last Name");
            console.print("2. Search By State");
            console.print("3. Search By City");
            console.print("4. Search By Zipcode");
            console.print("5. Exit");


            int input = console.getIntValue();

            switch(input){
                case 1:
                    searchByLastName();
                    break;
                case 2:
                    searchByState();
                    break;
                case 3:
                    searchByCity();
                    break;
                case 4:
                    searchByZip();
                    break;
                case 5:
                    repeat = false;
                    
                default:
                    console.print("Invalid entry.");
            }
        }
        
        addressBook.run();
    }
    
    public void searchByState(){
        List<Address> addresses = addressLambdaDao.list();
        List<String> stateList = new ArrayList();
        
        for (Address address : addresses) {
            
            if(!stateList.contains(address.getState())){
                stateList.add(address.getState());
            }
            
        }
        
        for (String string : stateList) {
            console.print(string);
        }
        console.print("Enter the state: ");
        String state = console.getState();
        Map<String, List<Person>> foundByStateAndCityMap = addressLambdaDao.searchByState(state);
        Set<String> cityKeySet = foundByStateAndCityMap.keySet();
        Iterator i = cityKeySet.iterator();
        
        
        while(i.hasNext()){
            String currentCity = i.next().toString();
            console.print(currentCity);
            List<Person> peopleInCity = foundByStateAndCityMap.get(currentCity);
            for (Person person : peopleInCity) {
                console.print("     " + person.getFirstName() + " " + person.getLastName());
            }
        }
        
    }
    
    public void searchByCity(){
        List<Address> getCityList = addressLambdaDao.list();
        for (Address address : getCityList) {
            console.print(address.getCity());
        }
        
        console.print("Input city: ");
        String city = console.getString();
        List<Address> foundByCityList = addressLambdaDao.searchByCity(city);
        
        listAddresses(foundByCityList);
    }
    
    public void searchByLastName(){
        console.print("Input last name: ");
        String lastName = console.getString();
        List<Address> foundByLastNameList = addressLambdaDao.searchByLastName(lastName);
        listAddresses(foundByLastNameList);
    }
    
    public void searchByZip(){
        List<Address> getZipcodeList = addressLambdaDao.list();
        for (Address address : getZipcodeList) {
            console.print(address.getZipcode());
        }
        
        console.print("Input zipcode: ");
        String zipcode = console.getString();
        List<Address> foundByZipCode = addressLambdaDao.searchByZip(zipcode);
        listAddresses(foundByZipCode);
    }
    
    public void listAddresses(List<Address> addressList){
        for (Address address : addressList) {
            console.print(" " + address.getId() + " | "  + address.getPerson().getLastName() + ", " + address.getPerson().getFirstName() + " " + 
                    address.getHouseNumber() + " " + address.getStreetName() + " " + address.getCity() + " " + address.getState());
                   
        }
    }
}
