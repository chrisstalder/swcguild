/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Address;
import com.mycompany.dto.Person;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface AddressBookDao {
    
    public Address create(Address address);
    public Address get(String lastName);
    public void update(Address address);
    public void delete(Address address);
    public List<Address> list();
    public List<Address> searchByLastName(String lastName);
    public List<Address> searchByCity(String city);
    public Map<String,List<Person>> searchByState(String state);
//    public List<Address> searchByStateAndCity(String state, String city);
    public List<Address> searchByZip(String zipcode);
    public void encode();
    public List<Address> decode();
}
