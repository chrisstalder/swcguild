/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Address {
    private Person person;
    private int id;
    private int houseNumber;
    private String streetName;
    private String city;
    private String state;
    private String country;
    private String apartment;
    private String zipcode;

    public Person getPerson() {
        return person;
    }


    public int getId() {
        return id;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public String getApartment() {
        return apartment;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    
    public void setId(int id) {
        this.id = id;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
    
    public List<Person> getPersons() {
        return new ArrayList();
    }
    
    
}
