/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Address;
import com.mycompany.dto.Person;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBookLambdaImpl implements AddressBookDao{
    private List<Address> addresses = new ArrayList();
    private int nextId = addresses.size() + 1;
    
    public AddressBookLambdaImpl(){
        addresses = decode();
    }
    @Override
    public Address create(Address address) {
        address.setId(nextId);
        nextId++;
        addresses.add(address);
        
        encode();
        
        return address;
    }

    @Override
    public Address get(String lastName) {
        addresses
                .stream()
                .filter(a -> {
                    return a.getPerson().getLastName().equals(lastName);
                }
        );
        return null;      
    }

    @Override
    public void update(Address address) {
        addresses
                .stream()
                .filter((Address a) -> a.getId() == address.getId())
                .collect(Collectors.toList())
                .listIterator()
                .remove();
        addresses.add(address);
        encode();
    }

    @Override
    public void delete(Address address) {
        addresses
                .stream()
                .filter((Address a) -> a.getId() == address.getId())
                .collect(Collectors.toList())
                .listIterator()
                .remove();
        encode();
    }

    @Override
    public List<Address> list() {
        return addresses;
    }

    @Override
    public List<Address> searchByLastName(String lastName) {
        return addresses
                .stream()
                .filter((Address a) -> a.getPerson().getLastName().equals(lastName))
                .collect(Collectors.toList());
    }

    @Override
    public List<Address> searchByCity(String city) {
        return addresses
                .stream()
                .filter((Address a) -> a.getCity().equals(city))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String,List<Person>> searchByState(String state) {
        Map<String, List<Person>> foundByStateAndCityMap = 
         addresses
         .stream()
         .collect(Collectors.toMap(Address::getCity, 
                                   (a) ->  { 
                                                List<Person> onePersonList = new ArrayList();
                                                onePersonList.add(a.getPerson());
                                                return onePersonList; 
                                               }, 
                                   (old, latest) -> {
                                                        old.addAll(latest );
                                                        return old;
                                                    }));
        return foundByStateAndCityMap;
                
        
    }

    @Override
    public List<Address> searchByZip(String zipcode) {
        return addresses
                .stream()
                .filter((Address a) -> a.getZipcode().equals(zipcode))
                .collect(Collectors.toList());
    }

    @Override
    public void encode(){
        final String TOKEN = "::";
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("addressesLambda.txt"));
            
            for (Address address : addresses) {
                out.print(address.getId());
                out.print(TOKEN);
                out.print(address.getPerson().getFirstName());
                out.print(TOKEN);
                out.print(address.getPerson().getLastName());
                out.print(TOKEN);
                out.print(address.getHouseNumber());
                out.print(TOKEN);
                out.print(address.getStreetName());
                out.print(TOKEN);
                out.print(address.getCity());
                out.print(TOKEN);
                out.print(address.getState());
                out.print(TOKEN);
                out.print(address.getZipcode());
                out.print(TOKEN);
                out.print(address.getApartment());
                out.print(TOKEN);
                out.print(address.getCountry());
                out.println();
            }
            
            out.flush();
            out.close();
            
            
        } catch (IOException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public List<Address> decode(){
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("addressesLambda.txt")));
            
            while (sc.hasNextLine()){
                
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");
                
                int id = Integer.parseInt(stringParts[0]);
                int houseNumber = Integer.parseInt(stringParts[3]);
                
                Address address = new Address();
                Person person = new Person();
                person.setFirstName(stringParts[1]);
                person.setLastName(stringParts[2]);
                
                address.setId(id);
                address.setPerson(person);
                address.setHouseNumber(houseNumber);
                address.setStreetName(stringParts[4]);
                address.setCity(stringParts[5]);
                address.setState(stringParts[6]);
                address.setZipcode(stringParts[7]);
                address.setApartment(stringParts[8]);
                address.setCountry(stringParts[9]);
                
                addresses.add(address);
                nextId = 1 + addresses.size();
                
            }
            
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return addresses;
    }

    


}
