/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Address;
import com.mycompany.dto.Person;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoImpl implements AddressBookDao{
    private List<Address> addresses = new ArrayList();
    private int nextId = 1;
   
    
    public AddressBookDaoImpl(){
        addresses = decode();
    }
    
    @Override
    public Address create(Address address){
        address.setId(nextId);
        nextId++;
        addresses.add(address);
        
        encode();
        
        return address;
    }
    
    @Override
    public Address get(String lastName){
        
        for (Address address : addresses) {
            if(lastName.equals(address.getPerson().getLastName()))
                return address;
        }
        return null;
    }
    
    public void delete(Address address){
        Address found = null;
        
        for (Address myAddress : addresses) {
            if(myAddress.getId() == address.getId()){
                found = myAddress;
            }
        }
        addresses.remove(found);
        encode();
    }
    
    @Override
    public void update(Address address){
        Address found = null;
        for (Address myAddress : addresses) {
            if(myAddress.getId() == address.getId()){
                found = myAddress;
            }
        }
        addresses.remove(found);
        addresses.add(address);
        encode();
    }
    
    @Override
    public List<Address> list(){
        return addresses;
    }
    
    public void totalAddresses(List<Address> addresses){
        System.out.println(addresses.size());
    }
    
    @Override
    public void encode(){
        final String TOKEN = "::";
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("addresses.txt"));
            
            for (Address address : addresses) {
                out.print(address.getId());
                out.print(TOKEN);
                out.print(address.getPerson().getFirstName());
                out.print(TOKEN);
                out.print(address.getPerson().getLastName());
                out.print(TOKEN);
                out.print(address.getHouseNumber());
                out.print(TOKEN);
                out.print(address.getStreetName());
                out.print(TOKEN);
                out.print(address.getCity());
                out.print(TOKEN);
                out.print(address.getState());
                out.print(TOKEN);
                out.print(address.getZipcode());
                out.print(TOKEN);
                out.print(address.getApartment());
                out.print(TOKEN);
                out.print(address.getCountry());
                out.println();
            }
            
            out.flush();
            out.close();
            
            
        } catch (IOException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public List<Address> decode(){
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("addresses.txt")));
            
            while (sc.hasNextLine()){
                
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");
                
                int id = Integer.parseInt(stringParts[0]);
                int houseNumber = Integer.parseInt(stringParts[3]);
                
                Address address = new Address();
                Person person = new Person();
                person.setFirstName(stringParts[1]);
                person.setLastName(stringParts[2]);
                address.setId(id);
                address.setPerson(person);
                address.setHouseNumber(houseNumber);
                address.setStreetName(stringParts[4]);
                address.setCity(stringParts[5]);
                address.setState(stringParts[6]);
                address.setZipcode(stringParts[7]);
                address.setApartment(stringParts[8]);
                address.setCountry(stringParts[9]);
                
                addresses.add(address);
                nextId = 1 + addresses.size();
                
            }
            
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return addresses;
    }

    @Override
    public List<Address> searchByLastName(String lastName) {
        List<Address> foundByLastNameList = new ArrayList();
        for (Address address : addresses) {
            if(lastName.equals(address.getPerson().getLastName()))
                foundByLastNameList.add(address);
        }
        return foundByLastNameList;
    }

    @Override
    public List<Address> searchByCity(String city) {
        List<Address> foundByCityList = new ArrayList();
        for (Address address : addresses) {
            if(city.equals(address.getCity()))
                foundByCityList.add(address);
        }
        return foundByCityList;
    }

    @Override
    public Map<String,List<Person>> searchByState(String state) {
        Map<String, List<Person>> addressByCityMap = new HashMap();
        List<Address> foundByStateList = new ArrayList();
        List<String> cityList = new ArrayList();
  
        for (Address address : addresses) {
            if(state.equals(address.getState())){   //iterating through original address list to find all with a certain state
                foundByStateList.add(address);      
                cityList.add(address.getCity());    //adding each city to a separate list
            }
        }
        
        for (String city : cityList) {
            List<Person> peopleInCity = new ArrayList(); //declaring new list for each city
            for (Address address : foundByStateList) {
                if(address.getCity().equals(city)){        //doing some for loop wizadry
                    peopleInCity.add(address.getPerson());
                }
            }
            addressByCityMap.put(city, peopleInCity); //adding to map city, and people
            
        }
        
        return addressByCityMap;
    }

    @Override
    public List<Address> searchByZip(String zipcode) {
        List<Address> foundByZipList = new ArrayList();
        for (Address address : addresses) {
            if(zipcode.equals(address.getZipcode()))
                foundByZipList.add(address);
        }
        return foundByZipList;
    }

    

   
}
