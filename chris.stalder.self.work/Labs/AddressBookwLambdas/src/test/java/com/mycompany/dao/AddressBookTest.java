/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dto.Address;
import com.mycompany.dto.Person;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AddressBookTest {
    
    public AddressBookTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCreate(){
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        Address address = new Address();
        Person person = new Person();
        person.setFirstName("Chris");
        person.setLastName("Stalder");
        address.setPerson(person);
        address.setHouseNumber(614);
        address.setStreetName("East 4th street");
        address.setCity("Dover");
        address.setState("OH");
        address.setZipcode("44622");
        address.setCountry("USA");
        Address result = instance.create(address);
        
        Assert.assertEquals(address, result);
    }
    
    @Test
    public void testGet(){
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        String name = "Stalder";
        Address myAddress = new Address();
        List<Address> list = instance.list();
        for (Address address : list) {
            if("Stalder".equals(address.getPerson().getLastName()))
                myAddress = address;
        }
        
        
        Address result = instance.get(name);
        
        Assert.assertSame(result, myAddress);
    }
    
    @Test
    public void testUpdate(){
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        List<Address> list = instance.list();
        Person p = new Person();
        int nextId = list.size();
        Address address = new Address();
        address.setId(nextId);
        
        p.setFirstName("Chris");
        p.setLastName("Stalder");
        
        address.setPerson(p);
        address.setHouseNumber(614);
        address.setStreetName("East 4th street");
        address.setCity("Dover");
        address.setState("OH");
        address.setZipcode("44622");
        address.setCountry("USA");
        
        
        for (Address address1 : list) {
            if(address1.getId() == address.getId())
                address = address1;
        }
        Person person = new Person();
        person.setFirstName("Craig");
        person.setLastName("Stalder");
        address.setPerson(person);
        instance.update(address);
        
        String result = address.getPerson().getFirstName();
        
        Assert.assertEquals("Craig", result);
    }
    
    @Test
    public void testDelete(){
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        List<Address> list = instance.list();
        
        int currentArraySize = list.size();
        
        Address address = new Address();
        for (Address address1 : list) {
            if(address1.getId() == address.getId())
                address = address1;
        }
        
        instance.delete(address);
        boolean result = false;
        for (Address address1 : list) {
            if(address1.getId() == address.getId())
                result = true;
        }
        
        Assert.assertFalse(result);
    }
}
