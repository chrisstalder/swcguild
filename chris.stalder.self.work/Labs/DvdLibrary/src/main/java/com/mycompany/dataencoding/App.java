/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dataencoding;

import com.mycompany.controllers.DvdLibraryController;

/**
 *
 * @author apprentice
 */
public class App {
    
    public static void main(String[] args) {
        DvdLibraryController controller = new DvdLibraryController();
        
        controller.run();
    }
}
