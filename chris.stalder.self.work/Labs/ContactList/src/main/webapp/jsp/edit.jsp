<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Hello Controller Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Contact List</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/admin/">Admin</a></li>
                </ul>    
            </div>

            <div class="row">
                <div class="col-md-6">

                    <form method="POST" action="./" class="form-horizontal">

                        <input type="hidden" name="id" value="${contact.id}"/>

                        <div class="form-group">
                            <label for="first-name" class="col-md-4 control-label">First: </label>
                            <div class="col-md-8">
                                <input id="first-name" name="firstName" value="${contact.firstName}"/><br/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="last-name" class="col-md-4 control-label">Last: </label>
                            <div class="col-md-8">
                                <input id="last-name"name="lastName" value="${contact.lastName}" /><br/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="company" class="col-md-4 control-label">Company: </label>
                            <div class="col-md-8">
                                <input id="company"name="company" value="${contact.company}" /><br/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label"> Email: </label>
                            <div class="col-md-8">
                                <input id="email"name="email" value="${contact.email}" /><br/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label"> Phone:</label>
                            <div class="col-md-8">
                                <input id="phone"name="phone" value="${contact.phone}" /><br/>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-default center-block"/>

                    </form>
                </div>
            </div>    

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

