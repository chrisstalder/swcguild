/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $('#tax-create-submit').on('click', function (e) {
        $('#add-tax-validation-errors').empty();
        e.preventDefault();

//        alert('works');


        var taxData = JSON.stringify({
            state: $('#tax-state-input').val(),
            taxRate: $('#tax-taxRate-input').val()
        });

        $.ajax({
            url: contextRoot + '/tax/',
            type: 'POST',
            data: taxData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                console.log(data);

                var tableRow = buildTaxRow(data);
                $('#tax-table').append($(tableRow));

                state: $('#tax-state-input').val('');
                taxRate: $('#tax-taxRate-input').val('');

            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {

                    $('#add-tax-validation-errors').append(error.message + "<br />");

                });
            }
        });

    });

    $('#showTaxModal').on('show.bs.modal', function (e) {
        var link = $(e.relatedTarget);

        var tax = link.data('tax-id');

        $.ajax({
            url: contextRoot + '/tax/' + tax,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#tax-state').text(data.state);
                $('#tax-taxrate').text(data.taxRate);
            },
            error: function (data, status) {

            }

        });

    });

    $('#editTaxModal').on('show.bs.modal', function (e) {
        var link = $(e.relatedTarget);

        var tax = link.data('tax-id');
        $('#edit-tax-validation-errors').empty();
        $.ajax({
            url: contextRoot + '/tax/' + tax,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#edit-tax-state').val(data.state);
                $('#edit-tax-taxrate').val(data.taxRate);
                $('#edit-id').val(data.id);
            },
            error: function (data, status) {

            }

        });

    });

    $('#edit-tax-button').on('click', function (e) {
        $('#edit-tax-validation-errors').empty();
        var taxData = JSON.stringify({
            state: $('#edit-tax-state').val(),
            taxRate: $('#edit-tax-taxrate').val(),
            id: $('#edit-id').val()
        });

        $.ajax({
            url: contextRoot + '/tax/',
            type: 'PUT',
            data: taxData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", 'application/json');
                xhr.setRequestHeader("Content-type", 'application/json');
            },
            success: function (data, status) {
                $('#editTaxModal').modal('hide');

                var tableRow = buildTaxRow(data);

                $('#tax-row-' + data.id).replaceWith($(tableRow));
                console.log(data);
            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {

                    $('#edit-tax-validation-errors').append(error.message + "<br />");

                });
            }
        });

    });

    function buildTaxRow(data) {

        return "<tr id='tax-row-" + data.id + "'>  \n\
                <td> <a data-tax-id='" + data.id + "' data-toggle='modal' data-target='#showTaxModal'>" + data.state + "</td><td> " + data.taxRate + "</td>    \n\
                <td> <a data-tax-id='" + data.id + "' data-toggle='modal' data-target='#editTaxModal'>Edit</a>  </td>   \n\
                <td> <a data-tax-id='" + data.id + "' class='delete-link'>Delete</a>  </td>   \n\
                </tr>  ";

    }

});

$(document).on('click', '#delete-tax', function (e) {

    e.preventDefault();

    var tax = $(e.target).data('tax-id');

    $.ajax({
        type: 'DELETE',
        url: contextRoot + "/tax/" + tax,
        success: function (data, status) {
            $('#tax-row-' + tax).remove();
        },
        error: function (data, status) {

        }
    });

});