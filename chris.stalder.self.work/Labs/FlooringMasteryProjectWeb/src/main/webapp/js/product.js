/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $('#create-product-submit').on('click', function (e) {
        $('#add-product-validation-errors').empty();
        e.preventDefault();

//        alert('works');

        var productData = JSON.stringify({
            type: $('#product-type-input').val(),
            costPerSqFoot: $('#material-cost-per-sq-ft-input').val(),
            laborPerSqFoot: $('#labor-cost-per-sq-ft-input').val()
        });

        $.ajax({
            url: contextRoot + '/product/',
            type: 'POST',
            data: productData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                console.log(data);

                var tableRow = buildProductRow(data);
                $('#product-table').append($(tableRow));

                type: $('#product-type-input').val('');
                costPerSqFoot: $('#material-cost-per-sq-ft-input').val('');
                laborPerSqFoot: $('#labor-cost-per-sq-ft-input').val('');

            },
            error: function (data, status) {
                
                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {
                    $('#add-product-validation-errors').append(error.message + "<br />");

                });
            }
        });

    });

    $('#showProductModal').on('show.bs.modal', function (e) {

        var link = $(e.relatedTarget);

        var productId = link.data('product-id');

        $.ajax({
            url: contextRoot + "/product/" + productId,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#product-type').text(data.type);
                $('#product-material-cost-per-sq-ft').text(data.costPerSqFoot);
                $('#product-labor-cost-per-sq-ft').text(data.laborPerSqFoot);
            },
            error: function (data, status) {
            }

        });

    });

    $('#editProductModal').on('show.bs.modal', function (e) {

        var link = $(e.relatedTarget);

        var productId = link.data('product-id');
        $('#edit-product-validation-errors').empty();
        $.ajax({
            url: contextRoot + "/product/" + productId,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#edit-product-type').val(data.type);
                $('#edit-product-material-cost-per-sq-ft').val(data.costPerSqFoot);
                $('#edit-product-labor-cost-per-sq-ft').val(data.laborPerSqFoot);
                $('#edit-id').val(data.id);
            },
            error: function (data, status) {

            }

        });

    });

    $('#edit-product-button').on('click', function (e) {
        $('#edit-product-validation-errors').empty();
        var productData = JSON.stringify({
            type: $('#edit-product-type').val(),
            costPerSqFoot: $('#edit-product-material-cost-per-sq-ft').val(),
            laborPerSqFoot: $('#edit-product-labor-cost-per-sq-ft').val(),
            id: $('#edit-id').val()
        });


        $.ajax({
            url: contextRoot + "/product/",
            type: 'PUT',
            data: productData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                $('#editProductModal').modal('hide');
                var tableRow = buildProductRow(data);

                $('#product-row-' + data.id).replaceWith($(tableRow));
                console.log(data);
            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {

                    $('#edit-product-validation-errors').append(error.message + "<br />");

                });
            }

        });
    });

    function buildProductRow(data) {

        return "<tr id='product-row-" + data.id + "'>  \n\
                <td> <a data-product-id='" + data.id + "' data-toggle='modal' data-target='#showProductModal'>" + data.type + "</td><td> " + data.costPerSqFoot + "</td><td> " + data.laborPerSqFoot + "</td>    \n\
                <td> <a data-product-id='" + data.id + "' data-toggle='modal' data-target='#editProductModal'>Edit</a>  </td>   \n\
                <td> <a data-product-id='" + data.id + "' class='delete-link'>Delete</a>  </td>   \n\
                </tr>  ";

    }


});

$(document).on('click', '#delete-product', function (e) {

    e.preventDefault();

    var productId = $(e.target).data('product-id');

    $.ajax({
        type: 'DELETE',
        url: contextRoot + "/product/" + productId,
        success: function (data, status) {
            $('#product-row-' + productId).remove();
        },
        error: function (data, status) {

        }
    });

});
