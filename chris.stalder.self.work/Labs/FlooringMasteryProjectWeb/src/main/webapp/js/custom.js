/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadDropdown();
    $('#create-submit').on('click', function (e) {
        e.preventDefault();
//        alert('works');
        $('#add-order-validation-errors').empty();
        var orderData = JSON.stringify({
            date: $('#date-input').val(),
            customerId: $('#customer-input').val(),
            taxId: $('#state-input').val(),
            productId: $('#product-input').val(),
            area: $('#area-input').val()

        });

        $.ajax({
            url: contextRoot + "/order/",
            type: "POST",
            data: orderData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                console.log(data);

                var tableRow = buildOrderRow(data);
                $('#order-table').append($(tableRow));

                $('#date-input').val('');
                $('#customer-input').val('');
                $('#state-input').val('');
                $('#product-input').val('');
                $('#area-input').val('');
                loadDropdown(data.date);
//              
            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {
                    var error_type = error.fieldName;

                    if (error_type === 'date') {
                        $('#date-input-div').addClass('has-error');
                        $('#add-order-validation-date-error').append(error.message + "<br />");
                    } else if (error_type === 'customerId') {
                        $('#customer-input-div').addClass('has-error');
                        $('#add-order-validation-customer-error').append(error.message + "<br />");
                    } else if (error_type === 'taxId') {
                        $('#state-input-div').addClass('has-error');
                        $('#add-order-validation-state-error').append(error.message + "<br />");
                    } else if (error_type === 'productId') {
                        $('#product-input-div').addClass('has-error');
                        $('#add-order-validation-product-error').append(error.message + "<br />");
                    } else if (error_type === 'area') {
                        $('#area-input-div').addClass('has-error');
                        $('#add-order-validation-area-error').append(error.message + "<br />");
                    }


                });
            }

        });

    });


    $('#showOrderModal').on('show.bs.modal', function (e) {

        var link = $(e.relatedTarget);

        var orderId = link.data('order-id');

//        
        $.ajax({
            url: contextRoot + "/order/" + orderId,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#order-date').text(data.date);
                $('#order-customer-name').text(data.customer.name);
                $('#order-state').text(data.tax.state);
                $('#order-product').text(data.product.type);
                $('#order-area').text(data.area);
                $('#order-material-cost-per').text(data.product.costPerSqFoot);
                $('#order-labor-cost-per').text(data.product.laborPerSqFoot);
                $('#order-total-material-cost').text(data.materialCost);
                $('#order-total-labor-cost').text(data.laborCost);
                $('#order-tax').text(data.totalTax);
                $('#order-total').text(data.total);

            },
            error: function (data, status) {

            }

        });



    });

    $('#editOrderModal').on('show.bs.modal', function (e) {

        var link = $(e.relatedTarget);

        var orderId = link.data('order-id');

        $.ajax({
            url: contextRoot + "/order/" + orderId,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
//                var dateFormat = require('dateformat');
//                var now = data.date;
//                dateFormat(now, 'MM/dd/yyyy');
                $('#edit-order-date').val(data.date);
                console.log(data.date);
                $('#edit-order-customer-name').val(data.customer.name);
                $('#edit-order-customer-name option[value='+ data.customer.id+']');
                console.log(data.customer.name);
                $('#edit-order-state').val(data.tax.state);
                console.log(data.tax.state);
                $('#edit-order-product').val(data.product.type);
                console.log(data.product.type);
                $('#edit-order-area').val(data.area);
                console.log(data.area);
                $('#edit-id').val(data.globalId);
                console.log(data.globalId);
//                console.log(data.id);
//                console.log(data);
            },
            error: function (data, status) {
            }
        });
    });

    $('#edit-order-button').on('click', function (e) {

        var orderData = JSON.stringify({
            date: $('#edit-order-date').val(),
            customerId: $('#edit-order-customer-name').val(),
            taxId: $('#edit-order-state').val(),
            productId: $('#edit-order-product').val(),
            area: $('#edit-order-area').val(),
            id: $('#edit-id').val()

        });

        $.ajax({
            url: contextRoot + "/order/",
            type: "PUT",
            data: orderData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                $('#editOrderModal').modal('hide');

                var tableRow = buildOrderRow(data);

                $('#order-row-' + data.globalId).replaceWith($(tableRow));
            },
            error: function (data, status) {
                alert("error");
                console.log(data);
            }

        });

    });

    function loadDropdown(selectedValue) {
        $('#orderDatesMenu').empty();
        $.ajax({
            url: contextRoot + "/menu/",
            type: "GET",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                var select = document.getElementById("orderDatesMenu");

                var startOption = document.createElement("option");
                startOption.text = "Select Date";
                select.add(startOption);

                for (var i = 0; i < data.length; i++) {
                    var option = document.createElement("option");

                    option.text = data[i];
                    select.add(option);
                    console.log(data[i]);

                    if (selectedValue) {
                        option.selected = true;
                        console.log(selectedValue);
                    }
                }

                buildTable(selectedValue);

            },
            error: function (data, status) {

            }

        });
    }


    function buildOrderRow(data) {

        return "<tr id='order-row-" + data.globalId + "'>  \n\
                <td> " + data.date + "</a></td>  \n\
                <td> <a data-order-id='" + data.globalId + "' data-toggle='modal' data-target='#showOrderModal'>" + data.customer.name + "</td><td> " + data.tax.state + "</td><td> " + data.product.type + "</td><td> " + data.area + "</td>    \n\
                <td> " + data.totalTax + "</td><td> " + data.total + "</td>    \n\
                <td> <a data-order-id='" + data.globalId + "' data-toggle='modal' data-target='#editOrderModal'>Edit</a>  </td>   \n\
                <td> <a data-order-id='" + data.globalId + "' class='delete-link'>Delete</a>  </td>   \n\
                </tr>  ";

    }

    function buildTable(selectedDate) {
//        var selectedDate = $('#orderDatesMenu').val();
        $('#order-table tbody').remove();
        $.ajax({
            url: contextRoot + "/menu/" + selectedDate,
            type: "POST",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                for (var i = 0; i < data.length; i++) {
                    var tableRow = buildOrderRow(data[i]);
                    $('#order-table').append(tableRow);
                    console.log(i);
                }
            },
            error: function (data, status) {
            }

        });
    }
    $('#orderDatesMenu').on('change', function (e) {
//        alert(this.value);

        var selectedDate = this.value;

        if (selectedDate !== null) {
            $('#order-table tbody').remove();
            $.ajax({
                url: contextRoot + "/menu/" + selectedDate,
                type: "POST",
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-type", "application/json");
                },
                success: function (data, status) {

                    for (var i = 0; i < data.length; i++) {
                        var tableRow = buildOrderRow(data[i]);
                        $('#order-table').append(tableRow);
                        console.log(i);
                    }


                },
                error: function (data, status) {
//                    var errors = data.responseJSON.errors;
//
//                    $.each(errors, function (index, error) {
//
//                        $('#edit-order-validation-errors').append(error.message + "<br />");
//
//                    });
                }

            });

        }
    });

    $('#add-customer-button').on('click', function (e) {
        e.preventDefault();
//        alert('works');
        $('#add-order-validation-errors').empty();
        var customerData = JSON.stringify({
            name: $('#customer-name-input').val()
        });

        $.ajax({
            url: contextRoot + "/customer/",
            type: "POST",
            data: customerData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                console.log(data);
                $('#addCustomerModal').modal('hide');
                var select = document.getElementById('#customer-input');
                var option = document.createElement("option");

                option.text = data.name;
                select.add(option);

//              
            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {

                    $('#customer-input-div').addClass('has-error');
                    $('#add-customer-validation-error').append(error.message + "<br />");
                });
            }

        });

    });
});


$(document).on('click', '.delete-link', function (e) {

    e.preventDefault();

    var orderId = $(e.target).data('order-id');

    $.ajax({
        type: 'DELETE',
        url: contextRoot + "/order/" + orderId,
        success: function (data, status) {
            $('#order-row-' + orderId).remove();
        },
        error: function (data, status) {

        }
    });

});