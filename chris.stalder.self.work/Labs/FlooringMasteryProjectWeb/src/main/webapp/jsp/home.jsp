<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Mastery Project  |   Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>Flooring Mastery Project</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/admin/login">Admin</a></li>
                </ul>    
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="navbar">
                            <form action="${pageContext.request.contextPath}/" method="POST">

                                <select class="btn btn-default dropdown" name="orderDatesMenu" id="orderDatesMenu">
                                    <option>Select Date</option>
                                    <%--<c:forEach items="${orderDates}" var="date">--%>
                                        <!--<option value="${date}" ${selectedDate.equals(date)? 'selected' : ''}>${date}</option>-->
                                    <%--</c:forEach>--%>
                                </select>
                                <!--<input type="submit" class="btn btn-primary" value="Get Orders"/>-->


                            </form>
                        </div>
                    </div>
                    <table class="table table-bordered table-hover" id="order-table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Customer Name</th>
                                <th>State</th>
                                <th>Product</th>
                                <th>Area</th>
                                <th>Tax</th>
                                <th>Total</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${orders}" var="order">
                                <tr id="order-row-${order.globalId}">
                                    <c:set var="tax" value="${order.tax}"/>
                                    <c:set var="total" value="${order.total}"/>
                                    <fmt:setLocale value="en_US"/>
                                    <td><fmt:formatDate pattern="MM/dd/yyyy" value="${order.date}"/></td>
                                    <td><a data-order-id="${order.globalId}" data-toggle="modal" data-target="#showOrderModal">${order.customer.name}</a></td>
                                    <td>${order.tax.state}</td>
                                    <td>${order.product.type}</td>
                                    <td>${order.area}</td>
                                    <td><fmt:formatNumber value="${tax}" type="currency"/></p></td>
                                    <td><fmt:formatNumber value="${total}" type="currency"/></p></td>
                                    <td><a data-order-id="${order.globalId}" data-toggle="modal" data-target="#editOrderModal">Edit</a></td>
                                    <td><a data-order-id="${order.globalId}" class="delete-link">Delete</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>

                    </table>
                </div>
                <div class="col-md-4">
                    <form method="POST" class="form-horizontal">

                        <div class="form-group" id='date-input-div'>
                            <label for="date" class="col-md-4 control-label">Date: </label>
                            <div class="col-md-8">
                                <input name="date" type="date" id="date-input" class="form-control"></input>
                            </div>
                            <div id="add-order-validation-date-error" class="validation-errors pull-right"></div>
                        </div>

                        <div class="form-group" id='customer-input-div'>
                            <label for="customer-name" class="col-md-4 control-label">Customer Name: </label>
                            <!--                            <div class="col-md-8">
                                                            <input type="text" id="customer-name-input" class="form-control"></input>
                                                        </div>-->
                            <div class="col-md-8">
                                <select class="btn btn-default dropdown" id="customer-input" name="customer">
                                    <c:forEach items="${existingCustomers}" var="customer">
                                        <option value="${customer.id}"class="form-control">${customer.name}</option>
                                    </c:forEach>
                                </select>
                                <!--<input type="text" id="state-input" class="form-control"></input>-->
                            </div>
                            <div id="add-order-validation-customer-error" class="validation-errors pull-right"></div>
                        </div>

                        <div class="form-group" id='new-customer'>
                            <label for="new-customer" class="col-md-4 control-label"></label>
                            <div class="col-md-1">
                                <input value="New Customer" id="create-customer-submit" class="btn btn-xs btn-default" data-toggle="modal" data-target="#addCustomerModal">
                            </div>
                        </div>

                        <div class="form-group" id='state-input-div'>
                            <label for="state" class="col-md-4 control-label">State: </label>
                            <div class="col-md-8">
                                <select class="btn btn-default dropdown" id="state-input" name="state">
                                    <c:forEach items="${taxList}" var="tax">
                                        <option value="${tax.id}"class="form-control">${tax.state}</option>
                                    </c:forEach>
                                </select>
                                <!--<input type="text" id="state-input" class="form-control"></input>-->
                            </div>
                            <div id="add-order-validation-state-error" class="validation-errors pull-right"></div>
                        </div>

                        <div class="form-group" id='product-input-div'>
                            <label for="type" class="col-md-4 control-label">Product: </label>
                            <div class="col-md-8">
                                <select class="btn btn-default dropdown" id="product-input" name="type">
                                    <c:forEach items="${products}" var="product">
                                        <option value="${product.id}"class="form-control">${product.type}</option>
                                    </c:forEach>
                                </select>
                                <!--                                <input type="text" id="product-input" class="form-control"></input>-->
                            </div>
                            <div id="add-order-validation-product-error" class="validation-errors pull-right"></div>
                        </div>

                        <div class="form-group" id='area-input-div'>
                            <label for="area" class="col-md-4 control-label">Area: </label>
                            <div class="col-md-8">
                                <input type="text" id="area-input" class="form-control"></input>
                            </div>
                            <div id="add-order-validation-area-error" class="validation-errors pull-right"></div>
                        </div>

                        <input id="create-submit" type="submit" class="btn btn-primary center-block"/>
                    </form><br>
                    <!--<div id="add-order-validation-errors" class="pull-right"></div>-->
                </div>
            </div>
        </div>

        <div id="showOrderModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Order Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">

                            <tr>
                                <th>Order Date:</th>
                                <td id="order-date"></td>
                            </tr>
                            <tr>
                                <th>Customer Name:</th>
                                <td id="order-customer-name"></td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td id="order-state"></td>
                            </tr>
                            <tr>
                                <th>Product:</th>
                                <td id="order-product"></td>
                            </tr>
                            <tr>
                                <th>Area:</th>
                                <td id="order-area"></td>
                            </tr>
                            <tr>
                                <th>Material Cost Per Sq Foot:</th>
                                <td id="order-material-cost-per"></td>
                            </tr>
                            <tr>
                                <th>Labor Cost Per Sq Foot:</th>
                                <td id="order-labor-cost-per"></td>
                            </tr>
                            <tr>
                                <th>Total Material Cost:</th>
                                <td id="order-total-material-cost"></td>
                            </tr>
                            <tr>
                                <th>Total Labor Cost:</th>
                                <td id="order-total-labor-cost"></td>
                            </tr>
                            <tr>
                                <th>Tax:</th>
                                <td id="order-tax"></td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td id="order-total"></td>
                            </tr>

                        </table>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="editOrderModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Order Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <input type="hidden" id="edit-id"/>
                            <tr>
                                <th>Order Date:</th>
                                <td>
                                    <input name="date" type="date" id="edit-order-date" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <th>Customer Name:</th>
                                <td>
                                    <!--<input type="text" id="edit-order-customer-name">-->
                                    <select class="btn btn-default dropdown" id="edit-order-customer-name" name="customer">
                                        <c:forEach items="${existingCustomers}" var="customer">
                                            <option value="${customer.id}" class="form-control">${customer.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td>
                                    <!--<input type="text" id="edit-order-state">-->
                                    <select class="btn btn-default dropdown" id="edit-order-state" name="state">
                                        <c:forEach items="${taxList}" var="tax">
                                            <option value="${tax.id}"class="form-control">${tax.state}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Product:</th>
                                <td>
                                    <!--<input type="text" id="edit-order-product">-->
                                    <select class="btn btn-default dropdown" id="edit-order-product" name="type">
                                        <c:forEach items="${products}" var="product">
                                            <option value="${product.id}"class="form-control">${product.type}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Area:</th>
                                <td>
                                    <input type="text" id="edit-order-area">
                                </td>
                            </tr>


                        </table><br>

                        <div id="edit-order-validation-errors" class="pull-right"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="edit-order-button">Save Changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="addCustomerModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Customer</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
<!--                            <tr>
                                <th>Customer Name: </th>
                                <td>
                                    <input type="text" id="customer-name-input">
                                </td>
                            </tr>-->
                            <div class="form-group" id='customer-input-div'>
                                <label for="customer" class="col-md-4 control-label">Customer Name: </label>
                                <div class="col-md-8">
                                    <input type="text" id="customer-name-input">                                </div>
                                <div id="add-customer-validation-date-error" class="validation-errors pull-right"></div>
                            </div>

                        </table><br>

<!--                        <div id="add-customer-validation-error" class="validation-errors pull-right"></div>-->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="add-customer-button">Save</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!--        <div id="loginModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Login</h4>
                            </div>
                            <div class="modal-body">
        
                                <table class="table table-bordered">
        
                                    <tr>
                                        <th>Enter Admin Password:</th>
                                        <td>
                                            <input type="password" id="password-input">
                                        </td>
                                    </tr>
                                    <tr>
                                        <div id="invalid-password" class="center-block"></div>
                                    </tr>
        
                                </table><br>
        
                                
        
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="whywontyouworkbutton">Enter</button>
                            </div>
                        </div> /.modal-content 
                    </div> /.modal-dialog 
                </div> /.modal -->

        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/custom.js"></script>



    </body>
</html>

