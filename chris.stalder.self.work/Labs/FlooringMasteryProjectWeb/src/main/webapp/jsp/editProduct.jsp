<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Mastery Project  |   Edit</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Flooring Mastery Project</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                </ul>    
            </div>
            <div class="row">
                <div class="col-md-8">
                    <form:form method="POST" commandName="product" action="./">
                        <div class="form-group">
                            <label for="" class="col-md-4">Product Type: </label>
                            <div class="col-md-8">
                                <form:input path="type" class="form-control" value="${product.type}"></form:input>
                                <form:errors path="type" class="form-control"/>                             
                            </div>
                        </div><br><br>

                        <div class="form-group">
                            <label for="" class="col-md-4">Material Cost Per Sq Foot: </label>
                            <div class="col-md-8">
                                <form:input path="costPerSqFoot" class="form-control" type="number" value="${product.costPerSqFoot}"></form:input>
                                <form:errors path="costPerSqFoot" class="form-control"/>                             
                            </div>
                        </div><br><br>

                        <div class="form-group">
                            <label for="" class="col-md-4">Labor Cost Per Sq Foot: </label>
                            <div class="col-md-8">
                                <form:input path="laborPerSqFoot" class="form-control" type="number" value="${product.laborPerSqFoot}"></form:input>
                                <form:errors path="laborPerSqFoot" class="form-control"/>                             
                            </div>
                        </div><br><br>

                        <input type="submit" class="btn btn-primary center-block"/>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

