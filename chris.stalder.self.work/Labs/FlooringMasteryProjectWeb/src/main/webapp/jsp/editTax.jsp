<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Mastery Project  |   Edit</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Flooring Mastery Project</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                </ul>    
            </div>
            <div class="row">
                <div class="col-md-8">
                    <form:form method="POST" commandName="tax" action="./">
                        <div class="form-group">
                            <label for="state" class="col-md-4">State: </label>
                            <div class="col-md-8">
                                <form:input path="state" class="form-control" value="${tax.state}"></form:input>
                                <form:errors path="state" class="form-control"/>                             
                            </div>
                        </div><br><br>

                        <div class="form-group">
                            <label for="" class="col-md-4">Tax Rate: </label>
                            <div class="col-md-8">
                                <form:input path="taxRate" class="form-control" type="number" value="${tax.taxRate}"></form:input>
                                <form:errors path="taxRate" class="form-control"/>                             
                            </div>
                        </div><br><br>

                        <input type="submit" class="btn btn-primary center-block"/>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

