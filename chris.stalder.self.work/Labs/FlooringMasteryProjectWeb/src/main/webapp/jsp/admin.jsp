<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Mastery Project  |   Admin</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <!--<link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">-->
        <link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Flooring Mastery Project</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/admin">Admin</a></li>
                </ul>    
            </div>
            <div class="row">
                <div class="container">
                    <form action="${pageContext.request.contextPath}/admin/changePassword" method="GET">
                        <input type="submit" class="btn btn-default center-block" value="Change Password"/>
                    </form>
                </div>
            </div><br><br>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-hover" id="product-table">
                        <tr>
                            <td>Product Type</td>
                            <td>Material Cost Per Sq Foot</td>
                            <td>Labor Cost Per Sq Foot</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                        <c:forEach items="${products}" var="product">
                            <tr id="product-row-${product.id}">
                                <td><a data-product-id="${product.id}" data-toggle="modal" data-target="#showProductModal">${product.type}</a></td>
                                <td><fmt:formatNumber value="${product.costPerSqFoot}" type="currency"/></p></td>
                                <td><fmt:formatNumber value="${product.laborPerSqFoot}" type="currency"/></p></td>
                                <td><a data-product-id="${product.id}" data-toggle="modal" data-target="#editProductModal">Edit</a></td>
                                <td><a data-product-id="${product.id}" class="delete-link" id="delete-product">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="col-md-6">
                    <form method="POST" class="form-horizontal">
                        <div class="form-group">
                            <label for="" class="col-md-4">Product Type: </label>
                            <div class="col-md-8">
                                <input type="text" id="product-type-input" class="form-control"/>
                            </div>
                        </div><br><br>

                        <div class="form-group">
                            <label for="" class="col-md-4">Material Cost Per Sq Foot: </label>
                            <div class="col-md-8">
                                <input type="text" id="material-cost-per-sq-ft-input" class="form-control"/>                           
                            </div>
                        </div><br><br>

                        <div class="form-group">
                            <label for="" class="col-md-4">Labor Cost Per Sq Foot: </label>
                            <div class="col-md-8">
                                <input type="text" id="labor-cost-per-sq-ft-input" class="form-control"/>                     
                            </div>
                        </div><br><br>

                        <input id="create-product-submit" type="submit" class="btn btn-primary center-block"/>
                    </form><br>
                    <div id="add-product-validation-errors" class="pull-right"></div><br>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-hover" id="tax-table">
                        <tr>
                            <td>State</td>
                            <td>Tax Rate</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                        <c:forEach items="${taxes}" var="tax">
                            <tr id="tax-row-${tax.id}">
                                <td><a data-tax-id="${tax.id}" data-toggle="modal" data-target="#showTaxModal">${tax.state}</a></td>
                                <td><fmt:formatNumber type="number" maxIntegerDigits="2" value="${tax.taxRate}" /></td>
                                <td><a data-tax-id="${tax.id}" data-toggle="modal" data-target="#editTaxModal">Edit</a></td>
                                <td><a data-tax-id="${tax.id}" class="delete-link" id="delete-tax">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="col-md-6">
                    <form method="POST" class="form-horizontal">
                        <div class="form-group">
                            <label for="state" class="col-md-4">State: </label>
                            <div class="col-md-8">
                                <input type="text" id="tax-state-input" class="form-control"/>                           
                            </div>
                        </div><br><br>

                        <div class="form-group">
                            <label for="taxRate" class="col-md-4">Tax Rate: </label>
                            <div class="col-md-8">
                                <input type="text" id="tax-taxRate-input" class="form-control"/>                           
                            </div>
                        </div><br><br>

                        <input id="tax-create-submit" type="submit" class="btn btn-primary center-block"/>
                        <div id="add-tax-validation-errors" class="pull-right"></div>
                    </form>

                </div>
            </div>
        </div>

        <div id="showProductModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Product Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">

                            <tr>
                                <th>Product Type:</th>
                                <td id="product-type"></td>
                            </tr>
                            <tr>
                                <th>Material Cost Per Sq Ft:</th>
                                <td id="product-material-cost-per-sq-ft"></td>
                            </tr>
                            <tr>
                                <th>Labor Cost Per Sq Ft:</th>
                                <td id="product-labor-cost-per-sq-ft"></td>
                            </tr>

                        </table>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="editProductModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Product Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <input type="hidden" id="edit-id"/>
                            <tr>
                                <th>Product Type:</th>
                                <td>
                                    <input type="text" id="edit-product-type">
                                </td>
                            </tr>
                            <tr>
                                <th>Material Cost Per Sq Ft:</th>
                                <td> 
                                    <input type="text" id="edit-product-material-cost-per-sq-ft">
                                </td>
                            </tr>
                            <tr>
                                <th>Labor Cost Per Sq Ft:</th>
                                <td>
                                    <input type="text" id="edit-product-labor-cost-per-sq-ft">
                                </td>
                            </tr>

                        </table>

                        <div id="edit-product-validation-errors" class="pull-right"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-default" id="edit-product-button">Save</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="showTaxModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Tax Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">

                            <tr>
                                <th>State:</th>
                                <td id="tax-state"></td>
                            </tr>
                            <tr>
                                <th>Tax Rate:</th>
                                <td id="tax-taxrate"></td>
                            </tr>

                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="editTaxModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Tax Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">

                            <tr>
                                <th>State:</th>
                                <td>
                                    <input type="text" id="edit-tax-state">
                                </td>
                            </tr>
                            <tr>
                                <th>Tax Rate:</th>
                                <td> 
                                    <input type="text" id="edit-tax-taxrate">
                                </td>
                            </tr>


                        </table>

                        <div id="edit-tax-validation-errors" class="pull-right"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-default" id="edit-tax-button">Save</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/tax.js"></script>
        <script src="${pageContext.request.contextPath}/js/product.js"></script>

    </body>
</html>

