/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.controller;

import com.mycompany.flooringmasteryprojectweb.dao.AdminDao;
import com.mycompany.flooringmasteryprojectweb.dao.PasswordDao;
import com.mycompany.flooringmasteryprojectweb.dao.ProductDao;
import com.mycompany.flooringmasteryprojectweb.dao.TaxDao;
import com.mycompany.flooringmasteryprojectweb.dto.Product;
import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import com.mycompany.flooringmasteryprojectweb.utilities.StateConverter;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/product")
public class ProductController {

    AdminDao adminDao;
    TaxDao taxDao;
    ProductDao productDao;
    PasswordDao passwordDao;
    StateConverter stateConverter;

    @Inject
    public ProductController(AdminDao adminDao, TaxDao taxDao, ProductDao productDao, PasswordDao passwordDao, StateConverter sc) {
        this.productDao = productDao;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public Product createProduct(@Valid @RequestBody Product product) {

        productDao.Create(product);
        return product;

    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editProduct(@PathVariable("id") int id, Map model) {
        Product product = productDao.get(id);
        model.put("product", product);
        return "editProduct";
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @ResponseBody
    public Product editProductSubmit(@Valid @RequestBody Product product) {
//        if(bindingResult.hasErrors()){
//            model.put("product", product);
//            return "editProduct";
//        }

        productDao.update(product);
        return product;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Product deleteProduct(@PathVariable("id") int id) {
        Product product = productDao.get(id);
        productDao.delete(product);

        return product;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody //turns response into JSON
    public Product showProduct(@PathVariable("id") int id) {

        Product product = productDao.get(id);
        return product;
    }

}
