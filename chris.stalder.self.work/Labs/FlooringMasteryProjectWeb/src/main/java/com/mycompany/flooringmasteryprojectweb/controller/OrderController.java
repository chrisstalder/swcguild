/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.controller;

import com.mycompany.flooringmasteryprojectweb.dao.CustomerDao;
import com.mycompany.flooringmasteryprojectweb.dao.OrderDao;
import com.mycompany.flooringmasteryprojectweb.dao.ProductDao;
import com.mycompany.flooringmasteryprojectweb.dao.TaxDao;
import com.mycompany.flooringmasteryprojectweb.dto.CreateOrderCommand;
import com.mycompany.flooringmasteryprojectweb.dto.Customer;
import com.mycompany.flooringmasteryprojectweb.dto.Order;
import com.mycompany.flooringmasteryprojectweb.dto.Product;
import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Properties;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/order")
public class OrderController {

    private OrderDao orderDao;
    private ProductDao productDao;
    private TaxDao taxDao;
    private CustomerDao customerDao;

    @Inject
    public OrderController(OrderDao dao, ProductDao productDao, TaxDao taxDao, CustomerDao customerDao) {
        this.orderDao = dao;
        this.productDao = productDao;
        this.taxDao = taxDao;
        this.customerDao = customerDao;

    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public Order create(@Valid @RequestBody CreateOrderCommand orderInput) {

        Product product = productDao.get(orderInput.getProductId());

        Tax tax = taxDao.get(orderInput.getTaxId());

        double materialCost = RoundTo2Decimals(orderInput.getArea() * product.getCostPerSqFoot());
        double laborCost = RoundTo2Decimals(orderInput.getArea() * product.getLaborPerSqFoot());
        double totalTax = RoundTo2Decimals((materialCost + laborCost) * (tax.getTaxRate() / 100));
        double total = RoundTo2Decimals(materialCost + laborCost + totalTax);
        Order order = new Order();

        Customer customer = customerDao.get(orderInput.getCustomerId());
        
        order.setDate(orderInput.getDate());
        order.setCustomer(customer);
        order.setTax(tax);
        order.setProduct(product);
        order.setArea(orderInput.getArea());
        order.setCostPerSqFoot(product.getCostPerSqFoot());
        order.setLaborCostPerSqFoot(product.getLaborPerSqFoot());
        order.setMaterialCost(materialCost);
        order.setLaborCost(laborCost);
        order.setTotalTax(totalTax);
        order.setTotal(total);
//        sendEmail(order);
        orderDao.create(order);
        
        return order;

    }

    @RequestMapping(value = "/edit/{globalId}", method = RequestMethod.GET)
    public String edit(@PathVariable("globalId") int globalId, Map model) {
        Order order = orderDao.get(globalId);
        model.put("order", order);
        return "edit";
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @ResponseBody
    public Order editSubmit(@Valid @RequestBody CreateOrderCommand createOrderCommand) {
        Order order = orderDao.get(createOrderCommand.getId());
        order.setDate(createOrderCommand.getDate());
        
        Customer customer = customerDao.get(createOrderCommand.getCustomerId());
        order.setCustomer(customer);

        Tax tax = taxDao.get(createOrderCommand.getTaxId());
        order.setTax(tax);
        
        Product product = productDao.get(createOrderCommand.getProductId());
        order.setProduct(product);
        order.setArea(createOrderCommand.getArea());
        double materialCost = RoundTo2Decimals(order.getArea() * product.getCostPerSqFoot());
        double laborCost = RoundTo2Decimals(order.getArea() * product.getLaborPerSqFoot());
        double totalTax = RoundTo2Decimals((materialCost + laborCost) * (tax.getTaxRate() / 100));
        double total = RoundTo2Decimals(materialCost + laborCost + totalTax);

        order.setCostPerSqFoot(product.getCostPerSqFoot());
        order.setLaborCostPerSqFoot(product.getLaborPerSqFoot());
        order.setMaterialCost(materialCost);
        order.setLaborCost(laborCost);
        order.setTotalTax(totalTax);
        order.setTotal(total);

        orderDao.update(order);

        return order;
    }

    @RequestMapping(value = "/{globalId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Order delete(@PathVariable("globalId") int globalId) {
        Order order = orderDao.get(globalId);
        orderDao.delete(order);
        return order;
    }

    @RequestMapping(value = "/{globalId}", method = RequestMethod.GET)
    @ResponseBody //turns response into JSON
    public Order show(@PathVariable("globalId") Integer orderId) {

        Order order = orderDao.get(orderId);
        Customer customer = customerDao.get(order.getCustomer().getId());
        Product product = productDao.get(order.getProduct().getId());
        Tax tax = taxDao.get(order.getTax().getId());
        
        order.setCustomer(customer);
        order.setProduct(product);
        order.setTax(tax);
        return order;
    }

    public double RoundTo2Decimals(double val) {
        DecimalFormat df2 = new DecimalFormat("###.##");
        return Double.valueOf(df2.format(val));
    }

    public void sendEmail(Order order) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("account", "password");
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("cstalde1@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("christopher.jr.stalder@gmail.com"));
            message.setSubject("Testing Subject");
            message.setText("Dear Mail Crawler,"
                    + "\n\n No spam to my email, please!");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

      private MailSender mailSender;
    private SimpleMailMessage templateMessage;

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setTemplateMessage(SimpleMailMessage templateMessage) {
        this.templateMessage = templateMessage;
    }
}
