/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.controller;

import com.mycompany.flooringmasteryprojectweb.dao.CustomerDao;
import com.mycompany.flooringmasteryprojectweb.dao.OrderDao;
import com.mycompany.flooringmasteryprojectweb.dao.ProductDao;
import com.mycompany.flooringmasteryprojectweb.dao.TaxDao;
import com.mycompany.flooringmasteryprojectweb.dto.CreateOrderCommand;
import com.mycompany.flooringmasteryprojectweb.dto.Customer;
import com.mycompany.flooringmasteryprojectweb.dto.Order;
import com.mycompany.flooringmasteryprojectweb.dto.Product;
import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {

    private OrderDao orderDao;
    private TaxDao taxDao;
    private ProductDao productDao;
    private CustomerDao customerDao;
    
    @Inject
    public HomeController(OrderDao dao, TaxDao taxDao, ProductDao productDao, CustomerDao customerDao) {
        this.orderDao = dao;
        this.taxDao = taxDao;
        this.productDao = productDao;
        this.customerDao = customerDao;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Map model) {

        List<Date> orderDates = orderDao.listDates();

        List<Tax> taxList = taxDao.list();
        List<Product> products = productDao.list();
      
        List<Customer> existingCustomers = customerDao.list();
//        if (!"".equals(date)) {
//            List<Order> orders = orderDao.getList(date);
//            String selectedDate = date;
//            model.put("selectedDate", selectedDate);
//        }//else
        model.put("orderDates", orderDates);
        model.put("existingCustomers", existingCustomers);
        model.put("order", new CreateOrderCommand());
        model.put("taxList", taxList);
        model.put("products", products);
        return "home";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String getOrders(@RequestParam("orderDatesMenu") Date date, Map model) {
        List<Order> orders = orderDao.getList(date);
        List<Date> orderDates = orderDao.listDates();
        Date selectedDate = date;
        List<String> states = taxDao.listValidStates();
        List<String> products = productDao.listProductTypes();

        model.put("selectedDate", selectedDate);
        model.put("orderDates", orderDates);
        model.put("orders", orders);
        model.put("order", new CreateOrderCommand());
        model.put("validStates", states);
        model.put("products", products);
        return "home";
    }

}
