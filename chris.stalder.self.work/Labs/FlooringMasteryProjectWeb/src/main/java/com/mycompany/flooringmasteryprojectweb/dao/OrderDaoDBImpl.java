/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import com.mycompany.flooringmasteryprojectweb.dto.Customer;
import com.mycompany.flooringmasteryprojectweb.dto.Order;
import com.mycompany.flooringmasteryprojectweb.dto.Product;
import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class OrderDaoDBImpl implements OrderDao {

    private static final String SQL_INSERT_ORDER = "INSERT INTO orders (customer_id, product_id, tax_id, area, total_labor_cost, total_mat_cost, tax, total, order_date, cancelled) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String SQL_INSERT_ORDER_HISTORY = "INSERT INTO order_history (customer_id, order_id, date_placed) VALUES (?, ?, ?);";
    private static final String SQL_UPDATE_ORDER = "UPDATE orders SET customer_id = ?, product_id = ?, tax_id = ?, area = ?, total_labor_cost = ?, total_mat_cost = ?, tax = ?, total = ?, order_date = ? WHERE id = ?";
    private static final String SQL_CANCEL_ORDER = "UPDATE orders SET cancelled=? WHERE id= ?;";
    private static final String SQL_UPDATE_ORDER_HISTORY = "UPDATE order_history SET cancelled=? WHERE order_id=?;";
    private static final String SQL_SELECT_ORDER = "SELECT * FROM orders WHERE id = ?";
    private static final String SQL_GET_ORDER_LIST = "SELECT * FROM orders";
    private static final String SQL_GET_ORDER_LIST_BY_DATE = "SELECT * FROM orders WHERE DATE(order_date) = ? AND cancelled = ?";
    private static final String SQL_GET_DATE_LIST = "SELECT order_date FROM orders WHERE cancelled = ?;";

    private JdbcTemplate jdbcTemplate;

    public OrderDaoDBImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Order create(Order order) {

        jdbcTemplate.update(SQL_INSERT_ORDER,
                order.getCustomer().getId(),
                order.getProduct().getId(),
                order.getTax().getId(),
                order.getArea(),
                order.getLaborCost(),
                order.getMaterialCost(),
                order.getTotalTax(),
                order.getTotal(),
                order.getDate(),
                0);

        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        order.setGlobalId(id);

        jdbcTemplate.update(SQL_INSERT_ORDER_HISTORY,
                order.getCustomer().getId(),
                order.getGlobalId(),
                order.getDate());

        return order;
    }

    @Override
    public List<Order> getList(Date date) {
//        DateFormat format = new SimpleDateFormat("MMddyyyy");
//        Date parsedDate = new Date();
//        try {
//            parsedDate = format.parse(date);
//            format.format(parsedDate);
//        } catch (ParseException ex) {
//            Logger.getLogger(OrderDaoDBImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }

        List<Order> orders = jdbcTemplate.query(SQL_GET_ORDER_LIST_BY_DATE, new OrderMapper(), date, 0);
        return orders;
    }

    @Override
    public Order get(int id) {
        return jdbcTemplate.queryForObject(SQL_SELECT_ORDER, new OrderMapper(), id);
    }

    @Override
    public void update(Order order) {

        jdbcTemplate.update(SQL_UPDATE_ORDER,
                order.getCustomer().getId(),
                order.getProduct().getId(),
                order.getTax().getId(),
                order.getArea(),
                order.getLaborCost(),
                order.getMaterialCost(),
                order.getTotalTax(),
                order.getTotal(),
                order.getDate(),
                order.getGlobalId());
    }

    @Override
    public void delete(Order order) {
        jdbcTemplate.update(SQL_CANCEL_ORDER, 1, order.getGlobalId());
        
        jdbcTemplate.update(SQL_UPDATE_ORDER_HISTORY, 1, order.getGlobalId());
    }

    @Override
    public List<Order> list() {
        return jdbcTemplate.query(SQL_GET_ORDER_LIST, new OrderMapper());
    }

    @Override
    public List<Date> listDates() {
        List<Date> orderDates = jdbcTemplate.query(SQL_GET_DATE_LIST, new DateMapper(), 0);

        List<Date> dates = new ArrayList();
//        List<String> organizedList = new ArrayList();
//
//        DateFormat format = new SimpleDateFormat("MMddyyyy");
//        for (String date : orderDates) {
//            try {
//                dates.add(format.parse(date));
//            } catch (ParseException ex) {
//                Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }

        Collections.sort(orderDates);

        for (Date date : orderDates) {
            if (!dates.contains(date)) {
                dates.add((date));
            }
        }
        return dates;
    }

    private static final class OrderMapper implements RowMapper<Order> {

        @Override
        public Order mapRow(ResultSet rs, int i) throws SQLException {

            Order order = new Order();

            Customer customer = new Customer();
            customer.setId(rs.getInt("customer_id"));

            Product product = new Product();
            product.setId(rs.getInt("product_id"));

            Tax tax = new Tax();
            tax.setId(rs.getInt("tax_id"));

            order.setGlobalId(rs.getInt("id"));
            order.setCustomer(customer);
            order.setProduct(product);
            order.setTax(tax);
            order.setArea(rs.getDouble("area"));
            order.setLaborCost(rs.getDouble("total_labor_cost"));
            order.setMaterialCost(rs.getDouble("total_mat_cost"));
            order.setTotalTax(rs.getDouble("tax"));
            order.setTotal(rs.getDouble("total"));
            order.setDate(rs.getDate("order_date"));

            return order;

        }
    }

    private static final class DateMapper implements RowMapper<Date> {

        @Override
        public Date mapRow(ResultSet rs, int i) throws SQLException {

            Date orderDate = rs.getDate("order_date");

//            DateFormat format = new SimpleDateFormat("MMddyyyy");
//
//            String date = format.format(orderDate);
            return orderDate;

        }

    }

}
