/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import com.mycompany.flooringmasteryprojectweb.dto.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class ProductDaoDBImpl implements ProductDao {

    private static final String SQL_INSERT_PRODUCT = "INSERT INTO product(product_type, material_cost_per_sq_ft, labor_cost_per_sq_ft, active) VALUES(?, ?, ?, ?)";
    private static final String SQL_UPDATE_PRODUCT = "UPDATE product SET product_type = ?, material_cost_per_sq_ft = ?, labor_cost_per_sq_ft = ? WHERE id = ?";
    private static final String SQL_SOFT_DELETE_PRODUCT = "UPDATE product SET active=0, date_removed=? WHERE id=?;";
    private static final String SQL_SELECT_PRODUCT = "SELECT * FROM product WHERE id = ?";
    private static final String SQL_GET_PRODUCT_TYPE_LIST = "SELECT * FROM product";
    private static final String SQL_GET_PRODUCT_LIST = "SELECT * FROM product WHERE active = ?";

    private JdbcTemplate jdbcTemplate;

    public ProductDaoDBImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Product Create(Product product) {

        jdbcTemplate.update(SQL_INSERT_PRODUCT,
                product.getType(),
                product.getCostPerSqFoot(),
                product.getLaborPerSqFoot(),
                1);

        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        product.setId(id);
        return product;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Product get(int id) {
        return jdbcTemplate.queryForObject(SQL_SELECT_PRODUCT, new ProductMapper(), id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(Product product) {
        
        jdbcTemplate.update(SQL_UPDATE_PRODUCT, 
                product.getType(),
                product.getCostPerSqFoot(),
                product.getLaborPerSqFoot(),

                product.getId());
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Product product) {
        jdbcTemplate.update(SQL_SOFT_DELETE_PRODUCT, new Date(), product.getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Product> list() {
        return jdbcTemplate.query(SQL_GET_PRODUCT_LIST, new ProductMapper(), 1);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<String> listProductTypes() {
        return jdbcTemplate.query(SQL_GET_PRODUCT_TYPE_LIST, new ProductTypeMapper());
    }

    private static final class ProductMapper implements RowMapper<Product> {

        @Override
        public Product mapRow(ResultSet rs, int i) throws SQLException {

            Product product = new Product();

            product.setId(rs.getInt("id"));
            product.setType(rs.getString("product_type"));
            product.setCostPerSqFoot(rs.getDouble("material_cost_per_sq_ft"));
            product.setLaborPerSqFoot(rs.getDouble("labor_cost_per_sq_ft"));

            return product;

        }

    }

    private static final class ProductTypeMapper implements RowMapper<String> {

        @Override
        public String mapRow(ResultSet rs, int i) throws SQLException {


            String type = (rs.getString("product_type"));


            return type;

        }

    }

}
