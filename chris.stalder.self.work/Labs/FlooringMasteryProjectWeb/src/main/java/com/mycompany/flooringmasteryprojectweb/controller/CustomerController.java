/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.controller;

import com.mycompany.flooringmasteryprojectweb.dao.CustomerDao;
import com.mycompany.flooringmasteryprojectweb.dto.Customer;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/customer")
public class CustomerController {

    private CustomerDao customerDao;

    @Inject
    public CustomerController(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public Customer create(@Valid @RequestBody Customer customer) {
        
        customerDao.create(customer);
        return customer;
        
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @ResponseBody
    public Customer edit(@Valid @RequestBody Customer customer) {
        
        customerDao.update(customer);
        return customer;
        
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Customer show(@PathVariable("id") int id) {
        return customerDao.get(id);
    }

    @RequestMapping(value = "/{globalId}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") int id) {
        
        Customer customer = customerDao.get(id);
        customerDao.delete(customer);

    }
}
