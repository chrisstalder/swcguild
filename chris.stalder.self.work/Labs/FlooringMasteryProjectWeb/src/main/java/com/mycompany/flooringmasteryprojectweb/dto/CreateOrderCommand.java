/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
/**
 *
 * @author apprentice
 */
public class CreateOrderCommand {
    
    int id;
    @NotNull(message="You must choose a date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone="EST")
    Date date;
    
    @NotNull(message="You must supply a customer")
    int customerId;
    
    @NotNull(message="You must supply a state")
    int taxId;
    
    @NotNull(message="You must supply a product type")
    int productId;
    
    @Min(1)
    double area;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public Date getDate() {
        return date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int id) {
        this.customerId = id;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setState(int id) {
        this.taxId = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int id) {
        this.productId = id;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }
    

    
}
