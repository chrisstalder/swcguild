/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import com.mycompany.flooringmasteryprojectweb.dto.Customer;
import com.mycompany.flooringmasteryprojectweb.dto.Order;
import com.mycompany.flooringmasteryprojectweb.dto.Product;
import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class OrderDaoImpl implements OrderDao {

    List<List<Order>> ordersByDate = new ArrayList();
    private List<Order> orderList = new ArrayList();
    private List<String> orderDates = new ArrayList();
    private int globalId = orderList.size() + 1;

    public OrderDaoImpl() {
        orderList = decode();
    }

    @Override
    public Order create(Order order) {
        DateFormat format = new SimpleDateFormat("MMddyyyy");
        order.setGlobalId(globalId++);
        orderList.add(order);
        if (order.getDate() == null) {
            order.setDate(new Date());
        }
        String myDate = format.format(order.getDate());
        if (!orderDates.contains(myDate)) {
            orderDates.add(format.format(order.getDate()));
        }
//        List<Order> orderByDates = getList((order.getDate()));
//        int nextId = 0;
//        for (Order orderByDate : orderByDates) {
//            nextId++;
//        }
//        
//        order.setOrderId(nextId);
//        encode();
        return order;
    }

    @Override
    public List<Order> getList(Date date) {
        List<Order> list = new ArrayList();
//        for (Order order : orderList) {
//            DateFormat format = new SimpleDateFormat("MMddyyyy");
//            String date = format.format(order.getDate());
//            if (string.equals(date)) {
//                list.add(order);
//            }
//        }
        return list;
    }

    @Override
    public List<Date> listDates() {
        List<Date> dates = new ArrayList();
//        List<String> organizedList = new ArrayList();

//        DateFormat format = new SimpleDateFormat("MMddyyyy");
//        for (String date : orderDates) {
//            try {
//                dates.add(format.parse(date));
//            } catch (ParseException ex) {
//                Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }

        Collections.sort(dates);

//        for (Date date : dates) {
//            organizedList.add(format.format(date));
//        }
        return dates;
    }

    @Override
    public Order get(int globalId) {
        for (Order order : orderList) {
            if (order.getGlobalId() == globalId) {
                return order;
            }
        }
        return null;
    }

    @Override
    public void update(Order order) {

        Order found = null;
        for (Order order1 : orderList) {
            if (order.getGlobalId() == order1.getGlobalId()) {
                found = order1;
            }
        }

        orderList.remove(found);
        orderList.add(order);
        encode();

    }

    @Override
    public void delete(Order order) {
//        Order found = null;
//        for (Order order1 : orderList) {
//            if (globalId == order1.getGlobalId()) {
//                found = order1;
//            }
//        }

        orderList.remove(order);
        encode();
    }

    @Override
    public List<Order> list() {
        return new java.util.ArrayList<>(orderList);
    }

    public boolean checkFileExists(String date) {
        File f = new File("Orders_" + date + ".txt");
        if (!f.exists()) {
            return false;
        } else {
            return true;
        }
    }

    private void encode() {
        final String TOKEN = ",";

        for (String string : orderDates) {
            File directory = new File("./Orders");
            try {
                File file = new File(directory, "Orders_" + string + ".txt");
                PrintWriter out;
                boolean fileExist = checkFileExists(string);
                if (fileExist) {
                    out = new PrintWriter(new FileWriter(file, true));
                } else {
                    out = new PrintWriter(new FileWriter(file));
                }

                out.println("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");

                List<Order> orderByDates = new ArrayList();
                for (Order order : orderList) {
                    DateFormat format = new SimpleDateFormat("MMddyyyy");
                    String date = format.format(order.getDate());
                    if (string.equals(date)) {
                        orderByDates.add(order);
                    }
                }
                for (Order order : orderByDates) {
                    out.print(order.getOrderId());
                    out.print(TOKEN);
                    String name = order.getCustomer().getName().replaceAll("(,)", "~,");
                    out.print(name);
                    out.print(TOKEN);
                    out.print(order.getTax().getState());
                    out.print(TOKEN);
                    out.print(order.getTax().getTaxRate());
                    out.print(TOKEN);
                    out.print(order.getProduct().getType());
                    out.print(TOKEN);
                    out.print(order.getArea());
                    out.print(TOKEN);
                    out.print(order.getCostPerSqFoot());
                    out.print(TOKEN);
                    out.print(order.getLaborCostPerSqFoot());
                    out.print(TOKEN);
                    out.print(order.getMaterialCost());
                    out.print(TOKEN);
                    out.print(order.getLaborCost());
                    out.print(TOKEN);
                    out.print(order.getTax());
                    out.print(TOKEN);
                    out.print(order.getTotal());
                    out.print(TOKEN);
                    out.print(order.getGlobalId());
                    out.println();
                }

                out.flush();
                out.close();

            } catch (IOException ex) {
                Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private List<Order> decode() {

        ProductDao productDao = new ProductDaoImpl();
        File dir = new File("./Orders");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {

                List<Order> orders = new ArrayList();

                try {

                    Scanner sc = new Scanner(new BufferedReader(new FileReader(child.toString())));
                    File f = deleteEmptyFile(child.toString());
                    if (f.exists()) {
                        sc.nextLine();
                        while (sc.hasNextLine()) {

                            String currentLine = sc.nextLine();

                            String checkedLine = currentLine.replaceAll("(~,)", "");

                            String[] stringParts = checkedLine.split(",");

                            int id = Integer.parseInt(stringParts[0]);
                            int global = Integer.parseInt(stringParts[12]);
                            Tax stateTax = new Tax();
                            stateTax.setState(stringParts[2]);

                            double taxRate = Double.parseDouble(stringParts[3]);

                            Product product = new Product();

                            List<Product> productList = productDao.list();

                            for (Product product1 : productList) {
                                if (stringParts[4].equals(product1.getType())) {
                                    product = product1;
                                }
                            }

                            double area = Double.parseDouble(stringParts[5]);
                            double costPerSqFoot = Double.parseDouble(stringParts[6]);
                            double laborCostPerSqFoot = Double.parseDouble(stringParts[7]);
                            double matCost = Double.parseDouble(stringParts[8]);
                            double labCost = Double.parseDouble(stringParts[9]);
                            double tax = Double.parseDouble(stringParts[10]);
                            double total = Double.parseDouble(stringParts[11]);
                            String date = child.toString().replace("./Orders/Orders_", "").replace(".txt", "");
                            DateFormat format = new SimpleDateFormat("MMddyyyy");
                            Date myDate = new Date();
                            try {
                                myDate = format.parse(date);
                            } catch (ParseException ex) {
                                Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            

                            Customer customer = new Customer();
                            customer.setName(stringParts[1]);
                            Order order = new Order();
                            order.setGlobalId(globalId);
                            order.setOrderId(id);
                            order.setCustomer(customer);
                            order.setTax(stateTax);
                            order.setProduct(product);
                            order.setArea(area);
                            order.setCostPerSqFoot(costPerSqFoot);
                            order.setLaborCostPerSqFoot(laborCostPerSqFoot);
                            order.setMaterialCost(matCost);
                            order.setLaborCost(labCost);
                            order.setTotalTax(tax);
                            order.setTotal(total);
                            order.setDate(myDate);
                            order.setGlobalId(global);

                            orders.add(order);
//                            nextId = orders.size() + 1;
                            orderList.add(order);
                            globalId = orderList.size() + 1;
                            if (!orderDates.contains(child.toString().replace("./Orders/Orders_", "").replace(".txt", ""))) {
                                orderDates.add(date);
                            }
                        }
                        ordersByDate.add(orders);

                    }

                    sc.close();

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return orderList;
    }

    private File deleteEmptyFile(String fileName) {

        File f = new File(fileName);

        if (!f.exists()) {
            throw new IllegalArgumentException("Delete: no such file or directory: " + fileName);
        }

        if (!f.canWrite()) {
            throw new IllegalArgumentException("Delete: write protected");
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String fileString = fileName.substring(7, 15);
            try {
                br.readLine();
                if (br.readLine() == null) {
                    boolean success = f.delete();
                    if (!success) {
                        throw new IllegalArgumentException("Delete: deletion failed");
                    }

                }
            } catch (IOException ex) {
                Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return f;
    }
}
