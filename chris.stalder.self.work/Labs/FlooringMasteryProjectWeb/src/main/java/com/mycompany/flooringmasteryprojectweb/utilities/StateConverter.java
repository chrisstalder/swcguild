/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.utilities;

/**
 *
 * @author apprentice
 */
public class StateConverter {

    public String convertState(String state) {

            switch (state) {
                case ("ALABAMA"):
                case ("Alabama"):
                case ("alabama"):
                case ("al"):
                    state = "AL";
                    break;
                case ("ALASKA"):
                case ("Alaska"):
                case ("alaska"):
                case ("ak"):
                    state = "AK";
                    break;
                case ("ARIZONA"):
                case ("Arizona"):
                case ("arizona"):
                case ("az"):
                    state = "AZ";
                    break;
                case ("ARKANSAS"):
                case ("Arkansas"):
                case ("arkansas"):
                case ("ar"):
                    state = "AR";
                    break;
                case ("CALIFORNIA"):
                case ("California"):
                case ("california"):
                case ("ca"):
                    state = "CA";
                    break;
                case ("COLORADO"):
                case ("Colorado"):
                case ("colorado"):
                case ("co"):
                    state = "CO";
                    break;
                case ("CONNECTICUT"):
                case ("Connecticut"):
                case ("connecticut"):
                case ("ct"):
                    state = "CT";
                    break;
                case ("DELAWARE"):
                case ("Delaware"):
                case ("delaware"):
                case ("de"):
                    state = "DE";
                    break;
                case ("FLORIDA"):
                case ("Florida"):
                case ("florida"):
                case ("fl"):
                    state = "FL";
                    break;
                case ("GEORGIA"):
                case ("Georgia"):
                case ("georgia"):
                case ("ga"):
                    state = "GA";
                    break;
                case ("HAWAII"):
                case ("Hawaii"):
                case ("hawaii"):
                case ("hi"):
                    state = "HI";
                    break;
                case ("IDAHO"):
                case ("Idaho"):
                case ("idaho"):
                case ("id"):
                    state = "ID";
                    break;
                case ("ILLINOIS"):
                case ("Illinois"):
                case ("illinois"):
                case ("il"):
                    state = "IL";
                    break;
                case ("INDIANA"):
                case ("Indiana"):
                case ("indiana"):
                case ("in"):
                    state = "IN";
                    break;
                case ("IOWA"):
                case ("Iowa"):
                case ("iowa"):
                case ("ia"):
                    state = "IA";
                    break;
                case ("KANSAS"):
                case ("Kansas"):
                case ("kansas"):
                case ("ks"):
                    state = "KS";
                    break;
                case ("KENTUCKY"):
                case ("Kentucky"):
                case ("kentucky"):
                case ("ky"):
                    state = "KY";
                    break;
                case ("LOUISIANA"):
                case ("Louisiana"):
                case ("louisiana"):
                case ("la"):
                    state = "LA";
                    break;
                case ("MAINE"):
                case ("Maine"):
                case ("maine"):
                case ("me"):
                    state = "ME";
                    break;
                case ("MARYLAND"):
                case ("Maryland"):
                case ("maryland"):
                case ("md"):
                    state = "MD";
                    break;
                case ("MASSACHUSETTS"):
                case ("Massachusetts"):
                case ("massachusetts"):
                case ("ma"):
                    state = "MA";
                    break;
                case ("MICHIGAN"):
                case ("Michigan"):
                case ("michigan"):
                case ("mi"):
                    state = "MI";
                    break;
                case ("MINNESOTA"):
                case ("Minnesota"):
                case ("minnesota"):
                case ("mn"):
                    state = "MN";
                    break;
                case ("MISSISSIPPI"):
                case ("Mississippi"):
                case ("mississippi"):
                case ("ms"):
                    state = "MS";
                    break;
                case ("MISSOURI"):
                case ("Missouri"):
                case ("missouri"):
                case ("mo"):
                    state = "MO";
                    break;
                case ("MONTANA"):
                case ("Montana"):
                case ("montana"):
                case ("mt"):
                    state = "MT";
                    break;
                case ("NEBRASKA"):
                case ("Nebraska"):
                case ("nebraska"):
                case ("ne"):
                    state = "NE";
                    break;
                case ("NEVADA"):
                case ("Nevada"):
                case ("nevada"):
                case ("nv"):
                    state = "NV";
                    break;
                case ("NEW HAMPSHIRE"):
                case ("New Hampshire"):
                case ("New hampshire"):
                case ("new Hampshire"):
                case ("new hampshire"):
                case ("nh"):
                    state = "NH";
                    break;
                case ("NEW JERSEY"):
                case ("New Jersey"):
                case ("New jersey"):
                case ("new Jersey"):
                case ("new jersey"):
                case ("nj"):
                    state = "NJ";
                    break;
                case ("NEW MEXICO"):
                case ("New Mexico"):
                case ("new Mexico"):
                case ("New mexico"):
                case ("new mexico"):
                case ("nm"):
                    state = "NM";
                    break;
                case ("NEW YORK"):
                case ("New York"):
                case ("New york"):
                case ("new York"):
                case ("new york"):
                case ("ny"):
                    state = "NY";
                    break;
                case ("NORTH CAROLINA"):
                case ("North Carolina"):
                case ("North carolina"):
                case ("north Carolina"):
                case ("north carolina"):
                case ("nc"):
                    state = "NC";
                    break;
                case ("NORTH DAKOTA"):
                case ("North Dakota"):
                case ("North dakota"):
                case ("north Dakota"):
                case ("nd"):
                    state = "ND";
                    break;
                case ("OHIO"):
                case ("Ohio"):
                case ("ohio"):
                case ("oh"):
                    state = "OH";
                    break;
                case ("OKLAHOMA"):
                case ("Oklahoma"):
                case ("oklahoma"):
                case ("ok"):
                    state = "OK";
                    break;
                case ("OREGON"):
                case ("Oregon"):
                case ("oregon"):
                case ("or"):
                    state = "OR";
                    break;
                case ("PENNSYLVANIA"):
                case ("Pennsylvania"):
                case ("pennsylvania"):
                case ("pa"):
                    state = "PA";
                    break;
                case ("RHODE ISLAND"):
                case ("Rhode Island"):
                case ("rhode Island"):
                case ("Rhode island"):
                case ("rhode island"):
                case ("ri"):
                    state = "RI";
                    break;
                case ("SOUTH CAROLINA"):
                case ("South Carolina"):
                case ("South carolina"):
                case ("south Carolina"):
                case ("south carolina"):
                case ("sc"):
                    state = "SC";
                    break;
                case ("SOUTH DAKOTA"):
                case ("South Dakota"):
                case ("South dakota"):
                case ("south Dakota"):
                case ("south dakota"):
                case ("sd"):
                    state = "SD";
                    break;
                case ("TENNESSEE"):
                case ("Tennessee"):
                case ("tennessee"):
                case ("tn"):
                    state = "TN";
                    break;
                case ("TEXAS"):
                case ("Texas"):
                case ("texas"):
                case ("tx"):
                    state = "TX";
                    break;
                case ("UTAH"):
                case ("Utah"):
                case ("utah"):
                case ("ut"):
                    state = "UT";
                    break;
                case ("VERMONT"):
                case ("Vermont"):
                case ("vermont"):
                case ("vt"):
                    state = "VT";
                    break;
                case ("VIRGINIA"):
                case ("Virginia"):
                case ("virginia"):
                case ("va"):
                    state = "VA";
                    break;
                case ("WASHINGTON"):
                case ("Washington"):
                case ("washington"):
                case ("wa"):
                    state = "WA";
                    break;
                case ("WEST VIRGINIA"):
                case ("West Virginia"):
                case ("West virginia"):
                case ("west Virginia"):
                case ("west virginia"):
                case ("wv"):
                    state = "WV";
                    break;
                case ("WISCONSIN"):
                case ("Wisconsin"):
                case ("wisconsin"):
                case ("wi"):
                    state = "WI";
                    break;
                case ("WYOMING"):
                case ("Wyoming"):
                case ("wyoming"):
                case ("wy"):
                    state = "WY";
                    break;
                case ("AL"):
                    state = "AL";
                    break;
                case ("AK"):
                    state = "AK";
                    break;
                case ("AZ"):
                    state = "AZ";
                    break;
                case ("AR"):
                    state = "AR";
                    break;
                case ("CA"):
                    state = "CA";
                    break;
                case ("CO"):
                    state = "CO";
                    break;
                case ("CT"):
                    state = "CT";
                    break;
                case ("DE"):
                    state = "DE";
                    break;
                case ("FL"):
                    state = "FL";
                    break;
                case ("GA"):
                    state = "GA";
                    break;
                case ("HI"):
                    state = "HI";
                    break;
                case ("ID"):
                    state = "ID";
                    break;
                case ("IL"):
                    state = "IL";
                    break;
                case ("IN"):
                    state = "IN";
                    break;
                case ("IA"):
                    state = "IA";
                    break;
                case ("KS"):
                    state = "KS";
                    break;
                case ("KY"):
                    state = "KY";
                    break;
                case ("LA"):
                    state = "LA";
                    break;
                case ("ME"):
                    state = "ME";
                    break;
                case ("MD"):
                    state = "MD";
                    break;
                case ("MA"):
                    state = "MA";
                    break;
                case ("MI"):
                    state = "MI";
                    break;
                case ("MN"):
                    state = "MN";
                    break;
                case ("MS"):
                    state = "MS";
                    break;
                case ("MO"):
                    state = "MO";
                    break;
                case ("MT"):
                    state = "MT";
                    break;
                case ("NE"):
                    state = "NE";
                    break;
                case ("NV"):
                    state = "NV";
                    break;
                case ("NH"):
                    state = "NH";
                    break;
                case ("NJ"):
                    state = "NJ";
                    break;
                case ("NM"):
                    state = "NM";
                    break;
                case ("NY"):
                    state = "NY";
                    break;
                case ("NC"):
                    state = "NC";
                    break;
                case ("ND"):
                    state = "ND";
                    break;
                case ("OH"):
                    state = "OH";
                    break;
                case ("OK"):
                    state = "OK";
                    break;
                case ("OR"):
                    state = "OR";
                    break;
                case ("PA"):
                    state = "PA";
                    break;
                case ("RI"):
                    state = "RI";
                    break;
                case ("SC"):
                    state = "SC";
                    break;
                case ("SD"):
                    state = "SD";
                    break;
                case ("TN"):
                    state = "TN";
                    break;
                case ("TX"):
                    state = "TX";
                    break;
                case ("UT"):
                    state = "UT";
                    break;
                case ("VT"):
                    state = "VT";
                    break;
                case ("VA"):
                    state = "VA";
                    break;
                case ("WA"):
                    state = "WA";
                    break;
                case ("WV"):
                    state = "WV";
                    break;
                case ("WI"):
                    state = "WI";
                    break;
                case ("WY"):
                    state = "WY";
                    break;
            }
        return state;
    }
}
