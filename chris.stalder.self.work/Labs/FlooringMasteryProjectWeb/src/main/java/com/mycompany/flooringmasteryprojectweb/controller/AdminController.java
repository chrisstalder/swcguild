/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.controller;

import com.mycompany.flooringmasteryprojectweb.dao.AdminDao;
import com.mycompany.flooringmasteryprojectweb.dao.PasswordDao;
import com.mycompany.flooringmasteryprojectweb.dao.ProductDao;
import com.mycompany.flooringmasteryprojectweb.dao.TaxDao;
import com.mycompany.flooringmasteryprojectweb.dto.Product;
import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import com.mycompany.flooringmasteryprojectweb.utilities.StateConverter;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    AdminDao adminDao;
    TaxDao taxDao;
    ProductDao productDao;
    PasswordDao passwordDao;
    StateConverter stateConverter;
    boolean test;

    @Inject
    public AdminController(AdminDao adminDao, TaxDao taxDao, ProductDao productDao, PasswordDao passwordDao, StateConverter sc) {
        this.adminDao = adminDao;
        this.taxDao = taxDao;
        this.productDao = productDao;
        this.passwordDao = passwordDao;
        this.stateConverter = sc;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/checkingIn", method = RequestMethod.GET)
    public String enterPassword(@RequestParam("password") String entered) {
        String password = passwordDao.getPassword();
        if (entered.equals(password)) {
            return "redirect:/admin/";
        }

        return "redirect:/";

    }

    @RequestMapping(value = "/")
    public String home(Map model) {
        List<Tax> taxes = taxDao.list();
        List<Product> products = productDao.list();

        model.put("product", new Product());
        model.put("tax", new Tax());
        model.put("taxes", taxes);
        model.put("products", products);
        return "admin";
    }

    @RequestMapping(value = "/tax/", method = RequestMethod.POST)
    @ResponseBody
    public Tax createTax(@Valid @RequestBody Tax tax) {

        List<String> validStates = taxDao.listValidStates();
        String state = stateConverter.convertState(tax.getState());
        tax.setState(state);
        if (!validStates.contains(tax.getState())) {
            return taxDao.Create(tax);
        } else {
            return null;
        }

    }

    @RequestMapping(value = "/product/", method = RequestMethod.POST)
    @ResponseBody
    public Product createProduct(@Valid @RequestBody Product product) {

        List<String> productList = productDao.listProductTypes();
        if (!productList.contains(product.getType())) {
            productDao.Create(product);
            return product;
        }
        
        return null;
    }

    @RequestMapping(value = "/product/edit/{id}", method = RequestMethod.GET)
    public String editProduct(@PathVariable("id") int id, Map model) {
        Product product = productDao.get(id);
        model.put("product", product);
        return "editProduct";
    }

    @RequestMapping(value = "product/", method = RequestMethod.PUT)
    @ResponseBody
    public Product editProductSubmit(@Valid @RequestBody Product product) {
//        if(bindingResult.hasErrors()){
//            model.put("product", product);
//            return "editProduct";
//        }

        productDao.update(product);
        return product;
    }

    @RequestMapping(value = "/tax/edit/{id}", method = RequestMethod.GET)
    public String editTax(@PathVariable("id") int id, Map model) {
        Tax tax = taxDao.get(id);
        model.put("tax", tax);
        return "editTax";
    }

    @RequestMapping(value = "tax/", method = RequestMethod.PUT)
    @ResponseBody
    public Tax editTaxSubmit(@Valid @RequestBody Tax tax) {
//        if(bindingResult.hasErrors()){
//            model.put("tax", tax);
//            return "editTax";
//        }

        taxDao.update(tax);
        return tax;
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Product deleteProduct(@PathVariable("id") int id) {
        Product product = productDao.get(id);
        productDao.delete(product);

        return product;
    }

    @RequestMapping(value = "/tax/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Tax deleteTax(@PathVariable("id") int id) {
        Tax tax = taxDao.get(id);
        taxDao.delete(tax);
        return tax;
    }

    public void setMode() {
//        if (test == true) {
//            console.print("Currently, you are in test mode.");
//            console.print("Would you like to enter production mode?");
//            boolean answer = console.getYesNo();
//            if (answer) {
//                adminDao.setMode("production");
//            }
//
//        } else if (test == false) {
//            console.print("Currently, you are in production mode.");
//            console.print("Would you like to enter test mode?");
//            boolean answer = console.getYesNo();
//            if (answer) {
//                adminDao.setMode("test");
//            }
//        }
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public String changePassword() {
        return "changePassword";
    }

    @RequestMapping(value = "/submit")
    public String changePasswordSubmit(@RequestParam("newPassword") String newPassword, @RequestParam("currentPassword") String currentPassword) {
        String password = passwordDao.getPassword();
        if (currentPassword.equals(password)) {
            passwordDao.setPassword(newPassword);
            return "redirect:/admin/";
        }

        return "redirect:/";
    }

    public boolean validateState(String state) {
        boolean isValid = false;
        List<String> validStates = taxDao.listValidStates();

        if (!validStates.contains(state)) {
            isValid = true;
        }

        return isValid;
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    @ResponseBody //turns response into JSON
    public Product showProduct(@PathVariable("id") int id) {

        Product product = productDao.get(id);
        return product;
    }

    @RequestMapping(value = "/tax/{id}", method = RequestMethod.GET)
    @ResponseBody //turns response into JSON
    public Tax showTax(@PathVariable("id") int id) {

        Tax tax = taxDao.get(id);
        return tax;
    }

}
