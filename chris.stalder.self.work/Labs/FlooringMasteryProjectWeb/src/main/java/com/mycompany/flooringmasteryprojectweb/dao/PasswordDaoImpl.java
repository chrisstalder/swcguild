/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class PasswordDaoImpl implements PasswordDao {
    String password;
    
    public PasswordDaoImpl(){
        password = decode();
    }
    @Override
    public void setPassword(String string){
        password = string;
        encode();
    }
    
    @Override
    public String getPassword(){
        String current = password;
        return current;
    }
    public void encode(){
        try {
            PrintWriter out = new PrintWriter(new FileWriter("password.txt"));
            
            out.print(password);
            
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(PasswordDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public String decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("password.txt")));
            
            String currentLine = sc.nextLine();
            
            password = currentLine;
            
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PasswordDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return password;
    }
}
