/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import com.mycompany.flooringmasteryprojectweb.dto.Customer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class CustomerDaoDBImpl implements CustomerDao {

    private static final String SQL_INSERT_CUSTOMER = "INSERT INTO customer(name) VALUES(?)";
    private static final String SQL_UPDATE_CUSTOMER = "UPDATE customer SET name = ? WHERE id = ?";
    private static final String SQL_DELETE_CUSTOMER = "DELETE customer WHERE id = ?";
    private static final String SQL_SELECT_CUSTOMER = "SELECT * FROM customer WHERE id = ?";
    private static final String SQL_GET_CUSTOMER_LIST = "SELECT * FROM customer";

    private JdbcTemplate jdbcTemplate;

    public CustomerDaoDBImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Customer create(Customer customer) {

        jdbcTemplate.update(SQL_INSERT_CUSTOMER,
                customer.getName());

        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        customer.setId(id);
        return customer;

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Customer get(int globalId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_CUSTOMER, new CustomerMapper(), globalId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(Customer customer) {
        jdbcTemplate.update(SQL_UPDATE_CUSTOMER,
                customer.getName(),
                customer.getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Customer customer) {
        
        jdbcTemplate.update(SQL_DELETE_CUSTOMER, customer.getId());
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Customer> list() {
        return jdbcTemplate.query(SQL_GET_CUSTOMER_LIST, new CustomerMapper());
    }

    private static final class CustomerMapper implements RowMapper<Customer>{

        @Override
        public Customer mapRow(ResultSet rs, int i) throws SQLException {
            
            Customer customer = new Customer();
            
            customer.setId(rs.getInt("id"));
            customer.setName(rs.getString("name"));

            
            return customer;
            
        }
        
    }
    
}
