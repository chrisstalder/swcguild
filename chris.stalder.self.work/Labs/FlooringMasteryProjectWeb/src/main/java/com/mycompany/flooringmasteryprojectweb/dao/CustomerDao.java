/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import com.mycompany.flooringmasteryprojectweb.dto.Customer;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface CustomerDao {
    
    public Customer create(Customer customer);
    
    public Customer get(int globalId);
    
    public void update(Customer order);
    
    public void delete(Customer order); 
    
    public List<Customer> list();

}
