/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.controller;

import com.mycompany.flooringmasteryprojectweb.dao.AdminDao;
import com.mycompany.flooringmasteryprojectweb.dao.PasswordDao;
import com.mycompany.flooringmasteryprojectweb.dao.ProductDao;
import com.mycompany.flooringmasteryprojectweb.dao.TaxDao;
import com.mycompany.flooringmasteryprojectweb.dto.Product;
import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import com.mycompany.flooringmasteryprojectweb.utilities.StateConverter;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/tax")
public class TaxController {

    TaxDao taxDao;
    StateConverter stateConverter;


    @Inject
    public TaxController(TaxDao taxDao, StateConverter sc) {

        this.taxDao = taxDao;
        this.stateConverter = sc;
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public Tax createTax(@Valid @RequestBody Tax tax) {

        List<String> validStates = taxDao.listValidStates();
        String state = stateConverter.convertState(tax.getState());
        tax.setState(state);
        if (!validStates.contains(tax.getState())) {
            return taxDao.Create(tax);
        } else {
            return null;
        }

    }

    

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editTax(@PathVariable("id") int id, Map model) {
        Tax tax = taxDao.get(id);
        model.put("tax", tax);
        return "editTax";
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @ResponseBody
    public Tax editTaxSubmit(@Valid @RequestBody Tax tax) {
//        if(bindingResult.hasErrors()){
//            model.put("tax", tax);
//            return "editTax";
//        }

        taxDao.update(tax);
        return tax;
    }

    

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Tax deleteTax(@PathVariable("id") int id) {
        Tax tax = taxDao.get(id);
        taxDao.delete(tax);
        return tax;
    }

    
    public boolean validateState(String state) {
        boolean isValid = false;
        List<String> validStates = taxDao.listValidStates();

        if (!validStates.contains(state)) {
            isValid = true;
        }

        return isValid;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody //turns response into JSON
    public Tax showTax(@PathVariable("id") int id) {

        Tax tax = taxDao.get(id);
        return tax;
    }

}
