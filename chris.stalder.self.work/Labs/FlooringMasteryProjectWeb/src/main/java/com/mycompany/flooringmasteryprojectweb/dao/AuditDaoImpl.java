/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import com.mycompany.flooringmasteryprojectweb.dto.Audit;
import com.mycompany.flooringmasteryprojectweb.dto.Order;
import com.mycompany.flooringmasteryprojectweb.dao.AuditDao;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author apprentice
 */
public class AuditDaoImpl implements AuditDao {

    private List<Audit> systemActivity = new ArrayList();
    private int auditId;

    public AuditDaoImpl() {
        auditId = decode();
    }

    @Override
    public void create(JoinPoint jp) {
        Order order = new Order();
        Audit audit = new Audit();
        Object[] args = jp.getArgs();
        String methodName = jp.getSignature().getName();
        if ("update".equals(methodName)) {
            String orderDate = args[0].toString();
            List<Order> orders = (List<Order>) args[1];
            audit.setorderDate(orderDate);
            audit.setOrders(orders);
        } else {
            order = (Order) args[0];
            audit.setOrder(order);
        }
        audit.setId(auditId);
        auditId++;
        Date timeStamp = new Date();
        audit.setTimeStamp(timeStamp);
        audit.setMethodName(methodName);
        systemActivity.add(audit);
        encode();

    }

    private void encode() {
        final String TOKEN = "::";
        try {
            PrintWriter out = new PrintWriter(new FileWriter("System_Activity.txt", true));
//            out.println("Audit ID | Order Date  | Order ID  |   Time Stamp");
            for (Audit audit : systemActivity) {
                out.print(audit.getId());
                out.print(TOKEN);
                out.print(audit.getMethodName());
                out.print(TOKEN);
                if ("update".equals(audit.getMethodName())) {
                    for (Order order : audit.getOrders()) {
                        out.print(order.getGlobalId());
                        out.print(TOKEN);
                        out.print(order.getDate());
                        out.print(TOKEN);
                        out.print(order.getCustomer().getName());
                        out.print(TOKEN);
                    }                               //maps suck ass
                } else {
                    Order order = audit.getOrder();
                    out.print(order.getGlobalId());
                    out.print(TOKEN);
                    out.print(audit.getorderDate());
                    out.print(TOKEN);
                    out.print(order.getCustomer().getName());
                    out.print(TOKEN);
                }
                
                out.print(audit.getTimeStamp());
                out.println();
            }
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(AuditDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private int decode() {
        int count = 0;
        String line;
        try {
            BufferedReader br = new BufferedReader(new FileReader("System_Activity.txt"));
            try {
                while((line = br.readLine()) != null){
                    count++;
                }
            } catch (IOException ex) {
                Logger.getLogger(AuditDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AuditDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count +1;
    }
}
