/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface TaxDao {
    public Tax Create(Tax tax);
    public Tax get(int id);
    public void update(Tax tax);
    public void delete(Tax tax);
    public List<Tax> list();
    public List<String> listValidStates();
}
