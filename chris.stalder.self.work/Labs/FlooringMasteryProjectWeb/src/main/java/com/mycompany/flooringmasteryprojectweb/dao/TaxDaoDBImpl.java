/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class TaxDaoDBImpl implements TaxDao {

    private static final String SQL_INSERT_TAX = "INSERT INTO tax(state, tax_rate) VALUES(?, ?)";
    private static final String SQL_UPDATE_TAX = "UPDATE tax SET state = ?, tax_rate = ? WHERE id = ?";
    private static final String SQL_DELETE_TAX = "DELETE FROM tax WHERE id = ?";
    private static final String SQL_SELECT_TAX = "SELECT * FROM tax WHERE id = ?";
    private static final String SQL_GET_TAX_LIST = "SELECT * FROM tax";
    private static final String SQL_GET_STATE_LIST = "SELECT state FROM tax;";

    private JdbcTemplate jdbcTemplate;

    public TaxDaoDBImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Tax Create(Tax tax) {
        jdbcTemplate.update(SQL_INSERT_TAX,
                tax.getState(),
                tax.getTaxRate());

        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        tax.setId(id);
        return tax;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Tax get(int id) {
        Tax tax = jdbcTemplate.queryForObject(SQL_SELECT_TAX, new TaxMapper(), id);
        return tax;
    }

    @Override
    public void update(Tax tax) {
        
        jdbcTemplate.update(SQL_UPDATE_TAX, 
                tax.getState(),
                tax.getTaxRate(),
                tax.getId());
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Tax tax) {
        jdbcTemplate.update(SQL_DELETE_TAX, tax.getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Tax> list() {
        return jdbcTemplate.query(SQL_GET_TAX_LIST, new TaxMapper());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<String> listValidStates() {
        return jdbcTemplate.query(SQL_GET_STATE_LIST, new StateMapper());
    }
    
    private static final class TaxMapper implements RowMapper<Tax>{

        @Override
        public Tax mapRow(ResultSet rs, int i) throws SQLException {
            
            Tax tax = new Tax();
            
            tax.setId(rs.getInt("id"));
            tax.setState(rs.getString("state"));
            tax.setTaxRate(rs.getDouble("tax_rate"));

            
            return tax;
            
        }
        
    }
    
    private static final class StateMapper implements RowMapper<String>{

        @Override
        public String mapRow(ResultSet rs, int i) throws SQLException {

            String state = (rs.getString("state"));


            
            return state;
            
        }
        
    }

}
