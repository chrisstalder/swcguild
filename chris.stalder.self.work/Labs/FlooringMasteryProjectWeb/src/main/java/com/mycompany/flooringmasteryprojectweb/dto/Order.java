/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 *
 * @author apprentice
 */
public class Order {
    private int orderId;
    private int globalId;
    private Customer customer;
//    private String state;
//    
//    private double taxRate;
    
    private Tax tax;
    private Product product;
    private double area;
    
//    @NumberFormat(style = Style.CURRENCY, pattern="#.##")
    private double laborCostPerSqFoot;
    
//    @NumberFormat(style = Style.CURRENCY, pattern="#.##")
    private double costPerSqFoot;
    
//    @NumberFormat(style = Style.CURRENCY, pattern="#.##")
    private double laborCost;
    
//    @NumberFormat(style = Style.CURRENCY, pattern="#.##")
    private double materialCost;
    
//    @NumberFormat(style = Style.CURRENCY, pattern="#.##")
    private double totalTax;
    
//    @NumberFormat(style = Style.CURRENCY, pattern="#.##")
    private double total;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone="EST")
    private Date date;

    public int getOrderId() {
        return orderId;
    }

    public int getGlobalId() {
        return globalId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }
    
    

//    public String getState() {
//        return state;
//    }
//
//    public double getTaxRate() {
//        return taxRate;
//    }

    public Product getProduct() {
        return product;
    }

    public double getArea() {
        return area;
    }

    public double getLaborCostPerSqFoot() {
        return laborCostPerSqFoot;
    }

    public double getCostPerSqFoot() {
        return costPerSqFoot;
    }

    public double getLaborCost() {
        return laborCost;
    }

    public double getMaterialCost() {
        return materialCost;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public double getTotal() {
        return total;
    }

    public Date getDate() {
        return date;
    }

    
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setGlobalId(int globalId) {
        this.globalId = globalId;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

//    public void setState(String state) {
//        this.state = state;
//    }
//
//    public void setTaxRate(double taxRate) {
//        this.taxRate = taxRate;
//    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setLaborCostPerSqFoot(double laborCostPerSqFoot) {
        this.laborCostPerSqFoot = laborCostPerSqFoot;
    }

    public void setCostPerSqFoot(double costPerSqFoot) {
        this.costPerSqFoot = costPerSqFoot;
    }

    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }

    public void setMaterialCost(double materialCost) {
        this.materialCost = materialCost;
    }

    public void setTotalTax(double tax) {
        this.totalTax = tax;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    
}
