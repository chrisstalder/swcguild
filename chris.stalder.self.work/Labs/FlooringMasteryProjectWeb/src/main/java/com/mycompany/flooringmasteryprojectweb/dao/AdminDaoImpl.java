/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class AdminDaoImpl implements AdminDao {

    boolean test;
    boolean txt;

    @Override
    public boolean currentMode() {
        return test;
    }

    @Override
    public boolean currentFileType() {
        return txt;
    }

    @Override
    public void setMode(String mode) {
        if ("production".equals(mode)) {
            test = false;
        } else if ("test".equals(mode)) {
            test = true;
        }

        encode();
    }

    @Override
    public void setCurrentFileType(String fileType) {
        if ("txt".equals(fileType)) {
            txt = true;
        } else if ("xml".equals(fileType)) {
            txt = false;
        }
        encode();
    }

    public void encode() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter("config.txt"));

            if (test == false) {
                out.print("production");
            } else if (test == true) {
                out.print("test");
            }
            out.flush();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(AdminDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean decode() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("config.txt")));
            while (sc.hasNextLine()) {
                String line = sc.nextLine();

                if ("production".equals(line)) {
                    test = false;
                } else if ("test".equals(line)) {
                    test = true;
                }

            }
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AdminDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return test;
    }
}
