/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dto;

import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Product {
    private int id;
    
    @NotEmpty(message="You must supply a product type")
    private String type;
    
    @Min(1)
    private double costPerSqFoot;
    
    @Min(1)
    private double laborPerSqFoot;

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public double getCostPerSqFoot() {
        return costPerSqFoot;
    }

    public double getLaborPerSqFoot() {
        return laborPerSqFoot;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setType(String productType) {
        this.type = productType;
    }

    public void setCostPerSqFoot(double costPerSqFoot) {
        this.costPerSqFoot = costPerSqFoot;
    }

    public void setLaborPerSqFoot(double laborPerSqFoot) {
        this.laborPerSqFoot = laborPerSqFoot;
    }
    
}
