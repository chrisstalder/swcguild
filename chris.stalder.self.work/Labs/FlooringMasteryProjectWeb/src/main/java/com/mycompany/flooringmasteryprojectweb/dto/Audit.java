/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.dto;

import java.util.Date;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Audit {
    
    private String type;
    private Date timeStamp;
    private int id;
    private List<Order> orders;
    private Order order;
//    private int orderGlobalId;
//    private String orderCustomerName;
    private String methodName;
    private String orderDate;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

//    public int getOrderGlobalId() {
//        return orderGlobalId;
//    }
//
//    public void setOrderGlobalId(int orderGlobalId) {
//        this.orderGlobalId = orderGlobalId;
//    }
//
//    public String getOrderCustomerName() {
//        return orderCustomerName;
//    }
//
//    public void setOrderCustomerName(String orderCustomerName) {
//        this.orderCustomerName = orderCustomerName;
//    }
//
//    
//    
    public String getorderDate() {
        return orderDate;
    }

    public void setorderDate(String orderDate) {
        this.orderDate = orderDate;
    }
    
    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
    
    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    
}
