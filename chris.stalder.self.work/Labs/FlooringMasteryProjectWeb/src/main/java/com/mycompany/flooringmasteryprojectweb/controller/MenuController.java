/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmasteryprojectweb.controller;

import com.mycompany.flooringmasteryprojectweb.dao.CustomerDao;
import com.mycompany.flooringmasteryprojectweb.dao.OrderDao;
import com.mycompany.flooringmasteryprojectweb.dao.ProductDao;
import com.mycompany.flooringmasteryprojectweb.dao.TaxDao;
import com.mycompany.flooringmasteryprojectweb.dto.Customer;
import com.mycompany.flooringmasteryprojectweb.dto.Order;
import com.mycompany.flooringmasteryprojectweb.dto.Product;
import com.mycompany.flooringmasteryprojectweb.dto.Tax;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/menu")
public class MenuController {

    private OrderDao orderDao;
    private CustomerDao customerDao;
    private ProductDao productDao;
    private TaxDao taxDao;

    @Inject
    public MenuController(OrderDao dao, CustomerDao customerDao, ProductDao productDao, TaxDao taxDao) {
        this.orderDao = dao;
        this.customerDao = customerDao;
        this.productDao = productDao;
        this.taxDao = taxDao;
    }

    @RequestMapping(value = "/{selectedDate}", method = RequestMethod.POST)
    @ResponseBody
    public List<Order> getOrders(@PathVariable("selectedDate") String string) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        if (!"".equals(string)) {
            try {
                date = format.parse(string);
            } catch (ParseException ex) {
                Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
            }
            List<Order> orders = orderDao.getList(date);

            for (Order order : orders) {
                Customer customer = customerDao.get(order.getCustomer().getId());
                Product product = productDao.get(order.getProduct().getId());
                Tax tax = taxDao.get(order.getTax().getId());

                order.setCustomer(customer);
                order.setProduct(product);
                order.setTax(tax);
            }
            return orders;
        }
        return null;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public List<Date> getOrderDates() {
        List<Date> orderDates = orderDao.listDates();
        return orderDates;
    }
}
