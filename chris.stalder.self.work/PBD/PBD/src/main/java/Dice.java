/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Random;

public class Dice {
    
    public static void main(String[] args) {
        
        Random r = new Random();
        
        int d1 = 1 + r.nextInt(6);
        int d2 = 1 + r.nextInt(6);
        int total = d1 + d2;
        
        System.out.println("HERE COMES THE DICE!");
        System.out.println("Roll #1: " + d1);
        System.out.println("Roll #2: " + d2);
        System.out.println("The total is " + total + "!");
    }
}
