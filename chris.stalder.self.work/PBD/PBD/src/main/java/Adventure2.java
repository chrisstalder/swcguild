import java.util.Scanner;

public class Adventure2
{
	public static void main( String[] args )
	{
		Scanner keyboard = new Scanner(System.in);
		
		int nextroom = 1;
		String choice = "";

		while ( nextroom != 0 )
		{
			if ( nextroom == 1 ) //main room
			{
                                System.out.println("You are in a haunted mansion.");
                                System.out.println("Would you like to go \"upstairs\", or down the \"hallway\"?");
				System.out.print( "> " );
				choice = keyboard.nextLine();
				if ( choice.equals("upstairs") )
					nextroom = 2;
				else if ( choice.equals("hallway") )
					nextroom = 3;
				else
					System.out.println( "ERROR." );
			}
			if ( nextroom == 2 )//upstairs
			{
                                System.out.println("You go upstairs and enter a hallway. At the end of ");
                                System.out.println("the hallway you see a master \"bedroom\". There is also ");
                                System.out.println("a \"bathroom\" to your right. ");
                                System.out.println("Would you like to go to the \"bedroom\", the \"bathroom\", or \"back\"?");
				System.out.print( "> " );
				choice = keyboard.nextLine();
				if ( choice.equals("bedroom") )
					nextroom = 4;
                                else if ( choice.equals("bathroom") )
					nextroom = 5;
                                else if ( choice.equals("back") )
					nextroom = 1;
				else
					System.out.println( "ERROR." );
			}
			if ( nextroom == 3 )//hallway
			{
                                System.out.println("You walk down the hallway. At the end, there are two rooms.");
                                System.out.println("Do you take the one on the \"left\", the one on the \"right\", or do you go \"back\"?");
				System.out.print( "> " );
				choice = keyboard.nextLine();
				if ( choice.equals("left") )
					nextroom = 6;
                                else if ( choice.equals("right") )
					nextroom = 7;
                                else if ( choice.equals("back") )
					nextroom = 1;
				else
					System.out.println( "ERROR." );
			}
			if ( nextroom == 4 )//bedroom
			{
                                System.out.println("You are inside the master bedroom.");
                                System.out.println("At the far end of the room, a large window is open.");
                                System.out.println("Do you look out the window, \"yes\" or \"no\". Or you could go \"back\"");
				System.out.print( "> " );
				choice = keyboard.nextLine();
				if ( choice.equals("yes") ){
                                    System.out.println("You look out the window to see darkness;");
                                    System.out.println("the moon, your only light. Outside there is a creepy tree.");
                                    System.out.println("You stick your head out further to look around, and, suddenly");
                                    System.out.println("the window slams down. You have been decapitated.");
                                    nextroom = 0;
                                }
                                else if ( choice.equals("no") ){
                                    System.out.println("You do not look out the window, and instead");
                                    System.out.println("close it. As you close the window you catch the reflection");
                                    System.out.println("of someone behind you. You turn around and are stabbed. You ded.");
                                    nextroom = 0;
                                }
                                else if ( choice.equals("back") )
					nextroom = 2;
				else
					System.out.println( "ERROR." );
			}
			if ( nextroom == 5 )//bathroom
			{
                                System.out.println("After walking into the bathroom, the door slams violently"
                                + " behind you. The lights are off. Do you turn them on?");
				System.out.print( "> " );
				choice = keyboard.nextLine();
				if ( choice.equals("yes") ){
                                    System.out.println("You turn on the lights and see a dead woman in the mirror.");
                                    System.out.println("She devours your eternal soul.");
                                    nextroom = 0;
                                }
                                else if ( choice.equals("no") ){
                                    System.out.println("You do not turn on the lights, and spend");
                                    System.out.println("the rest of your life in eternal darkness.");
                                    nextroom = 0;
                                }
                                else if ( choice.equals("back") ){
					System.out.println("There is no going back!");
                                        nextroom = 5;
                                }
				else
					System.out.println( "ERROR." );
			}
			if ( nextroom == 6 )//kitchen
			{
                                System.out.println("You see that the room on the left is a kitchen, with");
                                System.out.println("a refridgerator to your right. You can see that the refridgerator");
                                System.out.println("is still on. Do you open it? \"yes\" or \"no\".");
                                System.out.println("You could also go \"back\"");
				System.out.print( "> " );
				choice = keyboard.nextLine();
				if ( choice.equals("yes") ){
                                    System.out.println("Inside you find the severed remains");
                                    System.out.println("of multiple humans. Seeing such a grotesque");
                                    System.out.println("sight shocks you and you have a heart attack");
                                    nextroom = 0;
                                }
                                else if ( choice.equals("no") ){
                                    System.out.println("You do not open the refridgerator, and"
                                        + " die from starvation.");
                                    nextroom = 0;
                                }
                                else if ( choice.equals("back") )
					nextroom = 3;
				else
					System.out.println( "ERROR." );
			}
			if ( nextroom == 7 )//lab
			{
                                System.out.println("You see that the room on the left is an old laboratory.");
                                System.out.println("You see two vials on a nearby desk. One is red, and the other is blue.");
                                System.out.println("Do you drink the \"red\" vial, the \"blue\" vial, or go \"back\"?");
				System.out.print( "> " );
				choice = keyboard.nextLine();
				if ( choice.equals("red") ){
                                    System.out.println("You drink the red vial, and are transported");
                                    System.out.println(" outside of the creepy mansion. Run for your life!");
                                    nextroom = 0;
                                }
                                else if ( choice.equals("blue") ){
                                    System.out.println("You foolishly drank the blue vial and were poisoned.");
                                    nextroom = 0;
                                }
                                else if ( choice.equals("back") )
					nextroom = 3;
				else
					System.out.println( "ERROR." );
			}
		}

		System.out.println( "\nEND." );
	}
	
}