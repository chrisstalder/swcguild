/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;
public class MoreUserInputOfData {
    public static void main(String[] args) {
        String firstName, lastName, loginName;
        int grade, studentID;
        float GPA = 0f;
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.print("First name: ");
        firstName = keyboard.next();
        
        System.out.print("Last name: ");
        lastName = keyboard.next();
        
        System.out.print("Grade(9-12): ");
        grade = keyboard.nextInt();
        
        System.out.print("Student ID: ");
        studentID = keyboard.nextInt();
        
        System.out.print("Login: ");
        loginName = keyboard.next();
        
        System.out.print("GPA(0.0-4.0): ");
        GPA = keyboard.nextFloat();
        
        System.out.println("Your Information: ");
        System.out.println("    Login: " + loginName);
        System.out.println("    ID: " + studentID);
        System.out.println("    Name: " + lastName + ", " + firstName);
        System.out.println("    GPA: " + GPA);
        System.out.println("    Grade: " + grade);
    }
}
