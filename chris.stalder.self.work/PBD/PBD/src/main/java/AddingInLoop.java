/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class AddingInLoop {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("I will add the numbers you give me.");
        System.out.print("Number:");
        int input = keyboard.nextInt();
        int sum = input;
        
        while (input != 0){
            System.out.print("Number:");
            input = keyboard.nextInt();
            sum += input;
        }
        
        System.out.println("The total is " + sum);
    }
}
