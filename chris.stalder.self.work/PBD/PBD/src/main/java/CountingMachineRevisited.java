/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class CountingMachineRevisited {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Count from: ");
        int start = keyboard.nextInt();
        System.out.print("Count to: ");
        int end = keyboard.nextInt();
        System.out.print("Count by: ");
        int countBy = keyboard.nextInt();
        
        for (int i = start; i <= end; i += countBy ){
            System.out.print(i + " ");
        }
        
    }
}
