/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;
import java.util.Random;

public class Nim {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        Random r = new Random();
        
        int pile1 = 3;
        int pile2 = 3;
        int pile3 = 3;
        int pileTotal = pile1 + pile2 + pile3;
        
        System.out.print("Player one, enter your name: ");
        String player1 = keyboard.next();
        System.out.print("Player two, enter your name: ");
        String player2 = keyboard.next();
        
        int counter = 1 + r.nextInt(2);
        
        while (pileTotal > 0){
            System.out.println("A: " + pile1 + " B: " + pile2 + " C: " + pile3);
            
            if(counter == 1){
         
                String choice = playersChoice(player1);
                int amount = playersAmount(choice, pile1, pile2, pile3);
                
                amount = noCheating(amount);
                
                switch(choice){
                    case ("A"):
                        pile1 -= amount;
                        break;
                    case ("B"):
                        pile2 -= amount;
                        break;
                    case ("C"):
                        pile3 -= amount;
                        break;
                }
                pileTotal = pile1 + pile2 + pile3;
                counter = 2;
                
            }else if (counter == 2){
                
                String choice = playersChoice(player2);
                int amount = playersAmount(choice, pile1, pile2, pile3);
                
                amount = noCheating(amount);
            
                switch(choice){
                    case ("A"):
                        pile1 -= amount;
                        break;
                    case ("B"):
                        pile2 -= amount;
                        break;
                    case ("C"):
                        pile3 -= amount;
                        break;
                }
                pileTotal = pile1 + pile2 + pile3;
                counter = 1;
            }
        }
        
        
        if (counter == 1){
            System.out.println(player1 + ", there are no counters left. So you win!");
        }else if (counter == 2){
            System.out.println(player2 + ", there are no counters left. So you win!");
        }
        
    }

    private static int noCheating(int amount) {
        Scanner keyboard = new Scanner(System.in);
        while (amount <= 0){
            System.out.println("You must choose at least 1. How many?");
            amount = keyboard.nextInt();
        }
        return amount;
    }
    
    private static String playersChoice(String player){
        Scanner keyboard = new Scanner(System.in);
        
        System.out.print(player +", choose a pile: ");
        String choice = keyboard.next();
        boolean isValid = false;
        
        while(isValid == false){

            if("A".equals(choice) || "B".equals(choice) || "C".equals(choice)){
                isValid = true;
            }else{
                System.out.println(choice + " is not an option!");
                System.out.print("Choose a pile: ");
                choice = keyboard.next();
            }
        }
        
        return choice;
    }
    
    private static int playersAmount(String choice, int pile1, int pile2, int pile3){
        Scanner keyboard = new Scanner(System.in);
        boolean isValid = false;
        
        System.out.println("How much would you like to remove from pile " + choice);
        int amount = keyboard.nextInt();
        
        while (isValid == false){
            if (("A".equals(choice) && (amount <= pile1)) || ("B".equals(choice) && (amount <= pile2)) || ("C".equals(choice) && (amount <= pile3))){
                isValid = true;
            }else{
                System.out.println("You can not remove that much from " + choice);
                System.out.println("How much would you like to remove from pile " + choice);
                amount = keyboard.nextInt();  
            }
        }
        return amount;
    }
}
