/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class HowOldAreYouSpecifically {
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Hey what's your name?");
        String name = keyboard.nextLine();
        
        System.out.println("Hi, " + name + ". How old are you?");
        int age = keyboard.nextInt();
        
        if(age < 16){
            System.out.println("You can't drive.");
        }else if (age >= 16 && age < 18){
            System.out.println("You can drive, but you can't vote.");
        }else if (age >= 18 && age < 25){
            System.out.println("You can vote, but you can't rent a car.");
        }else{
            System.out.println("You can do pretty much anything.");
        }
    }
}
