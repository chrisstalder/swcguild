/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class AddingValuesWithAForLoop {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        int sum = 0;
        
        System.out.println("Number:");
        int input = keyboard.nextInt();
        
        for (int i = 1; i <= input; i ++){
            System.out.print(i + " ");
            sum += i;
        }
        
        System.out.println("The sum is " + sum);
    }
}
