/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class TwoQuestions {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("TWO QUESTIONS!");
        System.out.println("Think of an object, and I'll try to guess what it is.");
        
        System.out.println("Question 1. Is it an animal, vegetable, or mineral?");
        String answer1 = keyboard.nextLine();
        
        System.out.println("Question 2. Is it bigger than a breadbox?");
        String answer2 = keyboard.nextLine();
        
        switch (answer1){
            case ("animal"):
                switch (answer2){
                    case ("no"):
                        System.out.println("My guess is you're thinking about a squirrel.");
                        System.out.println("I would ask you if I was right, but I don't actually care.");
                        break;
                    case ("yes"): 
                        System.out.println("My guess is you're thinking about a moose.");
                        System.out.println("I would ask you if I was right, but I don't actually care.");
                        break;
                }
                break;
            case ("vegetable"):
                switch (answer2){
                    case ("no"):
                        System.out.println("My guess is you're thinking about a carrot.");
                        System.out.println("I would ask you if I was right, but I don't actually care.");
                        break;
                    case ("yes"): 
                        System.out.println("My guess is you're thinking about a watermelon.");
                        System.out.println("I would ask you if I was right, but I don't actually care.");
                        break;
                }
                break;
            case ("mineral"):
                switch (answer2){
                    case ("no"):
                        System.out.println("My guess is you're thinking about a paper clip.");
                        System.out.println("I would ask you if I was right, but I don't actually care.");
                        break;
                    case ("yes"): 
                        System.out.println("My guess is you're thinking about a Camaro.");
                        System.out.println("I would ask you if I was right, but I don't actually care.");
                        break;
                }
                break;
        }
        
        
    }
}
