/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functions;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AreaCalculator {
    
    public static void main(String[] args) {
        run();
    }
    
    public static void run(){
        Scanner sc = new Scanner(System.in);
//shape	formula
//square	A = s²
//rectangle	A = l × w
//triangle	A = ½bh
//circle	A = πr²

        System.out.println("Shape    |   Formula");
        System.out.println("=====================");
        System.out.println("square   |   A = s²");
        System.out.println("rectangle|   A = l × w");
        System.out.println("triangle |   A = ½bh");
        System.out.println("circle   |   A = πr²");
        
        String selection = sc.nextLine();
        boolean repeat = true;
        while(repeat){
            switch(selection){
                case ("square"):
                    area_square();
                    break;
                case ("rectangle"):
                    area_rectangle();
                    break;
                case ("triangle"):
                    area_triangle();
                    break;
                case ("circle"):
                    area_circle();
                    break;
            }
        }
    }
    
    public static double area_circle(int radius){
        // returns the area of a circle
    }

    public static int area_rectangle(int length, int width) {
        // returns the area of a rectangle
    }


    public static int area_square(int side){
        // returns the area of a square
    }

    public static double area_triangle(int base, int height) {
        // returns the area of a triangle
    }
}
