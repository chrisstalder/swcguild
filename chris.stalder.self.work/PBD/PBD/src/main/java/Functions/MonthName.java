/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functions;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MonthName
{
	public static String month_name( int month )
	{
		String result;
		// Your code goes in here.
                switch(month){
                    case 1:
                        result = "January";
                        break;
                    case 2:
                        result = "February";
                        break;
                    case 3:
                        result = "March";
                        break;
                    case 4:
                        result = "April";
                        break;
                    case 5:
                        result = "May";
                        break;
                    case 6:
                        result = "June";
                        break;
                    case 7:
                        result = "July";
                        break;
                    case 8:
                        result = "August";
                        break;
                    case 9:
                        result = "September";
                        break;
                    case 10:
                        result = "October";
                        break;
                    case 11:
                        result = "November";
                        break;
                    case 12:
                        result = "December";
                        break;
                    default:
                        result = "error";
                        break;
                }
		
		return result;
	}


	public static void main( String[] args )
	{
            Scanner sc = new Scanner(System.in);
            System.out.println("Input number 1-12");
            int number = sc.nextInt();
            String month = month_name(number);
            System.out.println(month);
	}
}