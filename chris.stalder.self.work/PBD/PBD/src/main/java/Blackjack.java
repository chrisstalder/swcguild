/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;
import java.util.Random;

public class Blackjack {
    
   static Scanner keyboard = new Scanner(System.in);
    
    public static void main(String[] args) {

        int playerCard1 = dealCard();
        int playerCard2 = dealCard();

        int dealerCard1 = dealCard();
        int dealerCard2 = dealCard();
        int newTotal=0;

        int playerTotal = cardTotal( playerCard1, playerCard2);
        int dealerTotal = cardTotal( dealerCard1, dealerCard2);

        boolean cardTotalValidation = isValid(playerTotal);
        
        System.out.println("You drew " + playerCard1 + " and " + playerCard2);
        System.out.println("Your total is " + playerTotal);
        
        while (cardTotalValidation){
            System.out.println("Do you want a hit?");
            String response = keyboard.next();
        
            boolean answer = answerValidation(response);
            if (answer){
                int nextCard = dealCard();
                newTotal = cardTotal(playerTotal, nextCard);
                cardTotalValidation = isValid(newTotal);
                System.out.println("Your total is " + newTotal);
            }else{
                cardTotalValidation = false;
            }  
        }
        
        if (playerTotal > dealerTotal && playerTotal <= 21){
            System.out.println("You win!");
        }else if (playerTotal <= 21){
            System.out.println("You lose!");
        }
    }
    
    public static boolean isValid(int cardTotal){
        boolean isValid = false;
            if (cardTotal <= 21)
                isValid = true;
        return isValid;
    }
    
    public static int dealCard(){
        Random r = new Random();
        int card = 2 + r.nextInt(10);
        
        return card;
    }
    
    public static int cardTotal(int card1, int card2){
        int total = card1 + card2;
        return total;
    }
    
    public static boolean answerValidation(String response){
        
        boolean isValid = false;
            
            if ("yes".equals(response)){
                isValid = true;
            }
            else if ("no".equals(response)){
                isValid = false;
            }
            else{
                System.out.println("I couldn't quite hear you. Do you want a hit?");
                response = keyboard.next();
            }
        
        
        return isValid;
    }
    
}
