/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphics;

/**
 *
 * @author apprentice
 */
import java.awt.*;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

public class SmilingFaceFunction extends Canvas
{
    public void paint( Graphics g )
    {
        Random r = new Random();

        drawSmilingFace(g,100,100);
        drawSmilingFace(g,400,350);
        for (int i = 0; i < 10; i++) {
            try {
                int x = r.nextInt(800);
                int y = r.nextInt(500);
                drawSmilingFace(g, x, y);
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(SmilingFaceFunction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    public void drawSmilingFace( Graphics g, int x, int y )
    {
        // Draws a smiling face on the screen, where the point (x,y) is
        //  the upper-left corner of a box containing the face.
        
        
        // draw circle for the head
        g.setColor(Color.YELLOW);
        g.fillOval(x, y, 200, 200);
        // draw eyes
        g.setColor(Color.BLACK);
        g.fillOval(x+50, y+50, 25, 50);
        g.fillOval(x+125, y+50, 25, 50);
        // draw mouth
        g.fillArc(x+40, y+115, 120, 50, 160, 220);
    }

    public static void main(String[] args)
    {
        // You can change the title or size here if you want.
        JFrame win = new JFrame("Smiling Face Function");
        win.setSize(1024,768);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        win.add( new SmilingFaceFunction() );
        win.setVisible(true);
    }

}