package Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.awt.*;
import javax.swing.JFrame;

public class ASmilingFace extends Canvas{
    
    public void paint (Graphics g){
        g.setColor(Color.YELLOW);
        g.fillOval(200, 150, 300, 300);
        
        g.setColor(Color.red);
        g.fillOval(250, 200, 50, 50);
        g.setColor(Color.red);
        g.fillOval(400, 200, 50, 50);
        g.setColor(Color.black);
        g.drawArc(225, 300, 250, 50, 180, 180);
    }
    
    public static void main(String[] args) {
        JFrame win = new JFrame("A Circle");
        win.setSize(800, 600);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ASmilingFace canvas = new ASmilingFace();
        win.add(canvas);
        win.setVisible(true);
    }
}
