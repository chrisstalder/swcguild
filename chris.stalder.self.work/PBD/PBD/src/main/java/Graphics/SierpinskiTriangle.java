package Graphics;

import java.awt.Canvas;
import java.awt.Graphics;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class SierpinskiTriangle extends Canvas{
    public static void main(String[] args) {
        JFrame win = new JFrame("Sierpinski Triangle");
        win.setSize(1024, 768);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SierpinskiTriangle canvas = new SierpinskiTriangle();
        win.add(canvas);
        win.setVisible(true);
    }
    
    public void paint(Graphics g){
        Random r = new Random();
        int x1 = 512;
        int y1 = 109;
        int x2 = 146;
        int y2 = 654;
        int x3 = 876;
        int y3 = 654;
        
        int x = 512;
        int y = 382;
        
        for (int i = 0; i < 50000; i++) {
            g.drawLine(x,y,x,y);
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
                Logger.getLogger(SierpinskiTriangle.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            int number = 1 + r.nextInt(3);
            
            switch(number){
                case 1:
                    int dx = x - x1;
                    int dy = y - y1;
                    x = x - dx/2;
                    y = y - dy/2;
                    break;
                case 2:
                    dx = x - x2;
                    dy = y - y2;
                    x = x - dx/2;
                    y = y - dy/2;
                    break;
                case 3:
                    dx = x - x3;
                    dy = y - y3;
                    x = x - dx/2;
                    y = y - dy/2;
                    break;
            }
        }
    }
}
