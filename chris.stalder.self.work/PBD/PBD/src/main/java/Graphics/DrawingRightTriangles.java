package Graphics;


import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.Random;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class DrawingRightTriangles extends Canvas{
    public static void main(String[] args) {
        JFrame win = new JFrame("Drawing Right Triangles");
        win.setSize(1024, 1024);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        DrawingRightTriangles canvas = new DrawingRightTriangles();
        win.add(canvas);
        win.setVisible(true);
    }
    
    public void paint(Graphics g){
        Random r = new Random();
        for (int i = 0; i < 500; i++) {
            int red = r.nextInt(255);
            int blue = r.nextInt(255);
            int green = r.nextInt(255);
            Color randomColor = new Color(red, blue, green);
            Polygon triangle = new Polygon();
            int point1x = r.nextInt(1024);
            int point1y = r.nextInt(1024);
            int point2x = point1x + 50;
            int point2y = point1y + 50;
            int point3x = point1x;
            int point3y = point1y + 50;
            
            triangle.addPoint(point1x, point1y);
            triangle.addPoint(point2x, point2y);
            triangle.addPoint(point3x, point3y);
            
            g.setColor(randomColor);
            g.fillPolygon(triangle);
        }
    }
}
