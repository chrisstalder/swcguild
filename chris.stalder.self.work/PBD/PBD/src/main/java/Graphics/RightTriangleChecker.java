package Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class RightTriangleChecker {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Enter three integers:");
        System.out.print("Side 1: ");
        int num1 = keyboard.nextInt();
        System.out.println("Side 2: ");
        int num2 = keyboard.nextInt();
        
        while (num2 < num1){
            System.out.println(num2 + " is smaller than " + num1 + ". Try again.");
            System.out.println("Side 2: ");
            num2 = keyboard.nextInt();
        }
        
        System.out.println("Side 3: ");
        int num3 = keyboard.nextInt();
        
        while (num3 < num2){
            System.out.println(num3 + " is smaller than " + num2 + ". Try again.");
            System.out.println("Side 3: ");
            num3 = keyboard.nextInt();
        }
        
        if (num3 == num2){
            System.out.println("You three sides are " + num1 + " " + num2 + " " + num3);
            System.out.println("These do not make a right triangle.");
        }else{
            System.out.println("You three sides are " + num1 + " " + num2 + " " + num3);
            System.out.println("These *do* make a right triangle.");
        }
    }
}
