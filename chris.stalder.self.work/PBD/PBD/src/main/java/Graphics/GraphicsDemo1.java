package Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.awt.*;
import javax.swing.JFrame;

public class GraphicsDemo1 extends Canvas
{
    public void paint( Graphics g )
    {
        g.setColor(Color.green);
        g.drawRect(50,20,100,200);  // draw a rectangle
        g.fillOval(160,20,100,200); // draw a filled-in oval
        g.setColor(Color.blue);
        g.fillRect(200,400,200,20); // a filled-in rectangle
        g.drawOval(200,430,200,100);
        
        g.setColor(Color.black);
        g.drawString("Graphics are pretty neat.", 500, 100);
        int x = getWidth() / 2;
        int y = getHeight() / 2;
        g.drawString("The first letter of this string is at (" + x + "," + y + ")", x, y);
        
        g.setColor(Color.red);
        g.fillRect(600, 400, 200, 200);
        
    }

    public static void main( String[] args )
    {
        // You can change the title or size here if you want.
        JFrame win = new JFrame("GraphicsDemo1");
        win.setSize(800,600);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GraphicsDemo1 canvas = new GraphicsDemo1();
        win.add( canvas );
        win.setVisible(true);
    }
}

/*How big is the window that appears? How many pixels wide? How many pixels tall?
800 px wide and 600 px tall

In the call to the function g.drawRect(), there are four numbers. What do they mean? Try changing them to figure it out.
the first two numbers decide the location of the rectangle. the second two numbers decide the size of the rectangle

What about the call to fillOval()? What do the four numbers mean here?
the first two numbers decide the location of the oval. the second two numbers decide the size of the oval.

What are the two numbers in the call to drawString()?
the two numbers determine the location of where to draw the string

What happens when two objects overlap? Which one is drawn on top?
Add a red, filled-in square somewhere in the lower-right of the canvas.*/
