package Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.awt.*;
import javax.swing.JFrame;

public class ACircle extends Canvas{
    
    public void paint (Graphics g){
        g.setColor(Color.GREEN);
        g.fillOval(200, 150, 300, 300);
    }
    
    public static void main(String[] args) {
        JFrame win = new JFrame("A Circle");
        win.setSize(800, 600);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ACircle canvas = new ACircle();
        win.add(canvas);
        win.setVisible(true);
    }
}
