package Graphics;


import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class ABoringTriangle extends Canvas{
    
    public void paint(Graphics g){
        Color myPurple = new Color(255, 0, 255);
        g.setColor(myPurple);
        
        Polygon triangle = new Polygon();
        triangle.addPoint(300, 300);
        triangle.addPoint(350, 350);
        triangle.addPoint(250, 350);
        
        g.fillPolygon(triangle);
    }
    
    public static void main(String[] args) {
        JFrame win = new JFrame("A Boring Triangle");
        win.setSize(600, 600);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ABoringTriangle canvas = new ABoringTriangle();
        win.add(canvas);
        win.setVisible(true);
    }
}
