package Graphics;


import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class AClockFace extends Canvas{
    
    public void paint(Graphics g){
        g.setColor(Color.black);
        g.drawOval(100, 150, 400, 400);
        g.setColor(Color.gray);
        g.fillOval(300, 335, 10, 10);
        g.setColor(Color.black);
        g.drawLine(305,340,125,335);
        g.drawLine(305,340,335,450);
        g.setFont(new Font("Calibri", Font.BOLD, 32)); // 36-pt plain
        g.drawString("12", 280, 200);
        g.setFont(new Font("Calibri", Font.BOLD, 32)); // 36-pt plain
        g.drawString("6", 290, 525);
        g.setFont(new Font("Calibri", Font.BOLD, 32)); // 36-pt plain
        g.drawString("3", 450, 340);
        g.setFont(new Font("Calibri", Font.BOLD, 32)); // 36-pt plain
        g.drawString("9", 110, 345);
        
    }
    
    public static void main(String[] args) {
        JFrame win = new JFrame("A Clock Face");
        win.setSize(600, 800);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        AClockFace canvas = new AClockFace();
        win.add(canvas);
        win.setVisible(true);
    }
}
