/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphics;

/**
 *
 * @author apprentice
 */
import java.awt.*;
import java.util.Random;
import javax.swing.JFrame;

/**
 * **********************************************************************************
 ** B O X Y 3 - write and use function - boxes different sizes **
 **********************************************************************************
 */
class Boxy3 extends Canvas {

    public void paint(Graphics window) {
        //                  color       x    y   w   h
        drawBox(window, Color.YELLOW, 200, 300, 50, 30);
        drawBox(window, Color.GREEN, 400, 220, 30, 150);

        // draw six more boxes -- different colors, different places, different sizes
        Random r = new Random();
        for (int i = 0; i < 6; i++) {
            int red = r.nextInt(255);
            int blue = r.nextInt(255);
            int green = r.nextInt(255);
            Color myColor = new Color(red, blue, green);
            int x = r.nextInt(600);
            int y = r.nextInt(300);
            int w = r.nextInt(200);
            int h = r.nextInt(200);
            drawBox(window, myColor, x, y, w, h);
        }
    }

    public void drawBox(Graphics window, Color c, int x, int y, int w, int h) {
        // add code to draw a WxH box in color c at (x,y)
        window.setColor(c);
        window.fillRect(x, y, w, h);
        window.setColor(Color.WHITE);
        window.fillRect(x + 10, y + 10, w-20, h-20);
    }

    public static void main(String[] args) {
        Canvas canvas = new Boxy3();
        JFrame win = new JFrame("Boxy3 - write and use function - boxes different sizes");
        win.setSize(800, 600);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        canvas.setBackground(Color.WHITE);
        win.add(canvas);
        win.setVisible(true);
    }
}
