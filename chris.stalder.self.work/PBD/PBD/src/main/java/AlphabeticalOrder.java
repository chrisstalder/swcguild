/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class AlphabeticalOrder {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("What is your last name?");
        
        String lastName = keyboard.nextLine();
        
        if (lastName.compareTo("Carswell") <= 0){
            System.out.println("You don't have to wait long");
        }else if ((lastName.compareTo("Carswell") > 0) && (lastName.compareTo("Jones") <= 0)){
            System.out.println("That's not bad"); 
        }else if ((lastName.compareTo("Jones") > 0) && (lastName.compareTo("Smith") <= 0)){
            System.out.println("Looks like a bit of a wait"); 
        }else if ((lastName.compareTo("Smith") > 0) && (lastName.compareTo("Young")<= 0)){
            System.out.println("It's gonna be a while");
        }else if ((lastName.compareTo("Young") > 0)){
            System.out.println("Not going anywhere for a while?");
        }
    }
}
 