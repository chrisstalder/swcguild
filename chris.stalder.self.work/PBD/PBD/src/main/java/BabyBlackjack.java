/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Random;

public class BabyBlackjack {
    
    public static void main(String[] args) {
        
        Random r = new Random();
        
        System.out.println("Baby Blackjack!");
        
        int playerCard1 = 1 + r.nextInt(10);
        int playerCard2 = 1 + r.nextInt(10);
        int playerTotal = playerCard1 + playerCard2;
        int dealerCard1 = 1 + r.nextInt(10);
        int dealerCard2 = 1 + r.nextInt(10);
        int dealerTotal = dealerCard1 + dealerCard2;
        
        System.out.println("You drew " + playerCard1 + " and " + playerCard2);
        System.out.println("Your total is " + playerTotal);
        System.out.println("");
        System.out.println("");
        System.out.println("The dealer drew " + dealerCard1 + " and " + dealerCard2);
        System.out.println("Dealer's total is " + dealerTotal);
        
        if (playerTotal > dealerTotal){
            System.out.println("You win!");
        }else{
            System.out.println("You lose!");
        }
    }
}
