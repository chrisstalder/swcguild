/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class DumbCalculator {
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("What is your first number?");
        float number1 = keyboard.nextFloat();
        
        System.out.println("What is your second number?");
        float number2 = keyboard.nextFloat();
        
        System.out.println("What is your third number?");
        float number3 = keyboard.nextFloat();
        
        float answer = (number1 + number2 + number3) / 2;
        
        System.out.println("(" + number1 + "+" + number2 + "+" + number3 + ") / 2 = " + answer);
    }
}
