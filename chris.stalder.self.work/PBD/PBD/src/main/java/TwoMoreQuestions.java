/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class TwoMoreQuestions {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Question 1: Does it belong inside or outside or both?");
        String answer = keyboard.nextLine();
        System.out.println("Question 2: Is it alive?");
        String answer2 = keyboard.nextLine();
        
        if ("inside".equals(answer) && "yes".equals(answer2)){
            System.out.println("I'm gonna guess you're thinking of a houseplant");
            System.out.println("I'd ask you if I'm right but I really don't care");
        }
        
        if ("outside".equals(answer) && "yes".equals(answer2)){
            System.out.println("I'm gonna guess you're thinking of a python");
            System.out.println("I'd ask you if I'm right but I really don't care");
        }
        
        if ("both".equals(answer) && "yes".equals(answer2)){
            System.out.println("I'm gonna guess you're thinking of a dog");
            System.out.println("I'd ask you if I'm right but I really don't care");
        }
        if ("inside".equals(answer) && "no".equals(answer2)){
            System.out.println("I'm gonna guess you're thinking of a shower curtain");
            System.out.println("I'd ask you if I'm right but I really don't care");
        }
        
        if ("outside".equals(answer) && "no".equals(answer2)){
            System.out.println("I'm gonna guess you're thinking of a billboard");
            System.out.println("I'd ask you if I'm right but I really don't care");
        }
        
        if ("both".equals(answer) && "no".equals(answer2)){
            System.out.println("I'm gonna guess you're thinking of a cell phone");
            System.out.println("I'd ask you if I'm right but I really don't care");
        }
        
    }
}
