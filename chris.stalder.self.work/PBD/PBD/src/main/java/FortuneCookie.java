/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Random;

public class FortuneCookie {
    
    public static void main(String[] args) {
        
        Random r = new Random();
        
        int fortune = 1 + r.nextInt(11);
        
        switch (fortune){
            case 1:
                System.out.println("Today it's up to you to create the peacefulness you long for.");
                break;
            case 2:
                System.out.println("A friend asks only for your time not your money.");
                break;
            case 3:
                System.out.println("If you refuse to accept anything but the best, you very often get it.");
                break;
            case 4:
                System.out.println("A smile is your passport into the hearts of others.");
                break;
            case 5:
                System.out.println("A good way to keep healthy is to eat more Chinese food.");
                break;
            case 6:
                System.out.println("Your high-minded principles spell success.");
                break;
            case 7:
                System.out.println("Hard work pays off in the future, laziness pays off now.");
                break;
            case 8:
                System.out.println("Change can hurt, but it leads a path to something better.");
                break;
            case 9:
                System.out.println("Enjoy the good luck a companion brings you.");
                break;
            case 10:
                System.out.println("People are naturally attracted to you.");
                break;
            case 11:
                System.out.println("You look pretty :)");
                break;
            default:
                System.out.println("Error!");
        }
        
        int lottoNum = 1 + r.nextInt(54);
        
        for (int i = 0; i < 6; i++){  //prints out 5 random numbers with a "-" seperating them
            lottoNum = 1 + r.nextInt(54);
            System.out.print(lottoNum + " - ");
        }
        System.out.print(lottoNum); //prints out the last lottoNum
    }
}
