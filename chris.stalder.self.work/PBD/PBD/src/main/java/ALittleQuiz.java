/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class ALittleQuiz {
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        int answer = 0;
        float rightAnswer = 0f;
        float totalQuestions = 0f;    
        
        System.out.print("Are you ready for a little quiz?(y/n)");
        String prompt = keyboard.next();
        
        System.out.println("Okay, great! Here it comes!");
        System.out.println("Please use numeric value to answer:");
        System.out.println("");
        System.out.println("Q1) What is the capital of Alaska?");
        System.out.println("            1. Melbourne");
        System.out.println("            2. Anchorage");
        System.out.println("            3. Juneau");
        answer = keyboard.nextByte();
        
        //Using switch statements, if character picks right answer, rightAnswer
        //will increment by one, then totalQuestion will increment by one
        switch (answer){
            case 3:
                //right
                System.out.println("That is correct!");
                rightAnswer++;
                answer = 0;
                totalQuestions++;
                break;
            default:
                //wrong
                System.out.println("That is incorrect!");
                answer = 0;
                totalQuestions++;
        }
        
        System.out.println("Q2) Can you store the value \"cat\" in a variable of type int?");
        System.out.println("            1. Yes");
        System.out.println("            2. No");
        answer = keyboard.nextByte();
        
        switch (answer){
            case 2:
                //right
                System.out.println("That is correct!");
                rightAnswer++;
                answer = 0;
                totalQuestions++;
                break;
            default:
                //wrong
                System.out.println("That is incorrect!");
                answer = 0;
                totalQuestions++;
        }

        System.out.println("Q3) What is the result of 9+6/3?");
        System.out.println("            1. 5");
        System.out.println("            2. 11");
        System.out.println("            3. 15/3");
        answer = keyboard.nextByte();
        
        switch (answer){
            case 2:
                //right
                System.out.println("That is correct!");
                rightAnswer++;
                answer = 0;
                totalQuestions++;
                break;
            default:
                //wrong
                System.out.println("That is incorrect!");
                answer = 0;
                totalQuestions++;
        }

        System.out.println("Overall, you got " + rightAnswer + " out of " + totalQuestions);
        
        float grade = 0.0f;
        grade = (100 * (rightAnswer/totalQuestions));
        
        if(grade == 100.0){
            System.out.println("You scored 100%! Fantastic job!A+");
        }else if (grade < 100.0 && grade >= 90.0){
            System.out.println("You scored " + grade + "! Good job! A");
        }else if (grade < 90.0 && grade >= 80.0){
            System.out.println("You scored " + grade + "! Keep on studying! B");
        
        }else if (grade < 80.0 && grade >= 70.0){
            System.out.println("You scored " + grade + "! I think you could do better! C");
        
        }else if (grade < 70.0 && grade >= 60.0){
            System.out.println("You scored " + grade + "...Are you even trying? D");
        
        }else{
            System.out.println("You scored " + grade + "%. It's students like you that drive me to drink. F");
        }
        
        System.out.println(grade);
        System.out.println("Thank you for taking my quiz! :)");
    }
}
