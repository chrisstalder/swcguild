/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class CompareTo {
    
    public static void main(String[] args) {
        
        System.out.print("Comparing \"axe\" with \"dog\" produces ");
        int i = "axe".compareTo("dog");
        System.out.println(i);

        System.out.print("Comparing \"applebee's\" with \"apple\" produces ");
        System.out.println( "applebee's".compareTo("apple") );

        System.out.print("Comparing \"banana\" with \"apple\" produces ");
        System.out.println( "banana".compareTo("apple") );

        System.out.print("Comparing \"mother\" with \"father\" produces ");
        System.out.println( "mother".compareTo("father") );

        System.out.print("Comparing \"search\" with \"field\" produces ");
        System.out.println( "search".compareTo("field") );

        System.out.print("Comparing \"logic\" with \"float\" produces ");
        System.out.println( "logic".compareTo("float") );

        System.out.print("Comparing \"clan\" with \"jarhead\" produces ");
        System.out.println( "clan".compareTo("jarhead") );

        System.out.print("Comparing \"mud\" with \"tree\" produces ");
        System.out.println( "mud".compareTo("tree") );

        System.out.print("Comparing \"is\" with \"was\" produces ");
        System.out.println( "is".compareTo("was") );
        
        System.out.print("Comparing \"rats\" with \"star\" produces ");
        System.out.println( "rats".compareTo("star") );
        
        System.out.print("Comparing \"fudge\" with \"fudge\" produces ");
        System.out.println( "fudge".compareTo("fudge") );

        System.out.print("Comparing \"wear\" with \"wear\" produces ");
        System.out.println( "wear".compareTo("wear") );

        
    }
    
}
