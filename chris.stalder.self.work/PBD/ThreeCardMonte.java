/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;
import java.util.Random;

public class ThreeCardMonte {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        Random r = new Random();
        
        int ace = 1 + r.nextInt(3);
        
        System.out.println("You slide up to Fast Eddie's card table and plop down your cash");
        System.out.println("He glances at you out of the corner of his eye and starts shuffling.");
        System.out.println("He lays down three cards.");
        System.out.println("");
        System.out.println("Which one is the ace?");
        System.out.println("##  ##  ##");
        System.out.println("##  ##  ##");
        System.out.println("1   2   3");

        int guess = keyboard.nextInt();
        
        switch (ace){
            case 1:
                switch (guess){
                    case 1:
                        System.out.println("You nailed it! Fast Eddie reluctantly hands over your winnings, scowling.");
                        System.out.println("AA  ##  ##");
                        System.out.println("AA  ##  ##");
                        System.out.println("1   2   3");
                        break;
                    default:
                        System.out.println("Ha! Fast Eddie wins again! The ace was card number " + ace);
                        System.out.println("AA  ##  ##");
                        System.out.println("AA  ##  ##");
                        System.out.println("1   2   3");
                }
                break;
            case 2:
                switch (guess){
                    case 2:
                        System.out.println("You nailed it! Fast Eddie reluctantly hands over your winnings, scowling.");
                        System.out.println("##  AA  ##");
                        System.out.println("##  AA  ##");
                        System.out.println("1   2   3");
                        break;
                    default:
                        System.out.println("Ha! Fast Eddie wins again! The ace was card number " + ace);
                        System.out.println("##  AA  ##");
                        System.out.println("##  AA  ##");
                        System.out.println("1   2   3");
                }
                break;
            case 3:
                switch (guess){
                    case 3:
                        System.out.println("You nailed it! Fast Eddie reluctantly hands over your winnings, scowling.");
                        System.out.println("##  ##  AA");
                        System.out.println("##  ##  AA");
                        System.out.println("1   2   3");
                        break;
                    default:
                        System.out.println("Ha! Fast Eddie wins again! The ace was card number " + ace);
                        System.out.println("##  ##  AA");
                        System.out.println("##  ##  AA");
                        System.out.println("1   2   3");
                }
                break;
        }
        
        
    }
    
}
