/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class CollatzSequence {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.print("Starting Number: ");
        int input = keyboard.nextInt();
        
        while (input != 1){
            System.out.print(input + " ");
            
            if (input % 2 == 0){
                input = input / 2;
            }else{
                input = (input * 3) + 1;
            }    
        }
    }
}
