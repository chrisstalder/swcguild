/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class VariablesAndNames {
    public static void main(String[] args){
        int cars, drivers, passengers, cars_not_driven, cars_driven;
        double space_in_a_car, carpool_capacity, average_passengers_per_car;
        
        //cars is a a whole number int
        cars = 100;
        //space_in_a_car is a double, if it is changed to 4, the carpool_capacity
        //will still be a double because carpool_capacity is declared as a double.
        space_in_a_car = 4;
        //drivers is a whole number int
        drivers = 30;
        //passengers is a whole number int
        passengers = 90;
        //cars_not_driven is the int cars - int drivers
        cars_not_driven = cars - drivers;
        //cars_driven is the int drivers;
        cars_driven = drivers;
        //carpool_capacity is the variable cars_driven times the variable space_in_a_car
        carpool_capacity = cars_driven * space_in_a_car;
        //average_passengers_per_car is the variable passengers divided by the variable cars_driven
        average_passengers_per_car = passengers / cars_driven;


        System.out.println( "There are " + cars + " cars available." );
        System.out.println( "There are only " + drivers + " drivers available." );
        System.out.println( "There will be " + cars_not_driven + " empty cars today." );
        System.out.println( "We can transport " + carpool_capacity + " people today." );
        System.out.println( "We have " + passengers + " to carpool today." );
        System.out.println( "We need to put about " + average_passengers_per_car + " in each car." );
    }
}
