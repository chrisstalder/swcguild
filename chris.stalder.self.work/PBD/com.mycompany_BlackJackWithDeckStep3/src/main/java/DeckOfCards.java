
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class DeckOfCards {

    
    public List<Card> DeckOfCards(){
        
        Card ASpades = new Card("Ace of Spades", 11);
        Card AClubs = new Card("Ace of Clubs", 11);
        Card AHearts = new Card("Ace of Hearts", 11);
        Card ADiamonds = new Card("Ace of Diamonds", 11);
        Card KSpades = new Card("King of Spades", 10);
        Card KClubs = new Card("King of Clubs", 10);
        Card KHearts = new Card("King of Hearts", 10);
        Card KDiamonds = new Card("King of Diamonds", 10);
        Card QSpades = new Card("Queen of Spades", 10);
        Card QClubs = new Card("Queen of Clubs", 10);
        Card QHearts = new Card("Queen of Hearts", 10);
        Card QDiamonds = new Card("Queen of Diamonds", 10);
        Card JSpades = new Card("Jack of Spades", 10);
        Card JClubs = new Card("Jack of Clubs", 10);
        Card JHearts = new Card("Jack of Hearts", 10);
        Card JDiamonds = new Card("Jack of Diamonds", 10);
        Card TenSpades = new Card("10 of Spades", 10);
        Card TenClubs = new Card("10 of Clubs", 10);
        Card TenHearts = new Card("10 of Hearts", 10);
        Card TenDiamonds = new Card("10 of Diamonds", 10);
        Card NineSpades = new Card("9 of Spades", 9);
        Card NineClubs = new Card("9 of Clubs", 9);
        Card NineHearts = new Card("9 of Hearts", 9);
        Card NineDiamonds = new Card("9 of Diamonds", 9);
        Card EightSpades = new Card("8 of Spades", 8);
        Card EightClubs = new Card("8 of Clubs", 8);
        Card EightHearts = new Card("8 of Hearts", 8);
        Card EightDiamonds = new Card("8 of Diamonds", 8);
        Card SevenSpades = new Card("7 of Spades", 7);
        Card SevenClubs = new Card("7 of Clubs", 7);
        Card SevenHearts = new Card("7 of Hearts", 7);
        Card SevenDiamonds = new Card("7 of Diamonds", 7);
        Card SixSpades = new Card("6 of Spades", 6);
        Card SixClubs = new Card("6 of Clubs", 6);
        Card SixHearts = new Card("6 of Hearts", 6);
        Card SixDiamonds = new Card("6 of Diamonds", 6);
        Card FiveSpades = new Card("5 of Spades", 5);
        Card FiveClubs = new Card("5 of Clubs", 5);
        Card FiveHearts = new Card("5 of Hearts", 5);
        Card FiveDiamonds = new Card("5 of Diamonds", 5);
        Card FourSpades = new Card("4 of Spades", 4);
        Card FourClubs = new Card("4 of Clubs", 4);
        Card FourHearts = new Card("4 of Hearts", 4);
        Card FourDiamonds = new Card("4 of Diamonds", 4);
        Card ThreeSpades = new Card("3 of Spades", 3);
        Card ThreeClubs = new Card("3 of Clubs", 3);
        Card ThreeHearts = new Card("3 of Hearts", 3);
        Card ThreeDiamonds = new Card("3 of Diamonds", 3);
        Card TwoSpades = new Card("2 of Spades", 2);
        Card TwoClubs = new Card("2 of Clubs", 2);
        Card TwoHearts = new Card("2 of Hearts", 2);
        Card TwoDiamonds = new Card("2 of Diamonds", 2);
//        
//        Card[] deck = new Card[52];
//        deck[0] = ASpades;
//        deck[1] = AClubs;
//        deck[2] = AHearts;
//        deck[3] = ADiamonds;
//        deck[4] = KSpades;
//        deck[5] = KClubs;
//        deck[6] = KHearts;
//        deck[7] = KDiamonds;
//        deck[8] = QSpades;
//        deck[9] = QClubs;
//        deck[10] = QHearts;
//        deck[11] = QDiamonds;
//        deck[12] = JSpades;
//        deck[13] = JClubs;
//        deck[14] = JHearts;
//        deck[15] = JDiamonds;
//        deck[16] = TenSpades;
//        deck[17] = TenClubs;
//        deck[18] = TenHearts;
//        deck[19] = TenDiamonds;
//        deck[20] = NineSpades;
//        deck[21] = NineClubs;
//        deck[22] = NineHearts;
//        deck[23] = NineDiamonds;
//        deck[24] = EightSpades;
//        deck[25] = EightClubs;
//        deck[26] = EightHearts;
//        deck[27] = EightDiamonds;
//        deck[28] = SevenSpades;
//        deck[29] = SevenClubs;
//        deck[30] = SevenHearts;
//        deck[31] = SevenDiamonds;
//        deck[32] = SixSpades;
//        deck[33] = SixClubs;
//        deck[34] = SixHearts;
//        deck[35] = SixDiamonds;
//        deck[36] = FiveSpades;
//        deck[37] = FiveClubs;
//        deck[38] = FiveHearts;
//        deck[39] = FiveDiamonds;
//        deck[40] = FourSpades;
//        deck[41] = FourClubs;
//        deck[42] = FourHearts;
//        deck[43] = FourDiamonds;
//        deck[44] = ThreeSpades;
//        deck[45] = ThreeClubs;
//        deck[46] = ThreeHearts;
//        deck[47] = ThreeDiamonds;
//        deck[48] = TwoSpades;
//        deck[49] = TwoClubs;
//        deck[50] = TwoHearts;
//        deck[51] = TwoDiamonds;
        List<Card> deck = new ArrayList();
        deck.add(ASpades);
        deck.add(AClubs);
        deck.add(AHearts);
        deck.add(ADiamonds);
        deck.add(KSpades);
        deck.add(KClubs);
        deck.add(KHearts);
        deck.add(KDiamonds);
        deck.add(QSpades);
        deck.add(QClubs);
        deck.add(QHearts);
        deck.add(QDiamonds);
        deck.add(JSpades);
        deck.add(JClubs);
        deck.add(JHearts);
        deck.add(JDiamonds);
        deck.add(TenSpades);
        deck.add(TenClubs);
        deck.add(TenHearts);
        deck.add(TenDiamonds);
        deck.add(NineSpades);
        deck.add(NineClubs);
        deck.add(NineHearts);
        deck.add(NineDiamonds);
        deck.add(EightSpades);
        deck.add(EightClubs);
        deck.add(EightHearts);
        deck.add(EightDiamonds);
        deck.add(SevenSpades);
        deck.add(SevenClubs);
        deck.add(SevenHearts);
        deck.add(SevenDiamonds);
        deck.add(SixSpades);
        deck.add(SixClubs);
        deck.add(SixHearts);
        deck.add(SixDiamonds);
        deck.add(FiveSpades);
        deck.add(FiveClubs);
        deck.add(FiveHearts);
        deck.add(FiveDiamonds);
        deck.add(FourSpades);
        deck.add(FourClubs);
        deck.add(FourHearts);
        deck.add(FourDiamonds);
        deck.add(ThreeSpades);
        deck.add(ThreeClubs);
        deck.add(ThreeHearts);
        deck.add(ThreeDiamonds);
        deck.add(TwoSpades);
        deck.add(TwoClubs);
        deck.add(TwoHearts);
        deck.add(TwoDiamonds);
        
        return deck;
    }
    
    public Card dealCard(List<Card> deck){
        Random r = new Random();
        int randomIndex = r.nextInt(deck.size());
        Card dealtCard = new Card(deck.get(randomIndex).getName(), deck.get(randomIndex).getValue());
        deck.remove(deck.get(randomIndex));
        return dealtCard;
    }
    
    public int cardTotal(Card card1, Card card2){
        int card1Value = card1.getValue();
        int card2Value = card2.getValue();
        int cardTotal = card1Value + card2Value;
        
        return cardTotal;
    }
    
    public int hitTotal(int currentScore, Card dealtCard){
        
        int card = dealtCard.getValue();
        int cardTotal = currentScore + card;
        
        return cardTotal;
    }

}
