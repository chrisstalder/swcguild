/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;
import java.util.Random;

public class Nim {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        Random r = new Random();
        
        int pile1 = 3;
        int pile2 = 3;
        int pile3 = 3;
        int pileTotal = pile1 + pile2 + pile3;
        
        System.out.print("Player one, enter your name: ");
        String player1 = keyboard.next();
        System.out.print("Player two, enter your name: ");
        String player2 = keyboard.next();
        
        int first = 1 + r.nextInt(2);
        int second = 0;
        
        while (pileTotal > 0){
            System.out.println("A: " + pile1 + " B: " + pile2 + " C: " + pile3);
            
            if(first == 1){
         
                System.out.print(player1 +", choose a pile: ");
                String choice = keyboard.next();
                System.out.println("How many to remove from pile " + choice);
                int amount = keyboard.nextInt();
                
                amount = noCheating(amount, keyboard);
                
                switch(choice){
                    case ("A"):
                        pile1 -= amount;
                        break;
                    case ("B"):
                        pile2 -= amount;
                        break;
                    case ("C"):
                        pile3 -= amount;
                        break;
                }
                pileTotal = pile1 + pile2 + pile3;
                first = 2;
                
            }else if (first == 2){
                
                System.out.print(player2 +", choose a pile: ");
                String choice = keyboard.next();
                System.out.println("How many to remove from pile " + choice);
                int amount = keyboard.nextInt();
                
                amount = noCheating(amount, keyboard);
            
                switch(choice){
                    case ("A"):
                        pile1 -= amount;
                        break;
                    case ("B"):
                        pile2 -= amount;
                        break;
                    case ("C"):
                        pile3 -= amount;
                        break;
                }
                pileTotal = pile1 + pile2 + pile3;
                first = 1;
            }
        }
        
        
        if (first == 1){
            System.out.println(player1 + ", there are no counters left. So you win!");
        }else if (first == 2){
            System.out.println(player2 + ", there are no counters left. So you win!");
        }
        
    }

    private static int noCheating(int amount, Scanner keyboard) {
        while (amount <= 0){
            System.out.println("You must choose at least 1. How many?");
            amount = keyboard.nextInt();
        }
        return amount;
    }
}
