/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class MoreVariablesAndPrinting {
    public static void main(String[] args){
        String name, eyes, teeth, hair;
        int age, height, weight;
        double heightInCentimeters, weightInKilos;

        name = "Christopher J.R. Stalder";
        age = 26;     // not a lie
        height = 70;  // inches
        weight = 210; // lbs
        eyes = "Brown";
        teeth = "White";
        hair = "Brown";
        heightInCentimeters = height * 2.54;
        weightInKilos = weight * 0.453592;

        System.out.println( "Let's talk about " + name + "." );
        System.out.println( "He's " + height + " inches tall." );
        System.out.println("Or if you prefer, he's " + heightInCentimeters + " centimeters tall.");
        System.out.println( "He's " + weight + " pounds heavy." );
        System.out.println("Or if you prefer, he weighs " + weightInKilos + " kilograms.");
        System.out.println( "He's got " + eyes + " eyes and " + hair + " hair." );
        System.out.println( "His teeth are usually " + teeth + " depending on the coffee." );

        // This line is tricky; try to get it exactly right.
        System.out.println( "If I add " + age + ", " + height + ", and " + weight
            + " I get " + (age + height + weight) + "." );
    }
}
