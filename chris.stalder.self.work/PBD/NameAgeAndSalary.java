/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class NameAgeAndSalary {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        
        String name;
        int age;
        float wage = 0f;
        
        System.out.println("Hello, what is your name?");
        name = keyboard.nextLine();
        
        System.out.println("Hi, " + name + ". How old are you?");
        age = keyboard.nextInt();
        
        if(age <= 21){
            System.out.println("Oh, you're " + age +"? you're still a young buck!");
        }else if(age > 21 && age <= 50){
            System.out.println("So, you're " + age + ". That's not old at all!");
        }else if (age > 50 && age <= 70){
            System.out.println("You're " + age + "? Man...really getting up there huh?");
        }else if (age > 70 && age<= 80){
            System.out.println("Well, they say " + age + " is the new 50.");
        }else if (age > 80){
            System.out.println("Oh man, " + age + "is a long time. You've had a good run, right?");
        }
        
        System.out.println("How much do you make, " + name + "?");
        wage = keyboard.nextInt();
        
        if (wage <= 10){
            System.out.println(wage + "? I hope that's hourly, and not yearly LMAO :)");
        }else if (wage > 10 && wage <= 20){
            System.out.println(wage + "! That's probably a pretty nice paycheck, huh?");
        }else if (wage > 20){
            System.out.println(wage + "!!? Dang, lend me a 100 bucks, please!");
        }
    }
}
