/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class BabyNim {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        int pile1 = 3;
        int pile2 = 3;
        int pile3 = 3;
        int pileTotal = pile1 + pile2 + pile3;
        
        while (pileTotal > 0){
            
            System.out.println("A: " + pile1 + " B: " + pile2 + " C: " + pile3);
            System.out.print("Choose a pile: ");
            String choice = keyboard.next();
            System.out.println("How many to remove from pile " + choice);
            int amount = keyboard.nextInt();
            
            switch(choice){
                case ("A"):
                    pile1 -= amount;
                    break;
                case ("B"):
                    pile2 -= amount;
                    break;
                case ("C"):
                    pile3 -= amount;
                    break;
            }
            pileTotal = pile1 + pile2 + pile3;
        }
        
        System.out.println("All piles are empty. Good job!");
        
    }
}
