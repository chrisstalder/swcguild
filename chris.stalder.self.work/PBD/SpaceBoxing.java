/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class SpaceBoxing {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.print("Please enter your current weight on Earth:");
        float earthWeight = keyboard.nextFloat();
        
        System.out.println("Here is a list of planets you can choose from:");
        System.out.println("1. Venus    2. Mars     3. Jupiter");
        System.out.println("4. Saturn   5. Uranus   6. Neptune");
        
        System.out.print("Which planet are you visiting?");
        int planetNum = keyboard.nextInt();
        
        float planetWeight = 0f;
        String planetName = "";
        switch (planetNum){
            case 1:
                //venus
                planetName = "Venus";
                planetWeight = (float)(earthWeight * 0.78);
                System.out.println("Your weight on " + planetName + " would be " + planetWeight);
                break;
            case 2:
                //mars
                planetName = "Mars";
                planetWeight = (float)(earthWeight * 0.39);
                System.out.println("Your weight on " + planetName + " would be " + planetWeight);
                break;
            case 3:
                //jupiter
                planetName = "Jupiter";
                planetWeight = (float)(earthWeight * 2.65);
                System.out.println("Your weight on " + planetName + " would be " + planetWeight);
                break;
            case 4:
                //saturn
                planetName = "Saturn";
                planetWeight = (float)(earthWeight * 1.17);
                System.out.println("Your weight on " + planetName + " would be " + planetWeight);
                break;
            case 5:
                //uranus
                planetName = "Uranus";
                planetWeight = (float)(earthWeight * 1.05);
                System.out.println("Your weight on " + planetName + " would be " + planetWeight);
                break;
            case 6:
                //neptune
                planetName = "Neptune";
                planetWeight = (float)(earthWeight * 1.23);
                System.out.println("Your weight on " + planetName + " would be " + planetWeight);
                break;
            default:
                //error 
                System.out.println("That is not a planet listed.");
        }
        
        
    }
    
}
