/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class BMICalculator {
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Your height (feet only)");
        int heightInFeet = keyboard.nextInt();
        
        System.out.println("You height (inches)");
        int heightInInches = keyboard.nextInt();
        
        System.out.println("Your weight in pounds");
        float weightInPounds = keyboard.nextFloat();
        
        heightInFeet *= 12;
        
        float height = (float)((heightInFeet + heightInInches) * 0.025);
        float weight = (float)(weightInPounds * 0.45);
        
        float BMI = (weight/(height * height));
        
        System.out.println("Your BMI is " + BMI);
    }
    
}
