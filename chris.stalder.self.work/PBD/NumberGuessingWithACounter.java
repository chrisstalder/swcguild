/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;
import java.util.Random;

public class NumberGuessingWithACounter {
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        Random r = new Random();
        
        System.out.println("I'm thinking of a number 1 - 10.");
        System.out.print("Your guess:");
        int guess = keyboard.nextInt();
        int tries = 0;
        
        int secretNumber = 1 + r.nextInt(10);
        tries++;
        while (guess != secretNumber){
            System.out.println("That is incorrect.");
            System.out.print("Guess again: ");
            guess = keyboard.nextInt();
            tries++;
        }
        
        if (guess == secretNumber){
            System.out.println("That's correct. My secret number was " + secretNumber + "!");
            System.out.println("It took you " + tries + " tries.");
        }
            
    }
}
