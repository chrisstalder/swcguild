/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class StillUsingVariables {
    public static void main(String[] args){
        String myName = "Christopher Joseph Ray Stalder";
        short graduationYear = 2016;
        
        System.out.println( "My name is " + myName + " and I'll graduate in " + graduationYear + "." );
    }
}
