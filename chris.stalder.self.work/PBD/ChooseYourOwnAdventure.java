/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class ChooseYourOwnAdventure {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("WELCOME TO MY ADVENTURE!!!!");
        System.out.println("");
        System.out.println("You are in a haunted mansion.");
        System.out.println("Would you like to go \"upstairs\", or down the \"hallway\"?");
        
        String choice1 = keyboard.nextLine();
        
        switch (choice1){
            case ("upstairs"):
                System.out.println("You go upstairs and enter a hallway. At the end of "
                        + "the hallway you see a master \"bedroom\". There is also "
                        + "a \"bathroom\" to your right. ");
                System.out.println("Would you like to go to the bedroom or the bathroom?");
                String choice2 = keyboard.nextLine();
                
                switch(choice2){
                    case ("bedroom"):
                        System.out.println("You are inside the master bedroom."
                                + "At the far end of the room, a large window is open."
                                + "Do you look out the window?");
                        String choice3 = keyboard.nextLine();
                        
                        switch (choice3){
                            case ("yes"):
                                System.out.println("You look out the window to see darkness;");
                                System.out.println("the moon, your only light. Outside there is a creepy tree.");
                                System.out.println("You stick your head out further to look around, and, suddenly");
                                System.out.println("the window slams down. You have been decapitated.");
                                break;
                            case ("no"):
                                System.out.println("You do not look out the window, and instead");
                                System.out.println("close it. As you close the window you catch the reflection");
                                System.out.println("of someone behind you. You turn around and are stabbed. You ded.");
                                break;
                        }
                        break;
                    case ("bathroom"):
                        System.out.println("After walking into the bathroom, the door slams violently"
                                + " behind you. The lights are off. Do you turn them on?");
                        choice3 = keyboard.nextLine();
                        
                        switch (choice3){
                            case ("yes"):
                                System.out.println("You turn on the lights and see a dead woman in the mirror.");
                                System.out.println("She devours your eternal soul.");
                                break;
                            case ("no"):
                                System.out.println("You do not turn on the lights, and spend");
                                System.out.println("the rest of your life in eternal darkness.");
                                break;
                        }
                        break;
                }
                break;
            case ("hallway"):
                System.out.println("You walk down the hallway. At the end, there are two rooms.");
                System.out.println("Do you take the one on the \"left\" or the one on the \"right\"?");
                choice2 = keyboard.nextLine();
                
                switch (choice2){
                    case ("left"):
                        System.out.println("You see that the room on the left is a kitchen, with");
                        System.out.println("a refridgerator to your right. You can see that the refridgerator");
                        System.out.println("is still on. Do you open it?");
                        String choice3 = keyboard.nextLine();
                        
                        switch (choice3){
                            case ("yes"):
                                System.out.println("Inside you find the severed remains");
                                System.out.println("of multiple humans. Seeing such a grotesque");
                                System.out.println("sight shocks you and you have a heart attack");
                                break;
                            case ("no"):
                                System.out.println("You do not open the refridgerator, and"
                                        + " die from starvation.");
                                break;
                        }
                        break;
                    case ("right"):
                        System.out.println("You see that the room on the left is an old laboratory.");
                        System.out.println("You see two vials on a nearby desk. One is red, and the other is blue.");
                        System.out.println("Do you drink the \"red\" or \"blue\" vial?");
                                choice3 = keyboard.nextLine();
                                
                                switch (choice3){
                                    case ("red"):
                                        System.out.println("You drink the red vial, and are transported");
                                        System.out.println(" outside of the creepy mansion. Run for your life!");
                                        break;
                                    case ("blue"):
                                        System.out.println("You foolishly drank the blue vial and were poisoned.");
                                        break;
                                }
                        break;
                }
                break;
            default:
                System.out.println("That was not an option");
        }
    }
}
