/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class SafeSquareRoot {
    
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("SQUARE ROOT!");
        System.out.print("Enter a number:");
        double input = keyboard.nextInt();
        
        while (input < 0){
            System.out.println("You can't take the square root of a negative number.");
            System.out.print("Enter a number:");
            input = keyboard.nextInt();
        }
        
        double sqrRoot = Math.sqrt(input);
        
        System.out.println("The square root of " + input + " is " + sqrRoot);
    }
}
