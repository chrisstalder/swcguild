/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

   
import java.util.Random;
import java.util.Scanner;

public class HiLoWithLimitedTries {
    
    public static void main(String[] args) {
        
        Random r = new Random();
        Scanner keyboard = new Scanner(System.in);
        int answer = 1 + r.nextInt(100);
        int tries = 0;
        int maxTries = 7;
        
        System.out.println("I'm thinking of a number 1 - 100. Try to guess it.");
        int guess = keyboard.nextInt();
        tries++;
        
        while (guess != answer && tries < maxTries){
        
            if (guess < answer){
                System.out.println("Sorry you are too low. Guess again.");
                guess = keyboard.nextInt();
                tries++;
            }else if (guess > answer){
                System.out.println("Sorry you are too high. Guess again.");
                guess = keyboard.nextInt();
                tries++;
            }
        }
        
        if (tries >= maxTries){
            System.out.println("Sorry you didn't guess it in 7 tries. It was " + answer);
        }
        
        if (guess == answer){
            System.out.println("You guessed it! What are the odds?!");
        }
        
    }
}


