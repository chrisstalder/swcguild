/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Random;
import java.util.Scanner;

public class OneShotHiLo {
    
    public static void main(String[] args) {
        
        Random r = new Random();
        Scanner keyboard = new Scanner(System.in);
        int answer = 1 + r.nextInt(100);
        
        System.out.println("I'm thinking of a number 1 - 100. Try to guess it.");
        int guess = keyboard.nextInt();
        
        if (guess < answer){
            System.out.println("Sorry you are too low. I was thinking of " + answer);
        }
        if (guess == answer){
            System.out.println("You guessed it! What are the odds?!");
        }
        if (guess > answer){
            System.out.println("Sorry you are too high. I was thinking of " + answer);
        }
    }
}
