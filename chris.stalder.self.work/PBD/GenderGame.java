/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class GenderGame {
    
    public static void main(String[] args) {
        
    
        Scanner keyboard = new Scanner(System.in);
        String marriage = "";
    
        System.out.print("What is your gender (M or F):");
        String gender = keyboard.next();
        
        System.out.print("First name:");
        String firstName = keyboard.next();
        
        System.out.print("Last name:");
        String lastName = keyboard.next();
        
        System.out.print("Age:");
        int age = keyboard.nextInt();
        
        if ((age >= 20) && ("F".equals(gender))){
            System.out.print("Are you married, " + firstName + " (y or n)");
            marriage = keyboard.next();
        }
        
        
        if ("M".equals(gender))
            System.out.println("Then I shall call you Mr. " + lastName);
        else if (("F".equals(gender)) && ("y".equals(marriage)))
            System.out.println("Then I shall call you Mrs. " + lastName);
        else if (("F".equals(gender)) && ("n".equals(marriage)))
            System.out.println("Then I shall call you Ms. " + lastName);
        
        
    }
    
}

/*What is your gender (M or F): F
First name: Kim
Last name: Kardashian
Age: 32 

Are you married, Kim (y or n)? y

Then I shall call you Mrs. Kardashian.*/