/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class AgeInFiveYears {
    public static void main(String[] args) {
        String name;
        int age;
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Hello, what is your name?");
        name = keyboard.nextLine();
        
        System.out.println("Hello, " + name + ". How old are you?");
        age = keyboard.nextInt();
        
        System.out.println("Did you know that in 5 years you will be " + (age + 5));
        System.out.println("And five years ago you were " + (age - 5));
    }
}
