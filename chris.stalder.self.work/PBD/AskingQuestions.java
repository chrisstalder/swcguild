/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class AskingQuestions {
    public static void main(String[] args){
        
        Scanner keyboard = new Scanner(System.in);
        
        int age, weight, heightInFeet, heightInInches;
        
        System.out.print("How old are you?");
        age = keyboard.nextInt();
        
        System.out.print("How many feet tall are you?");
        heightInFeet = keyboard.nextInt();
        
       System.out.print("And how many inches?");
        heightInInches = keyboard.nextInt();
        
        System.out.print("How much do you weigh?");
        weight = keyboard.nextInt();
        
        
        System.out.println("So, you are " + age + " years old, " + heightInFeet + "'" + heightInInches + " tall, and " + weight + " heavy.");
        
        
    }
}