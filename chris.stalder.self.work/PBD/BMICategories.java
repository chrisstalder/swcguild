/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class BMICategories {
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Your height (feet only)");
        int heightInFeet = keyboard.nextInt();
        
        System.out.println("You height (inches)");
        int heightInInches = keyboard.nextInt();
        
        System.out.println("Your weight in pounds");
        float weightInPounds = keyboard.nextFloat();
        
        heightInFeet *= 12;
        
        float height = (float)((heightInFeet + heightInInches) * 0.0254);
        float weight = (float)(weightInPounds * 0.453592);
        
        float BMI = (weight/(height * height));
        
        
        System.out.println("Your BMI is " + BMI);
        
        if (BMI < 18.5){
            System.out.println("BMI Categori: underweight");
            float normalBMI = (float)(18.5 - BMI);
            float weightToNormal = (float)((normalBMI *(height * height)/0.453592));
            int weightToNormalPounds = (int)weightToNormal;
            System.out.println("You should gain " + weightToNormalPounds + " lbs to be in a normal BMI range");

        }
        else if (BMI >= 18.5 && BMI <= 24.9){
            System.out.println("BMI Categori: normal weight");
        }
        if (BMI >= 25.0 && BMI <= 29.99 ){
            System.out.println("BMI Categori: overweight");
            float normalBMI = (float)(BMI - 24.9);
            float weightToNormal = (float)((normalBMI *(height * height)/0.453592));
            int weightToNormalPounds = (int)weightToNormal;
            System.out.println("You should lose " + weightToNormalPounds + " lbs to be in a normal BMI range");
        }
        if (BMI >= 30.0){
            System.out.println("BMI Categori: obese");
            float normalBMI = (float)(BMI - 24.9);
            float weightToNormal = (float)((normalBMI *(height * height)/0.453592));
            int weightToNormalPounds = (int)weightToNormal;
            System.out.println("You should lose " + weightToNormalPounds + " lbs to be in a normal BMI range");
        }
    }
    
}


