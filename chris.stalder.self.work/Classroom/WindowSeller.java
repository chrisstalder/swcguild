/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */

import java.util.Scanner;

public class WindowSeller {
    public static void main(String[] args) {
        
   
        Scanner keyboard = new Scanner(System.in);

        float height = 0f;
        float width = 0f;
        float glass = 0f;
        float trim = 0f;
        int numOfWindows;
        final float MAX_HEIGHT = 25.5f;
        final float MIN_HEIGHT = 1.0f;
        final float MAX_WIDTH = 18.75f;
        final float MIN_WIDTH = 1.0f;

        do{
            System.out.println("What is the height of the window in inches?");
            height = keyboard.nextFloat();
        }while((height > MAX_HEIGHT) || (height < MIN_HEIGHT));
          
        do{
            System.out.println("What is the width of the window?");
            width = keyboard.nextFloat();
        }while((width > MAX_WIDTH) || (width < MIN_WIDTH));
        
            System.out.println("How many windows do you need?");
            numOfWindows = keyboard.nextInt();
            System.out.println("How much will you spend on glass?");
            glass = keyboard.nextFloat();
            System.out.println("How much will you spend on trim?");
            trim = keyboard.nextFloat();
        
        float area = (width * height);
        float perimeter = ((width + height) * 2);
        float glassCost = (area * glass);
        float trimCost = (perimeter * trim);
        float windowCost = (glassCost + trimCost);
        float totalCost = (windowCost * numOfWindows);
        
        System.out.println("The area of the glass will be: " + area);
        System.out.println("The perimeter of the trim will be: " + perimeter);
        System.out.println("The glass will cost: $" + glassCost);
        System.out.println("The trim will cost: $" + trimCost);
        System.out.println("Your cost per window will be: " + windowCost);
        System.out.println("Your total cost will be: $" + totalCost);
        }
    }

    
}
/* 25 inches tall 18.75 inches wide, atleast an inch
if they dont enter a valid number ask the user to reinput
*/