/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
import java.util.Scanner;

public class PrimeFinder {
    
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        
        int primeCount = 0;
        int count = 0;
        boolean playAgain = false;
        String again = "";
        System.out.println("This program will find all the prime numbers between ");
        System.out.println("0 and number of your choice. ");
        
       do{
            playAgain = generatePrimes(keyboard, count, primeCount);
            
        } while (playAgain == true);
    }

    private static boolean generatePrimes(Scanner keyboard, int count, int primeCount) {
        boolean playAgain;
        String again;
        playAgain = false;
        System.out.print("Please enter a value: ");
        int input = keyboard.nextInt();
        while(input <= 0){
            System.out.println("Invalid number. Please enter a positive integer");
            input = keyboard.nextInt();
        }
        for(int i = 0; i <= input; i++){
            
            for(int j = 1; j <= i; j++){
                
                if(i % j == 0){
                    count++;
                }
            }
            
            if (count == 2){
                System.out.println(i);
                primeCount++;
            }
            
            count = 0;
            
        }
        System.out.println("The amount of primes between 0 and " + input + " are " + primeCount);
        primeCount = 0;
        System.out.println("Would you like to play again? (y/n)");
        again = keyboard.next();
        if("y".equals(again)){
            playAgain = true;
        }
        return playAgain;
    }
}
