/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class BlackJackTest {
    
    public BlackJackTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testDealCard(){
        //a card dealt will be between a value of 2 and 11
        BlackJack bj = new BlackJack();
        int min = 2;
        int max = 11;
        int result = bj.dealCard();
        
        Assert.assertTrue(min <= result && result <= max);
    }
    @Test
    public void testCardTotal(){
        //two numbers should be added
        BlackJack bj = new BlackJack();
        int result = bj.cardTotal(2, 11);
        
        Assert.assertTrue(result == (2 + 11));
    }
    @Test
    public void isValid(){
        //should return true if cardTotal isn't more than 21
        BlackJack bj = new BlackJack();
        boolean result = bj.isValid(18);
        
        Assert.assertEquals(true, result);
    }
    
    @Test
    public void isValid2(){
        //should return false if cardTotal is more than 21
        BlackJack bj = new BlackJack();
        boolean result = bj.isValid(22);
        
        Assert.assertEquals(false, result);
    }
    
    @Test
    public void testAnswerValidation(){
        //returns true if response is yes
        
        BlackJack bj = new BlackJack();
        String response = "yes";
        boolean result = bj.answerValidation(response);
        
        Assert.assertEquals(true, result);
    }
    
    @Test
    public void testdealerHit(){
        //if dealer total is less than or equal to 16, they should hit
        BlackJack bj = new BlackJack();
        boolean result = bj.dealerHit(16, 12);
        
        Assert.assertEquals(true, result);
    }
    
    
    @Test
    public void testdealerHit2(){
        //if dealer total is less than player total, they should hit
        BlackJack bj = new BlackJack();
        boolean result = bj.dealerHit(16, 18);
        
        Assert.assertEquals(true, result);
    }
    
}
