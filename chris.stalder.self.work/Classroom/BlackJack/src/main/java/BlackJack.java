

import java.util.Random;
import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author apprentice
 */
public class BlackJack {
    
    static Scanner keyboard = new Scanner(System.in);
    int playerCard1 = dealCard();
    int playerCard2 = dealCard();
    int dealerCard1 = dealCard();
    int dealerCard2 = dealCard();
    int newTotal=0;
    int playerTotal = cardTotal( playerCard1, playerCard2);
    int dealerTotal = cardTotal( dealerCard1, dealerCard2);
    boolean cardTotalValidation = isValid(playerTotal);
    boolean dealerCardTotalValidation = isValid(dealerTotal);
    boolean answer = true;
    boolean dealerAnswer = true;
    boolean anotherHit = true;
    boolean dealerAnotherHit = true;
    
    public void run(){
 
        System.out.println("Dealer draws " + dealerCard1);
        System.out.println("You drew " + playerCard1 + " and " + playerCard2);
        System.out.println("Your total is " + playerTotal);

        while (anotherHit){
            
            System.out.println("Do you want a hit?");
            String response = keyboard.next();
            answer = answerValidation(response);
            
            while (cardTotalValidation && answer){

                int nextCard = dealCard();
                newTotal = cardTotal(playerTotal, nextCard);
                cardTotalValidation = isValid(newTotal);
                if (cardTotalValidation){
                    playerTotal = newTotal;
                    System.out.println("You drew " + nextCard);
                    System.out.println("Your total is " + newTotal);
                 
                    System.out.println("Do you want another hit?");
                    response = keyboard.next();
                    answer = answerValidation(response);
                }
                else if (cardTotalValidation == false){
                    System.out.println("You drew " + nextCard);
                    System.out.println("You went over! Dealer Wins!");
                    anotherHit = false;
                }
            }   
            if (answer == false)
                anotherHit = false;     
        }
        
       if (answer == false && cardTotalValidation){
           
            while (dealerAnotherHit){    
            
            dealerAnswer = dealerHit(dealerTotal, playerTotal);
            
            while (dealerCardTotalValidation && dealerAnswer){

                int dealerNextCard = dealCard();
                int dealerNewTotal = cardTotal(dealerTotal, dealerNextCard);
                dealerCardTotalValidation = isValid(dealerNewTotal);
                if (dealerCardTotalValidation){
                    System.out.println("Dealer will hit.");
                    dealerTotal = dealerNewTotal;
                    System.out.println("Dealer drew " + dealerNextCard);
                    System.out.println("Dealer's total is " + dealerNewTotal);
                 
                    dealerAnswer = dealerHit(dealerTotal, playerTotal);
                }
                else if (dealerCardTotalValidation == false){
                    System.out.println("Dealer drew " + dealerNextCard);
                    System.out.println("Dealer busts! You win!");
                    dealerAnotherHit = false;
                }

            }
            
            if (dealerAnswer == false)
                dealerAnotherHit = false;  
        }
        if (playerTotal > dealerTotal && playerTotal <= 21 )
            System.out.println("You win!");
        else if (playerTotal < dealerTotal && dealerTotal <= 21)
            System.out.println("The dealer wins!");    
       }    
    }

    
    public int dealCard(){
        Random r = new Random();
        int card = 2 + r.nextInt(10);
        
        return card;
    }
    
    public int cardTotal(int card1, int card2){
        int cardTotal = card1 + card2;
        return cardTotal;
    }
    
    public boolean isValid(int cardTotal){
        boolean isValid;
        isValid = cardTotal <= 21;
        
        return isValid;
    }
    
    public boolean answerValidation(String response){
        
        boolean isValid = false;
        boolean whileLoop = false;
        
        while (!whileLoop){
            if ("yes".equals(response)){
                isValid = true;
                whileLoop = true;
            }
            else if ("no".equals(response)){
                isValid = false;
                whileLoop = true;
            }
            else{
                System.out.println("I couldn't quite hear you. Do you want a hit?");
                response = keyboard.next();
                whileLoop = false;
            }
        }
        
        return isValid;
    }
    
    public boolean dealerHit(int dealerTotal, int playerTotal){
        boolean shouldHit;
        
        if (dealerTotal <= 16 || dealerTotal < playerTotal)
            shouldHit = true;
        else
            shouldHit = false;
        
        return shouldHit;
    }
}
