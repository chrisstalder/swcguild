/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class CommonEnd {
    
    public boolean commonEnd(int[] n1, int[] n2){
        boolean isValid = false;
        if (n1[0] == n2[0] || n1[n1.length-1] == n2[n2.length-1])
            isValid = true;
        
        return isValid;
    }
    
}
