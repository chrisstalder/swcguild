/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Diff21 {
    public int Diff21(int n) {
        
        int difference = 0;
        
        if (n > 21)
            difference = (n - 21) * 2;
        else
            difference = 21 - n;
        return difference;
  
    }
}
