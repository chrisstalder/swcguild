/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class AreWeInTrouble {
    public boolean areWeInTrouble(boolean aSmile, boolean bSmile) {
       boolean inTrouble = false;
       
       if((aSmile && bSmile) || (!aSmile && !bSmile))
           inTrouble = true;
       return inTrouble;
    }
}
