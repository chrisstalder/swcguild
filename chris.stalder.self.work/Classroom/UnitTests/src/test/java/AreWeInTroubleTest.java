/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AreWeInTroubleTest {
    
    public AreWeInTroubleTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void areWeInTroubleTest(){
       AreWeInTrouble awt = new AreWeInTrouble();
        boolean result = awt.areWeInTrouble(true, true);
        
        Assert.assertEquals(true, result);
    }
    
    @Test
    public void areWeInTroubleTest1(){
       AreWeInTrouble awt = new AreWeInTrouble();
        boolean result = awt.areWeInTrouble(false, false);
        
        Assert.assertEquals(true, result);
    }
    
    @Test
    public void areWeInTroubleTest2(){
       AreWeInTrouble awt = new AreWeInTrouble();
        boolean result = awt.areWeInTrouble(true, false);
        
        Assert.assertEquals(false, result);
    }
    
}
