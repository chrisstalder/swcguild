/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SumTest {
    
    public SumTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testSum1(){
        
        Sum s = new Sum();
        
        int result = s.sum(new int[]{1, 2, 3});
        
        Assert.assertEquals(6, result);
    }
    
    @Test
    public void testSum2(){
        
        Sum s = new Sum();
        
        int result = s.sum(new int[]{5, 11, 2});
        
        Assert.assertEquals(18, result);
    }
    
    @Test
    public void testSum3(){
        
        Sum s = new Sum();
        int result = s.sum(new int[]{7, 0, 0});
        
        Assert.assertEquals(7, result);
    }
}
