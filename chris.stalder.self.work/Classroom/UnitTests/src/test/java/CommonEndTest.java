/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CommonEndTest {
    
    public CommonEndTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCommonEnd(){
        
        CommonEnd ce = new CommonEnd();
        boolean result = ce.commonEnd(new int[]{1, 2, 3}, new int[] {7, 3});
        
        Assert.assertEquals(true, result);
    }
    
    
    @Test
    public void testCommonEnd1(){
        CommonEnd ce = new CommonEnd();
        boolean result = ce.commonEnd(new int[]{1, 2, 3}, new int[] {7, 3, 2});
        
        Assert.assertEquals(false, result);
    }
    
    
    @Test
    public void testCommonEnd2(){
        
        CommonEnd ce = new CommonEnd();
        boolean result = ce.commonEnd(new int[]{1,2,3}, new int[] {1, 3});
        
        Assert.assertEquals(true, result);
    }
    
}
