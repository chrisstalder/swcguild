/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Diff21Test {
    
    public Diff21Test() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testDiff21(){
        Diff21 d = new Diff21();
        int result = d.Diff21(23);
        
        Assert.assertEquals(4, result);
    }
    
    
    @Test
    public void testDiff21a(){
        Diff21 d = new Diff21();
        int result = d.Diff21(10);
        
        Assert.assertEquals(11, result);
    }
    
    
    @Test
    public void testDiff21b(){
        Diff21 d = new Diff21();
        int result = d.Diff21(21);
        
        Assert.assertEquals(0, result);
    }
    
}
