/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ParrotTroubleTest {
    
    public ParrotTroubleTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testParrotTrouble(){
        ParrotTrouble p = new ParrotTrouble();
        boolean result = p.ParrotTrouble(true, 6);
        
        Assert.assertEquals(true, result);
    }
    
    
    @Test
    public void testParrotTrouble1(){
        ParrotTrouble p = new ParrotTrouble();
        boolean result = p.ParrotTrouble(true, 7);
        
        Assert.assertEquals(false, result);
    }
    
    
    @Test
    public void testParrotTrouble2(){
        ParrotTrouble p = new ParrotTrouble();
        boolean result = p.ParrotTrouble(false, 6);
        
        Assert.assertEquals(false, result);
    }
    
}
