/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SumDoubleTest {
    
    public SumDoubleTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testSumDouble(){
        SumDouble sd = new SumDouble();
        int result = sd.SumDouble(1, 2);
        
        Assert.assertEquals(3, result);
    }
    
    
    @Test
    public void testSumDouble1(){
        SumDouble sd = new SumDouble();
        int result = sd.SumDouble(3, 2);
        
        Assert.assertEquals(5, result);
    }
    
    
    @Test
    public void testSumDouble2(){
        SumDouble sd = new SumDouble();
        int result = sd.SumDouble(2, 2);
        
        Assert.assertEquals(8, result);
    }
    
}
