/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SameFirstLastTest {
    
    public SameFirstLastTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testSameFirstLast1(){
        SameFirstLast sml = new SameFirstLast();
        
        boolean result = sml.sameFirstLast(new int[]{1, 2, 3});
        
        Assert.assertEquals(false, result);
    }
    @Test
    public void testSameFirstLast2(){
        SameFirstLast sml = new SameFirstLast();
        boolean result = sml.sameFirstLast(new int[]{1, 2, 3, 1});
        
        Assert.assertEquals(true, result);
    }
    @Test
    public void testSameFirstLast3(){
        SameFirstLast sml = new SameFirstLast();
        boolean result = sml.sameFirstLast(new int[]{1, 2, 1});
        
        Assert.assertEquals(true, result);
    }
}
