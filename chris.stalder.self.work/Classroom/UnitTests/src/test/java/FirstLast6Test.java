/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FirstLast6Test {
    
    public FirstLast6Test() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testFirstLast6(){
        firstLast6 fl = new firstLast6();
        boolean result = fl.firstLast6(new int[]{1,2,6});
        
        Assert.assertTrue(result);
    }
    @Test
    public void testFirstLast62(){
        firstLast6 fl = new firstLast6();
        boolean result = fl.firstLast6(new int[]{6, 1, 2, 3});
        
        Assert.assertTrue(result);
    }
    @Test
    public void testFirstLast63(){
        firstLast6 fl = new firstLast6();
        boolean result = fl.firstLast6(new int[]{13, 6, 1, 2, 3});
        
        Assert.assertEquals(false, result);
    }
}
