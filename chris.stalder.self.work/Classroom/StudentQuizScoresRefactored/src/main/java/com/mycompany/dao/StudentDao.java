/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.dataencoding.App;
import com.mycompany.dto.Student;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class StudentDao {
    
    private List<Student> students = new ArrayList();
    private int nextId;
    
    public StudentDao(){
        students = decode();
    }
    
    
    
    public Student create(Student student){
        
        student.setId(nextId);
        nextId++;
        students.add(student);
        
        
        encode();
        
        return student;
        
        
    }
    
    public Student get(Integer id){
        
        for (Student myStudent : students) {
            if (myStudent.getId() == id){
                return myStudent;
            }
        }
        
        return null;
        
    }
    
    public void update(Student student){
        
        for (Student myStudent : students) {
            if (myStudent.getId()== student.getId()){
                students.remove(myStudent);
                students.add(student);
            }
        }
        
        encode();
 
    }
    
    public void delete(Student student){
        
        Student found = null;
        
        for (Student myStudent : students) {
            if (myStudent.getId() == student.getId()){
                found = myStudent;
            }
        }
        students.remove(found);
        encode();
    }
    
    private void encode(){
        
        final String TOKEN = "::";
        
        
        try {
            PrintWriter out = new PrintWriter(new FileWriter("students.txt"));
            
            for (Student student : students) {
                out.print(student.getId());
                out.print(TOKEN);
                out.print(student.getFirstName());
                out.print(TOKEN);
                out.print(student.getLastName());
                out.print(TOKEN);
                out.print(student.getGrades());
                out.print(TOKEN);
                out.print(student.getAvgGrade());
                out.println();
            }
            
            out.flush();
            out.close();
            
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
    
    private List<Student> decode(){
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("students.txt")));
            
            while (sc.hasNextLine()){
                String currentline = sc.nextLine();
                
                String[] stringParts = currentline.split("::");
                
                Student myStudent = new Student();
                
                int id = Integer.parseInt(stringParts[0]);
                
                myStudent.setId(id);
                myStudent.setFirstName(stringParts[1]);
                myStudent.setLastName(stringParts[2]);
                
                String gradeString = stringParts[3].replace("[", "").replace("]", "");
                String[] gradeArray = gradeString.split(", ");
                List<Integer> grades = new ArrayList();
                
                for (String string : gradeArray) {
                    int grade = Integer.parseInt(string);
                    grades.add(grade);
                }
                
                myStudent.setGrades(grades);
                
                float avgGrade = Float.parseFloat(stringParts[4]);
                
                myStudent.setAvgGrade(avgGrade);
                
                students.add(myStudent);
                nextId = 1 + students.size();
            }
            
            sc.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return students;
    }
    
    public List<Student> list(){
      return students; 
    }
}

