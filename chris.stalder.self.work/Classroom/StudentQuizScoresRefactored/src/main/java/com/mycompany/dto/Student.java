/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dto;

import java.util.List;

/**
 *
 * @author apprentice
 */
public class Student {
    
    private int id;
    private String firstName;
    private String lastName;
    private String cohort;
    private List<Integer> grades;
    private float avgGrade;
    
    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCohort() {
        return cohort;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setCohort(String cohort) {
        this.cohort = cohort;
    }

    

    public float getAvgGrade() {
        return avgGrade;
    }

    

    public void setAvgGrade(float avgGrade) {
        this.avgGrade = avgGrade;
    }

    public List<Integer> getGrades() {
        return grades;
    }

    public void setGrades(List<Integer> grades) {
        this.grades = grades;
    }


   
    
    
}
