/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controllers;

import com.mycompany.dao.StudentDao;
import com.mycompany.dto.Student;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class StudentController {

    private StudentDao dao = new StudentDao();
    private Map<String, Student> map = new HashMap();
    Scanner keyboard = new Scanner(System.in);
    
    public void run() {
        //kicks off menu
        
        int selection = 0;
        boolean playAgain = true;
        boolean isValid = false;
        
        while (playAgain == true) {

            System.out.println("Welcome to the Student Directory.\n");

            System.out.println("1) List all students.\n");
            System.out.println("2) Add a student and scores.\n");
            System.out.println("3) Remove a student. \n");
            System.out.println("4) View quiz scores and average scores. \n");
            System.out.println("5) View highest score. \n");
            System.out.println("6) View lowest score. \n");
            System.out.println("7) View class average. \n");
            System.out.println("8) End.\n");
            System.out.println("What would you like to do?");
            selection = keyboard.nextInt();
            isValid = false;
            switch (selection) {
                case 1:
                    listStudents();
                    break;
                case 2:
                    addStudent();
                    break;
                case 3:
                    removeStudent();
                    break;
                case 4:
                    listGrades();
                    break;
                case 5:
                    getHigh();
                    break;
                case 6:
                    getLow();
                    break;
                case 7:
                    classAvg();
                    break;
                case 8:
                    System.out.println("Thank you for stopping by! Come again any time!");
                    playAgain = false;
                    break;
                default:
                    System.out.println("That was not an option. Please enter 1 - 8");
                    break;
            }
        }
    }
    
    
    public void addStudent() {
        
        
        System.out.println("Please enter the new student's first name");
        String firstName = keyboard.next();
        System.out.println("Please enter the new student's last name");
        String lastName = keyboard.next();
        List<Integer>grades = generateScores();
        
        Student myStudent = new Student();
        myStudent.setFirstName(firstName);
        myStudent.setLastName(lastName);
        myStudent.setGrades(grades);
        myStudent.setAvgGrade(avgGrade(myStudent.getGrades()));
        dao.create(myStudent);
        
        
        
    }
    
    public void removeStudent(){
        List<Student> list = dao.list();

        for (Student student : list) {
            System.out.println(student.getId() + "| " + student.getFirstName() + " " + student.getLastName());
        }
        
        System.out.println("Please enter the id of the student you wish to remove");
        String input = keyboard.next();
        int id = Integer.parseInt(input);
      
        Student removedStudent = dao.get(id);
        dao.delete(removedStudent);
    }
    
    public List<Integer> generateScores() {
        int grade = 0;
        System.out.println("How many grades to enter?");
        int numOfGrades = keyboard.nextInt();
        List<Integer>newArray = new ArrayList();
        for (int i = 1; i <= numOfGrades; i++) {
            System.out.println("Enter grade " + i + ":");
            grade = keyboard.nextInt();
            while (grade < 0 || grade > 100) {
                System.out.println("That is not a valid number. Enter a score between 1 - 100");
                grade = keyboard.nextInt();
            }
                newArray.add(grade);
        }
        
        return newArray;
    }
    
    public float avgGrade(List<Integer> newArray) {
        int avgGrade = 0;
        int total = 0;

        for (int i = 0; i < newArray.size(); i++) {
            total += newArray.get(i);
        }

        avgGrade = total / newArray.size();
        return avgGrade;
    }
    
    public void listStudents() {
        
        List<Student> list = dao.list();
        System.out.println("========================");
        for (Student student : list) {
            System.out.println(student.getFirstName() + " " + student.getLastName());
        }
        System.out.println("========================");
        System.out.println("We currently have a total of " + list.size() + " students.");

    }

    public void listGrades() {
        List<Student> list = dao.list();
        for (Student student : list) {
            System.out.println(student.getFirstName() + " " + student.getLastName() + " - " + student.getGrades());
            System.out.println("The average score for " + student.getFirstName() + " " + student.getLastName() + " is " + student.getAvgGrade());
        }

    }
    
    public void getHigh() {
        List<Student> list = dao.list();
        Integer highest = 0;
        Student highestStudent = new Student();
        for (Student student : list) {
            List<Integer>scores = student.getGrades();
            for (Integer score : scores) {
                if (score > highest){
                    highest = score;
                    highestStudent = student;
                }
            }
        }
        System.out.println("The highest grade belongs to: " + highestStudent.getFirstName() + " " + highestStudent.getLastName() + " with a grade of: " + highest);
    }
    
    public void getLow() {
        List<Student> list = dao.list();
        Integer lowest = 100;
        Student dumby = new Student();
        for (Student student : list) {
            List<Integer>scores = student.getGrades();
            for (Integer score : scores) {
                if (score < lowest){
                    lowest = score;
                    dumby = student;
                }
            }
        }
        System.out.println("The lowest grade belongs to: " + dumby.getFirstName() + " " +dumby.getLastName() + " with a grade of: " + lowest);
    }
    
    
    public void classAvg() {
        List<Student> list = dao.list();
        int numOfGrades = 0;
        float total = 0f;
        
        for (Student student : list) {
            List<Integer> scores = student.getGrades();
            for (Integer score : scores) {
                total += score;
                numOfGrades ++;
            }
        }
        System.out.println("The average quiz score for the class is: " + total/numOfGrades);
    }
}
